#!/usr/bin/env sh
set -eu

envsubst '${FRONTEND_HOSTNAME}' < /etc/nginx/conf.d/default.conf.template | sed -z 's/\\\n//g' > /etc/nginx/conf.d/default.conf

exec "$@"
