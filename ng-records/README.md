# Dona Ana Records Frontend

## Installation

Although we develop inside of the docker container, it is a good idea to go
ahead and install the project locally as well. To do this run:

`npm install` from the `ng-records` directory
