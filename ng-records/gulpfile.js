var gulp = require('gulp');
var gzip = require('gulp-gzip');

gulp.task('compress', function() {
  return gulp.src(['./dist/**/*.js', './dist/**/*.css',  './dist/**/*.png'])
      .pipe(gzip())
      .pipe(gulp.dest('./dist'));
});
