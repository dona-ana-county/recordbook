import { Component } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { AnalyticsService } from './services/analytics.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-records';
  doneLoading: boolean;
  errorLoading: boolean;

  constructor(private constantService: ConstantService,
              private analyticsService: AnalyticsService,
              private titleService: Title) {

    this.doneLoading = false;
    this.errorLoading = false;
    const subscription = constantService.loadingComplete.subscribe(res => {
      this.titleService.setTitle(`${constantService.config.entityName} ${constantService.config.appName}`);
      analyticsService.init();
      this.doneLoading = true;
    });
    setTimeout(() => {
      if (this.doneLoading === false) {
        this.errorLoading = true;
        subscription.unsubscribe();
      }
    }, 5000);

    constantService.init();
  }
}
