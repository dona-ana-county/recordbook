export interface SearchQuery {
  search: string;
  sort: any;
  filters: Filter[];
  page: number;
  perPage: number;
}

export interface Filter {
  type: string;
  field: string;
  value: string;
  from?: string;
  to?: string;
}

export interface RequestSearchResult {
  requestId: string;
  requestorName: string;
  title: string;
  createDate: string;
  dueDate: string;
  closingDate: string;
  department: string;
  status: string;
  extensionStatus: string;
}

export interface PaginationInfo {
  numItems: number;
  reqList: RequestSearchResult[];
}
