import { CompletionStatus } from './request';
import { Staff } from './user';

export interface Department {
  id: string;
  name: string;
  staff?: Staff[];
  completionStatus?: CompletionStatus;
}
