import { User, Staff } from './user';

export interface Communication {
  id?: string;
  createDate?: Date;
  message: string;
}

export interface Alert extends Communication {
  sender: Staff;
  recipient?: Staff;
}

export interface Comment extends Communication {
  commenter: User;
}
