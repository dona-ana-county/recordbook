import { Department } from "./department";
import { Staff, Requestor } from "./user";
import { Content, Resource, Instruction, Note, Link } from "./content";
import { Comment, Alert } from "./communication";
import { AlertedItem } from "../components/multi-stack-box-selector/multi-stack-box-selector.component";
import { Lookup } from "./constant";

export interface Request {
  id?: string;
  title: string;
  publicTitle?: string;
  description?: string;
  publicDescription?: string;
  status?: RequestStatus;
  statusNote?: string;
  createDate?: string;
  dueDate?: string;
  passcode?: string;
  requestor?: Requestor;
  primaryDepartment?: Department;
  secondaryDepartments?: Department[];
  completionStatuses?: CompletionStatus[];
  assignedStaff?: Staff[];
  log?: LogMessage[];
  resources?: Resource[];
  notes?: Note[];
  instructions?: Instruction[];
  links?: Link[];
  alerts?: Alert[];
  comments?: Comment[];
  extensionStatus?: ExtensionStatus;
}

export interface PublicRequest {
  reCaptchaToken: string;
  request: Request;
}

export interface StaffRequest {
  request: Request;
  assignments: AlertedItem[];
}

export interface LogMessage {
  messageDate: string;
  message: string;
  eventCode: EventCode;
}

export interface CompletionStatus {
   department_id: string;
   completed: boolean;
   comment: string;
}

// tslint:disable-next-line: no-empty-interface
export interface RequestStatus extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface EventCode extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface ExtensionStatus extends Lookup {}
