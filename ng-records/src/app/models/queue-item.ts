import { RequestStatus } from './request';

export interface RequestQueueItem {
  createDate: string;
  id: string;
  extensionStatus: string;
  title: string;
  dueDate?: string;
  requestorName: string;
}


export interface ResourceQueueItem {
    id: string;
    title: string;
    requestId: string;
    uploadDate: Date;
    status: RequestStatus;
}
