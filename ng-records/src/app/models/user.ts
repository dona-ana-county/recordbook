import { Department } from './department';
import { Lookup } from './constant';

export interface User {
  id?: string;
  firstName: string;
  lastName: string;
  [key: string]: any; // Wut
}

export interface Staff extends User {
  email: string;
  username: string;
  token: AuthToken;
  groups: Group[];
  role: Role;
  externalAuth: boolean;
  department?: Department; // NOTE VT: Not crazy about this
  disabled: boolean;
}

export interface Requestor extends User {
  contactInfo: ContactInfo;
  primaryContact: ContactType;
}

export interface AuthToken {
  token: string;
  issuedDt: Date;
  revokedDt: Date;
  expirationDt: Date;
}

export interface ContactInfo {
  email?: string;
  secondary_emails?: string[];
  phone: string;
  fax?: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  postalCode: string;
  
}

// tslint:disable-next-line: no-empty-interface
export interface ContactType extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface Role extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface Group extends Lookup {}
