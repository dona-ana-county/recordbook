import { Staff } from './user';
import { Lookup } from './constant';

export interface Content {
  id?: string;
  title: string;
  releaseType?: ReleaseType;
  createDate?: Date;
  creator?: Staff;
  requestId?: string;
  editable?: boolean;
}

export interface Note extends Content {
  message: string;
}

export interface Instruction extends Content {
  message: string;
}
export interface Instruction {
  id?: string;
  title: string;
  createDate?: Date;
  releaseType: ReleaseType;
  creator: Staff;
  message: string;
}

export interface Link extends Content {
  url: string;
  description: string;
  createDate?: Date;
  releaseType: ReleaseType;
  creator: Staff;
}

export interface Resource extends Content {
  extension?: string;
  needsReview: boolean;
  underReview: boolean;
  message?: string;
  description?: string;
  revised?: boolean;
}

// tslint:disable-next-line: no-empty-interface
export interface ReleaseType extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface ContentType extends Lookup {}
