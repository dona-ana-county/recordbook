import { ContentType, ReleaseType } from './content';

export class ContentTypeLookup {
  items: ContentType[];

  public getContentType(code: string): ContentType {
    return this.items.find(c => c.code === code);
  }
}

export class ReleaseTypeLookup {
  items: ReleaseType[];

  public getReleaseType(code: string): ContentType {
    return this.items.find(c => c.code === code);
  }
}

export interface Lookup {
  code: string;
  value: string;
}

// tslint:disable-next-line: no-empty-interface
export interface Reason extends Lookup {}

export interface GovernmentEntity {
  id?: string;
  name: string;
  email: string;
}

// tslint:disable-next-line: no-empty-interface
export interface QueueItemType extends Lookup {}

// tslint:disable-next-line: no-empty-interface
export interface QueueType extends Lookup {}

export interface State {
  name: string;
  abbreviation: string;
}

export interface ContactType{
  code: string;
  value: string;

}

export interface ConfigType {
  appName: string;
  entityName: string;
  entityEmail: string;
  recaptchaSiteKey: string;
  gtagTrackingId: string;
}
