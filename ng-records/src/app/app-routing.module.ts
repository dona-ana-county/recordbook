import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninComponent } from './pages/signin/signin.component';
import { HomeComponent } from './pages/home/home.component';
import { StaffDashboardComponent } from './pages/staff-dashboard/staff-dashboard.component';
import { PublicNewRequestComponent } from './pages/public-new-request/public-new-request.component';
import { StaffNewRequestComponent } from './pages/staff-new-request/staff-new-request.component';
import { RequestManagementComponent } from './pages/request-management/request-management.component';
import { StaffSearchRequestComponent } from './pages/staff-search-request/staff-search-request.component';
import { PublicSearchRequestComponent } from './pages/public-search-request/public-search-request.component';
import { PublicViewRequestComponent } from './pages/public-view-request/public-view-request.component';
import { LegalReviewManagementComponent } from './pages/legal-review-management/legal-review-management.component';
import { RequestorViewRequestComponent } from './pages/requestor-view-request/requestor-view-request.component';
import { SuperUserManagementComponent } from './pages/super-user-management/super-user-management.component';
import { SuperDepartmentManagementComponent } from './pages/super-department-management/super-department-management.component';
import { SuperGovernmentEntitiesManagementComponent } from './pages/super-government-entities-management/super-government-entities-management.component';

import { AuthGuardService } from './services/auth-guard.service';
import { AdminGuardService } from './services/admin-guard.service';

const routes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'home', component: HomeComponent },
  { path: 'staffSearch', component: StaffSearchRequestComponent },
  { path: 'publicSearch', component: PublicSearchRequestComponent },
  { path: 'viewRequest/:requestId', component: PublicViewRequestComponent },
  {
    path: 'viewRequest/:requestId/:passCode',
    component: RequestorViewRequestComponent
  },
  {
    path: 'dashboard',
    component: StaffDashboardComponent,
    canActivate: [AuthGuardService]
  },
  { path: 'newRequest', component: PublicNewRequestComponent },
  {
    path: 'staffRequest',
    component: StaffNewRequestComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'legalReviewManagement/:resourceId',
    component: LegalReviewManagementComponent,
    canActivate: [AuthGuardService]
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'requestManagement/:requestId',
    component: RequestManagementComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'superUser',
    component: SuperUserManagementComponent,
    canActivate: [AdminGuardService]
  },
  {
    path: 'superUser/userManagement',
    component: SuperUserManagementComponent,
    canActivate: [AdminGuardService]
  },
  {
    path: 'superUser/departmentManagement',
    component: SuperDepartmentManagementComponent,
    canActivate: [AdminGuardService]
  },
  {
    path: 'superUser/governmentEntitiesManagement',
    component: SuperGovernmentEntitiesManagementComponent,
    canActivate: [AdminGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
