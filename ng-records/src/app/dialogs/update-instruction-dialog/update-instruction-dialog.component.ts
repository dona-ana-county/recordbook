import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { ReleaseType, Instruction } from "src/app/models/content";
import { Request } from "src/app/models/request";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { TextService } from "src/app/services/text.service";
import { ConstantService } from "src/app/services/constant.service";
import { textTransformNewLines, textTransform } from "src/utils/textTransform";

@Component({
  selector: "app-update-instruction-dialog",
  templateUrl: "./update-instruction-dialog.component.html",
  styleUrls: ["./update-instruction-dialog.component.scss"],
})
export class UpdateInstructionDialogComponent implements OnInit {
  @ViewChild("title") titleRef: ElementRef;
  @ViewChild("instruction") instructionRef: ElementRef;

  instructionControl: FormControl;
  releaseTypeControl: FormControl;
  titleControl: FormControl;

  instructionError = false;
  releaseError = false;

  releaseTypes: ReleaseType[];
  instruction: Instruction;

  request: Request;
  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef<UpdateInstructionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.instruction = data.instruction;
    this.titleControl = new FormControl(this.instruction.title, [
      Validators.required,
    ]);
    this.instructionControl = new FormControl(
      textTransformNewLines(this.instruction.message),
      [Validators.required]
    );
    this.releaseTypeControl = new FormControl(
      this.instruction.releaseType.code,
      [Validators.required]
    );
  }

  ngOnInit() {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateInstruction() {
    if (this.instructionControl.hasError("required")) {
      this.instructionError = true;
      return;
    } else if (this.releaseTypeControl.hasError("required")) {
      this.releaseError = true;
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        message: textTransform(this.instructionRef.nativeElement.value),
        releaseType: this.constantService.getReleaseType(
          this.releaseTypeControl.value
        ),
      });
    }
  }
}
