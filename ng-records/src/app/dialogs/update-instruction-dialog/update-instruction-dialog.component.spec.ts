import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInstructionDialogComponent } from './update-instruction-dialog.component';

describe('UpdateInstructionDialogComponent', () => {
  let component: UpdateInstructionDialogComponent;
  let fixture: ComponentFixture<UpdateInstructionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInstructionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInstructionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
