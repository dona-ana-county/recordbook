import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';

import { TextService } from 'src/app/services/text.service';
import { Content, ContentType } from 'src/app/models/content';

@Component({
  selector: 'app-delete-content-dialog',
  templateUrl: './delete-content-dialog.component.html',
  styleUrls: ['./delete-content-dialog.component.scss']
})
export class DeleteContentDialogComponent {
  reason: string;

  reasonControl = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef<DeleteContentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public text: TextService,
  ) {}

  onSubmit(): void {
    this.dialogRef.close({ reason: this.reasonControl.value });
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
