import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TextService } from 'src/app/services/text.service';
import { Note, ReleaseType } from 'src/app/models/content';
import { ConstantService } from 'src/app/services/constant.service';
import { textTransformNewLines, textTransform } from 'src/utils/textTransform';
import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-update-note-dialog',
  templateUrl: './update-note-dialog.component.html',
  styleUrls: ['./update-note-dialog.component.scss']
})
export class UpdateNoteDialogComponent implements OnInit {
  @ViewChild('title') titleRef: ElementRef;
  @ViewChild('note') noteRef: ElementRef;

  noteControl: FormControl;
  titleControl: FormControl;
  releaseTypeControl: FormControl;

  noteError = false;
  releaseError = false;

  releaseTypes: ReleaseType[];
  note: Note;

  request: Request;
  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef<UpdateNoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.note = data.note;
    this.noteControl = new FormControl(textTransformNewLines(this.note.message), [
      Validators.required
    ]);
    this.titleControl = new FormControl(this.note.title, [Validators.required]);
    this.releaseTypeControl = new FormControl(this.note.releaseType.code, [
      Validators.required
    ]);
  }

  ngOnInit() {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateNote() {
    if (this.titleControl.hasError('required')) {
      return;
    } else if (this.noteControl.hasError('required')) {
      this.noteError = true;
      return;
    } else if (this.releaseTypeControl.hasError('required')) {
      this.releaseError = true;
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        message: textTransform(this.noteRef.nativeElement.value),
        releaseType: this.constantService.getReleaseType(this.releaseTypeControl.value)
      });
    }
  }
}
