import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDueDateDialogComponent } from './update-due-date-dialog.component';

describe('UpdateDueDateDialogComponent', () => {
  let component: UpdateDueDateDialogComponent;
  let fixture: ComponentFixture<UpdateDueDateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDueDateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDueDateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
