import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequestService } from 'src/app/services/request.service';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { FormControl, Validators } from '@angular/forms';
import { DateTime } from 'luxon';
import { deserializeDate, forbiddenDateValidator, serializeDate } from 'src/utils/date';

@Component({
  selector: 'app-update-due-date-dialog',
  templateUrl: './update-due-date-dialog.component.html',
  styleUrls: ['./update-due-date-dialog.component.scss']
})
export class UpdateDueDateDialogComponent implements OnInit {
  minDate: Date;
  maxDate: Date;
  startAt: Date;
  request: Request;


  @ViewChild('note') noteRef: ElementRef;

  dateControl = new FormControl('', [Validators.required]);
  noteControl = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef<UpdateDueDateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private requestService: RequestService,
    public text: TextService
  ) {
    this.request = data.request;
    this.minDate = deserializeDate(this.request.createDate);
    const dueDate = DateTime.fromISO(this.request.dueDate);
    this.startAt = dueDate.toJSDate();

    if (!data.isAdmin) {
      const minDate = dueDate.plus({days: -3}).toJSDate();
      this.minDate = this.minDate < minDate ? minDate : this.minDate;
      this.maxDate = dueDate.plus({days:  3}).toJSDate();
    }
    this.dateControl.setValidators([Validators.required, forbiddenDateValidator(dueDate)]);
    this.dateControl.updateValueAndValidity();
  }

  dateClass = (d: Date): string => {
    if (d.toLocaleDateString() === this.minDate.toLocaleDateString()) {
      return 'calendar-create-date';
    } else if (d.toLocaleDateString() === this.startAt.toLocaleDateString()) {
        return 'calendar-due-date';
    }
    return undefined;
  }

  dateFilter = (d: Date): boolean => {
    return ! (d.toLocaleDateString() === this.startAt.toLocaleDateString());
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  extendDate() {
    if (!this.dateControl.hasError('required')
        && !this.dateControl.hasError('forbiddenDate')) {
          let desc: string;
          if (this.noteRef) {
            desc = this.noteRef.nativeElement.value;
          }

          this.dialogRef.close({
            date: serializeDate(this.dateControl.value),
            reason: desc
          });
    }
  }
}
