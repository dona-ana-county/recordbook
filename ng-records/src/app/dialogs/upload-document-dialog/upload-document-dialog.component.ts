import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ReleaseType } from "src/app/models/content";
import { FormControl, Validators } from "@angular/forms";
import { TextService } from "src/app/services/text.service";
import { ConstantService } from "src/app/services/constant.service";
import { textTransform } from "src/utils/textTransform";
import { Request } from "src/app/models/request";

@Component({
  selector: "app-upload-document-dialog",
  templateUrl: "./upload-document-dialog.component.html",
  styleUrls: ["./upload-document-dialog.component.scss"]
})
export class UploadDocumentDialogComponent implements OnInit {
  @ViewChild("titleInput") titleInput: ElementRef;
  @ViewChild("fileDescription") descriptionInput: ElementRef;
  @ViewChild("legalComment") legalCommentInput: ElementRef;

  /*
   * Set the file size limit here in mb. When changing ensure that it is changed in the upload
   * revised dialog.
   */
  fileSizeLimit = 300;

  fileData: File = null;
  requestId: string;
  request: Request;
  needsReview = false;
  dragenter = false;
  titleError = false;
  titleControl = new FormControl("", [Validators.required]);
  releaseError = false;
  releaseTypeControl = new FormControl("", [Validators.required]);
  selectedReleaseType: ReleaseType;
  basic: boolean;
  legalFileSize: boolean;
  hasEmail = true;

  releaseTypes: ReleaseType[];

  constructor(
    public dialogRef: MatDialogRef<UploadDocumentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.legalFileSize = true;
    if (data.basic) {
      this.basic = true;
      this.needsReview = false;
      this.selectedReleaseType = this.releaseTypes.find(t => t.code === "PRV");
    } else {
      this.basic = false;
    }
  }

  ngOnInit(): void {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  updateReleaseType(selection) {
    this.selectedReleaseType = selection.value;
  }

  onDrop(e) {
    e.preventDefault();
    this.fileData = e.dataTransfer.files.item(0);
    this.checkFileLimit(this.fileData.size);
    this.titleControl.reset(this.fileData.name);
  }

  dragStart(e) {}

  onDrag(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  onDragEnter(e) {
    this.dragenter = true;
  }

  onDragleave() {
    this.dragenter = false;
  }

  onFileSelected(files: FileList) {
    this.fileData = files.item(0);
    this.checkFileLimit(this.fileData.size);
    this.titleControl.reset(this.fileData.name);
  }

  uploadFile() {
    let desc: string;
    if (this.descriptionInput) {
      desc = textTransform(this.descriptionInput.nativeElement.value);
    }
    let msg: string;
    if (this.legalCommentInput) {
      msg = this.legalCommentInput.nativeElement.value;
    }
    const data = {
      file: this.fileData,
      title: this.titleInput.nativeElement.value,
      needsReview: this.needsReview,
      releaseType: this.selectedReleaseType,
      message: msg,
      description: desc
    };

    if (this.titleControl.hasError("required") && !this.basic) {
      this.titleError = true;
      return;
    } else if (this.releaseTypeControl.hasError("required") && !this.basic) {
      this.releaseError = true;
      return;
    } else {
      this.dialogRef.close(data);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  clearFiles(): void {
    this.fileData = null;
    this.legalFileSize = true;
  }

  checkFileLimit(fileSize: number): void {
    const mb = 1024 * 1024;
    if (fileSize / mb > this.fileSizeLimit) {
      this.legalFileSize = false;
    } else {
      this.legalFileSize = true;
    }
  }
}
