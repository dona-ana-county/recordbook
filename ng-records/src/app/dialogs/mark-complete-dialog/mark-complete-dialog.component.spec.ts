import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkCompleteDialogComponent } from './mark-complete-dialog.component';

describe('MarkCompleteDialogComponent', () => {
  let component: MarkCompleteDialogComponent;
  let fixture: ComponentFixture<MarkCompleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkCompleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkCompleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
