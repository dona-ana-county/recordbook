import { ElementRef, Inject, ViewChild } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { TextService } from "src/app/services/text.service";
import { textTransformNewLines, textTransform } from "src/utils/textTransform";

@Component({
  selector: "app-mark-complete-dialog",
  templateUrl: "./mark-complete-dialog.component.html",
  styleUrls: ["./mark-complete-dialog.component.scss"],
})
export class MarkCompleteDialogComponent implements OnInit {
  @ViewChild("comment") commentRef: ElementRef;

  key: string;
  dialogTitle: string;
  commentControl = new FormControl("");
  deptId: string;
  completionStatus: boolean;
  comment = "";
  request: Request;

  constructor(
    public dialogRef: MatDialogRef<MarkCompleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService
  ) {}

  ngOnInit(): void {
    console.log(this.data);
    this.request = this.data.request;
    this.key = this.data.key;
    this.deptId = this.data.data.id;
    this.completionStatus = this.data.data.completed.value;
    this.dialogTitle =
      this.key === "edit_comment" ? "Edit Comment":
      (this.completionStatus ? "Mark Incomplete": "Mark Complete");

    if (this.data.data.comment) {
      this.comment = this.data.data.comment;
      this.commentControl.setValue(textTransformNewLines(this.comment));
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateCompletionStatus() {
    if (this.completionStatus && this.key === "completed") {
      this.comment = "";
    } else if (this.commentRef.nativeElement.value) {
      this.comment = this.commentRef.nativeElement.value;
    }

    this.dialogRef.close({
      id: this.deptId,
      completed: this.key === "completed" ? !this.completionStatus : this.completionStatus,
      comment: textTransform(this.comment),
    });
  }
}
