import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ReleaseType } from 'src/app/models/content';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-approve-resource-dialog',
  templateUrl: './approve-resource-dialog.component.html',
  styleUrls: ['./approve-resource-dialog.component.scss']
})
export class ApproveResourceDialogComponent {
  releaseError = false;
  releaseTypeControl: FormControl;

  releaseTypes: ReleaseType[];

  constructor(
    public dialogRef: MatDialogRef<ApproveResourceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.releaseTypeControl = new FormControl(data.type.code, [Validators.required]);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

    approveResource() {
    // submit with changed release type
    this.dialogRef.close({
      releaseType: this.constantService.getReleaseType(this.releaseTypeControl.value)
    });
  }
}
