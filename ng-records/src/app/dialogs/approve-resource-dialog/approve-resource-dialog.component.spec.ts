import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveResourceDialogComponent } from './approve-resource-dialog.component';

describe('ApproveResourceDialogComponent', () => {
  let component: ApproveResourceDialogComponent;
  let fixture: ComponentFixture<ApproveResourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveResourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveResourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
