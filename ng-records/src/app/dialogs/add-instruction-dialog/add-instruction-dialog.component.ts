import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReleaseType } from 'src/app/models/content';
import { FormControl, Validators } from '@angular/forms';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { textTransform } from 'src/utils/textTransform';
import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-add-instruction-dialog',
  templateUrl: './add-instruction-dialog.component.html',
  styleUrls: ['./add-instruction-dialog.component.scss']
})
export class AddInstructionDialogComponent implements OnInit {
  @ViewChild('title') titleRef: ElementRef;
  @ViewChild('instruction') instructionRef: ElementRef;

  instructionControl = new FormControl('', [Validators.required]);
  releaseTypeControl = new FormControl('', [Validators.required]);

  instructionError = false;
  releaseError = false;

  releaseTypes: ReleaseType[];
  selectedReleaseType: ReleaseType;

  request: Request;
  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef<AddInstructionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
  }

  ngOnInit() {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  updateReleaseType(selection) {
    this.selectedReleaseType = selection.value;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  createInstruction() {
    if (this.instructionControl.hasError('required')) {
      this.instructionError = true;
      return;
    } else if (this.releaseTypeControl.hasError('required')) {
      this.releaseError = true;
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        message: textTransform(this.instructionRef.nativeElement.value),
        releaseType: this.selectedReleaseType
      });
    }
  }
}
