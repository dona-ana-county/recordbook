import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadRevisedDialogComponent } from './upload-revised-dialog.component';

describe('UploadRevisedDialogComponent', () => {
  let component: UploadRevisedDialogComponent;
  let fixture: ComponentFixture<UploadRevisedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadRevisedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadRevisedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
