import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UploadDocumentDialogComponent } from '../upload-document-dialog/upload-document-dialog.component';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-upload-revised-dialog',
  templateUrl: './upload-revised-dialog.component.html',
  styleUrls: ['./upload-revised-dialog.component.scss']
})
export class UploadRevisedDialogComponent implements OnInit {
  /*
   * Set the file size limit here in mb. When changing ensure that it is changed in the upload
   * document dialog.
   */
  fileSizeLimit = 300;
  legalFileSize: boolean;

  fileData: File = null;
  dragenter = false;
  constructor(
    public dialogRef: MatDialogRef<UploadDocumentDialogComponent>,
    public text: TextService
  ) {
    this.legalFileSize = true;
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onDrop(e) {
    e.preventDefault();
    this.fileData = e.dataTransfer.files.item(0);
  }

  dragStart(e) {}

  onDrag(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  onDragEnter(e) {
    this.dragenter = true;
  }

  onDragleave() {
    this.dragenter = false;
  }

  onFileSelected(files: FileList) {
    this.fileData = files.item(0);
    this.checkFileLimit(this.fileData.size);
  }

  uploadDoc() {
    const data = {
      file: this.fileData
    };

    this.dialogRef.close(data);
  }

  clearFiles(): void {
    this.fileData = null;
    this.legalFileSize = true;
  }

  checkFileLimit(fileSize: number): void {
    const mb = 1024 * 1024;
    if (fileSize / mb > this.fileSizeLimit) {
      this.legalFileSize = false;
    } else {
      this.legalFileSize = true;
    }
  }
}
