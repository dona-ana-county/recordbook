import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-basic-confirmation-dialog',
  templateUrl: './basic-confirmation-dialog.component.html',
  styleUrls: ['./basic-confirmation-dialog.component.scss']
})
export class BasicConfirmationDialogComponent implements OnInit {
  heading: string;
  cancel: string;
  confirm: string;

  constructor(
    public dialogRef: MatDialogRef<BasicConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService
  ) {
    if (data.heading) {
      this.heading = data.heading;
    } else {
      this.heading = 'Heading';
    }

    if (data.cancel) {
      this.cancel = data.cancel;
    } else {
      this.cancel = text.get('cancel');
    }

    if (data.heading) {
      this.confirm = data.confirm;
    } else {
      this.confirm = 'Confirm';
    }
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  closeDialog(): void {
    this.dialogRef.close(true);
  }
}
