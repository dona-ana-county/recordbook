import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicConfirmationDialogComponent } from './basic-confirmation-dialog.component';

describe('BasicConfirmationDialogComponent', () => {
  let component: BasicConfirmationDialogComponent;
  let fixture: ComponentFixture<BasicConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
