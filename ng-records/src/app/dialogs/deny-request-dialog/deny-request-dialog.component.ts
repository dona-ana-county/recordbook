import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';
import { RequestService } from 'src/app/services/request.service';
import { ConstantService } from 'src/app/services/constant.service';
import { TextService } from 'src/app/services/text.service';

import { Request } from 'src/app/models/request';
import { GovernmentEntity, Reason } from 'src/app/models/constant';

export interface DenyRequestDialogData {
  request: Request;
}

@Component({
  selector: 'app-deny-request-dialog',
  templateUrl: './deny-request-dialog.component.html',
  styleUrls: ['./deny-request-dialog.component.scss']
})
export class DenyRequestDialogComponent {
  denyReasons: Reason[];
  entities: GovernmentEntity[];
  selectedReason: Reason;
  selectedEntity: string;
  request: Request;
  nameControl: FormControl;
  emailControl: FormControl;
  noteControl = new FormControl('', [Validators.required]);

  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef<DenyRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DenyRequestDialogData,
    private requestService: RequestService,
    private constantService: ConstantService,
    public text: TextService
  ) {
    this.request = data.request;
    this.entities = [];

    this.nameControl = new FormControl('', [Validators.required]);
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);

    this.constantService.getAllDenialReasons().subscribe(res => {
      this.denyReasons = res;
      this.selectedReason = this.denyReasons[0];
    });

    this.constantService.getGovernmentEntities().subscribe(res => {
      this.entities = res;
      this.entities.push({ id: '0', name: 'Other', email: 'email' });
      this.selectedEntity = this.entities[0].id;
      this.nameControl = new FormControl(this.entities[0].name, [Validators.required]);
      this.emailControl = new FormControl(this.entities[0].email, [
        Validators.required,
        Validators.email
      ]);
    });

    this.hasEmail = this.request.requestor.primaryContact.code ===  'EML';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitDenial(): void {
    if (this.selectedReason.code === 'OTH') {
      this.selectedReason.value = this.noteControl.value;
    }
    if (this.selectedReason.code === 'WGE') {
      if (this.nameControl.invalid || this.emailControl.invalid) {
        this.nameControl.markAsTouched();
        this.emailControl.markAsTouched();
        return;
      }
      const entity = {
        name: this.nameControl.value,
        email: this.emailControl.value
      };
      this.requestService
        .denyRequestWrongGovernmentEntity(this.request, this.selectedReason, entity)
        .subscribe(res => {
          this.dialogRef.close('Request: ' + this.request.id + ' was denied.');
        });
    } else {
      this.requestService.denyRequest(this.request.id, this.selectedReason).subscribe(res => {
        this.dialogRef.close('Request: ' + this.request.id + ' was denied.');
      });
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }

  selectionChange(event) {
    this.selectedEntity = event.value;
    if (this.selectedEntity === '0') {
      this.nameControl = new FormControl('', [Validators.required]);
      this.emailControl = new FormControl('', [Validators.required, Validators.email]);
    } else {
      const entity = this.getEntity(this.selectedEntity);
      this.nameControl = new FormControl(entity.name, [Validators.required]);
      this.emailControl = new FormControl(entity.email, [Validators.required, Validators.email]);
    }
  }

  getEntity(id: string): GovernmentEntity {
    for (const entity of this.entities) {
      if (id === entity.id) {
        return entity;
      }
    }
    return null;
  }
}
