import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-requestor-enter-passcode',
  templateUrl: './requestor-enter-passcode.component.html',
  styleUrls: ['./requestor-enter-passcode.component.scss']
})
export class RequestorEnterPasscodeComponent implements OnInit {
  @ViewChild('passcode') passRef: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<RequestorEnterPasscodeComponent>
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onVerify(): void {
    this.dialogRef.close(this.passRef.nativeElement.value);
  }
}
