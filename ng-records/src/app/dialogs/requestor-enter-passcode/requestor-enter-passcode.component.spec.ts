import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorEnterPasscodeComponent } from './requestor-enter-passcode.component';

describe('RequestorEnterPasscodeComponent', () => {
  let component: RequestorEnterPasscodeComponent;
  let fixture: ComponentFixture<RequestorEnterPasscodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorEnterPasscodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorEnterPasscodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
