import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Note, ReleaseType, Resource } from "src/app/models/content";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UpdateNoteDialogComponent } from "../update-note-dialog/update-note-dialog.component";
import { TextService } from "src/app/services/text.service";
import { ConstantService } from "src/app/services/constant.service";
import { textTransformNewLines, textTransform } from "src/utils/textTransform";
import { Request } from 'src/app/models/request';

@Component({
  selector: "app-update-resource-dialog",
  templateUrl: "./update-resource-dialog.component.html",
  styleUrls: ["./update-resource-dialog.component.scss"]
})
export class UpdateResourceDialogComponent implements OnInit {
  @ViewChild("title") titleRef: ElementRef;
  @ViewChild("description") descriptionRef: ElementRef;

  releaseTypeControl: FormControl;
  titleControl: FormControl;
  descriptionControl: FormControl;

  resource: Resource;
  request: Request;
  hasEmail = false;

  releaseTypes: ReleaseType[];

  constructor(
    public dialogRef: MatDialogRef<UpdateNoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.resource = data.document;
    this.releaseTypeControl = new FormControl(this.resource.releaseType.code, [
      Validators.required
    ]);
    this.titleControl = new FormControl(this.resource.title, [
      Validators.required
    ]);
    this.descriptionControl = new FormControl(
      textTransformNewLines(this.resource.description)
    );
  }

  ngOnInit(): void {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateNote() {
    if (
      this.titleControl.hasError("required") ||
      this.releaseTypeControl.hasError("required")
    ) {
      return;
    } else {
      let desc: string;
      if (this.descriptionRef) {
        desc = textTransform(this.descriptionRef.nativeElement.value);
      }
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        description: desc,
        releaseType: this.constantService.getReleaseType(
          this.releaseTypeControl.value
        )
      });
    }
  }
}
