import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import {
  RequestService
} from 'src/app/services/request.service';
import {
  Request
} from 'src/app/models/request';
import {
  TextService
} from 'src/app/services/text.service';
import {
  FormControl,
  Validators
} from '@angular/forms';
import {
  DateTime
} from 'luxon';
import {
  forbiddenDateValidator,
  deserializeDate,
  serializeDate
} from 'src/utils/date';
import {
  NotificationService
} from 'src/app/services/notification.service';

@Component({
  selector: 'app-extend-due-date-dialog',
  templateUrl: './extend-due-date-dialog.component.html',
  styleUrls: ['./extend-due-date-dialog.component.scss']
})
export class ExtendDueDateDialogComponent implements OnInit {
  minDate: Date;
  startAt: Date;
  request: Request;

  header: string;
  footer: string;

  @ViewChild('note') noteRef: ElementRef;

  dateControl = new FormControl('');
  noteControl = new FormControl('', [Validators.required]);

  dateVisible = false;
  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef < ExtendDueDateDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private requestService: RequestService,
    public text: TextService,
    private notificationService: NotificationService
  ) {
    this.request = data.request;
    this.header = data.header;
    this.footer = data.footer;

    if (this.request.extensionStatus.code !== 'NON') {
      this.dateVisible = true;
      this.minDate = deserializeDate(this.request.createDate);
      const dueDate = DateTime.fromISO(this.request.dueDate);
      this.startAt = dueDate.toJSDate();
      this.dateControl.setValidators([Validators.required, forbiddenDateValidator(dueDate)]);
      this.dateControl.updateValueAndValidity();
    }
    this.hasEmail = this.request.requestor.primaryContact.code === 'EML';
  }

  updateTemplate(event) {
    if (this.request.extensionStatus.code !== 'NON') {
      const params = {
        dueDt: serializeDate(this.dateControl.value)
      };
      this.notificationService.getTemplate('burdensome', this.request.id, params).subscribe((template) => {
        this.header = template.header;
        this.footer = template.footer;
      });
    }
  }

  dateClass = (d: Date): string => {
    if (d.toLocaleDateString() === this.minDate.toLocaleDateString()) {
      return 'calendar-create-date';
    } else if (d.toLocaleDateString() === this.startAt.toLocaleDateString()) {
      return 'calendar-due-date';
    }
    return undefined;
  }

  dateFilter = (d: Date): boolean => {
    return !(d.toLocaleDateString() === this.startAt.toLocaleDateString());
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  extendDate() {
    if (!this.dateControl.hasError('required')) {
      let desc: string;
      if (this.noteRef) {
        desc = this.noteRef.nativeElement.value;
      }
      this.dialogRef.close({
        date: serializeDate(this.dateControl.value),
        description: desc
      });
    }
  }
}
