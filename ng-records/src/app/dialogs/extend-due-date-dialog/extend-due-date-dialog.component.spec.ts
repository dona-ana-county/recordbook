import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendDueDateDialogComponent } from './extend-due-date-dialog.component';

describe('ExtendDueDateDialogComponent', () => {
  let component: ExtendDueDateDialogComponent;
  let fixture: ComponentFixture<ExtendDueDateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendDueDateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendDueDateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
