import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRequestDialogComponent } from './assign-request-dialog.component';

describe('AssignRequestDialogComponent', () => {
  let component: AssignRequestDialogComponent;
  let fixture: ComponentFixture<AssignRequestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignRequestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRequestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
