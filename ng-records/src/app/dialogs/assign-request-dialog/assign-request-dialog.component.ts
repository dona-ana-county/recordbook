import { Component, OnInit, Inject } from "@angular/core";
import { Validators, FormControl } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { DenyRequestDialogComponent } from "../deny-request-dialog/deny-request-dialog.component";
import {
  AlertedItem
} from "src/app/components/multi-stack-box-selector/multi-stack-box-selector.component";

import { StorageService } from "src/app/services/storage.service";
import { TextService } from "src/app/services/text.service";
import { DepartmentService } from "src/app/services/department.service";

import { Request } from "src/app/models/request";
import { Department } from "src/app/models/department";

@Component({
  selector: "app-assign-request-dialog",
  templateUrl: "./assign-request-dialog.component.html",
  styleUrls: ["./assign-request-dialog.component.scss"],
})
export class AssignRequestDialogComponent {
  departments: Department[];

  request: Request;

  primaryControl = new FormControl("", [Validators.required]);
  selectorItems: Department[];
  departmentMessages: any[] = [];

  secondaryAssignments: AlertedItem[];

  constructor(
    public dialogRef: MatDialogRef<DenyRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    public storageService: StorageService,
    public departmentService: DepartmentService
  ) {
    this.request = data.request;
    this.request = {
      title: data.request.title,
      description: data.request.description,
      requestor: data.request.requestor,
      primaryDepartment: null,
      secondaryDepartments: [],
    };
    this.departmentService.getDepartments().subscribe((res) => {
      this.departments = res;
      this.updateSelectorItems();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitAssignment(): void {
    if (this.primaryControl.hasError("required")) {
      return;
    }

    this.request.primaryDepartment = this.getDepartment(
      this.primaryControl.value.id
    );

    const data = {
      primaryDepartment: this.request.primaryDepartment,
      secondaryDepartments: this.request.secondaryDepartments,
      primaryAssignment: { id: this.request.primaryDepartment.id },
      secondaryAssignments: this.secondaryAssignments,
    };
    this.dialogRef.close(data);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  onPrimaryDepartmentChange(event) {
    this.request.primaryDepartment = event.value;
    this.updateSelectorItems();
  }

  updateSelectorItems() {
    this.selectorItems = [];
    for (const dept of this.departments) {
      if (dept !== this.request.primaryDepartment) {
        this.selectorItems.push(dept);
      }
    }
  }

  getDepartment(id: string): Department {
    for (const dept of this.departments) {
      if (dept.id === id) {
        return dept;
      }
    }
  }

  onBoxesChange(event) {
    this.secondaryAssignments = event;
  }
}
