import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-deny-resource-dialog',
  templateUrl: './deny-resource-dialog.component.html',
  styleUrls: ['./deny-resource-dialog.component.scss']
})
export class DenyResourceDialogComponent implements OnInit {
  reasonControl = new FormControl('', [Validators.required]);
  @ViewChild('reason') reasonRef: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<DenyResourceDialogComponent>,
    public text: TextService
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  denyResource() {
    if (!this.reasonControl.hasError('required')) {
      this.dialogRef.close({ reason: this.reasonRef.nativeElement.value });
    }
    return;
  }
}
