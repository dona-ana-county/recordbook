import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenyResourceDialogComponent } from './deny-resource-dialog.component';

describe('DenyResourceDialogComponent', () => {
  let component: DenyResourceDialogComponent;
  let fixture: ComponentFixture<DenyResourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenyResourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenyResourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
