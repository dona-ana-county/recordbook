import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';

import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-re-open-request-dialog',
  templateUrl: './re-open-request-dialog.component.html',
  styleUrls: ['./re-open-request-dialog.component.scss'],
})
export class ReOpenRequestDialogComponent implements OnInit {
  requestId: string;
  noteText: string;

  noteControl = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef<ReOpenRequestDialogComponent>,
    private constantService: ConstantService,
    private requestService: RequestService,
    @Inject(MAT_DIALOG_DATA) public data: string,
    public text: TextService,
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  reOpenRequest(): void {
    this.dialogRef.close({ reason: this.noteControl.value });
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
