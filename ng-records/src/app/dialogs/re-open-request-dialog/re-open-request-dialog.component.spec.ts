import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReOpenRequestDialogComponent } from './re-open-request-dialog.component';

describe('ReOpenRequestDialogComponent', () => {
  let component: ReOpenRequestDialogComponent;
  let fixture: ComponentFixture<ReOpenRequestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReOpenRequestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReOpenRequestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
