import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongCustodianDialogComponent } from './wrong-custodian-dialog.component';

describe('WrongCustodianDialogComponent', () => {
  let component: WrongCustodianDialogComponent;
  let fixture: ComponentFixture<WrongCustodianDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WrongCustodianDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WrongCustodianDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
