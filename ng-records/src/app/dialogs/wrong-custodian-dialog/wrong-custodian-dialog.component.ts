import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';
import { RequestService } from 'src/app/services/request.service';
import { ConstantService } from 'src/app/services/constant.service';
import { TextService } from 'src/app/services/text.service';

import { Request } from 'src/app/models/request';
import { GovernmentEntity, Reason } from 'src/app/models/constant';

export interface WrongCustodianDialogData {
  request: Request;
}

@Component({
  selector: 'app-wrong-custodian-dialog',
  templateUrl: './wrong-custodian-dialog.component.html',
  styleUrls: ['./wrong-custodian-dialog.component.scss']
})
export class WrongCustodianDialogComponent implements OnInit {
  entities: GovernmentEntity[];
  selectedEntity: string;
  request: Request;
  nameControl: FormControl;
  emailControl: FormControl;

  hasEmail = false;

  constructor(
    public dialogRef: MatDialogRef<WrongCustodianDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: WrongCustodianDialogData,
    private requestService: RequestService,
    private constantService: ConstantService,
    public text: TextService
  ) { }

  ngOnInit(): void {
    this.request = this.data.request;
    this.entities = [];

    this.nameControl = new FormControl('', [Validators.required]);
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);

    this.constantService.getGovernmentEntities().subscribe(res => {
      this.entities = res;
      this.entities.push({ id: '0', name: 'Other', email: 'email' });
      this.selectedEntity = this.entities[0].id;
      this.nameControl = new FormControl(this.entities[0].name, [Validators.required]);
      this.emailControl = new FormControl(this.entities[0].email, [
        Validators.required,
        Validators.email
      ]);
    });

    this.hasEmail = this.request.requestor.primaryContact.code === 'EML';
  }

  submitWrongCustodian(): void {
    console.log('submitted');
    if (this.nameControl.invalid || this.emailControl.invalid) {
      this.nameControl.markAsTouched();
      this.emailControl.markAsTouched();
      return;
    }
    const entity = {
      name: this.nameControl.value,
      email: this.emailControl.value
    };

    this.requestService.forwardRequestWrongGovernmentEntity(this.request, entity)
      .subscribe(res => {
          this.dialogRef.close('Request: ' + this.request.id + ' was forwarded to ' + entity.name + '.');
      });
  }

  cancel(): void {
    this.dialogRef.close();
  }

  selectionChange(event) {
    this.selectedEntity = event.value;
    if (this.selectedEntity === '0') {
      this.nameControl = new FormControl('', [Validators.required]);
      this.emailControl = new FormControl('', [Validators.required, Validators.email]);
    } else {
      const entity = this.getEntity(this.selectedEntity);
      this.nameControl = new FormControl(entity.name, [Validators.required]);
      this.emailControl = new FormControl(entity.email, [Validators.required, Validators.email]);
    }
  }

  getEntity(id: string): GovernmentEntity {
    for (const entity of this.entities) {
      if (id === entity.id) {
        return entity;
      }
    }
    return null;
  }

}
