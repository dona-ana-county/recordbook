import { Component, OnInit, ViewChild, ElementRef, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateDepartmentDialogComponent } from '../create-department-dialog/create-department-dialog.component';
import { GovernmentEntity } from 'src/app/models/constant';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-update-government-entity-dialog',
  templateUrl: './update-government-entity-dialog.component.html',
  styleUrls: ['./update-government-entity-dialog.component.scss']
})
export class UpdateGovernmentEntityDialogComponent implements OnInit, AfterViewInit {
  @ViewChild('name') nameRef: ElementRef;
  emailControl = new FormControl('', [Validators.required, Validators.email]);
  formGroup: FormGroup;
  governmentEntity: GovernmentEntity;
  popper = false;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateDepartmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService
  ) {
    this.formGroup = this.formBuilder.group({});
    this.governmentEntity = data.governmentEntity;
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.nameRef.nativeElement.value = this.governmentEntity.name;
    this.emailControl.setValue(this.governmentEntity.email);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirmPopper() {
    this.closePopper();
    const result = {
      delete: true
    };
    this.dialogRef.close(result);
  }

  closePopper(): void {
    this.popper = false;
  }
  onOpenPopper(): void {
    this.popper = true;
  }

  updateGovernmentEntity() {
    if (this.nameRef.nativeElement.value && this.emailControl.valid) {
      this.governmentEntity.name = this.nameRef.nativeElement.value;
      this.governmentEntity.email = this.emailControl.value;
      const result = {
        governmentEntity: this.governmentEntity
      };

      this.dialogRef.close(result);
    }
  }
}
