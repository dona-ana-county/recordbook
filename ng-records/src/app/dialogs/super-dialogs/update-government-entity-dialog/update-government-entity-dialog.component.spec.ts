import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGovernmentEntityDialogComponent } from './update-government-entity-dialog.component';

describe('UpdateGovernmentEntityDialogComponent', () => {
  let component: UpdateGovernmentEntityDialogComponent;
  let fixture: ComponentFixture<UpdateGovernmentEntityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGovernmentEntityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGovernmentEntityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
