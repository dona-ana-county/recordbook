import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
  AfterViewInit
} from '@angular/core';
import { Department } from 'src/app/models/department';
import { Role, Staff, Group } from 'src/app/models/user';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { SuperService } from 'src/app/services/super.service';

@Component({
  selector: 'app-update-staff-dialog',
  templateUrl: './update-staff-dialog.component.html',
  styleUrls: ['./update-staff-dialog.component.scss']
})
export class UpdateStaffDialogComponent implements OnInit, AfterViewInit {
  @ViewChild('firstName') firstNameRef: ElementRef;
  @ViewChild('lastName') lastNameRef: ElementRef;
  @ViewChild('username') usernameRef: ElementRef;
  emailControl = new FormControl('', [Validators.required, Validators.email]);

  selectedRole: string;
  selectedDepartment: string;
  departments: Department[];
  staffGroups: Group[];
  roles: Role[];
  formGroup: FormGroup;
  staff: Staff;
  currentStaffDepartment: Department;
  groupValues: Group[];
  allGroups: Group[];
  popper = false;
  notOnLogin = true;
  externalAuth = false;

  constructor(
    private formBuilder: FormBuilder,
    public text: TextService,
    public dialogRef: MatDialogRef<UpdateStaffDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private superService: SuperService,
    private constantService: ConstantService
  ) {
    this.roles = this.constantService.roles;
    this.allGroups = this.constantService.groups;
    this.staff = data.staff;
    this.currentStaffDepartment = data.currentStaffDepartment;
    this.staffGroups = data.staff.group;
    this.departments = data.departments;
    this.selectedRole = null;
    this.selectedDepartment = null;
    this.formGroup = this.formBuilder.group({});
  }

  ngOnInit() {
    this.groupValues = this.staff.groups;
  }

  groupSelectionChanged(event) {
    this.staffGroups = event.selectedItems;
  }

  ngAfterViewInit() {
    this.firstNameRef.nativeElement.value = this.staff.firstName;
    this.lastNameRef.nativeElement.value = this.staff.lastName;
    this.usernameRef.nativeElement.value = this.staff.username;
    this.emailControl.setValue(this.staff.email);
    this.selectedRole = this.staff.role.code;
    this.selectedDepartment = this.currentStaffDepartment.id;
    this.externalAuth = this.staff.externalAuth;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onTabChanged(event) {
    if (event.index === 2) {
      this.notOnLogin = false;
    } else {
      this.notOnLogin = true;
    }
  }

  disableStaff() {
    const staffToDisable: Staff = {
      id: this.staff.id,
      firstName: this.staff.firstName,
      lastName: this.staff.lastName,
      groups: this.staff.groups,
      email: this.staff.email,
      username: this.staff.username,
      role: this.staff.role,
      token: this.staff.token,
      disabled: this.staff.disabled,
      externalAuth: this.staff.externalAuth
    };
    this.superService.disableStaff(staffToDisable).subscribe(res => {
      if (res) {
        this.staff.disabled = res.disabled;
      }
    });
  }

  enableStaff() {
    const staffToDisable: Staff = {
      id: this.staff.id,
      firstName: this.staff.firstName,
      lastName: this.staff.lastName,
      groups: this.staff.groups,
      email: this.staff.email,
      username: this.staff.username,
      role: this.staff.role,
      token: this.staff.token,
      disabled: this.staff.disabled,
      externalAuth: this.staff.externalAuth
    };
    this.superService.enableStaff(staffToDisable).subscribe(res => {
      if (res) {
        this.staff.disabled = res.disabled;
      }
    });
  }

  onConfirmPopper() {
    this.closePopper();
    const result = {
      delete: {
        id: this.staff.id,
        firstName: this.firstNameRef.nativeElement.value,
        lastName: this.lastNameRef.nativeElement.value,
        groups: this.staffGroups,
        email: this.emailControl.value,
        username: this.usernameRef.nativeElement.value,
        role: this.constantService.getRole(this.selectedRole),
        externalAuth: this.externalAuth
      }
    };
    this.dialogRef.close(result);
  }

  closePopper(): void {
    this.popper = false;
  }

  onOpenPopper(): void {
    this.popper = true;
  }

  roleSelected(event) {}

  departmentSelected(event) {}

  updateStaff() {
    if (
      this.firstNameRef.nativeElement.value &&
      this.lastNameRef.nativeElement.value &&
      this.emailControl.valid &&
      this.usernameRef.nativeElement.value &&
      this.selectedRole &&
      this.selectedDepartment
    ) {
      const result = {
        staff: {
          id: this.staff.id,
          firstName: this.firstNameRef.nativeElement.value,
          lastName: this.lastNameRef.nativeElement.value,
          groups: this.staffGroups,
          email: this.emailControl.value,
          username: this.usernameRef.nativeElement.value,
          role: this.constantService.getRole(this.selectedRole),
          externalAuth: this.externalAuth
        },

        department: this.getDepartment(this.selectedDepartment)
      };

      this.dialogRef.close(result);
    }
  }

  getDepartment(id: string): Department {
    for (const dept of this.departments) {
      if (dept.id === id) {
        return dept;
      }
    }
    return null;
  }
}
