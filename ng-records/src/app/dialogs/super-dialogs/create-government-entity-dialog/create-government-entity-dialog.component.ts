import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateDepartmentDialogComponent } from '../create-department-dialog/create-department-dialog.component';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-create-government-entity-dialog',
  templateUrl: './create-government-entity-dialog.component.html',
  styleUrls: ['./create-government-entity-dialog.component.scss']
})
export class CreateGovernmentEntityDialogComponent implements OnInit {
  @ViewChild('name') nameRef: ElementRef;
  emailControl: FormControl;
  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateDepartmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService
  ) {
    this.formGroup = this.formBuilder.group({});
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  createGovernmentEntity() {
    if (this.nameRef.nativeElement.value && this.emailControl.valid) {
      const result = {
        name: this.nameRef.nativeElement.value,
        email: this.emailControl.value
      };

      this.dialogRef.close(result);
    }
  }
}
