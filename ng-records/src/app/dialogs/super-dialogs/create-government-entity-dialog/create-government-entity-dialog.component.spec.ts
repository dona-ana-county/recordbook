import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGovernmentEntityDialogComponent } from './create-government-entity-dialog.component';

describe('CreateGovernmentEntityDialogComponent', () => {
  let component: CreateGovernmentEntityDialogComponent;
  let fixture: ComponentFixture<CreateGovernmentEntityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGovernmentEntityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGovernmentEntityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
