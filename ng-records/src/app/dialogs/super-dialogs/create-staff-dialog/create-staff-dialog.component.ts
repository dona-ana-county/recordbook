import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { Department } from 'src/app/models/department';
import { Role, Staff, Group } from 'src/app/models/user';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateStaffDialogComponent } from '../update-staff-dialog/update-staff-dialog.component';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-create-staff-dialog',
  templateUrl: './create-staff-dialog.component.html',
  styleUrls: ['./create-staff-dialog.component.scss']
})
export class CreateStaffDialogComponent {
  @ViewChild('firstName') firstNameRef: ElementRef;
  @ViewChild('lastName') lastNameRef: ElementRef;
  @ViewChild('username') usernameRef: ElementRef;
  emailControl = new FormControl('', [Validators.required, Validators.email]);
  selectedRole: Role;
  selectedDepartment: string;
  departments: Department[];
  roles: Role[];
  formGroup: FormGroup;
  externalAuth = false;
  groups: Group[];
  groupValues: Group[];

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    public dialogRef: MatDialogRef<UpdateStaffDialogComponent>,
    private constantService: ConstantService
  ) {
    this.groupValues = [];
    this.groups = constantService.groups;
    this.roles = this.constantService.roles;
    this.departments = data.departments;
    this.selectedRole = null;
    this.selectedDepartment = null;
    this.formGroup = this.formBuilder.group({});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  roleSelected(event) {
    this.selectedRole = event.value;
  }

  departmentSelected(event) {}

  updateStaff() {
    if (
      this.firstNameRef.nativeElement.value &&
      this.lastNameRef.nativeElement.value &&
      this.emailControl.valid &&
      this.usernameRef.nativeElement.value &&
      this.selectedRole &&
      this.selectedDepartment
    ) {
      const result = {
        staff: {
          firstName: this.firstNameRef.nativeElement.value,
          lastName: this.lastNameRef.nativeElement.value,
          email: this.emailControl.value,
          username: this.usernameRef.nativeElement.value,
          role: this.selectedRole,
          externalAuth: this.externalAuth,
          groups: this.groupValues
        },
        department: this.getDepartment(this.selectedDepartment)
      };

      this.dialogRef.close(result);
    }
  }

  getDepartment(id: string): Department {
    for (const dept of this.departments) {
      if (dept.id === id) {
        return dept;
      }
    }
    return null;
  }

  onTabChanged(event) {}

  groupSelectionChanged(event) {}
}
