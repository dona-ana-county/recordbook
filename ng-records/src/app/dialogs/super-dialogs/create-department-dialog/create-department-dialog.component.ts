import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-create-department-dialog',
  templateUrl: './create-department-dialog.component.html',
  styleUrls: ['./create-department-dialog.component.scss'],
})
export class CreateDepartmentDialogComponent implements OnInit {
  @ViewChild('name') nameRef: ElementRef;
  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateDepartmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
  ) {
    this.formGroup = this.formBuilder.group({});
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  createDepartment() {
    if (this.nameRef.nativeElement.value) {
      const result = {
        name: this.nameRef.nativeElement.value,
      };

      this.dialogRef.close(result);
    }
  }
}
