import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Department } from 'src/app/models/department';
import { FormControl, Validators } from '@angular/forms';
import { TextService } from 'src/app/services/text.service';
import { DepartmentService } from 'src/app/services/department.service';

@Component({
  selector: 'app-update-department-dialog',
  templateUrl: './update-department-dialog.component.html',
  styleUrls: ['./update-department-dialog.component.scss']
})
export class UpdateDepartmentDialogComponent implements OnInit {
  name = new FormControl('', [Validators.required]);
  updatedDept: Department;
  constructor(
    public dialogRef: MatDialogRef<UpdateDepartmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit() {
    this.name.setValue(this.data.name);
    this.updatedDept = this.data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateDepartmentName() {
    if (!this.name.hasError('required')) {
      this.updatedDept.name = this.name.value;
      this.departmentService
        .updateDepartment(this.updatedDept)
        .subscribe(res => {
          this.dialogRef.close(res);
        });
    }
  }
}
