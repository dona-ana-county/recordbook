import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseRequestDialogComponent } from './close-request-dialog.component';

describe('CloseRequestDialogComponent', () => {
  let component: CloseRequestDialogComponent;
  let fixture: ComponentFixture<CloseRequestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseRequestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseRequestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
