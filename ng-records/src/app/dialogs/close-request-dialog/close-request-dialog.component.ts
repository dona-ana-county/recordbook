import {
  Component,
  OnInit,
  Inject,
  Input
} from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import {
  Validators,
  FormControl
} from '@angular/forms';

import {
  TextService
} from 'src/app/services/text.service';
import {
  ConstantService
} from 'src/app/services/constant.service';

import {
  Reason
} from 'src/app/models/constant';

import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-close-request-dialog',
  templateUrl: './close-request-dialog.component.html',
  styleUrls: ['./close-request-dialog.component.scss']
})
export class CloseRequestDialogComponent implements OnInit {

  closeReasons: Reason[];
  selectedReason: Reason;

  request: Request;
  hasEmail = false;

  noteControl = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef < CloseRequestDialogComponent > ,
    private constantService: ConstantService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
  ) {}

  ngOnInit(): void {
    this.request = this.data.request;

    if (this.request && this.request.requestor
      && this.request.requestor.primaryContact
      && this.request.requestor.primaryContact.code) {
        this.hasEmail = this.request.requestor.primaryContact.code ===  'EML';
      }

    this.constantService.getAllCloseReasons().subscribe((res) => {
      this.closeReasons = res;
      this.selectedReason = this.closeReasons[0];
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  closeRequest(): void {
    if (this.selectedReason.code === 'OTH') {
      this.selectedReason.value = this.noteControl.value;
    }
    this.dialogRef.close({
      selectedReason: this.selectedReason
    });
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
