import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Department } from 'src/app/models/department';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-close-warning-dialog',
  templateUrl: './close-warning-dialog.component.html',
  styleUrls: ['./close-warning-dialog.component.scss']
})
export class CloseWarningDialogComponent implements OnInit {

  request: Request;
  depts: Department[];

  constructor(
    public dialogRef: MatDialogRef<CloseWarningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService
  ) { }

  ngOnInit(): void {
    this.request = this.data.request;
    this.depts = this.data.depts;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmClose() {
     this.dialogRef.close({close: true});
  }

}
