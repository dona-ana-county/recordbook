import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseWarningDialogComponent } from './close-warning-dialog.component';

describe('CloseWarningDialogComponent', () => {
  let component: CloseWarningDialogComponent;
  let fixture: ComponentFixture<CloseWarningDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloseWarningDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseWarningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
