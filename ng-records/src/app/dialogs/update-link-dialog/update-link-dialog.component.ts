import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ReleaseType, Link } from 'src/app/models/content';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { textTransform, textTransformNewLines } from 'src/utils/textTransform';
import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-update-link-dialog',
  templateUrl: './update-link-dialog.component.html',
  styleUrls: ['./update-link-dialog.component.scss']
})
export class UpdateLinkDialogComponent implements OnInit {
  @ViewChild('title') titleRef: ElementRef;
  @ViewChild('link') linkRef: ElementRef;
  @ViewChild('description') descriptionRef: ElementRef;

  titleControl: FormControl;
  linkControl: FormControl;
  releaseTypeControl: FormControl;
  descriptionControl: FormControl;

  titleError = false;
  linkError = false;
  releaseError = false;
  releaseTypes: ReleaseType[];

  request: Request;
  hasEmail = false;

    link: Link;

  constructor(
    public dialogRef: MatDialogRef<UpdateLinkDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
    this.link = data.link;
    this.titleControl = new FormControl(this.link.title, [Validators.required]);
    this.linkControl = new FormControl(this.link.url, [Validators.required]);
    this.descriptionControl = new FormControl(textTransformNewLines(this.link.description), [Validators.required]);
    this.releaseTypeControl = new FormControl(this.link.releaseType.code, [Validators.required]);
  }

  ngOnInit() {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateLink() {
    if (
      this.titleControl.hasError('required') ||
      this.linkControl.hasError('required') ||
      this.releaseTypeControl.hasError('required')
    ) {
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        url: this.linkRef.nativeElement.value,
        description: textTransform(this.descriptionRef.nativeElement.value),
        releaseType: this.constantService.getReleaseType(this.releaseTypeControl.value)
      });
    }
  }
}
