import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ReleaseType } from "src/app/models/content";
import { FormControl, Validators } from "@angular/forms";
import { TextService } from "src/app/services/text.service";
import { ConstantService } from "src/app/services/constant.service";
import { textTransform } from "src/utils/textTransform";
import { Request } from "src/app/models/request";

@Component({
  selector: "app-add-link-dialog",
  templateUrl: "./add-link-dialog.component.html",
  styleUrls: ["./add-link-dialog.component.scss"]
})
export class AddLinkDialogComponent implements OnInit {
  @ViewChild("title") titleRef: ElementRef;
  @ViewChild("link") linkRef: ElementRef;
  @ViewChild("description") descriptionRef: ElementRef;

  titleError = false;
  titleControl = new FormControl("", [Validators.required]);

  linkError = false;
  linkControl = new FormControl("", [Validators.required]);

  releaseError = false;
  releaseTypeControl = new FormControl("", [Validators.required]);

  request: Request;
  hasEmail = false;

  releaseTypes: ReleaseType[];

  selectedReleaseType: ReleaseType;

  constructor(
    public dialogRef: MatDialogRef<AddLinkDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
  }

  ngOnInit(): void {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateReleaseType(selection) {
    this.selectedReleaseType = selection.value;
  }

  addLink() {
    if (
      this.titleControl.hasError("required") ||
      this.linkControl.hasError("required") ||
      this.releaseTypeControl.hasError("required")
    ) {
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        link: this.linkRef.nativeElement.value,
        message: textTransform(this.descriptionRef.nativeElement.value),
        releaseType: this.selectedReleaseType
      });
    }
  }
}
