import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';
import { ReleaseType } from 'src/app/models/content';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { textTransform } from 'src/utils/textTransform';
import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-add-note-dialog',
  templateUrl: './add-note-dialog.component.html',
  styleUrls: ['./add-note-dialog.component.scss']
})
export class AddNoteDialogComponent implements OnInit {
  @ViewChild('title') titleRef: ElementRef;
  @ViewChild('note') noteRef: ElementRef;

  noteControl = new FormControl('', [Validators.required]);
  titleControl = new FormControl('', [Validators.required]);

  noteError = false;

  releaseError = false;
  releaseTypeControl = new FormControl('PRV', [Validators.required]);

  releaseTypes: ReleaseType[];

  request: Request;
  hasEmail = false;


  constructor(
    public dialogRef: MatDialogRef<AddNoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public text: TextService,
    private constantService: ConstantService
  ) {
    this.releaseTypes = this.constantService.releaseTypes;
  }

  ngOnInit() {
    this.request = this.data.request;

    if (
      this.request &&
      this.request.requestor &&
      this.request.requestor.primaryContact &&
      this.request.requestor.primaryContact.code
    ) {
      this.hasEmail = this.request.requestor.primaryContact.code === "EML";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  createNote() {
    if (this.titleControl.hasError('required')) {
      return;
    } else if (this.noteControl.hasError('required')) {
      this.noteError = true;
      return;
    } else if (this.releaseTypeControl.hasError('required')) {
      return;
    } else {
      this.dialogRef.close({
        title: this.titleRef.nativeElement.value,
        message: textTransform(this.noteRef.nativeElement.value),
        releaseType: this.constantService.getReleaseType(this.releaseTypeControl.value)
      });
    }
  }
}
