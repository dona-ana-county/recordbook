import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiChipSelectorComponent } from './multi-chip-selector.component';

describe('MultiChipSelectorComponent', () => {
  let component: MultiChipSelectorComponent;
  let fixture: ComponentFixture<MultiChipSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiChipSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiChipSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
