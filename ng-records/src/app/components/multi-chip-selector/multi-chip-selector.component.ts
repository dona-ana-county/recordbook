import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-multi-chip-selector',
  templateUrl: './multi-chip-selector.component.html',
  styleUrls: ['./multi-chip-selector.component.scss'],
})
export class MultiChipSelectorComponent implements OnInit {
  @Input() allItems: any[];
  @Input() selectedItems: any[];
  @Output() onSelectionChange = new EventEmitter();

  selectedItem: string;
  availableItems: any[] = [];

  constructor(public text: TextService) {}

  ngOnInit() {
    this.selectedItem = null;
    this.updateAvailableItems();
  }

  itemSelected(event) {
    this.selectedItems.push(event.value);
    this.updateAvailableItems();
  }

  updateAvailableItems() {
    this.availableItems = [];
    for (const item of this.allItems) {
      if (!this.selectedItems.includes(item)) {
        this.availableItems.push(item);
      }
    }
    this.onSelectionChange.emit({ selectedItems: this.selectedItems });
  }

  removeChip(item: string) {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (this.selectedItems[i] === item) {
        this.selectedItems.splice(i, 1);
      }
    }
    this.updateAvailableItems();
  }
}
