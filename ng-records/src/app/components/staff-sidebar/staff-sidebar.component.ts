import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-staff-sidebar',
  templateUrl: './staff-sidebar.component.html',
  styleUrls: ['./staff-sidebar.component.scss']
})
export class StaffSidebarComponent implements OnInit {
  user = false;
  expandedMenu: boolean;
  @Input() active: string;

  constructor(
    private router: Router,
    private storageService: StorageService,
    public text: TextService
  ) {}

  ngOnInit() {
    this.expandedMenu = this.storageService.getSideToggle();
    if (this.storageService.getStaff()) {
      this.user = true;
    }
  }

  changeRoute(page) {
    this.router.navigate([page]);
  }

  setSideMenuToggle() {
    this.storageService.updateSideToggle(!this.expandedMenu);
    this.expandedMenu = !this.expandedMenu;
  }
}
