import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-super-sidebar',
  templateUrl: './super-sidebar.component.html',
  styleUrls: ['./super-sidebar.component.scss'],
})
export class SuperSidebarComponent implements OnInit {
  expandedMenu: boolean;
  @Input() active: string;

  constructor(
    private router: Router,
    private storageService: StorageService,
    public text: TextService,
  ) {}

  ngOnInit() {}

  changeRoute(page) {
    this.router.navigate([page]);
  }

  setSideMenuToggle() {
    this.storageService.updateSideToggle(!this.expandedMenu);
    this.expandedMenu = !this.expandedMenu;
  }
}
