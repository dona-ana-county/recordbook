import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnDestroy,
  AfterContentInit,
  OnChanges
} from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { Staff } from 'src/app/models/user';
import { Alert } from 'src/app/models/communication';

export interface AlertedItem {
  id: any;
  alert?: Alert;
}
interface Box {
  item: any;
  message: string;
  isMessageVisible: boolean;
  newOption: boolean;
}

@Component({
  selector: 'app-multi-stack-box-selector',
  templateUrl: './multi-stack-box-selector.component.html',
  styleUrls: ['./multi-stack-box-selector.component.scss']
})
export class MultiStackBoxSelectorComponent implements OnInit, OnDestroy, OnChanges {
  @Input() allSelectableItems: any[];
  @Input() selectedItems: any[];
  @Input() originalItems: any[];
  @Input() placeholder: string;
  @Input() showItemStatus: boolean;
  @Output() boxesChange = new EventEmitter();
  @Output() action = new EventEmitter();
  availableItems: any[];
  boxes: Box[];
  sender: Staff;

  constructor(private storageService: StorageService) {}

  ngOnInit() {
    this.sender = this.storageService.getStaff();
    delete this.sender.token;
    if (!this.originalItems) {
      this.originalItems = [];
    }
    this.updateSelections();
  }

  ngOnDestroy() {
    for (const box of this.boxes) {
      box.message = '';
      box.isMessageVisible = false;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.allSelectableItems) {
      this.updateSelections();
    }
  }

  clear() {
    this.boxes = [];
    for (const item of this.selectedItems) {
      this.boxes.push({
        item,
        message: '',
        isMessageVisible: false,
        newOption: false
      });
    }
  }

  private updateSelections() {
    this.updateAvailableItems();
    this.updateBoxes();
  }

  updateBoxes() {
    const copy: Box[] = [];
    if (this.boxes) {
      for (const box of this.boxes) {
        copy.push({
          item: box.item,
          message: box.message,
          isMessageVisible: box.isMessageVisible,
          newOption: box.newOption
        });
      }
    }
    this.boxes = [];
    for (const item of this.selectedItems) {
      const box = copy.find(boxCopy => boxCopy.item.id === item.id);
      if (box) {
        this.boxes.push({
          item,
          message: box.message,
          isMessageVisible: box.isMessageVisible,
          newOption: box.newOption
        });
      } else {
        let isNewOption: boolean;
        if (this.originalItems.find(originalItem => originalItem.id === item.id)) {
          isNewOption = false;
        } else {
          isNewOption = true;
        }
        this.boxes.push({
          item,
          message: '',
          isMessageVisible: false,
          newOption: isNewOption
        });
      }
    }
    this.emitBoxMessages();
  }

  updateAvailableItems() {
    this.availableItems = [];
    for (const item of this.allSelectableItems) {
      // Does not contain
      if (!this.selectedItems.find(selectedItem => item.id === selectedItem.id)) {
        this.availableItems.push(item);
      }
    }
  }

  addItem(item: any) {
    this.selectedItems.push(item);
    this.updateSelections();
  }

  removeItem(item: any): void {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (item.id === this.selectedItems[i].id) {
        this.selectedItems.splice(i, 1);
      }
    }
    this.updateSelections();
  }

  getSelectedItems(): AlertedItem[] {
    const alertedObjects: AlertedItem[] = [];
    for (const item of this.selectedItems) {
      const object = { id: item.id };
      alertedObjects.push(object);
    }
    return alertedObjects;
  }

  onSelectChange(event) {
    this.addItem(event.value);
  }

  addMessage(box: any, i: number) {
    this.boxes[i].isMessageVisible = true;
  }

  onTextChange(event, i: number) {
    this.boxes[i].message = event.target.value;
    this.emitBoxMessages();
  }

  emitBoxMessages() {
    const messages = [];
    for (const box of this.boxes) {
      if (box.message.length > 0) {
        messages.push({ id: box.item.id, alert: { sender: this.sender, message: box.message } });
      } else {
        messages.push({ id: box.item.id });
      }
    }
    this.boxesChange.emit(messages);
  }

  closeMessage(i: number) {
    this.boxes[i].isMessageVisible = false;
  }

  sendEvent(item, keyValue) {
    const out = {
      key: keyValue,
      data: item,
    };
    this.action.emit(out);
  }

  showIcon(item) {
    return item.completionStatus.completed;
  }

  calcButtonColor(completed: boolean) {
    return completed ? "warn" : "primary";
  }
}
