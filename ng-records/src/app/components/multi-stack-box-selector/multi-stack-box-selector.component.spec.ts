import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiStackBoxSelectorComponent } from './multi-stack-box-selector.component';

describe('MultiStackBoxSelectorComponent', () => {
  let component: MultiStackBoxSelectorComponent;
  let fixture: ComponentFixture<MultiStackBoxSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiStackBoxSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiStackBoxSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
