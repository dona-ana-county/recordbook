import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Staff } from 'src/app/models/user';
import { StorageService } from 'src/app/services/storage.service';
import { ToastrService } from 'ngx-toastr';
import { TextService } from 'src/app/services/text.service';
import { environment } from 'src/environments/environment';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() page: string;
  user = false;
  staff: Staff;
  selectedOption: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private toastr: ToastrService,
    public text: TextService,
    public constantService: ConstantService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.user = this.staff != null;
    if (this.user) {
      localStorage.setItem('language', 'en');
    }
    this.selectedOption = localStorage.getItem('language');
  }

  selectOption(selectedOption: string) {
    this.text.selectLanguage(selectedOption);
    this.selectedOption = selectedOption;
  }

  changeRoute(route: string) {
    this.router.navigate([route]);
  }

  logOut() {
    this.authService.logout().subscribe(res => {
      if (res.success === 1) {
        this.storageService.clearStaffData();
        this.router.navigate(['home']);
        this.toastr.success(res.msg, this.text.get('success'), { timeOut: 5000 });
      }
    });
  }
}
