export interface SteeleHeader {
  name: string;
  key: string;
  type?: string;
  label?: string;
  hasAction?: boolean;
  hasLink?: boolean;
  isEditable?: boolean;
  isSortable?: boolean;
  sortKey?: string;
  isBoolean?: any[];
}
