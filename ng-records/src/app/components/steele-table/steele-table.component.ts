import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { SteeleHeader } from "./header";
import { TableOut } from "./output";
import { PageEvent } from "@angular/material/paginator";
import { TextService } from "src/app/services/text.service";

@Component({
  // tslint:disable-next-line: component-selector
  selector: "steele-table",
  templateUrl: "./steele-table.component.html",
  styleUrls: ["./steele-table.component.scss"],
})
export class SteeleTableComponent implements OnInit, OnChanges {
  @Input() headers: SteeleHeader[];
  @Input() rows: object[];
  @Input() pagination: boolean;
  @Input() pageSize: number;
  displayRows: object[];
  pageIndex = 0;
  rowStart = 0;
  totalItems = 0;

  @Output() action = new EventEmitter();
  @Output() sortAction = new EventEmitter();
  pageEvent: PageEvent;

  constructor(public text: TextService) {}

  ngOnInit() {
    this.resetTable();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ("rows" in changes) {
      this.resetTable();
    }
  }

  onPaginateChange(event) {
    this.pageIndex = event.pageIndex;
    this.rowStart = this.pageIndex * this.pageSize;
    this.calculateRowEnd();
    this.populateViewRows();
  }

  resetTable() {
    if (this.rows) {
      this.totalItems = this.rows.length;
      this.calculateRowEnd();
      this.displayRows = this.rows;
      if (this.pagination) {
        this.populateViewRows();
      }
    }
  }

  populateViewRows() {
    const newArr = [];
    for (
      let i = this.rowStart;
      i < this.rowStart + this.pageSize && i < this.rows.length;
      i++
    ) {
      newArr.push(this.rows[i]);
    }
    this.displayRows = newArr;
  }

  calculateRowEnd(): void {
    // Not entirely sure if we need can do without this.
    // if (this.pageSize > this.rows.length) {
    //   this.pageSize = this.rows.length;
    // }
  }

  sendEvent(row, keyValue) {
    const out: TableOut = {
      key: keyValue,
      data: row,
    };
    this.action.emit(out);
  }

  sendSortEvent(sortKey: string, sortDirection: string) {
    const out = {
      sortKey,
      sortDirection
    };

    this.sortAction.emit(out);
  }

  getStatusId(value: string) {
    return value.toLowerCase();
  }

  calcHeaderClasses(header: SteeleHeader) {
    return {
      col: true,
      "table-headers": true,
      narrowCol:
        (header.type && header.key !== "title" && header.key !== "comment") ||
        header.key === "checkmark",
      narrowMedCol: header.key === "completed",
      descCol: header.key === "title",
      htmlDescCol: header.type === "html",
      medCol: header.key === "requestorName" || header.key === "department",
      pointerCursor: header.isSortable,
    };
  }

  calcSortClasses(header: SteeleHeader) {
    return {
      asc: header.key !== 'closingDate',
      desc: header.key === 'closingDate'
    };
  }

  calcRowClasses(row) {
    return {
      "row": true,
      "table-rows":true,
      "fade-in": true,
      "completed": row.completed
  };
}

  calcItemClasses(header: SteeleHeader) {
    return {
      col: true,
      narrowCol:
        (header.type && header.key !== "title" && header.key !== "comment") ||
        header.key === "checkmark",
      narrowMedCol: header.key === "completed",
      descCol: header.key === "title",
      htmlDescCol: header.type === "html",
      medCol: header.key === "requestorName" || header.key === "department",
    };
  }

  calcButtonColor(row) {
    return row.completed.value ? "warn" : "primary";
  }

  showButton(row) {
    return row.isAssignedMan;
  }

  showIcon(row) {
    return row.completed.value && row.isAssignedMan;
  }
}
