import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SteeleTableComponent } from './steele-table.component';

describe('SteeleTableComponent', () => {
  let component: SteeleTableComponent;
  let fixture: ComponentFixture<SteeleTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SteeleTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SteeleTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
