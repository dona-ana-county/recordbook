import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Request, StaffRequest } from 'src/app/models/request';
import { RequestService } from 'src/app/services/request.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { AuthService } from 'src/app/services/auth.service';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import {
  Requestor,
  ContactInfo,
  ContactType,
  Staff
} from 'src/app/models/user';
import { Resource, ReleaseType } from 'src/app/models/content';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { UploadDocumentDialogComponent } from 'src/app/dialogs/upload-document-dialog/upload-document-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { ContentService } from 'src/app/services/content.service';
import { ToastrService } from 'ngx-toastr';
import { AlertedItem } from 'src/app/components/multi-stack-box-selector/multi-stack-box-selector.component';
import { textTransform } from 'src/utils/textTransform';
import { validateAdmin } from 'src/utils/validateStaff';
import { ValidateState } from 'src/utils/validateState';
import { serializeDate } from 'src/utils/date';

@Component({
  selector: 'app-staff-new-request',
  templateUrl: './staff-new-request.component.html',
  styleUrls: ['./staff-new-request.component.scss']
})
export class StaffNewRequestComponent implements OnInit {
  title: string;
  description: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  fax: string;
  address: string;
  address2: string;
  city: string;
  state: string;
  zip: string;
  createDate: Date;
  preferredContact: ContactType;

  firstFormGroup: FormGroup;
  firstStepComplete = false;
  secondFormGroup: FormGroup;
  secondStepComplete = false;

  departmentAssignmentGroup: FormGroup;
  thirdStepComplete = false;

  fourthFormGroup: FormGroup;
  fourthStepComplete = false;

  contactTypes: ContactType[];
  releaseTypes: ReleaseType[];
  selectedType: string;

  isOptional = false;
  submitted = false;
  inProcess = false;
  loading = false;
  assignDepartment = false;
  isAdmin: boolean;

  backendValidationError = false;

  @Input() page: string;
  @ViewChild('stepper') stepper: MatStepper;
  user = false;
  staff: Staff;
  percent = 0;

  primaryDepartment: Department;
  secondaryDepartments: Department[] = [];

  userDepartment: Department;

  departments: Department[] = [];

  headers: SteeleHeader[] = [
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];
  fileDataList: any[];

  primaryControl = new FormControl('', [Validators.required]);
  selectorItems: Department[];
  departmentMessages: any[] = [];

  secondaryAssignments: AlertedItem[];
  radioSelection: string;
  radioOptions: string[] = [];
  phoneValidator = Validators.pattern('[0-9]{3}-[0-9]{3}-[0-9]{4}');

  constructor(
    public dialog: MatDialog,
    private requestService: RequestService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private storageService: StorageService,
    private deptService: DepartmentService,
    private authService: AuthService,
    public text: TextService,
    private contentService: ContentService,
    private toastrService: ToastrService,
    private constantService: ConstantService
  ) { }

  ngOnInit() {
    this.contactTypes = this.constantService.contactTypes;
    this.selectedType = this.constantService.getContactType('EML').code;
    this.releaseTypes = this.constantService.releaseTypes;

    this.fileDataList = [];
    this.firstFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      createDate: ['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email],
      address: ['', Validators.required],
      address2: [''],
      city: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
      state: ['New Mexico', [Validators.required, ValidateState]],
      phone: ['', [Validators.required, this.phoneValidator]],
      fax: ['', this.phoneValidator],
      preferredContact: ['', Validators.required]
    });

    this.departmentAssignmentGroup = this._formBuilder.group({});

    this.staff = this.storageService.getStaff();
    this.isAdmin = validateAdmin(this.staff);

    this.deptService
      .getDepartmentByStaff(this.staff.id)
      .subscribe((res: Department) => {
        this.userDepartment = res;
        this.radioOptions.push(this.text.get('assignment_queue'));
        this.radioOptions.push(this.userDepartment.name);
        this.radioSelection = this.text.get('assignment_queue');
      });
  }

  firstFormStepperNext(formGroup: FormGroup): void {
    if (formGroup.valid) {
      // all validators passed
      this.title = formGroup.controls.title.value;
      this.description = textTransform(formGroup.controls.description.value);
      this.createDate = formGroup.controls.createDate.value;
      this.firstFormGroup = formGroup;
    }
  }

  secondFormStepperNext(): void {
    if (this.secondFormGroup.valid) {
      // all validators passed
      const keys = Object.keys(this.secondFormGroup.controls);
      for (const key of keys) {
        this[key] = this.secondFormGroup.controls[key].value; // saves all variables dynamically
      }
    }

    return;
  }
  departmentAssignmentNext(data: any): void {
    if (data.assignDepartment) {
      this.primaryDepartment = data.primaryDepartment;
      this.secondaryDepartments = data.secondaryDepartments;
      this.secondaryAssignments = data.secondaryAssignments;
    } else {
      this.primaryDepartment = null;
      this.secondaryDepartments = [];
      this.secondaryAssignments = [];
    }
    this.assignDepartment = data.assignDepartment;
  }

  onRadioChange(event) {
    if (event.value === this.text.get('assignment_queue')) {
      this.primaryDepartment = null;
    } else {
      this.primaryDepartment = this.userDepartment;
    }
  }

  submitRequest() {
    this.inProcess = true;
    // primary should be what the staff member selects
    const contactInfo: ContactInfo = {
      phone: this.phone,
      address1: this.address,
      address2: this.address2,
      city: this.city,
      state: this.state,
      postalCode: this.zip
    };
    if (this.email) {
      contactInfo.email = this.email;
    }
    if (this.fax) {
      contactInfo.fax = this.fax;
    }
    const newRequestor: Requestor = {
      id: undefined,
      primaryContact: this.constantService.getContactType(
        this.preferredContact.code
      ),
      firstName: this.firstName,
      lastName: this.lastName,
      contactInfo
    };

    const newRequest: Request = {
      title: this.title,
      description: this.description,
      requestor: newRequestor,
      primaryDepartment: this.primaryDepartment,
      secondaryDepartments: this.secondaryDepartments,
      createDate: serializeDate(this.createDate)
    };
    const staffRequest = {} as StaffRequest;
    staffRequest.request = newRequest;
    staffRequest.assignments = this.secondaryAssignments;

    this.requestService.newStaffRequest(staffRequest).subscribe(response => {
      if (response.request) {
        this.backendValidationError = false;
        if (this.fileDataList.length > 0) {
          this.submitFiles(response.request.id);
        } else {
          this.percent = 100;
          this.submitted = true;
        }
      } else {
        this.inProcess = false;
        if (response.reqError && response.reqError.error) {
          this.backendValidationError = true;
        }
      }
    });
  }

  changeRoute() {
    this.router.navigate(['dashboard']);
  }

  onSelectionChange(event) {
    if (this.backendValidationError) {
      this.backendValidationError = false;
    }
  }

  openUploadDialog() {
    const dialogData: any = {
      basic: true
    };

    const dialogRef = this.dialog.open(UploadDocumentDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      const data = {
        fileData: result.file,
        title: result.title,
        status: 'Waiting',
        progress: 0
      };
      this.fileDataList.push(data);
    });
  }

  submitFiles(requestId: string) {
    for (const data of this.fileDataList) {
      this.submitFile(requestId, data);
    }
  }

  submitFile(requestId: string, fileData: any) {
    const formData = new FormData();
    formData.append('file', fileData.fileData);
    const staff: Staff = this.storageService.getStaff();
    staff.token = undefined;
    staff.groups = undefined;
    const resource: Resource = {
      title: fileData.title,
      creator: staff,
      needsReview: false,
      // releaseType: this.constantService.getReleaseType('DOC'),
      releaseType: { code: 'DOC', value: 'Request Document' },
      underReview: false,
      message: null,
      editable: false,
      description: null
    };
    formData.append('resource', JSON.stringify(resource));
    fileData.status = 'Uploading';
    this.contentService.uploadRequestFile(requestId, formData).subscribe(res => {
      if (res.progress) {
        fileData.progress = res.progress;
        if (fileData.progress === 100) {
          fileData.status = this.text.get('finishing_up');
        }
      }
      if (res.response) {
        const response = res.response;
        if (response.error) {
          return;
        }
        fileData.status = 'Done';
        if (this.allFilesDone()) {
          this.submitted = true;
        }
      }
    });
  }

  allFilesDone(): boolean {
    for (const f of this.fileDataList) {
      if (f.status !== 'Done') {
        return false;
      }
    }
    return true;
  }

  tableEvent(event) { }

  getDepartment(id: string): Department {
    for (const dept of this.departments) {
      if (dept.id === id) {
        return dept;
      }
    }
  }

  updateSelectorItems() {
    this.selectorItems = [];
    for (const dept of this.departments) {
      if (dept !== this.primaryDepartment) {
        this.selectorItems.push(dept);
      }
    }
  }

  onPrimaryDepartmentChange(event) {
    this.primaryDepartment = event.value;
    this.updateSelectorItems();
  }

  onBoxesChange(event) {
    this.secondaryAssignments = event;
  }
}
