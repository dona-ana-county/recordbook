import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffNewRequestComponent } from './staff-new-request.component';

describe('StaffNewRequestComponent', () => {
  let component: StaffNewRequestComponent;
  let fixture: ComponentFixture<StaffNewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffNewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffNewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
