import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentAssignmentStepComponent } from './department-assignment-step.component';

describe('DepartmentAssignmentStepComponent', () => {
  let component: DepartmentAssignmentStepComponent;
  let fixture: ComponentFixture<DepartmentAssignmentStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentAssignmentStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentAssignmentStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
