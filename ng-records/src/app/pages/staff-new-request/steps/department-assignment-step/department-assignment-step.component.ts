import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { TextService } from 'src/app/services/text.service';
import { Department } from 'src/app/models/department';
import { AuthService } from 'src/app/services/auth.service';
import { Staff } from 'src/app/models/user';
import { StorageService } from 'src/app/services/storage.service';
import { DepartmentService } from 'src/app/services/department.service';
import { AlertedItem } from 'src/app/components/multi-stack-box-selector/multi-stack-box-selector.component';
import { validateAdmin } from 'src/utils/validateStaff';

@Component({
  selector: 'app-department-assignment-step',
  templateUrl: './department-assignment-step.component.html',
  styleUrls: ['./department-assignment-step.component.scss']
})
export class DepartmentAssignmentStepComponent implements OnInit {
  departmentFormGroup: FormGroup;
  isAdmin: boolean;
  staff: Staff;
  userDepartment: Department;
  departments: Department[] = [];
  loading = false;
  secondaryAssignments: AlertedItem[];
  radioSelection: string;
  radioOptions: string[] = [];
  selectorItems: Department[];
  primaryControl = new FormControl('', [Validators.required]);

  @Output() stepperNext: EventEmitter<any> = new EventEmitter<any>();
  @Input() formGroup: FormGroup;
  @Input() primaryDepartment: Department;
  @Input() secondaryDepartments: Department[];
  @Input() assignDepartment: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    public text: TextService,
    private authService: AuthService,
    private storageService: StorageService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.isAdmin = validateAdmin(this.staff);

    this.departmentService.getDepartments().subscribe((res: Department[]) => {
      this.departments = res;

      this.loading = true;
    });

    this.departmentService
      .getDepartmentByStaff(this.staff.id)
      .subscribe((res: Department) => {
        this.userDepartment = res;
        this.radioOptions.push(this.text.get('assignment_queue'));
        this.radioOptions.push(this.userDepartment.name);
        this.radioSelection = this.text.get('assignment_queue');
      });
  }

  toggleDeptAssign() {
    this.assignDepartment = !this.assignDepartment;
    if (this.assignDepartment) {
      // need to add primary department validator;
      this.formGroup.addControl(
        'primaryDept',
        new FormControl('', Validators.required)
      );
      this.selectorItems = null;
      this.primaryDepartment = null;
      this.secondaryDepartments = [];
      this.secondaryAssignments = [];
    } else {
      this.formGroup.removeControl('primaryDept');
    }
  }

  onPrimaryDepartmentChange(event) {
    this.primaryDepartment = event.value;
    this.updateSelectorItems();
  }

  updateSelectorItems() {
    this.selectorItems = [];
    for (const dept of this.departments) {
      if (dept !== this.primaryDepartment) {
        this.selectorItems.push(dept);
      }
    }
  }

  onRadioChange(event) {
    if (event.value === this.text.get('assignment_queue')) {
      this.primaryDepartment = null;
      this.assignDepartment = false;
    } else {
      this.primaryDepartment = this.userDepartment;
      this.assignDepartment = true;
    }
  }

  onBoxesChange(event) {
    this.secondaryAssignments = event;
  }

  onNotify(): void {
    let data = {};

    if (this.assignDepartment && this.formGroup.valid) {
      data = {
        primaryDepartment: this.primaryDepartment,
        secondaryDepartments: this.secondaryDepartments,
        assignDepartment: this.assignDepartment,
        secondaryAssignments: this.secondaryAssignments
      };
      this.stepperNext.emit(data);
    } else if (!this.assignDepartment) {
      data = { assignDepartment: this.assignDepartment };
      this.stepperNext.emit(data);
    }
    return;
  }
}
