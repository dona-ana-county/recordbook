import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-about-request',
  templateUrl: './about-request.component.html',
  styleUrls: ['./about-request.component.scss']
})
export class AboutRequestComponent implements OnInit {
  @Input() formGroup: FormGroup;
  createDate: Date;
  today: Date;

  @Output() stepperNext: EventEmitter<FormGroup> = new EventEmitter<
    FormGroup
  >();
  constructor(private _formBuilder: FormBuilder, public text: TextService) {}

  ngOnInit() {
    this.createDate = new Date();
    this.today = new Date();
  }

  onNotify(): void {
    this.stepperNext.emit(this.formGroup);
  }
}
