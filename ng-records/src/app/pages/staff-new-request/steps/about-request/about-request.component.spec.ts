import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutRequestComponent } from './about-request.component';

describe('AboutRequestComponent', () => {
  let component: AboutRequestComponent;
  let fixture: ComponentFixture<AboutRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
