import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { TextService } from 'src/app/services/text.service';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl,
  AbstractControl
} from '@angular/forms';
import { Observable, from } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ContactType } from 'src/app/models/user';
import stateJSON from 'src/utils/stateList.json';
import { ConstantService } from 'src/app/services/constant.service';
import { State } from 'src/app/models/constant';

@Component({
  selector: 'app-requestor-information',
  templateUrl: './requestor-information.component.html',
  styleUrls: ['./requestor-information.component.scss']
})
export class RequestorInformationComponent implements OnInit {
  secondFormGroup: FormGroup;

  filteredOptions: Observable<string[]>;
  states: string[] = [];
  stateObjects: State[] = stateJSON;

  contactTypes: ContactType[];
  selectedType: ContactType;
  phoneValidator = Validators.pattern('[0-9]{3}-[0-9]{3}-[0-9]{4}');

  @Input() formGroup: FormGroup;

  @Output() stepperNext: EventEmitter<FormGroup> = new EventEmitter<
    FormGroup
  >();

  constructor(
    public text: TextService,
    private _formBuilder: FormBuilder,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    for (const state of stateJSON) {
      this.states.push(state.name);
    }

    this.contactTypes = this.constantService.contactTypes;
    this.selectedType = this.constantService.getContactType('EML');

    // this.filteredOptions = this.formGroup.controls.state.valueChanges.pipe(
    //   startWith(""),
    //   map(value => (typeof value === "string" ? value : value)),
    //   map(name => (name ? this._filter(name) : this.states.slice()))
    // );
  }

  // private _filter(name: string): any[] {
  //   const filterValue = name.toLowerCase();

  //   return this.states.filter(
  //     option => option.toLowerCase().indexOf(filterValue) === 0
  //   );
  // }

  // displayFn(state?: string): string | undefined {
  //   return state ? state : undefined;
  // }

  contactChanged(event) {
    if (event.value === 'Email') {
      this.formGroup.controls.email.setValidators([
        Validators.required,
        Validators.email
      ]);
      this.formGroup.controls.fax.setValidators([this.phoneValidator]);
    } else if (event.value === 'Phone') {
      this.formGroup.controls.fax.setValidators([this.phoneValidator]);
      this.formGroup.controls.email.setValidators([Validators.email]);
    } else if (event.value === 'Fax') {
      this.formGroup.controls.fax.setValidators([
        Validators.required,
        this.phoneValidator
      ]);
      this.formGroup.controls.email.setValidators([Validators.email]);
    } else if (event.value === 'Address') {
      this.formGroup.controls.fax.setValidators([this.phoneValidator]);
      this.formGroup.controls.email.setValidators([Validators.email]);
    }
  }

  onNotify(): void {
    this.stepperNext.emit(this.formGroup);
  }

  onKeyDownPhone(e) {
    this.formatControl(e, this.formGroup.controls.phone);
  }

  // formatState() {
  //   let stateVal = this.formGroup.controls.state.value;
  //   if (stateVal.length === 2) {
  //     // finds full name of state
  //     stateVal = this.convertStateAbbreviation(stateVal);
  //   } else {
  //     stateVal = stateVal.replace(/(?:^|\s)\S/g, (a: string) =>
  //       a.toUpperCase()
  //     );
  //   }
  //   this.formGroup.controls.state.setValue(stateVal);
  // }

  // convertStateAbbreviation(abbreviation: string) {
  //   for (const state of this.stateObjects) {
  //     if (state.abbreviation === abbreviation.toUpperCase()) {
  //       return state.name;
  //     }
  //   }
  //   return abbreviation;
  // }

  onKeyDownFax(e) {
    this.formatControl(e, this.formGroup.controls.fax);
  }

  formatControl(e: any, formControl: AbstractControl) {
    // Prevent non-digit input.
    if (
      // Allow: Delete, Backspace, Tab, Escape, Enter
      [46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
      (e.keyCode === 65 && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.keyCode === 67 && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.keyCode === 86 && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.keyCode === 88 && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.keyCode === 65 && e.metaKey === true) || // Cmd+A (Mac)
      (e.keyCode === 67 && e.metaKey === true) || // Cmd+C (Mac)
      (e.keyCode === 86 && e.metaKey === true) || // Cmd+V (Mac)
      (e.keyCode === 88 && e.metaKey === true) || // Cmd+X (Mac)
      (e.keyCode >= 35 && e.keyCode <= 39) // Home, End, Left, Right
    ) {
      return; // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }

    const key = e.charCode || e.keyCode || 0;
    if (key !== 8 && key !== 9) {
      if (formControl.value.length === 3) {
        formControl.setValue(formControl.value + '-');
      }
      if (formControl.value.length === 7) {
        formControl.setValue(formControl.value + '-');
      }
    }
  }
}
