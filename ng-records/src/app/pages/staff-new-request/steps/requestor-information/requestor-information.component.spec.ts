import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorInformationComponent } from './requestor-information.component';

describe('RequestorInformationComponent', () => {
  let component: RequestorInformationComponent;
  let fixture: ComponentFixture<RequestorInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
