import { Component, OnInit } from '@angular/core';
import { GovernmentEntity } from 'src/app/models/constant';
import { ConstantService } from 'src/app/services/constant.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateGovernmentEntityDialogComponent } from 'src/app/dialogs/super-dialogs/create-government-entity-dialog/create-government-entity-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { UpdateGovernmentEntityDialogComponent } from 'src/app/dialogs/super-dialogs/update-government-entity-dialog/update-government-entity-dialog.component';
import { TextService } from 'src/app/services/text.service';
import { SuperService } from 'src/app/services/super.service';

@Component({
  selector: 'app-super-government-entities-management',
  templateUrl: './super-government-entities-management.component.html',
  styleUrls: ['./super-government-entities-management.component.scss']
})
export class SuperGovernmentEntitiesManagementComponent implements OnInit {
  governmentEntities: GovernmentEntity[];

  constructor(
    private constantService: ConstantService,
    public dialog: MatDialog,
    private toastrService: ToastrService,
    public text: TextService,
    private superService: SuperService
  ) {}

  ngOnInit() {
    this.constantService.getGovernmentEntities().subscribe(res => {
      this.governmentEntities = res;
    });
  }

  openCreateGovernmentEntityDialog() {
    const dialogData: any = {};
    const dialogRef = this.dialog.open(CreateGovernmentEntityDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const entity: GovernmentEntity = {
          name: result.name,
          email: result.email
        };
        this.superService.newGovernmentEntity(entity).subscribe(res => {
          if (res && res.id) {
            this.toastrService.success('New Government Entity Added', this.text.get('success'), {
              timeOut: 3000
            });
            this.governmentEntities.push(res);
          }
        });
      }
    });
  }

  openUpdateGovernmentEntityDialog(entity: GovernmentEntity) {
    const dialogData: any = {
      governmentEntity: entity
    };

    const dialogRef = this.dialog.open(UpdateGovernmentEntityDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.delete) {
          this.superService.deleteGovernmentEntity(entity).subscribe(res => {
            if (res && res.id) {
              this.toastrService.success(entity.name + ' Deleted', this.text.get('success'), {
                timeOut: 3000
              });
              for (let i = 0; i < this.governmentEntities.length; i++) {
                if (this.governmentEntities[i].id === entity.id) {
                  this.governmentEntities.splice(i, 1);
                }
              }
            }
          });
          return;
        }

        const updatedEntity: GovernmentEntity = result.governmentEntity;

        this.superService.updateGovernmentEntity(updatedEntity).subscribe(res => {
          if (res && res.id) {
            this.toastrService.success(entity.name + ' Updated', this.text.get('success'), {
              timeOut: 3000
            });
          }
        });
      }
    });
  }
}
