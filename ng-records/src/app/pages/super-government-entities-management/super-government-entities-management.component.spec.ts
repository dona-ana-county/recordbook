import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperGovernmentEntitiesManagementComponent } from './super-government-entities-management.component';

describe('SuperGovernmentEntitiesManagementComponent', () => {
  let component: SuperGovernmentEntitiesManagementComponent;
  let fixture: ComponentFixture<SuperGovernmentEntitiesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperGovernmentEntitiesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperGovernmentEntitiesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
