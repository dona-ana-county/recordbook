import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperUserManagementComponent } from './super-user-management.component';

describe('SuperUserManagementComponent', () => {
  let component: SuperUserManagementComponent;
  let fixture: ComponentFixture<SuperUserManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperUserManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperUserManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
