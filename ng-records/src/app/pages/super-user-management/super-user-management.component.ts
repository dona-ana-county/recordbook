import { Component, OnInit } from '@angular/core';
import { Staff } from 'src/app/models/user';
import { MatDialog } from '@angular/material/dialog';
import { UpdateStaffDialogComponent } from 'src/app/dialogs/super-dialogs/update-staff-dialog/update-staff-dialog.component';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { CreateStaffDialogComponent } from 'src/app/dialogs/super-dialogs/create-staff-dialog/create-staff-dialog.component';
import { TextService } from 'src/app/services/text.service';
import { SuperService } from 'src/app/services/super.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-super-user-management',
  templateUrl: './super-user-management.component.html',
  styleUrls: ['./super-user-management.component.scss'],
})
export class SuperUserManagementComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private departmentService: DepartmentService,
    public text: TextService,
    private superService: SuperService,
    private toastr: ToastrService,
  ) {}

  users: Staff[] = [];

  departments: Department[];
  selectedUser: Staff;
  userDept: Department;
  departmentMap: Map<string, Department>;

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.superService.getAllStaff().subscribe((users: any) => {
      this.users = users;
      this.departmentMap = new Map();
      this.departmentService.getAllFullDepartments().subscribe((res) => {
        this.departments = res;
        for (const dept of this.departments) {
          for (const staff of dept.staff) {
            this.departmentMap.set(staff.id, dept);
          }
        }
        for (const user of this.users) {
          user.department = this.departmentMap.get(user.id);
        }
      });
    });
  }

  openAddUserDialog() {
    const dialogData: any = {
      departments: this.departments,
    };

    const dialogRef = this.dialog.open(CreateStaffDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.superService.newStaff(result.staff, result.department).subscribe((res) => {
          const newStaff: Staff = res;
          for (const dept of this.departments) {
            this.departmentMap.set(newStaff.id, dept);
          }
          newStaff.department = this.departmentMap.get(newStaff.id);
          this.refresh();
          this.toastr.success(this.text.get('user_created'), this.text.get('success'), {
            timeOut: 4000,
          });
        });
      }
    });
  }

  editUser(userObj: Staff) {
    this.selectedUser = userObj;

    this.departmentService.getDepartmentByStaff(userObj.id).subscribe((res) => {
      this.userDept = res;
      this.openEditUserDialog();
    });
  }

  openEditUserDialog() {
    const dialogData: any = {
      staff: this.selectedUser,
      currentStaffDepartment: this.userDept,
      departments: this.departments,
    };
    const dialogRef = this.dialog.open(UpdateStaffDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        return;
      }
      if (result.delete) {
        const staff: Staff = result.delete;
        this.superService.deleteStaff(staff).subscribe((res) => {
          if (res) {
            this.toastr.success(this.text.get('user_deleted'), this.text.get('success'), {
              timeOut: 4000,
            });
            this.refresh();
          }
        });
      } else {
        const staff: Staff = result.staff;
        const dept: Department = {
          id: result.department.id,
          name: result.department.name,
        };

        this.superService.updateStaff(staff, dept).subscribe((res) => {
          if (res) {
            this.toastr.success(this.text.get('user_updated'), this.text.get('success'), {
              timeOut: 4000,
            });
            Object.assign(this.selectedUser, staff);
            this.selectedUser.department = dept;
            this.refresh();
          }
        });
      }
    });
  }
}
