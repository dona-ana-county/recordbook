import { Component, OnInit } from '@angular/core';
import { Department } from 'src/app/models/department';
import { MatDialog } from '@angular/material/dialog';

import { DepartmentService } from 'src/app/services/department.service';
import { CreateDepartmentDialogComponent } from 'src/app/dialogs/super-dialogs/create-department-dialog/create-department-dialog.component';
import { Staff } from 'src/app/models/user';
import { TextService } from 'src/app/services/text.service';
import { UpdateDepartmentDialogComponent } from 'src/app/dialogs/super-dialogs/update-department-dialog/update-department-dialog.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-super-department-management',
  templateUrl: './super-department-management.component.html',
  styleUrls: ['./super-department-management.component.scss']
})
export class SuperDepartmentManagementComponent implements OnInit {
  loaded = false;
  departments: Department[];

  constructor(
    private departmentService: DepartmentService,
    public dialog: MatDialog,
    public text: TextService,
    public toastrSerivce: ToastrService
  ) {}

  ngOnInit() {
    this.departmentService.getAllFullDepartments().subscribe(res => {
      this.departments = res;
      this.loaded = true;
      console.log('all', this.departments);
    });
  }
  getTotalManagers(staff: Staff): number {
    let total = 0;
    staff.forEach((worker: Staff) => {
      if (worker.role.code === 'MAN') {
        total++;
      }
    });
    return total;
  }

  getTotalWorkers(staff: Staff): number {
    let total = 0;
    staff.forEach((worker: Staff) => {
      if (worker.role.code === 'WRK') {
        total++;
      }
    });
    return total;
  }
  openEditDepartmentDialog(department: Department) {
    const dialogRef = this.dialog.open(UpdateDepartmentDialogComponent, {
      width: '800px',
      data: department
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.toastrSerivce.success(
          'Department Name Updated',
          this.text.get('success'),
          {
            timeOut: 4000
          }
        );
      }
    });
  }

  openAddDepartmentDialog() {
    const dialogRef = this.dialog.open(CreateDepartmentDialogComponent, {
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(result => {
      const newDepartment = {} as Department;
      newDepartment.name = result.name;
      this.departmentService.newDepartment(newDepartment).subscribe(res => {
        const addedDepartment = res;
        addedDepartment.staff = [] as Staff[];
        this.departments.push(addedDepartment);
      });
    });
  }
}
