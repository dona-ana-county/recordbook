import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperDepartmentManagementComponent } from './super-department-management.component';

describe('SuperDepartmentManagementComponent', () => {
  let component: SuperDepartmentManagementComponent;
  let fixture: ComponentFixture<SuperDepartmentManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperDepartmentManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperDepartmentManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
