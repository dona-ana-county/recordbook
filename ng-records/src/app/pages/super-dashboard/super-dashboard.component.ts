import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-super-dashboard',
  templateUrl: './super-dashboard.component.html',
  styleUrls: ['./super-dashboard.component.scss']
})
export class SuperDashboardComponent implements OnInit {
  allDepartments: Department[];
  currentStaffDepartment: Department;

  constructor(
    public dialog: MatDialog,
    private departmentService: DepartmentService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.departmentService.getDepartments().subscribe(res => {
      this.allDepartments = res;
    });
  }
}
