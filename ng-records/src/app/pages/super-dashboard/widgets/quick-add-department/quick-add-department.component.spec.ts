import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickAddDepartmentComponent } from './quick-add-department.component';

describe('QuickAddDepartmentComponent', () => {
  let component: QuickAddDepartmentComponent;
  let fixture: ComponentFixture<QuickAddDepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickAddDepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickAddDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
