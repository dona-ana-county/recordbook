import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickAddUserComponent } from './quick-add-user.component';

describe('QuickAddUserComponent', () => {
  let component: QuickAddUserComponent;
  let fixture: ComponentFixture<QuickAddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickAddUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickAddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
