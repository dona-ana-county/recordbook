import { Component, OnInit } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { Router } from '@angular/router';
import { SearchQuery, Filter, RequestSearchResult } from 'src/app/models/search';
import { TextService } from 'src/app/services/text.service';
import { SearchService } from 'src/app/services/search.service';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { serializeDate } from 'src/utils/date';

@Component({
  selector: 'app-staff-search-request',
  templateUrl: './staff-search-request.component.html',
  styleUrls: ['./staff-search-request.component.scss'],
})
export class StaffSearchRequestComponent implements OnInit {
  defaultPerPage = 15;
  currentPage = 1;
  sortKey = 'create_date';
  sortDirection = 'desc';
  totalItems: number;
  totalPages: number;
  pageStart: number;
  pageEnd: number;

  requests: RequestSearchResult[];
  query: SearchQuery;
  allDepartments: Department[];

  search: string;
  department: string;
  status: string;
  extensionStatus: string;

  createFromDate: Date;
  createToDate: Date;
  dueFromDate: Date;
  dueToDate: Date;

  headers: SteeleHeader[] = [
    {
      name: 'id',
      key: 'requestId',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false,
      isSortable: true,
      sortKey: 'request_id',
    },
    {
      name: 'requestor',
      key: 'requestorName',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: true,
      sortKey: 'requestor_name',
    },
    {
      name: 'department',
      key: 'department',
      type: 'text',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: true,
      sortKey: 'department',
    },
    {
      name: 'due_date',
      key: 'dueDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: true,
      sortKey: 'due_date',
    },
    {
      name: 'extension_status',
      key: 'extensionStatus',
      type: 'lookup',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: false
    },
    {
      name: 'status',
      key: 'status',
      type: 'status',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: false
    },
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: true,
      sortKey: 'title',
    },
    {
      name: 'create_date',
      key: 'createDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false,
      isSortable: true,
      sortKey: 'create_date',
    }
  ];

  constructor(
    private searchService: SearchService,
    private router: Router,
    public text: TextService,
    private departmentService: DepartmentService,
  ) {}

  ngOnInit() {
    this.initQuery(this.defaultPerPage);
    this.sendQuery(this.currentPage, this.sortKey, this.sortDirection);
    this.departmentService.getDepartments().subscribe((res) => {
      this.allDepartments = res;
    });
  }

  initQuery(perPage: number): void {
    this.query = {} as SearchQuery;
    this.query.search = '';
    this.query.sort = '';
    this.query.filters = [];
    this.query.perPage = perPage;
  }

  sendQuery(pageNumber: number, sortKey, sortDirection) {
    this.query.page = pageNumber;
    this.query.sort = this.getSort(sortKey, sortDirection);

    this.searchService.staffRequestSearch(this.query).subscribe((res) => {
      this.requests = res.reqList;
      this.currentPage = this.query.page;
      this.sortKey = sortKey;
      this.sortDirection = sortDirection;
      this.totalItems = res.numItems;
      this.calcPagination();
    });
  }

  sortByColumn(event) {
    this.sendQuery(1, event.sortKey, event.sortDirection);
  }

  getSort(sortKey: string, sortDirection: string) {

    switch (sortKey) {
      case 'create_dt':
        const missingValueSort = sortDirection === 'desc' ? '_first' : '_last';
        return {"create_dt": {"order": sortDirection, "missing": missingValueSort}};

      case 'request_id':
        return {"_id": {"order": sortDirection}};

      case 'requestor_name':
        return {"requestor_name.raw": {"order": sortDirection}};

      case 'department':
        return {"department": {"order": sortDirection}};

      case 'due_date':
        return {"due_dt": {"numeric_type": "date", "order": sortDirection}};

      case 'title':
        return {"title.raw": {"order": sortDirection}};

      default:
        return {"create_dt": {"order": "desc", "missing": "_first"}};
    }
  }

  calcPagination() {
    const remainder = this.totalItems % this.defaultPerPage;
    const quotient = (this.totalItems - remainder) / this.defaultPerPage;
    this.totalPages = remainder === 0 ? quotient : quotient + 1;

    this.pageStart = 1 + ((this.currentPage - 1) * this.defaultPerPage);
    this.pageEnd = this.currentPage * this.defaultPerPage < this.totalItems ? this.currentPage * this.defaultPerPage : this.totalItems;
  }

  searchChange() {
    this.query.search = this.search.trim();
  }

  setFilters() {
    this.query.filters = [];
    if (this.status !== undefined) {
      const statusFilter = {} as Filter;
      statusFilter.type = 'by_value';
      statusFilter.field = 'status';
      statusFilter.value = this.status;
      this.query.filters.push(statusFilter);
    }
    if (this.extensionStatus !== undefined) {
      const extensionFilter = {} as Filter;
      extensionFilter.type = 'by_value';
      extensionFilter.field = 'extension_status';
      extensionFilter.value = this.extensionStatus;
      this.query.filters.push(extensionFilter);
    }
    if (this.department !== undefined) {
      const departmentFilter = {} as Filter;
      departmentFilter.type = 'by_value';
      departmentFilter.field = 'department';
      departmentFilter.value = this.department;
      this.query.filters.push(departmentFilter);
    }
    if (this.createFromDate !== undefined || this.createToDate !== undefined) {
      const createDateFilter = {} as Filter;
      createDateFilter.type = 'by_range';
      createDateFilter.field = 'create_dt';
      if (this.createFromDate !== undefined) {
        createDateFilter.from = serializeDate(this.createFromDate);
      }
      if (this.createToDate !== undefined) {
        createDateFilter.to = serializeDate(this.createToDate);
      }
      this.query.filters.push(createDateFilter);
    }
    if (this.dueFromDate !== undefined || this.dueToDate !== undefined) {
      const dueDateFilter = {} as Filter;
      dueDateFilter.type = 'by_range';
      dueDateFilter.field = 'due_dt';
      if (this.dueFromDate !== undefined) {
        dueDateFilter.from = serializeDate(this.dueFromDate);
      }
      if (this.dueToDate !== undefined) {
        dueDateFilter.to = serializeDate(this.dueToDate);
      }
      this.query.filters.push(dueDateFilter);
    }
    this.sendQuery(this.currentPage, this.sortKey, this.sortDirection);
  }

  clearCreateFrom() {
    this.createFromDate = undefined;
    this.setFilters();
  }

  clearCreateTo() {
    this.createToDate = undefined;
    this.setFilters();
  }

  clearDueDateTo() {
    this.dueToDate = undefined;
    this.setFilters();
  }

  clearDueDateFrom() {
    this.dueFromDate = undefined;
    this.setFilters();
  }

  clearSearch() {
    this.search = undefined;
    this.searchChange();
  }

  clearFilters() {
    this.initQuery(this.defaultPerPage);
    this.search = undefined;
    this.status = undefined;
    this.extensionStatus = undefined;
    this.department = undefined;
    this.createToDate = undefined;
    this.createFromDate = undefined;
    this.dueToDate = undefined;
    this.dueFromDate = undefined;
  }

  processEvent(event) {
    this.router.navigate(['requestManagement', event.data.requestId]);
  }

  exportResultsAsCSV() {
      this.searchService.exportSearchResults(this.query).subscribe((data) => {
        const blob = new Blob([data], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        const downloadLink = document.createElement('a'); 
        downloadLink.href = url;
        downloadLink.download = `search_results_${new Date().toISOString()}.csv`; 
        document.body.appendChild(downloadLink); 
        downloadLink.click(); 
        window.URL.revokeObjectURL(url); 
        downloadLink.remove(); 
      });
    }
  }
