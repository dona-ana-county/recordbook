import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffSearchRequestComponent } from './staff-search-request.component';

describe('StaffSearchRequestComponent', () => {
  let component: StaffSearchRequestComponent;
  let fixture: ComponentFixture<StaffSearchRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffSearchRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffSearchRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
