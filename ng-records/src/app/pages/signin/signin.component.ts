import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Staff } from 'src/app/models/user';
import { TextService } from 'src/app/services/text.service';
import { StorageService } from 'src/app/services/storage.service';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  username: string;
  password: string;
  usernameOrPassWrong: boolean;
  return: string;

  constructor(
    private auth: AuthService,
    private router: Router,
    public text: TextService,
    private route: ActivatedRoute,
    private storageService: StorageService,
    public constants: ConstantService
  ) {}

  ngOnInit() {
    this.usernameOrPassWrong = false;
    this.route.queryParams.subscribe(params => this.return = params.return || '/dashboard');
  }

  login() {
    if (this.username && this.password) {
      const credentials = {
        username: this.username,
        password: this.password
      };
      this.auth.login(credentials).subscribe(res => {
        if (res.success === 0) {
          this.usernameOrPassWrong = true;
        } else {
          const staff: Staff = res;
          this.storageService.setStaff(staff);
          if (staff.role.code === 'SUP') {
            this.router.navigate(['superUser']);
          } else {
            this.router.navigateByUrl(this.return);
          }
        }
      });
    }
  }

  onKeyEnter(event) {
    // user pressed enter
    if (event.keyCode === 13) {
      this.login();
    }
  }

  goHome() {
    this.router.navigate(['home']);
  }
}
