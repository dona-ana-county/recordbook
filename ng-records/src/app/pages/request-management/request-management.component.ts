import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Request } from 'src/app/models/request';
import { Staff } from 'src/app/models/user';
import { AnalyticsService } from 'src/app/services/analytics.service';
import { ConstantService } from 'src/app/services/constant.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';
import { validateAdmin, validateAssignedManager } from 'src/utils/validateStaff';


@Component({
  selector: 'app-request-management',
  templateUrl: './request-management.component.html',
  styleUrls: ['./request-management.component.scss']
})
export class RequestManagementComponent implements OnInit, OnDestroy {
  requestId: string;
  isAdmin = false;
  isWorker = false;
  isClerk = false;
  selectedIndex = 0;
  activeTab: string = null;

  isAssignedMan = false;
  hasEmail = false;

  staff: Staff;
  request: Request;

  subscriptions: Subscription[] = [];
  tabGroup: MatTabGroup;

  @ViewChild(MatTabGroup)
  set setTabGroup(tabGroup: MatTabGroup) {
    if (tabGroup) {
      this.tabGroup = tabGroup;
      this.processTabs();
    }
  }

  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private requestStore: RequestStoreService,
    public text: TextService,
    private constantService: ConstantService,
    private analyticsService: AnalyticsService
  ) {}

  ngOnInit() {
    this.requestId = null;
    this.staff = this.storageService.getStaff();

    this.constantService.getAllRoles().subscribe(res => {
      this.isWorker = this.staff.role === this.constantService.getRole('WRK');
    });

    this.route.params.subscribe(params => {
      this.requestId = params['requestId'];

      this.requestStore.initRequest(this.requestId, this.staff.id);

      this.subscriptions.push(
        this.requestStore.getRequest().subscribe((r: Request) => {
          this.request = r;
          this.validateStaff();

          if (this.request && this.request.requestor
            && this.request.requestor.primaryContact
            && this.request.requestor.primaryContact.code) {
            this.hasEmail = this.request.requestor.primaryContact.code ===  'EML';
          }
        })
      );
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
    this.requestStore.reset();
  }

  validateStaff() {
    this.isAdmin = validateAdmin(this.staff);
    this.isClerk = validateAdmin(this.staff);

    if (this.request && this.request.assignedStaff) {
      this.isAssignedMan = validateAssignedManager(
        this.staff.id,
        this.request.assignedStaff
      );
    }
  }

  isLoaded() {
    let loaded = true;
    if (!this.request || this.request === undefined || !this.request.status) {
      loaded = false;
    }
    return loaded;
  }

  requestReOpened(event) {
    if (event.resetTab) {
      this.selectedIndex = 0;
    }
  }

  getIndex(label: string): number {
    if (label === null) {
      return 0;
    }
    const tabs = this.tabGroup._tabs.toArray();
    for (let i = 0; i < tabs.length; i++) {
      const t = tabs[i];
      if (t.textLabel === label) {
        return i;
      }
    }
    return null;
  }

  processTabs() {
    const label = this.storageService.getLastTabIndex(this.requestId);
    let index = this.getIndex(label);
    this.route.fragment.subscribe({next: (fragment) => {
      if (fragment) {
        fragment = fragment.split('.', 1)[0];
        const i = this.getIndex(fragment);
        if (i !== null) {
          index = i;
        }
      }

      setTimeout( () => {
        this.selectedIndex  = index;
      }, 0);

    }});
  }

  tabChange(event) {
    this.storageService.setLastTabIndex(this.requestId, event.tab.textLabel);
    this.analyticsService.navigateAnchor(event.tab.textLabel);
    this.activeTab = event.tab.textLabel;
  }
}
