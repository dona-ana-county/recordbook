import { Component, Host, Input } from '@angular/core';
import { MatTab } from '@angular/material/tabs';

@Component({
  template: ''
})
export abstract class Tab {
  abstract onTabSelected(): void;

  @Input()
  set activeTab(name: string) {
    if (name === this.parent.textLabel) {
      this.onTabSelected();
    }
  }

  constructor(@Host() public parent: MatTab) {
  }
}
