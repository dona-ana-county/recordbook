import { Component, Host, OnDestroy, OnInit } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { Subscription } from 'rxjs';
import { Request } from 'src/app/models/request';
import { Staff } from 'src/app/models/user';
import { CommunicationService } from 'src/app/services/communication.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { StorageService } from 'src/app/services/storage.service';
import { Tab } from '../tab';

@Component({
  selector: 'app-alert-tab',
  templateUrl: './alert-tab.component.html',
  styleUrls: ['./alert-tab.component.scss'],
})
export class AlertTabComponent extends Tab implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  request: Request;
  staff: Staff;

  constructor(
    private communicationService: CommunicationService,
    private storageService: StorageService,
    private requestStore: RequestStoreService,
    @Host() parent: MatTab,
  ) {
    super(parent);
  }

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe(res => {
        this.request = res;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadAlerts();
  }
}
