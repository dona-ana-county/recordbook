import { Component, Host, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { Subscription } from 'rxjs';
import { SteeleTableComponent } from 'src/app/components/steele-table/steele-table.component';
import { Department } from 'src/app/models/department';
import { Request } from 'src/app/models/request';
import { Staff, User } from 'src/app/models/user';
import { ConstantService } from 'src/app/services/constant.service';
import { DepartmentService } from 'src/app/services/department.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { RequestService } from 'src/app/services/request.service';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';
import { SteeleHeader } from '../../../../components/steele-table/header';
import { Tab } from '../tab';



@Component({
  selector: 'app-worker-read-only-tab',
  templateUrl: './worker-read-only-tab.component.html',
  styleUrls: ['./worker-read-only-tab.component.scss']
})
export class WorkerReadOnlyTabComponent extends Tab implements OnInit, OnDestroy {
  headers: SteeleHeader[] = [
    {
      name: 'name',
      key: 'name'
    },
    {
      name: 'role',
      key: 'role'
    },
    {
      name: 'department',
      key: 'department'
    }
  ];

  subscriptions: Subscription[] = [];
  request: Request;
  user: Staff;
  departmentStaff: Staff[];
  assignedRequestStaff: Staff[];
  needsSaving = false;

  rows: any[] = [];
  selectorItems: any[];
  originalItems: any[];
  selectedItems: User[];
  changesMade = false;
  assignments: any[];

  @ViewChild(SteeleTableComponent, {static: true}) assignedWorkersTable: SteeleTableComponent;

  constructor(
    private storageService: StorageService,
    private departmentService: DepartmentService,
    private requestService: RequestService,
    private requestStore: RequestStoreService,
    public text: TextService,
    private constantService: ConstantService,
    @Host() parent: MatTab
  ) {
    super(parent);
  }

  ngOnInit() {
    this.user = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.makeRows();
      })
    );

    this.departmentService.getDepartmentByStaff(this.user.id).subscribe((res: Department) => {
      this.departmentStaff = res.staff;
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadAssignedStaff();
  }

  makeRows() {
    this.rows = [];
    for (const staff of this.request.assignedStaff) {
      this.rows.push({
        id: staff.id,
        name: staff.firstName + ' ' + staff.lastName,
        role: staff.role.value,
        department: this.findStaffDepartment(staff.id).name
      });
    }
    this.assignedWorkersTable.resetTable();
  }

  findStaffDepartment(staffId: string): Department {
    for (const dept of this.requestStore.getDepartments()) {
      for (const staff of dept.staff) {
        if (staff.id === staffId) {
          return { id: dept.id, name: dept.name };
        }
      }
    }
  }

  getStaff(staffId: string): Staff {
    return this.departmentStaff.find(x => {
      return x.id === staffId;
    });
  }

}

