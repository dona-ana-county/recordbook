import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerReadOnlyTabComponent } from './worker-read-only-tab.component';

describe('WorkerReadOnlyTabComponent', () => {
  let component: WorkerReadOnlyTabComponent;
  let fixture: ComponentFixture<WorkerReadOnlyTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerReadOnlyTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerReadOnlyTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
