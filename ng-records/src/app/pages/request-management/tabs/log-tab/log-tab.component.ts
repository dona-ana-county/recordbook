import { Component, Host, OnDestroy, OnInit } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { Subscription } from 'rxjs';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { Request } from 'src/app/models/request';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { Tab } from '../tab';

@Component({
  selector: 'app-log-tab',
  templateUrl: './log-tab.component.html',
  styleUrls: ['./log-tab.component.scss']
})
export class LogTabComponent extends Tab implements OnInit, OnDestroy {
  request: Request;
  subscriptions: Subscription[] = [];

  headers: SteeleHeader[] = [
    {
      name: 'event',
      key: 'eventCode',
      type: 'lookup',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'date',
      key: 'messageDate',
      type: 'datetime',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'message',
      key: 'message',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];

  constructor(private requestStore: RequestStoreService, @Host() parent: MatTab) {
    super(parent);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe(res => {
        this.request = res;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadLog();
  }

  tableAction(event) {}
}
