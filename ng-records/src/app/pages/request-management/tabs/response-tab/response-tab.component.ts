import { Component, Host, OnInit } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { AnalyticsService } from 'src/app/services/analytics.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { TextService } from 'src/app/services/text.service';
import { Tab } from '../tab';

@Component({
  selector: 'app-response-tab',
  templateUrl: './response-tab.component.html',
  styleUrls: ['./response-tab.component.scss']
})
export class ResponseTabComponent extends Tab implements OnInit {
  active = 'documents';

  constructor(
    public text: TextService,
    private requestStore: RequestStoreService,
    private analyticsService: AnalyticsService,
    private route: ActivatedRoute,
    @Host() parent: MatTab,
  ) {
    super(parent);
    this.route.fragment.subscribe({next: (fragment) => {
      if (fragment && fragment.indexOf('.') !== -1) {
        fragment = fragment.split('.', 2)[1];
        setTimeout(() => {
          this.showTab(fragment);
        }, 0);
      }
    }});
  }

  ngOnInit() {}

  onTabSelected(): void {
    this.requestStore.loadResources();
    this.requestStore.loadNotes();
    this.requestStore.loadLinks();
    this.requestStore.loadInstructions();
  }

  showTab(tab: string) {
    if (this.active !== tab) {
      this.active = tab;
      this.analyticsService.navigateAnchor(this.parent.textLabel + '.' + tab);
    }
  }
}
