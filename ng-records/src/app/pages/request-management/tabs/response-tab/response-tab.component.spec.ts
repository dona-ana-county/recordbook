import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponseTabComponent } from './response-tab.component';

describe('ResponseTabComponent', () => {
  let component: ResponseTabComponent;
  let fixture: ComponentFixture<ResponseTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponseTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
