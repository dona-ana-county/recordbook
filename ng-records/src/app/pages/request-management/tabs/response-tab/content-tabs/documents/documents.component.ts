import { Component, OnInit } from "@angular/core";
import { SteeleHeader } from "src/app/components/steele-table/header";
import { MatDialog } from "@angular/material/dialog";
import { UploadDocumentDialogComponent } from "src/app/dialogs/upload-document-dialog/upload-document-dialog.component";
import { ContentService } from "src/app/services/content.service";
import { Staff } from "src/app/models/user";
import { StorageService } from "src/app/services/storage.service";
import { ReleaseType, Resource } from "src/app/models/content";
import { Subscription } from "rxjs";
import { RequestStoreService } from "src/app/services/request-store.service";
import { Request } from "src/app/models/request";
import { ToastrService } from "ngx-toastr";
import { TextService } from "src/app/services/text.service";
import { UpdateResourceDialogComponent } from "src/app/dialogs/update-resource-dialog/update-resource-dialog.component";
import { DeleteContentDialogComponent } from "src/app/dialogs/delete-content-dialog/delete-content-dialog.component";
import { ConstantService } from "src/app/services/constant.service";
import {
  validateAssignedStaff,
  validatePDM,
  validateAdmin
} from "src/utils/validateStaff";

@Component({
  selector: "app-documents",
  templateUrl: "./documents.component.html",
  styleUrls: ["./documents.component.scss"]
})
export class DocumentsComponent implements OnInit {
  fileData: File = null;
  subscriptions: Subscription[] = [];
  request: Request;
  title: string;
  needsReview: boolean;
  releaseType: ReleaseType;
  message: string;
  description: string;

  isAssignedWorker = false;
  isAdmin = false;
  isPDM = false;
  staff: Staff;
  uploading = false;
  progress: number;
  uploadStatus: string;

  // headers: SteeleHeader[] = [
  //   {
  //     name: "title",
  //     key: "title",
  //     hasAction: false,
  //     hasLink: false,
  //     isEditable: false
  //   },
  //   {
  //     name: "date_uploaded",
  //     key: "createDate",
  //     type: "date",
  //     hasAction: false,
  //     hasLink: false,
  //     isEditable: false
  //   },
  //   {
  //     name: "type",
  //     key: "extension",
  //     hasAction: false,
  //     hasLink: false,
  //     isEditable: false
  //   },
  //   {
  //     name: "release_type",
  //     key: "releaseType",
  //     hasAction: false,
  //     hasLink: false,
  //     isEditable: false
  //   },
  //   {
  //     name: "download",
  //     key: "download",
  //     type: "icon",
  //     label: "cloud_download",
  //     hasAction: true,
  //     hasLink: false,
  //     isEditable: false
  //   }
  // ];

  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    private storageService: StorageService,
    private requestStore: RequestStoreService,
    private toastrService: ToastrService,
    public text: TextService,
    private toastr: ToastrService,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.validateStaff();
      })
    );
  }

  validateStaff(): void {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(
        this.staff.id,
        this.request.assignedStaff
      );
      this.isAdmin = validateAdmin(this.staff);
      if (this.request.primaryDepartment) {
        this.isPDM = validatePDM(
          this.staff.id,
          this.request.primaryDepartment.staff
        );
      }
    }
  }

  tableEvent(event) {
    if (event.key === "download") {
      const resource: Resource = event.data;
      this.contentService.downloadPrivateFile(resource);
    }
  }

  download(resource) {
    this.contentService.downloadPrivateFile(resource);
  }

  openUploadDialog(): void {
    const dialogData: any = {
      requestId: "",
      request: this.request
    };

    const dialogRef = this.dialog.open(UploadDocumentDialogComponent, {
      width: "800px",
      height: "auto",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fileData = result.file;
        this.title = result.title;
        this.needsReview = result.needsReview;
        this.releaseType = result.releaseType;
        this.message = result.message;
        this.description = result.description;
        this.onSubmit();
      }
    });
  }

  openDeleteResourceDialog(resource: Resource) {
    const resourceType = this.constantService.getContentType("RES");
    const dialogData = {
      content: resource,
      type: resourceType
    };
    const dialogRef = this.dialog.open(DeleteContentDialogComponent, {
      width: "800px",
      height: "auto",
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contentService
          .deleteContent(resource.id, resourceType, result.reason)
          .subscribe(res => {
            if (res) {
              this.toastr.success(
                "Resource Deleted!",
                this.text.get("success"),
                {
                  timeOut: 3000
                }
              );
            }
            this.requestStore.loadResources();
          });
      }
    });
  }

  openUpdateResourceDialog(resource: Resource) {
    const dialogData = {
      document: resource,
      request: this.request
    };

    const dialogRef = this.dialog.open(UpdateResourceDialogComponent, {
      width: "800px",
      height: "auto",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const staff: Staff = this.storageService.getStaff();
        staff.token = undefined;
        staff.groups = undefined;
        const updatedResource: Resource = {
          id: resource.id,
          title: result.title,
          releaseType: result.releaseType,
          creator: staff,
          extension: resource.extension,
          needsReview: resource.needsReview,
          underReview: resource.underReview,
          description: result.description
        };

        console.log(updatedResource);

        this.contentService
          .updateContent(
            this.request.id,
            this.constantService.getContentType("RES"),
            updatedResource
          )
          .subscribe(res => {
            if (res) {
              this.toastr.success(
                "Resource Updated!",
                this.text.get("success"),
                {
                  timeOut: 3000
                }
              );
            }
            this.requestStore.loadResources();
          });
      }
    });
  }

  onSubmit() {
    const formData = new FormData();
    formData.append("file", this.fileData);
    const staff: Staff = this.storageService.getStaff();
    staff.token = undefined;
    staff.groups = undefined;
    const resource: Resource = {
      title: this.title,
      creator: staff,
      needsReview: this.needsReview,
      releaseType: this.releaseType,
      underReview: false,
      message: this.message,
      description: this.description
    };

    formData.append("resource", JSON.stringify(resource));
    this.uploading = true;
    this.progress = 0;
    this.uploadStatus = this.text.get("uploading");
    this.contentService.uploadFile(this.request.id, formData).subscribe(
      res => {
        if (res.progress) {
          this.progress = res.progress;
          if (this.progress === 100) {
            this.uploadStatus = this.text.get("finishing_up");
          }
        }
        if (res.response) {
          this.uploading = false;
          const response = res.response;
          if (response.error) {
            return;
          }
          this.toastrService.success(
            response.title,
            "File Successfully Uploaded",
            {
              timeOut: 3000
            }
          );
          this.requestStore.loadResources();
        }
      },
      err => console.log("ERR", err)
    );
  }
}
