import { Component, OnInit } from '@angular/core';
import { AddInstructionDialogComponent } from 'src/app/dialogs/add-instruction-dialog/add-instruction-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Instruction } from 'src/app/models/content';
import { Request } from 'src/app/models/request';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { ContentService } from 'src/app/services/content.service';
import { StorageService } from 'src/app/services/storage.service';
import { Staff } from 'src/app/models/user';
import { TextService } from 'src/app/services/text.service';
import { UpdateInstructionDialogComponent } from 'src/app/dialogs/update-instruction-dialog/update-instruction-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ConstantService } from 'src/app/services/constant.service';
import { validateAssignedStaff, validateAdmin } from 'src/utils/validateStaff';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})
export class InstructionsComponent implements OnInit {
  subscriptions: Subscription[] = [];
  request: Request;
  staff: Staff;
  isAssignedWorker = false;
  isAdmin = false;

  constructor(
    public dialog: MatDialog,
    private requestStore: RequestStoreService,
    private contentService: ContentService,
    private storageService: StorageService,
    public text: TextService,
    private toastr: ToastrService,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.validateStaff();
      })
    );
  }
  validateStaff() {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(
        this.staff.id,
        this.request.assignedStaff
      );
      this.isAdmin = validateAdmin(this.staff);
    }
  }
  openInstructionDialog(): void {
    const dialogData: any = {
      requestId: 'hi',
      request: this.request
    };

    const dialogRef = this.dialog.open(AddInstructionDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      const staff: Staff = this.storageService.getStaff();
      staff.token = undefined;
      staff.groups = undefined;
      const instruction: Instruction = {
        message: result.message,
        title: result.title,
        releaseType: result.releaseType,
        creator: staff
      };
      this.contentService
        .createInstruction(this.request.id, instruction)
        .subscribe(res => {
          this.requestStore.loadInstructions();
        });
    });
  }

  openUpdateInstructionDialog(instruction: Instruction): void {
    const dialogData: any = {
      instruction,
      request: this.request
    };

    const dialogRef = this.dialog.open(UpdateInstructionDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      const staff: Staff = this.storageService.getStaff();
      staff.token = undefined;
      staff.groups = undefined;
      const updatedInstruction: Instruction = {
        id: instruction.id,
        message: result.message,
        title: result.title,
        releaseType: result.releaseType,
        creator: staff
      };
      this.contentService
        .updateContent(
          this.request.id,
          this.constantService.getContentType('INS'),
          updatedInstruction
        )
        .subscribe(res => {
          if (res) {
            this.toastr.success(
              'Instruction Updated!',
              this.text.get('success'),
              {
                timeOut: 3000
              }
            );
            this.requestStore.loadInstructions();
          }
        });
    });
  }
}
