import { Component, OnInit } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { AddLinkDialogComponent } from 'src/app/dialogs/add-link-dialog/add-link-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Staff } from 'src/app/models/user';
import { StorageService } from 'src/app/services/storage.service';
import { Link } from 'src/app/models/content';
import { Subscription } from 'rxjs';
import { ContentService } from 'src/app/services/content.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { UpdateLinkDialogComponent } from 'src/app/dialogs/update-link-dialog/update-link-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ConstantService } from 'src/app/services/constant.service';
import { validateAssignedStaff, validateAdmin } from 'src/utils/validateStaff';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  headers: SteeleHeader[] = [
    {
      name: 'description',
      key: 'name',
      type: 'text',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'link',
      key: 'link',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false
    }
  ];
  rows: any[] = [];

  subscriptions: Subscription[] = [];
  request: Request;

  staff: Staff;
  isAssignedWorker = false;
  isAdmin = false;

  constructor(
    public dialog: MatDialog,
    private requestStore: RequestStoreService,
    private contentService: ContentService,
    private storageService: StorageService,
    public text: TextService,
    private toastr: ToastrService,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.validateStaff();
      })
    );
  }
  validateStaff() {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(this.staff.id, this.request.assignedStaff);
      this.isAdmin = validateAdmin(this.staff);
    }
  }
  goToUrl(link: string) {
    window.open(link, '_blank');
  }

  openLinkDialog(): void {
    const dialogData: any = {
      requestId: '',
      request: this.request
    };

    const dialogRef = this.dialog.open(AddLinkDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      const staff: Staff = this.storageService.getStaff();
      staff.token = undefined;
      staff.groups = undefined;
      const link: Link = {
        title: result.title,
        url: result.link,
        description: result.message,
        releaseType: result.releaseType,
        creator: staff
      };
      this.contentService.createLink(this.request.id, link).subscribe(res => {
        this.requestStore.loadLinks();
      });
    });
  }

  openUpdateLinkDialog(link: Link) {
    const dialogData: any = {
      link,
      request: this.request
    };


    const dialogRef = this.dialog.open(UpdateLinkDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      const staff: Staff = this.storageService.getStaff();
      staff.token = undefined;
      staff.groups = undefined;
      const updatedLink: Link = {
        id: link.id,
        description: result.description,
        url: result.url,
        title: result.title,
        releaseType: result.releaseType,
        creator: staff
      };
      this.contentService
        .updateContent(this.request.id, this.constantService.getContentType('LNK'), updatedLink)
        .subscribe(res => {
          if (res) {
            this.toastr.success('Link Updated!', this.text.get('success'), {
              timeOut: 3000
            });
            this.requestStore.loadLinks();
          }
        });
    });
  }
}
