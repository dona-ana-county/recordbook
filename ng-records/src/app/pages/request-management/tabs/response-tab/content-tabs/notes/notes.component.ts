import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddNoteDialogComponent } from 'src/app/dialogs/add-note-dialog/add-note-dialog.component';
import { Note } from 'src/app/models/content';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { Subscription } from 'rxjs';
import { Request } from 'src/app/models/request';
import { ContentService } from 'src/app/services/content.service';
import { StorageService } from 'src/app/services/storage.service';
import { Staff } from 'src/app/models/user';
import { TextService } from 'src/app/services/text.service';
import { UpdateNoteDialogComponent } from 'src/app/dialogs/update-note-dialog/update-note-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ConstantService } from 'src/app/services/constant.service';
import { validateAssignedStaff, validateAdmin } from 'src/utils/validateStaff';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  subscriptions: Subscription[] = [];
  request: Request;

  staff: Staff;
  isAssignedWorker = false;
  isAdmin = false;

  constructor(
    public dialog: MatDialog,
    private requestStore: RequestStoreService,
    private contentService: ContentService,
    private storageService: StorageService,
    public text: TextService,
    private toastr: ToastrService,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.validateStaff();
      })
    );
  }
  validateStaff() {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(
        this.staff.id,
        this.request.assignedStaff
      );
      this.isAdmin = validateAdmin(this.staff);
    }
  }
  openNoteDialog(): void {
    const dialogData: any = {
      request: this.request
    };

    const dialogRef = this.dialog.open(AddNoteDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const staff: Staff = this.storageService.getStaff();
        staff.token = undefined;
        staff.groups = undefined;
        const note: Note = {
          message: result.message,
          title: result.title,
          releaseType: result.releaseType,
          creator: staff
        };
        this.contentService.createNote(this.request.id, note).subscribe(res => {
          this.requestStore.loadNotes();
        });
      }
    });
  }

  openUpdateNoteDialog(note: Note) {
    const dialogData: any = {
      note,
      request: this.request
    };

    const dialogRef = this.dialog.open(UpdateNoteDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      const staff: Staff = this.storageService.getStaff();
      staff.token = undefined;
      staff.groups = undefined;
      const updatedNote: Note = {
        id: note.id,
        message: result.message,
        title: result.title,
        releaseType: result.releaseType,
        creator: staff
      };
      this.contentService
        .updateContent(
          this.request.id,
          this.constantService.getContentType('NOT'),
          updatedNote
        )
        .subscribe(res => {
          if (res) {
            this.toastr.success('Note Updated!', this.text.get('success'), {
              timeOut: 3000
            });
            this.requestStore.loadNotes();
          }
        });
    });
  }
}
