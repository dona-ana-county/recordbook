import { Component, Host, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { MultiStackBoxSelectorComponent } from 'src/app/components/multi-stack-box-selector/multi-stack-box-selector.component';
import { SteeleTableComponent } from 'src/app/components/steele-table/steele-table.component';
import { Department } from 'src/app/models/department';
import { Request } from 'src/app/models/request';
import { Staff, User } from 'src/app/models/user';
import { ConstantService } from 'src/app/services/constant.service';
import { DepartmentService } from 'src/app/services/department.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { RequestService } from 'src/app/services/request.service';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';
import { SteeleHeader } from '../../../../components/steele-table/header';
import { Tab } from '../tab';



@Component({
  selector: 'app-worker-tab',
  templateUrl: './worker-tab.component.html',
  styleUrls: ['./worker-tab.component.scss']
})
export class WorkerTabComponent extends Tab implements OnInit, OnDestroy {
  headers: SteeleHeader[] = [
    {
      name: 'name',
      key: 'name'
    },
    {
      name: 'role',
      key: 'role'
    },
    {
      name: 'department',
      key: 'department'
    }
  ];

  subscriptions: Subscription[] = [];
  request: Request;
  user: Staff;
  departmentStaff: Staff[];
  assignedRequestStaff: Staff[];
  needsSaving = false;

  rows: any[] = [];
  selectorItems: any[];
  originalItems: any[];
  selectedItems: User[];
  changesMade = false;
  assignments: any[];

  @ViewChild(MultiStackBoxSelectorComponent) boxSelector: MultiStackBoxSelectorComponent;
  @ViewChild(SteeleTableComponent, {static: true}) assignedWorkersTable: SteeleTableComponent;

  constructor(
    private storageService: StorageService,
    private departmentService: DepartmentService,
    private requestService: RequestService,
    private toastrService: ToastrService,
    private requestStore: RequestStoreService,
    public text: TextService,
    private constantService: ConstantService,
    @Host() parent: MatTab
  ) {
    super(parent);
  }

  ngOnInit() {
    this.user = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.makeRows();
        this.setOriginalItems();
        if (this.departmentStaff) {
          this.setSelectorItems();
        }
        if (this.originalsChanged()) {
          this.changesMade = true;
        } else {
          this.changesMade = false;
        }
      })
    );

    this.departmentService.getDepartmentByStaff(this.user.id).subscribe((res: Department) => {
      this.departmentStaff = res.staff;
      this.setSelectorItems();
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadAssignedStaff();
  }

  setOriginalItems() {
    this.originalItems = [];
    this.selectedItems = [];
    for (const o of this.request.assignedStaff) {
      if (o.role.code !== this.constantService.getRole('MAN').code) {
        this.originalItems.push(o);
        const newStaff: User = {
          id: o.id,
          firstName: o.firstName,
          lastName: o.lastName,
          name: o.firstName + ' ' + o.lastName
        };
        this.selectedItems.push(newStaff);
      }
    }
  }

  setSelectorItems() {
    this.selectorItems = [];
    for (const s of this.departmentStaff) {
      if (s.role.code !== this.constantService.getRole('MAN').code) {
        const namedStaff: User = {
          id: s.id,
          firstName: s.firstName,
          lastName: s.lastName,
          name: s.firstName + ' ' + s.lastName
        };
        this.selectorItems.push(namedStaff);
      }
    }
  }

  makeRows() {
    this.rows = [];
    for (const staff of this.request.assignedStaff) {
      this.rows.push({
        id: staff.id,
        name: staff.firstName + ' ' + staff.lastName,
        role: staff.role.value,
        department: this.findStaffDepartment(staff.id).name
      });
    }
    this.assignedWorkersTable.resetTable();
  }

  findStaffDepartment(staffId: string): Department {
    for (const dept of this.requestStore.getDepartments()) {
      for (const staff of dept.staff) {
        if (staff.id === staffId) {
          return { id: dept.id, name: dept.name };
        }
      }
    }
  }

  getStaff(staffId: string): Staff {
    return this.departmentStaff.find(x => {
      return x.id === staffId;
    });
  }

  saveChanges() {
    this.requestService.updateRequestStaff(this.request.id, this.assignments).subscribe(res => {
      this.requestStore.loadAssignedStaff();
      this.boxSelector.clear();
      this.toastrService.success('', 'Worker Assignment Updated!', {
        timeOut: 3000
      });
    });
  }

  onBoxesChange(event) {
    this.assignments = event;
    if (this.originalsChanged()) {
      this.changesMade = true;
    } else {
      this.changesMade = false;
    }
  }

  originalsChanged(): boolean {
    if (this.originalItems.length === this.selectedItems.length) {
      for (const originalItem of this.originalItems) {
        if (!this.selectedItems.find(item => item.id === originalItem.id)) {
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  }
}
