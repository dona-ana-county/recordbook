import { Component, Host, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatTab } from '@angular/material/tabs';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import {
  AlertedItem,
  MultiStackBoxSelectorComponent
} from 'src/app/components/multi-stack-box-selector/multi-stack-box-selector.component';
import { Department } from 'src/app/models/department';
import { Request } from 'src/app/models/request';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { RequestService } from 'src/app/services/request.service';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';
import { Tab } from '../tab';
import { MarkCompleteDialogComponent } from "src/app/dialogs/mark-complete-dialog/mark-complete-dialog.component";
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-department-tab',
  templateUrl: './department-tab.component.html',
  styleUrls: ['./department-tab.component.scss']
})
export class DepartmentTabComponent extends Tab implements OnInit, OnDestroy {
  departments: Department[];
  subscriptions: Subscription[] = [];

  request: Request;

  primaryControl: FormControl;
  selectorItems: Department[];
  originalItems: Department[];
  originalPrimary: string;
  departmentMessages: any[] = [];

  secondaryAssignments: AlertedItem[];
  changesMade = false;

  @ViewChild(MultiStackBoxSelectorComponent) boxSelector: MultiStackBoxSelectorComponent;

  constructor(
    public dialog: MatDialog,
    private requestStore: RequestStoreService,
    public storageService: StorageService,
    public text: TextService,
    private requestService: RequestService,
    private toastr: ToastrService,
    @Host() parent: MatTab,
  ) {
    super(parent);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        if (this.request.primaryDepartment) {
          this.primaryControl = new FormControl(this.request.primaryDepartment.id, [
            Validators.required
          ]);
          this.departments = this.requestStore.getDepartments();
          this.setOriginalItems();
          this.updateSelectorItems();
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadDepartments();
  }

  setOriginalItems() {
    const newOriginals = [];
    this.originalPrimary = this.primaryControl.value;
    for (const dept of this.request.secondaryDepartments) {
      newOriginals.push(dept);
    }
    this.originalItems = newOriginals;

    if (this.originalsChanged()) {
      this.changesMade = true;
    } else {
      this.changesMade = false;
    }
  }

  updateSelectorItems() {
    this.selectorItems = [];
    for (const dept of this.departments) {
      if (dept.id !== this.request.primaryDepartment.id) {
        this.selectorItems.push(dept);
      }
    }
  }

  onPrimaryDepartmentChange(event) {
    for (const dept of this.departments) {
      if (dept.id === event.value) {
        this.request.primaryDepartment = dept;
        this.updateSelectorItems();
      }
    }
    if (this.originalsChanged()) {
      this.changesMade = true;
    } else {
      this.changesMade = false;
    }
  }

  onBoxesChange(event) {
    this.secondaryAssignments = event;
    if (this.originalsChanged()) {
      this.changesMade = true;
    } else {
      this.changesMade = false;
    }
  }

  submitAssignment(): void {
    if (this.primaryControl.hasError('required')) {
      return;
    }

    this.assignDepartments();
  }

  assignDepartments(): void {
    this.requestService
      .updateDepartments(
        this.request.id,
        { id: this.request.primaryDepartment.id },
        this.secondaryAssignments,

      )
      .subscribe(res => {
        if (res) {
          this.toastDepartmentAssignSuccessful();
          this.requestStore.loadDepartments();
          this.primaryControl.patchValue(this.request.primaryDepartment.name);
        }
      });
  }

  toastDepartmentAssignSuccessful() {
    this.boxSelector.clear();
    this.toastr.success(
      this.text.get('department_assignments_successful'),
      this.text.get('success'),
      {
        timeOut: 3000
      }
    );
    this.setOriginalItems();
  }

  originalsChanged(): boolean {
    if (this.originalPrimary !== this.primaryControl.value) {
      return true;
    }

    if (this.originalItems.length === this.request.secondaryDepartments.length) {
      for (const originalItem of this.originalItems) {
        if (!this.request.secondaryDepartments.find(item => item.id === originalItem.id)) {
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  }

  markCompletionStatusDialog(event) {
    const data = {...event.data, completed: {
      name: event.data.completionStatus.completed ? "Remove Completion" : "Complete",
      value: event.data.completionStatus.completed
    }, comment: event.data.completionStatus.comment};

    const dialogData: any = {
      request: this.request,
      key: event.key,
      data,
    };

    const dialogRef = this.dialog.open(MarkCompleteDialogComponent, {
      width: "800px",
      height: "auto",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.requestService
          .updateCompletion(
            this.request.id,
            result.id,
            result.completed,
            result.comment
          )
          .subscribe((response) => {
            if (response) {
              this.requestStore.loadSecondaryDepartments();
              this.toastCompletionUpdateSuccess();
            }
          });
      }
    });
  }

  toastCompletionUpdateSuccess() {
    this.toastr.success(
      "Completion Status Updated!",
      this.text.get("success"),
      {
        timeOut: 3000,
      }
    );
  }
}
