import { AfterViewInit, Component, ElementRef, Host, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTab } from '@angular/material/tabs';
import { Subscription } from 'rxjs';
import { Comment } from 'src/app/models/communication';
import { Request, RequestStatus } from 'src/app/models/request';
import { Staff } from 'src/app/models/user';
import { CommunicationService } from 'src/app/services/communication.service';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { StorageService } from 'src/app/services/storage.service';
import { TextService } from 'src/app/services/text.service';
import { validateAdmin, validateAssignedStaff } from 'src/utils/validateStaff';
import { Tab } from '../tab';

@Component({
  selector: 'app-communications-tab',
  templateUrl: './communications-tab.component.html',
  styleUrls: ['./communications-tab.component.scss']
})
export class CommunicationsTabComponent extends Tab
  implements OnInit, OnDestroy, AfterViewInit {
  subscriptions: Subscription[] = [];
  request: Request;
  @Input() status: RequestStatus;
  @ViewChild('messageArea') messageAreaRef: ElementRef;

  alerts = [];
  comments: Comment[];
  newComment: string;
  staff: Staff;
  isAdmin = false;
  isAssignedWorker = false;

  constructor(
    private requestStore: RequestStoreService,
    private commService: CommunicationService,
    private storageService: StorageService,
    public text: TextService,
    @Host() parent: MatTab,
  ) {
    super(parent);
  }

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe(res => {
        this.request = res;

        this.isAdmin = validateAdmin(this.staff);

        this.isAssignedWorker = validateAssignedStaff(
          this.staff.id,
          res.assignedStaff
        );
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  validateStaff() {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(
        this.staff.id,
        this.request.assignedStaff
      );
    }
  }

  ngAfterViewInit() {
    const messageArea: HTMLDivElement = this.messageAreaRef.nativeElement;
    messageArea.scrollTop = this.messageAreaRef.nativeElement.scrollHeight;
  }

  onTabSelected(): void {
    this.requestStore.loadComments();
  }

  sendComment() {
    const comment = {} as Comment;
    comment.message = this.newComment;
    comment.commenter = {
      id: this.staff.id,
      firstName: this.staff.firstName,
      lastName: this.staff.lastName
    };
    this.commService
      .staffNewComment(this.request.id, comment)
      .subscribe(res => {
        // this.comments.push(res);
        this.requestStore.loadComments();
        this.newComment = null;
      });
  }

  isRequestor(comment: Comment): boolean {
    if (comment.commenter.id === this.request.requestor.id) {
      return true;
    }
    return false;
  }
}
