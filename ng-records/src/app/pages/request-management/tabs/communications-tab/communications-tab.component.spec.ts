import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationsTabComponent } from './communications-tab.component';

describe('CommunicationsTabComponent', () => {
  let component: CommunicationsTabComponent;
  let fixture: ComponentFixture<CommunicationsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
