import { Component, Host, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTab } from "@angular/material/tabs";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { SteeleHeader } from "src/app/components/steele-table/header";
import { SteeleTableComponent } from "src/app/components/steele-table/steele-table.component";
import { MarkCompleteDialogComponent } from "src/app/dialogs/mark-complete-dialog/mark-complete-dialog.component";
import { Department } from "src/app/models/department";
import { Request } from "src/app/models/request";
import { Staff } from "src/app/models/user";
import { RequestStoreService } from "src/app/services/request-store.service";
import { RequestService } from "src/app/services/request.service";
import { StorageService } from "src/app/services/storage.service";
import { TextService } from "src/app/services/text.service";
import { Tab } from "../tab";
import { validateAssignedManager } from 'src/utils/validateStaff';

@Component({
  selector: "app-department-read-only-tab",
  templateUrl: "./department-read-only-tab.component.html",
  styleUrls: ["./department-read-only-tab.component.scss"],
})
export class DepartmentReadOnlyTabComponent
  extends Tab
  implements OnInit, OnDestroy {
  primaryDepartmentName: string;
  departments: Department[];
  subscriptions: Subscription[] = [];

  staff: Staff;
  request: Request;

  headers: SteeleHeader[] = [
    {
      name: "department",
      key: "department",
    },
    {
      name: "checkmark",
      key: "checkmark",
    },
    {
      name: "completed",
      key: "completed",
      type: "button",
      hasAction: true,
    },
    {
      name: "comment",
      key: "comment",
      type: "html"
    },
    {
      name: "edit_comment",
      key: "edit_comment",
      type: "icon",
      label: "edit",
      hasAction: true
    }
  ];

  rows: any[] = [];

  @ViewChild(SteeleTableComponent, { static: true })
  secondaryDepartmentsTable: SteeleTableComponent;

  constructor(
    public dialog: MatDialog,
    private requestStore: RequestStoreService,
    public storageService: StorageService,
    public text: TextService,
    private toastr: ToastrService,
    private requestService: RequestService,
    @Host() parent: MatTab
  ) {
    super(parent);
  }

  ngOnInit() {
    this.staff = this.storageService.getStaff();

    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        if (this.request.primaryDepartment) {
          this.primaryDepartmentName = this.request.primaryDepartment.name;
          this.departments = this.requestStore.getDepartments();
          if (this.request.secondaryDepartments) {
            this.makeRows(this.request.secondaryDepartments);
          }
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadDepartments();
  }

  makeRows(secondaryDepartments) {
    this.rows = [];
    for (const dept of secondaryDepartments) {
      this.rows.push({
        id: dept.id,
        department: dept.name,
        isAssignedMan: validateAssignedManager(this.staff.id, dept.staff),
        checkmark: dept.completionStatus.completed ? "\u2705" : "",
        completed: {
          name: dept.completionStatus.completed ? "Remove Completion" : "Complete",
          value: dept.completionStatus.completed,
        },
        comment: dept.completionStatus.comment
      });
    }
    this.secondaryDepartmentsTable.resetTable();
  }

  markCompletionStatusDialog(event) {
    const dialogData: any = {
      request: this.request,
      key: event.key,
      data: event.data,
    };

    const dialogRef = this.dialog.open(MarkCompleteDialogComponent, {
      width: "800px",
      height: "auto",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.requestService
          .updateCompletion(
            this.request.id,
            result.id,
            result.completed,
            result.comment
          )
          .subscribe((response) => {
            if (response) {
              this.requestStore.loadSecondaryDepartments();
              this.makeRows(this.request.secondaryDepartments);
              this.toastCompletionUpdateSuccess();
            }
          });
      }
    });
  }

  toastCompletionUpdateSuccess() {
    this.toastr.success(
      "Completion Status Updated!",
      this.text.get("success"),
      {
        timeOut: 3000,
      }
    );
  }
}
