import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentReadOnlyTabComponent } from './department-read-only-tab.component';

describe('DepartmentReadOnlyTabComponent', () => {
  let component: DepartmentReadOnlyTabComponent;
  let fixture: ComponentFixture<DepartmentReadOnlyTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartmentReadOnlyTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentReadOnlyTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
