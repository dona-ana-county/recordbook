
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  Component,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTab } from "@angular/material/tabs";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { AssignRequestDialogComponent } from "src/app/dialogs/assign-request-dialog/assign-request-dialog.component";
import { CloseRequestDialogComponent } from "src/app/dialogs/close-request-dialog/close-request-dialog.component";
import { CloseWarningDialogComponent } from "src/app/dialogs/close-warning-dialog/close-warning-dialog.component";
import {
  DenyRequestDialogComponent,
  DenyRequestDialogData,
} from "src/app/dialogs/deny-request-dialog/deny-request-dialog.component";
import { ExtendDueDateDialogComponent } from "src/app/dialogs/extend-due-date-dialog/extend-due-date-dialog.component";
import { ReOpenRequestDialogComponent } from "src/app/dialogs/re-open-request-dialog/re-open-request-dialog.component";
import { UpdateDueDateDialogComponent } from "src/app/dialogs/update-due-date-dialog/update-due-date-dialog.component";
import { WrongCustodianDialogComponent } from 'src/app/dialogs/wrong-custodian-dialog/wrong-custodian-dialog.component';
import { Resource } from "src/app/models/content";
import { Department } from "src/app/models/department";
import { Request } from "src/app/models/request";
import { Staff,Group } from "src/app/models/user";
import { ConstantService } from "src/app/services/constant.service";
import { ContentService } from "src/app/services/content.service";
import { NotificationService } from "src/app/services/notification.service";
import { RequestStoreService } from "src/app/services/request-store.service";
import { RequestService } from "src/app/services/request.service";
import { StorageService } from "src/app/services/storage.service";
import { TextService } from "src/app/services/text.service";
import { textTransform, textTransformNewLines } from "src/utils/textTransform";
import {
  validateAdmin,
  validateAssignedStaff,
  validateClerk,
  validatePDM,
} from "src/utils/validateStaff";
import { Tab } from "../tab";
import stateJSON from 'src/utils/stateList.json';
import contactTypes from 'src/utils/contactTypes.json';
import { State, ContactType } from 'src/app/models/constant';

@Component({
  selector: "app-info-tab",
  templateUrl: "./info-tab.component.html",
  styleUrls: ["./info-tab.component.scss"],
})
export class InfoTabComponent extends Tab implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  @Input() departments: Department[];
  @Output() onRequestReOpened = new EventEmitter();
  @ViewChild('phoneNumber') phoneNumberRef: ElementRef;
  request: Request;
  requestDocs: Resource[];
  editPublicDescription: boolean;
  editPublicTitle: boolean;
  newLinedDescription: string;
  newLinedTitle: string;
  staff: Staff;
  isManager: boolean;
  publicDescriptionChanged: boolean;
  publicTitleChanged: boolean;
  hasEmail = false;

  isAssignedWorker = false;
  isPDM = false;
  isAdmin: boolean;
  isClerk: boolean;
  isIPRAAdmin: boolean

  isEditContactInfo = false;
  contactInfoForm: FormGroup;

  states: string[] = [];
  stateObjects: State[] = stateJSON;
  stateControl = new FormControl();

  contactTypes: string[] = [];
  contactTypeObjects: ContactType[] = contactTypes;
  contactTypeControl = new FormControl();

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private toastr: ToastrService,
    private requestService: RequestService,
    private requestStore: RequestStoreService,
    public text: TextService,
    private storageService: StorageService,
    private contentService: ContentService,
    private constantService: ConstantService,
    private notificationService: NotificationService,
    @Host() parent: MatTab,
    private formBuilder: FormBuilder
  ) {
    super(parent);
  }

    ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.requestDocs = [];
  
    if (this.staff.role === this.constantService.getRole("MAN")) {
      this.isManager = true;
    } else {
      this.isManager = false;
    }
    this.editPublicDescription = false;
    this.editPublicTitle = false;
    this.subscriptions.push(
      this.requestStore.getRequest().subscribe((r: Request) => {
        this.request = r;
        this.getRequestDocs();
  
        this.validateStaff();
        
        this.newLinedDescription = textTransformNewLines(
          this.request.publicDescription
        );
  
        this.newLinedTitle = textTransformNewLines(this.request.publicTitle);
  
        this.publicDescriptionChanged =
          this.request.description !== this.request.publicDescription;
        
        this.publicTitleChanged = this.request.title !== this.request.publicTitle;
        this.hasEmail = this.request.requestor.primaryContact.code === "EML";
      })
    );
  
    this.checkIfIPRAAdmin();
  
    this.requestService.getFullRequest(this.request.id).subscribe((request: Request) => {
      const currentContactInfo = request.requestor.contactInfo;
      this.contactInfoForm = this.formBuilder.group({
        primaryContact: [request.requestor.primaryContact.code || '', Validators.required],
        email: [currentContactInfo.email || '', [Validators.required, Validators.email]],
        secondary_emails: [currentContactInfo.secondary_emails ? currentContactInfo.secondary_emails.join(', ') : '', [Validators.pattern(/^[^,]+(,[^,]+)*$/)]],
        phone: [currentContactInfo.phone || '', [Validators.required, Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]],
        fax: [currentContactInfo.fax || ''], 
        address1: [currentContactInfo.address1 || '', Validators.required],
        address2: [currentContactInfo.address2 || ''], 
        city: [currentContactInfo.city || ''],
        state: [currentContactInfo.state || '', Validators.required],
        postalCode: [currentContactInfo.postalCode || '', [Validators.required, Validators.pattern(/^\d{5}$/)]]
      });
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => {
      s.unsubscribe();
    });
  }

  onTabSelected(): void {
    this.requestStore.loadInfo();
  }

  validateStaff() {
    if (this.request) {
      this.isAssignedWorker = validateAssignedStaff(
        this.staff.id,
        this.request.assignedStaff
      );
      if (this.request.primaryDepartment) {
        this.isPDM = validatePDM(
          this.staff.id,
          this.request.primaryDepartment.staff
        );
      }
      this.isAdmin = validateAdmin(this.staff);
      this.isClerk = validateClerk(this.staff);
    }
  }

  getRequestDocs() {
    this.requestDocs = [];
    for (const doc of this.request.resources) {
      if (doc.releaseType.code === "DOC") {
        this.requestDocs.push(doc);
      }
    }
  }

  savePublicDescription() {
    const formattedText = textTransform(this.newLinedDescription);
    this.requestService
      .updatePublicDescription(this.request.id, formattedText)
      .subscribe((res) => {
        if (res) {
          this.toastr.success(
            this.text.get("public_description_updated"),
            this.text.get("success"),
            {
              timeOut: 3000,
            }
          );
          this.editPublicDescription = false;
          this.requestStore.loadInfo();
        }
      });
  }

  savePublicTitle() {
    const formattedTitle = textTransform(this.newLinedTitle);
    this.requestService
      .updatePublicTitle(this.request.id, formattedTitle)
      .subscribe((res) => {
        if (res) {
          this.toastr.success(
            this.text.get("public_title_updated"),
            this.text.get("success"),
            {
              timeOut: 3000,
            }
          );
          this.editPublicTitle = false;
          this.requestStore.loadInfo();
        }
      });
  }

  openDenyDialog(): void {
    const dialogData: DenyRequestDialogData = {
      request: this.request,
    };

    const dialogRef = this.dialog.open(DenyRequestDialogComponent, {
      width: "800px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        if (!this.toastr.findDuplicate("", result, false, false)) {
          this.toastr.success(result, this.text.get("success"), {
            timeOut: 4000,
          });
        }
        this.router.navigate(["dashboard"]);
      }
    });
  }

  openWrongCustodianDialog(): void {
    const dialogData = {
      request: this.request
    };

    const dialogRef = this.dialog.open (WrongCustodianDialogComponent, {
      width: "800px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        if (!this.toastr.findDuplicate("", result, false, false)) {
          this.toastr.success(result, this.text.get("success"), {
            timeOut: 4000,
          });
        }
      }
    });
  }

  closeRequest() {
    const status = this.checkCompletionStatus();
    if (!status.complete) {
      this.closeWarning(status.depts);
    } else {
      this.closeCompletedRequest();
    }
  }

  closeCompletedRequest() {
    const dialogData = {
      request: this.request,
    };

    const dialogRef = this.dialog.open(CloseRequestDialogComponent, {
      width: "500px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.requestService
          .closeRequest(this.request.id, result.selectedReason)
          .subscribe((res) => {
            if (!res.error) {
              this.toastr.success(
                this.text.get("request_closed"),
                this.text.get("success"),
                {
                  timeOut: 4000,
                }
              );
              this.requestStore.loadInfo();
            }
          });
      }
    });
  }

  closeWarning(depts: Department[]) {
    const dialogData = {
      request: this.request,
      depts,
    };

    const dialogRef = this.dialog.open(CloseWarningDialogComponent, {
      width: "500px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.closeCompletedRequest();
      }
    });
  }

  updateDueDate() {
    const dialogData = {
      request: this.request,
      isAdmin: this.isAdmin,
    };
    const dialogRef = this.dialog.open(UpdateDueDateDialogComponent, {
      width: "500px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.requestService
          .updateDueDate(this.request.id, result.reason, result.date)
          .subscribe((res) => {
            if (!res.error) {
              this.toastr.success(
                this.text.get("date_updated"),
                this.text.get("success"),
                {
                  timeOut: 4000,
                }
              );
              this.requestStore.loadInfo();
            }
          });
      }
    });
  }

  extendDueDate() {
    const templateName =
      this.request.extensionStatus.code === "NON" ? "extension" : "burdensome";
    this.notificationService
      .getTemplate(templateName, this.request.id)
      .subscribe((template) => {
        const dialogData = {
          request: this.request,
          header: template.header,
          footer: template.footer,
        };
        const dialogRef = this.dialog.open(ExtendDueDateDialogComponent, {
          width: "900px",
          data: dialogData,
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            this.requestService
              .extendDueDate(this.request.id, result.description, result.date)
              .subscribe((res) => {
                if (!res.error) {
                  this.toastr.success(
                    this.text.get("date_updated"),
                    this.text.get("success"),
                    {
                      timeOut: 4000,
                    }
                  );
                  this.requestStore.loadInfo();
                }
              });
          }
        });
      });
  }

  reOpenRequest() {
    const dialogRef = this.dialog.open(ReOpenRequestDialogComponent, {
      width: "500px",
      data: {
        heading: this.text.get("reopen_request_dialog_text"),
        cancel: this.text.get("cancel"),
        confirm: this.text.get("yes"),
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.requestService
          .reOpenRequest(this.request.id, result)
          .subscribe((res) => {
            if (!res.error) {
              this.toastr.success(
                this.text.get("request_reopened"),
                this.text.get("success"),
                {
                  timeOut: 4000,
                }
              );
              this.requestStore.loadInfo();
              this.requestStore.loadLog();
            }
          });
        this.requestStore.loadInfo();
      }
    });
  }

  openAssignDialog(): void {
    const dialogData = {
      request: this.request,
      departments: this.departments,
    };

    const dialogRef = this.dialog.open(AssignRequestDialogComponent, {
      width: "800px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== undefined) {
        this.requestService
          .updateDepartments(
            this.request.id,
            result.primaryAssignment,
            result.secondaryAssignments
          )
          .subscribe((res) => {
            this.requestStore.loadInfo();
            this.requestStore.loadDepartments();
            this.requestStore.loadAssignedStaff();
          });
      }
    });
  }

  showPublicDescription() {
    this.editPublicDescription = !this.editPublicDescription;
  }

  showPublicTitle() {
    this.editPublicTitle = !this.editPublicTitle;
  }

  goToPublicView() {
    // this.router.navigate(['viewRequest', this.request.id]);
    window.open("viewRequest/" + this.request.id, "_blank");
  }

  goToRequestorView() {
    // this.router.navigate([
    //   'viewRequest',
    //   this.request.id,
    //   this.request.passcode,
    // ]);
    window.open("viewRequest/" + this.request.id + "/" + this.request.passcode);
  }

  download(resource) {
    this.contentService.downloadPrivateFile(resource);
  }

  checkCompletionStatus(): { complete: boolean; depts: Department[] } {
    let complete = true;
    const depts = [];
    for (const dept of this.request.secondaryDepartments) {
      if (dept.completionStatus && !dept.completionStatus.completed) {
        complete = false;
        depts.push(dept);
      }
    }
    return { complete, depts };
  }
 
  doEditContactInfo() {
    this.isEditContactInfo = !this.isEditContactInfo;
  } 

  checkIfIPRAAdmin() {
    const ipraAdminGroup = this.constantService.getGroup("IPA");
    this.isIPRAAdmin = this.staff.groups.some(group => group.code === ipraAdminGroup.code);
  }

 saveContactInfo(): void {
  if (this.contactInfoForm.valid) {
    if (this.request && this.request.id && this.request.requestor && this.request.requestor.contactInfo) {
      const updatedContactInfo = this.contactInfoForm.value;
      const primaryContactCode = updatedContactInfo.primaryContact;
      const updatedcontactInfoPayload = {
        ...this.request.requestor.contactInfo,
        primary_contact: updatedContactInfo.primaryContact,
        email: updatedContactInfo.email,
        secondary_emails: updatedContactInfo.secondary_emails ? updatedContactInfo.secondary_emails.split(',').map(email => email.trim()) : [],
        phone: updatedContactInfo.phone,
        fax: updatedContactInfo.fax,
        address1: updatedContactInfo.address1,
        address2: updatedContactInfo.address2,
        city: updatedContactInfo.city,
        state: updatedContactInfo.state,
        postalCode: updatedContactInfo.postalCode
      };
      this.requestService.updateRequestorContactInfo(this.request.id, updatedcontactInfoPayload).subscribe({
        next: () => {
          this.request.requestor.primaryContact.code = primaryContactCode;
          this.request.requestor.contactInfo = updatedcontactInfoPayload;
          this.doEditContactInfo();
          this.toastr.success(
            this.text.get("contact_info_updated"), 
            this.text.get("success"),
            {
              timeOut: 3000,
            }
          );
        }
      });
    }
  }
}
  
  onKeyDown(e) {

    // Prevent non-digit input on the phoneNumber field.
    if (
      // Allow: Delete, Backspace, Tab, Escape, Enter
      [46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
      (e.keyCode === 65 && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.keyCode === 67 && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.keyCode === 86 && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.keyCode === 88 && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.keyCode === 65 && e.metaKey === true) || // Cmd+A (Mac)
      (e.keyCode === 67 && e.metaKey === true) || // Cmd+C (Mac)
      (e.keyCode === 86 && e.metaKey === true) || // Cmd+V (Mac)
      (e.keyCode === 88 && e.metaKey === true) || // Cmd+X (Mac)
      (e.keyCode >= 35 && e.keyCode <= 39) // Home, End, Left, Right
    ) {
      return;  // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }

    const key = e.charCode || e.keyCode || 0;
    if (key !== 8 && key !== 9) {
      if (this.phoneNumberRef.nativeElement.value.length === 3) {
        this.phoneNumberRef.nativeElement.value =
          this.phoneNumberRef.nativeElement.value + '-';
      }
      if (this.phoneNumberRef.nativeElement.value.length === 7) {
        this.phoneNumberRef.nativeElement.value =
          this.phoneNumberRef.nativeElement.value + '-';
      }
      if (this.contactInfoForm.controls.postalCode.value.length >= 5) {
        e.preventDefault();
      }
    }
  }
                                                        
}
