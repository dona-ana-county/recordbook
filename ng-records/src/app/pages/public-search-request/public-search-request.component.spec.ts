import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicSearchRequestComponent } from './public-search-request.component';

describe('PublicSearchRequestComponent', () => {
  let component: PublicSearchRequestComponent;
  let fixture: ComponentFixture<PublicSearchRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicSearchRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicSearchRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
