import { Component, OnInit } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { Router } from '@angular/router';
import {
  SearchQuery,
  Filter,
  RequestSearchResult
} from 'src/app/models/search';
import { TextService } from 'src/app/services/text.service';
import { SearchService } from 'src/app/services/search.service';
import { StorageService } from 'src/app/services/storage.service';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { serializeDate } from 'src/utils/date';

@Component({
  selector: 'app-public-search-request',
  templateUrl: './public-search-request.component.html',
  styleUrls: ['./public-search-request.component.scss']
})
export class PublicSearchRequestComponent implements OnInit {
  defaultPerPage = 15;
  currentPage = 1;
  totalItems: number;
  totalPages: number;
  pageStart: number;
  pageEnd: number;

  requests: RequestSearchResult[];
  query: SearchQuery;

  search: string;
  department: string;
  status: string;
  departments: Department[];

  createFromDate: Date;
  createToDate: Date;
  dueFromDate: Date;
  dueToDate: Date;

  headers: SteeleHeader[] = [
    {
      name: 'id',
      key: 'requestId',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false
    },
    {
      name: 'requestor',
      key: 'requestorName',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'department',
      key: 'department',
      type: 'text',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'due_date',
      key: 'dueDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'status',
      key: 'status',
      type: 'status',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];

  constructor(
    private searchService: SearchService,
    private router: Router,
    public text: TextService,
    private storageService: StorageService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit() {
    if (this.storageService.getStaff()) {
      this.router.navigate(['staffSearch']);
    }
    this.initQuery(this.defaultPerPage);
    this.sendQuery(this.currentPage);

    this.departmentService.getDepartments().subscribe(res => {
      this.departments = res;
      this.department = 'all';
    });
  }

  initQuery(perPage: number): void {
    this.query = {} as SearchQuery;
    this.query.search = '';
    this.query.sort = '';
    this.query.filters = [];
    this.query.perPage = perPage;
  }

  sendQuery(pageNumber: number) {
    this.query.page = pageNumber;

    this.searchService.publicRequestSearch(this.query).subscribe(res => {
      this.requests = res.reqList;
      console.log(this.requests);
      this.currentPage = this.query.page;
      this.totalItems = res.numItems;
      this.calcPagination();
    });
  }

  calcPagination() {
    const remainder = this.totalItems % this.defaultPerPage;
    const quotient = (this.totalItems - remainder) / this.defaultPerPage;
    this.totalPages = remainder === 0 ? quotient : quotient + 1;

    this.pageStart = 1 + ((this.currentPage - 1) * this.defaultPerPage);
    this.pageEnd = this.currentPage * this.defaultPerPage < this.totalItems ? this.currentPage * this.defaultPerPage : this.totalItems;
  }

  searchChange() {
    this.query.search = this.search.trim();
  }

  setFilters() {
    this.query.filters = [];
    if (this.status !== undefined) {
      const statusFilter = {} as Filter;
      statusFilter.type = 'by_value';
      statusFilter.field = 'status';
      statusFilter.value = this.status;
      this.query.filters.push(statusFilter);
    }
    if (this.department !== undefined) {
      const departmentFilter = {} as Filter;
      departmentFilter.type = 'by_value';
      departmentFilter.field = 'department';
      departmentFilter.value = this.department;
      this.query.filters.push(departmentFilter);
    }
    if (this.createFromDate !== undefined || this.createToDate !== undefined) {
      const createDateFilter = {} as Filter;
      createDateFilter.type = 'by_range';
      createDateFilter.field = 'create_dt';
      if (this.createFromDate !== undefined) {
        createDateFilter.from = serializeDate(this.createFromDate);
      }
      if (this.createToDate !== undefined) {
        createDateFilter.to = serializeDate(this.createToDate);
      }
      this.query.filters.push(createDateFilter);
    }
    if (this.dueFromDate !== undefined || this.dueToDate !== undefined) {
      const dueDateFilter = {} as Filter;
      dueDateFilter.type = 'by_range';
      dueDateFilter.field = 'due_dt';
      if (this.dueFromDate !== undefined) {
        dueDateFilter.from = serializeDate(this.dueFromDate);
      }
      if (this.dueToDate !== undefined) {
        dueDateFilter.to = serializeDate(this.dueToDate);
      }
      this.query.filters.push(dueDateFilter);
    }
    this.sendQuery(this.currentPage);
  }

  clearCreateFrom() {
    this.createFromDate = undefined;
    this.setFilters();
  }

  clearCreateTo() {
    this.createToDate = undefined;
    this.setFilters();
  }

  clearDueDateTo() {
    this.dueToDate = undefined;
    this.setFilters();
  }

  clearDueDateFrom() {
    this.dueFromDate = undefined;
    this.setFilters();
  }

  clearSearch() {
    this.search = undefined;
    this.searchChange();
  }

  clearFilters() {
    this.initQuery(this.defaultPerPage);
    this.search = undefined;
    this.status = undefined;
    this.department = undefined;
    this.createToDate = undefined;
    this.createFromDate = undefined;
    this.dueToDate = undefined;
    this.dueFromDate = undefined;
  }

  processEvent(event) {
    this.router.navigate(['viewRequest', event.data.requestId]);
  }
}
