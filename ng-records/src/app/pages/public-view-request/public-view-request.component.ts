import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Request } from 'src/app/models/request';
import { RequestService } from 'src/app/services/request.service';
import { TextService } from 'src/app/services/text.service';
import { AnalyticsService } from 'src/app/services/analytics.service';

@Component({
  selector: 'app-public-view-request',
  templateUrl: './public-view-request.component.html',
  styleUrls: ['./public-view-request.component.scss'],
})
export class PublicViewRequestComponent implements OnInit {
  request: Request;

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestService,
    public text: TextService,
    private analyticsService: AnalyticsService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const requestId = params['requestId'];
      this.requestService.getPublicRequest(requestId).subscribe((res) => {
        this.request = res;
      });
    });
  }
  tabChange(event) {
    this.analyticsService.navigateAnchor(event.tab.textLabel);
  }
}
