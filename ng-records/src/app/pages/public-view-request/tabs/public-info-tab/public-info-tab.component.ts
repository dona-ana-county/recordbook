import { Component, OnInit, Input } from '@angular/core';
import { Request } from 'src/app/models/request';
import { RequestStoreService } from 'src/app/services/request-store.service';
import { TextService } from 'src/app/services/text.service';
import { MatDialog } from '@angular/material/dialog';
import { RequestorEnterPasscodeComponent } from 'src/app/dialogs/requestor-enter-passcode/requestor-enter-passcode.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-public-info-tab',
  templateUrl: './public-info-tab.component.html',
  styleUrls: ['./public-info-tab.component.scss'],
})
export class PublicInfoTabComponent implements OnInit {
  @Input() request: Request;

  constructor(
    private requestStore: RequestStoreService,
    public text: TextService,
    public dialog: MatDialog,
    private router: Router,
  ) {}

  ngOnInit() {}

  openPasscodeDialog(): void {
    const dialogRef = this.dialog.open(RequestorEnterPasscodeComponent, {
      width: '800px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe((pass) => {
      if (pass) {
        this.router.navigate(['viewRequest/', this.request.id, pass]);
      }
    });
  }
}
