import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicInfoTabComponent } from './public-info-tab.component';

describe('PublicInfoTabComponent', () => {
  let component: PublicInfoTabComponent;
  let fixture: ComponentFixture<PublicInfoTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicInfoTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicInfoTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
