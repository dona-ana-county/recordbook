import { Component, OnInit, Input } from '@angular/core';
import { Request } from 'src/app/models/request';
import { ReleaseTypeLookup } from 'src/app/models/constant';
import { ReleaseType } from 'src/app/models/content';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { AnalyticsService } from 'src/app/services/analytics.service';

@Component({
  selector: 'app-public-content-tab',
  templateUrl: './public-content-tab.component.html',
  styleUrls: ['./public-content-tab.component.scss'],
})
export class PublicContentTabComponent implements OnInit {
  @Input() request: Request;
  active = 'documents';
  releaseTypes = new ReleaseTypeLookup();
  releaseType: ReleaseType;

  constructor(public text: TextService, private constantService: ConstantService, private analyticsService: AnalyticsService) {}

  ngOnInit() {
    this.constantService.getAllReleaseTypes().subscribe((res) => {
      this.releaseTypes.items = res;
      this.releaseType = this.releaseTypes.getReleaseType('PUB');
    });
  }

  showTab(tab: string) {
    if (this.active !== tab) {
      this.active = tab;
      this.analyticsService.navigateAnchor('responseTab-' + tab);
    }
  }
}
