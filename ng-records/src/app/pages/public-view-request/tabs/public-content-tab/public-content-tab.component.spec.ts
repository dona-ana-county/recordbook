import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicContentTabComponent } from './public-content-tab.component';

describe('PublicContentTabComponent', () => {
  let component: PublicContentTabComponent;
  let fixture: ComponentFixture<PublicContentTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicContentTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicContentTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
