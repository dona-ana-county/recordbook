import { Component, OnInit, Input } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { MatDialog } from '@angular/material/dialog';
import { AddLinkDialogComponent } from 'src/app/dialogs/add-link-dialog/add-link-dialog.component';
import { ContentService } from 'src/app/services/content.service';
import { ContentType, ReleaseType, Link } from 'src/app/models/content';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-public-links',
  templateUrl: './public-links.component.html',
  styleUrls: ['./public-links.component.scss'],
})
export class PublicLinksComponent implements OnInit {
  @Input() request: Request;

  headers: SteeleHeader[] = [
    {
      name: 'name',
      key: 'name',
      type: 'text',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
    {
      name: 'link',
      key: 'link',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false,
    },
  ];

  rows: any[] = [];
  links: Link[] = [];

  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    public text: TextService,
    private constantService: ConstantService,
    private requestService: RequestService,
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService.getPublicLinks(this.request.id).subscribe((res: Link[]) => {
      this.links = res;
    });
  }

  openLinkDialog(): void {
    const dialogData: any = {
      requestId: 'hi',
    };

    const dialogRef = this.dialog.open(AddLinkDialogComponent, {
      width: '800px',
      height: '500px',
      data: {
        request: dialogData,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  goToUrl(link: string) {
    window.open(link, '_blank');
  }
}
