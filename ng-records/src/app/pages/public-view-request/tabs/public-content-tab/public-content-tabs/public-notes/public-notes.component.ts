import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Note } from 'src/app/models/content';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { RequestService } from 'src/app/services/request.service';


@Component({
  selector: 'app-public-notes',
  templateUrl: './public-notes.component.html',
  styleUrls: ['./public-notes.component.scss']
})
export class PublicNotesComponent implements OnInit {

  @Input() request: Request;
  notes: Note[] = [];

  constructor(
    public dialog: MatDialog,
    public text: TextService,
    private requestService: RequestService
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService
      .getPublicNotes(this.request.id)
      .subscribe((res: Note[]) => {
        this.notes = res;
        // console.log(this.notes);
      });
  }

}
