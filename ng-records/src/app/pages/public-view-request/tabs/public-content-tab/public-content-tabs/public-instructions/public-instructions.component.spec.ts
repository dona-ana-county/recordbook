import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicInstructionsComponent } from './public-instructions.component';

describe('PublicInstructionsComponent', () => {
  let component: PublicInstructionsComponent;
  let fixture: ComponentFixture<PublicInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
