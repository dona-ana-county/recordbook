import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { SteeleHeader } from 'src/app/components/steele-table/header';

import { ContentService } from 'src/app/services/content.service';
import { TextService } from 'src/app/services/text.service';
import { RequestService } from 'src/app/services/request.service';

import { Resource } from 'src/app/models/content';
import { Request } from 'src/app/models/request';

@Component({
  selector: 'app-public-documents',
  templateUrl: './public-documents.component.html',
  styleUrls: ['./public-documents.component.scss']
})
export class PublicDocumentsComponent implements OnInit {
  subscriptions = [] as Subscription[];

  @Input() request: Request;

  documents = [] as Resource[];
  fileData: File = null;
  title: string;
  needsReview: boolean;

  headers: SteeleHeader[] = [
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'date_uploaded',
      key: 'createDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'type',
      key: 'extension',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'download',
      key: 'download',
      type: 'icon',
      label: 'cloud_download',
      hasAction: true,
      hasLink: false,
      isEditable: false
    }
  ];
  rows: any[] = [];

  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    public text: TextService,
    private requestService: RequestService
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService.getPublicResources(this.request.id).subscribe(res => {
      this.documents = res;
    });
  }

  download(resource) {
    this.contentService.downloadFile(resource);
  }
}
