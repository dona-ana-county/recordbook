import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicViewRequestComponent } from './public-view-request.component';

describe('PublicViewRequestComponent', () => {
  let component: PublicViewRequestComponent;
  let fixture: ComponentFixture<PublicViewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicViewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicViewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
