import { Component, OnInit, Input } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { Resource } from 'src/app/models/content';
import { Router } from '@angular/router';
import { TextService } from 'src/app/services/text.service';
import { ResourceQueueItem } from 'src/app/models/queue-item';

@Component({
  selector: 'app-legal-queue-panel',
  templateUrl: './legal-queue-panel.component.html',
  styleUrls: ['./legal-queue-panel.component.scss'],
})
export class LegalQueuePanelComponent implements OnInit {
  @Input() legalQueue: ResourceQueueItem[];
  headers: SteeleHeader[] = [
    {
      name: 'title',
      key: 'title',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false,
    },
    {
      name: 'request_id',
      key: 'requestId',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false,
    },
    {
      name: 'upload_date',
      key: 'uploadDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
  ];

  constructor(private router: Router, public text: TextService) {}

  ngOnInit() {
    this.headers.push({
      name: 'status',
      key: 'status',
      type: 'status',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    });
  }

  processEvent(event) {
    if (event.key === 'requestId') {
      this.router.navigate(['requestManagement', event.data.requestId]);
    } else if (event.key === 'title') {
      this.router.navigate(['legalReviewManagement', event.data.id]);
    }
  }
}
