import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalQueuePanelComponent } from './legal-queue-panel.component';

describe('LegalQueuePanelComponent', () => {
  let component: LegalQueuePanelComponent;
  let fixture: ComponentFixture<LegalQueuePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalQueuePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalQueuePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
