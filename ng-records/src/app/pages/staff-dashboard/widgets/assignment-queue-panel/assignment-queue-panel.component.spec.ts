import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentQueuePanelComponent } from './assignment-queue-panel.component';

describe('AssignmentQueuePanelComponent', () => {
  let component: AssignmentQueuePanelComponent;
  let fixture: ComponentFixture<AssignmentQueuePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentQueuePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentQueuePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
