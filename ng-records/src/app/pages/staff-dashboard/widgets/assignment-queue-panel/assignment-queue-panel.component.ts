import { Component, OnInit, Input } from '@angular/core';
import { Staff } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { QueueService } from 'src/app/services/queue.service';
import { Router } from '@angular/router';
import { RequestQueueItem } from 'src/app/models/queue-item';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { TextService } from 'src/app/services/text.service';
import { validateAdmin, validateClerk } from 'src/utils/validateStaff';

@Component({
  selector: 'app-assignment-queue-panel',
  templateUrl: './assignment-queue-panel.component.html',
  styleUrls: ['./assignment-queue-panel.component.scss']
})
export class AssignmentQueuePanelComponent implements OnInit {
  staff: Staff;
  isAdmin: boolean;
  isClerk: boolean;

  @Input() assignmentQueue: RequestQueueItem[];

  headers: SteeleHeader[] = [
    {
      name: 'id',
      key: 'id',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false
    },
    {
      name: 'requestor',
      key: 'requestorName',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'due_date',
      key: 'dueDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'extension_status',
      key: 'extensionStatus',
      type: 'lookup',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];

  constructor(
    private storageService: StorageService,
    private router: Router,
    public text: TextService
  ) {}

  ngOnInit() {}

  processEvent(event) {
    this.router.navigate(['requestManagement', event.data.id]);
  }
}
