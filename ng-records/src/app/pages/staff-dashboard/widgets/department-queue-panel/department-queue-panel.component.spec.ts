import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentQueuePanelComponent } from './department-queue-panel.component';

describe('DepartmentQueuePanelComponent', () => {
  let component: DepartmentQueuePanelComponent;
  let fixture: ComponentFixture<DepartmentQueuePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentQueuePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentQueuePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
