import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { QueueService } from 'src/app/services/queue.service';
import { Router } from '@angular/router';
import { Department } from 'src/app/models/department';
import { DepartmentService } from 'src/app/services/department.service';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { TextService } from 'src/app/services/text.service';
import { ViewEncapsulation } from '@angular/core';
import { RequestQueueItem } from 'src/app/models/queue-item';

@Component({
  selector: 'app-department-queue-panel',
  templateUrl: './department-queue-panel.component.html',
  styleUrls: ['./department-queue-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DepartmentQueuePanelComponent implements OnInit {
  @Input() departmentQueue: RequestQueueItem[];
  @Input() department: Department;
  @Input() isAdmin: boolean;

  // For IPRA ADMINS
  departments: Department[];
  selectedDepartment: string;
  showDropdown = false;
  queueLoaded = false;

  headers: SteeleHeader[] = [
    {
      name: 'id',
      key: 'id',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false
    },
    {
      name: 'requestor',
      key: 'requestorName',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'due_date',
      key: 'dueDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'extension_status',
      key: 'extensionStatus',
      type: 'lookup',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];

  constructor(
    private router: Router,
    public text: TextService,
    private departmentService: DepartmentService,
    private queueService: QueueService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.selectedDepartment = this.storageService.getLastDepartmentQueueSelected(
      this.department.id
    );
    this.loadQueue(this.selectedDepartment);
    this.departmentService.getDepartments().subscribe(res => {
      this.departments = res;
      this.queueLoaded = true;
    });
  }

  onDepartmentChange(event) {
    this.queueLoaded = false;
    this.loadQueue(event.value);
  }

  loadQueue(selectedQueue: string) {
    if (selectedQueue === 'all') {
      this.storageService.setLastDepartmentQueueSelected('all');
      this.queueService.getAllDepartmentQueues().subscribe(res => {
        this.departmentQueue = res;
        this.queueLoaded = true;
      });
    } else {
      this.selectedDepartment = selectedQueue;
      this.storageService.setLastDepartmentQueueSelected(this.selectedDepartment);
      this.queueService.getDepartmentQueue(this.selectedDepartment).subscribe(res => {
        this.departmentQueue = res;
        this.queueLoaded = true;
      });
    }
  }

  processEvent(event) {
    this.router.navigate(['requestManagement', event.data.id]);
  }
}
