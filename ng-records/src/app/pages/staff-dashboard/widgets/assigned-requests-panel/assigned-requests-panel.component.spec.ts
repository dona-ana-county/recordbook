import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedRequestsPanelComponent } from './assigned-requests-panel.component';

describe('AssignedRequestsPanelComponent', () => {
  let component: AssignedRequestsPanelComponent;
  let fixture: ComponentFixture<AssignedRequestsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedRequestsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedRequestsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
