import { Component, OnInit, Input } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { TextService } from 'src/app/services/text.service';
import { Router } from '@angular/router';
import { RequestQueueItem } from 'src/app/models/queue-item';

@Component({
  selector: 'app-assigned-requests-panel',
  templateUrl: './assigned-requests-panel.component.html',
  styleUrls: ['./assigned-requests-panel.component.scss']
})
export class AssignedRequestsPanelComponent implements OnInit {
  headers: SteeleHeader[] = [
    {
      name: 'id',
      key: 'id',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false
    },
    {
      name: 'requestor',
      key: 'requestorName',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'due_date',
      key: 'dueDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'extension_status',
      key: 'extensionStatus',
      type: 'lookup',
      hasAction: false,
      hasLink: false,
      isEditable: false
    },
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false
    }
  ];
  @Input() assignedRequests: RequestQueueItem[];
  constructor(public text: TextService, private router: Router) {}

  ngOnInit() {}

  processEvent(event) {
    this.router.navigate(['requestManagement', event.data.id]);
  }
}
