import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { QueueService } from 'src/app/services/queue.service';
import { StorageService } from 'src/app/services/storage.service';
import { Staff } from 'src/app/models/user';
import { DepartmentService } from 'src/app/services/department.service';
import { Department } from 'src/app/models/department';
import { ToastrService } from 'ngx-toastr';
import { Resource } from 'src/app/models/content';
import { ResourceQueueItem, RequestQueueItem } from 'src/app/models/queue-item';
import { UserService } from 'src/app/services/user.service';
import {
  validateAdmin,
  validateClerk,
  validateLegal
} from 'src/utils/validateStaff';

@Component({
  selector: 'app-staff-dashboard',
  templateUrl: './staff-dashboard.component.html',
  styleUrls: ['./staff-dashboard.component.scss']
})
export class StaffDashboardComponent implements OnInit {
  staff: Staff;
  department: Department;
  isAdmin: boolean;
  isClerk: boolean;
  isWorker: boolean;
  isLegal: boolean;
  isManager: boolean;
  assignmentQueue: RequestQueueItem[];
  departmentQueue: RequestQueueItem[];
  legalQueue: ResourceQueueItem[];
  assignedRequests: RequestQueueItem[];

  constructor(
    private authService: AuthService,
    private router: Router,
    private queueService: QueueService,
    private departmentService: DepartmentService,
    private userService: UserService,
    private storageService: StorageService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.validateStaff();

    if (this.isAdmin || this.isClerk) {
      this.queueService.getAssignmentQueue().subscribe(res => {
        this.assignmentQueue = res;
      });
    }
    if (this.isManager) {
      this.departmentService
        .getDepartmentByStaff(this.staff.id)
        .subscribe((res: Department) => {
          this.department = res;
          this.queueService
            .getDepartmentQueue(this.department.id)
            .subscribe(data => {
              this.departmentQueue = data;
            });
        });
    }
    if (this.isWorker) {
      // get assigned requests
      this.queueService.getStaffAssignedQueue(this.staff.id).subscribe(res => {
        this.assignedRequests = res;
      });
    }
    if (this.isLegal) {
      // getting legal Queue
      this.queueService.getLegalQueue().subscribe(res => {
        this.legalQueue = res;
      });
    }
  }
  validateStaff() {
    this.isAdmin = validateAdmin(this.staff);
    this.isClerk = validateClerk(this.staff);
    this.isLegal = validateLegal(this.staff);
    if (this.staff.role.code === 'WRK') {
      this.isWorker = true;
    }

    if (this.staff.role.code === 'MAN') {
      this.isManager = true;
    }
  }
  changeRoute(page: string) {
    this.router.navigate([page]);
  }
}
