import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicNewRequestComponent } from './public-new-request.component';

describe('PublicNewRequestComponent', () => {
  let component: PublicNewRequestComponent;
  let fixture: ComponentFixture<PublicNewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicNewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicNewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
