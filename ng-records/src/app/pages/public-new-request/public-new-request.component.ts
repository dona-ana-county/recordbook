import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Requestor, ContactInfo, ContactType } from 'src/app/models/user';
import { Request, PublicRequest } from 'src/app/models/request';
import { RequestService } from 'src/app/services/request.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { TextService } from 'src/app/services/text.service';
import { textTransform } from 'src/utils/textTransform';
import stateJSON from 'src/utils/stateList.json';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';
import { ConstantService } from 'src/app/services/constant.service';
import { ValidateState } from 'src/utils/validateState';
import { State } from 'src/app/models/constant';

@Component({
  selector: 'app-public-new-request',
  templateUrl: './public-new-request.component.html',
  styleUrls: ['./public-new-request.component.scss']
})
export class PublicNewRequestComponent implements OnInit {
  @ViewChild('title') titleRef: ElementRef;
  @ViewChild('description') descriptionRef: ElementRef;
  @ViewChild('firstName') firstNameRef: ElementRef;
  @ViewChild('lastName') lastNameRef: ElementRef;
  @ViewChild('phoneNumber') phoneNumberRef: ElementRef;
  @ViewChild('address1') address1Ref: ElementRef;
  @ViewChild('address2') address2Ref: ElementRef;
  @ViewChild('city') cityRef: ElementRef;
  @ViewChild('zip') zipRef: ElementRef;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  isOptional = false;
  submitted = false;
  inprocess = false;

  states: string[] = [];
  stateObjects: State[] = stateJSON;
  stateControl = new FormControl();

  filteredOptions: Observable<string[]>;
  reCaptchaToken = '';
  contactTypes: ContactType[];
  siteKey = '' ;

  backendValidationError = false;

  constructor(
    private requestService: RequestService,
    private _formBuilder: FormBuilder,
    private router: Router,
    public text: TextService,
    private storageService: StorageService,
    private constantService: ConstantService
  ) {}

  ngOnInit() {
    for (const state of stateJSON) {
      this.states.push(state.name);
    }

    this.contactTypes = this.constantService.contactTypes;

    if (this.storageService.getStaff()) {
      this.router.navigate(['staffRequest']);
    }

    this.firstFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      secondary_emails: ['', this.emailListValidator()],
      address: ['', Validators.required],
      address2: [''],
      city: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
      state: ['New Mexico', [Validators.required, ValidateState]],
      phone: [
        '',
        [Validators.required, Validators.pattern('[0-9]{3}-[0-9]{3}-[0-9]{4}')]
      ]
    });

    this.thirdFormGroup = this._formBuilder.group({
      recaptchaReactive: ['', Validators.required]
    });

    this.siteKey = this.constantService.config.recaptchaSiteKey;
  }

  resolvedRecaptcha(captchaResponse: string) {
    this.reCaptchaToken = captchaResponse;
  }

  submitRequest() {
    this.inprocess = true;
  
    const contactInfo: ContactInfo = {
      email: this.secondFormGroup.controls.email.value,
      phone: this.phoneNumberRef.nativeElement.value,
      address1: this.address1Ref.nativeElement.value,
      address2: this.address2Ref.nativeElement.value,
      city: this.cityRef.nativeElement.value,
      state: this.secondFormGroup.controls.state.value,
      postalCode: this.zipRef.nativeElement.value,
      secondary_emails: this.secondFormGroup.controls.secondary_emails.value.split(',').map((email: string) => email.trim())
    };
  
    const newRequestor: Requestor = {
      id: undefined,
      primaryContact: this.constantService.getContactType('EML'),
      firstName: this.firstNameRef.nativeElement.value,
      lastName: this.lastNameRef.nativeElement.value,
      contactInfo
    };
    const formattedDesc = textTransform(
      this.descriptionRef.nativeElement.value
    );
    const newRequest: Request = {
      id: undefined,
      title: this.titleRef.nativeElement.value,
      description: formattedDesc,
      createDate: null,
      primaryDepartment: null,
      secondaryDepartments: null,
      status: null,
      dueDate: null,
      requestor: newRequestor
    };
    const newPublicRequest: PublicRequest = {
      request: newRequest,
      reCaptchaToken: this.reCaptchaToken
    };
  
    this.requestService.newPublicRequest(newPublicRequest).subscribe(res => {
      if (res.success === 1) {
        this.backendValidationError = false;
        this.submitted = true;
      } else {
        this.inprocess = false;
        if (res.msg === 'Not a valid email address.') {
            this.backendValidationError = true;
          }
      }
    });
  }

  changeRoute(route: string) {
    if (route === 'home') {
      this.router.navigate(['home']);
    } else {
      window.open('https://donaanacounty.org', '_self');
    }
  }

  onSelectionChange(event) {
    if (this.backendValidationError) {
      this.backendValidationError = false;
    }
  }

  emailListValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const emails = control.value.split(',').map((email: string) => email.trim());
      const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
      const invalidEmails = emails.filter((email: string) => !emailPattern.test(email));
      return invalidEmails.length > 0 ? { 'invalidEmailList': { value: control.value } } : null;
    };
  }

  onKeyDown(e) {

    // Prevent non-digit input.
    if (
      // Allow: Delete, Backspace, Tab, Escape, Enter
      [46, 8, 9, 27, 13].indexOf(e.keyCode) !== -1 ||
      (e.keyCode === 65 && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.keyCode === 67 && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.keyCode === 86 && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.keyCode === 88 && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.keyCode === 65 && e.metaKey === true) || // Cmd+A (Mac)
      (e.keyCode === 67 && e.metaKey === true) || // Cmd+C (Mac)
      (e.keyCode === 86 && e.metaKey === true) || // Cmd+V (Mac)
      (e.keyCode === 88 && e.metaKey === true) || // Cmd+X (Mac)
      (e.keyCode >= 35 && e.keyCode <= 39) // Home, End, Left, Right
    ) {
      return;  // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }

    const key = e.charCode || e.keyCode || 0;
    if (key !== 8 && key !== 9) {
      if (this.phoneNumberRef.nativeElement.value.length === 3) {
        this.phoneNumberRef.nativeElement.value =
          this.phoneNumberRef.nativeElement.value + '-';
      }
      if (this.phoneNumberRef.nativeElement.value.length === 7) {
        this.phoneNumberRef.nativeElement.value =
          this.phoneNumberRef.nativeElement.value + '-';
      }
    }
  }
}
