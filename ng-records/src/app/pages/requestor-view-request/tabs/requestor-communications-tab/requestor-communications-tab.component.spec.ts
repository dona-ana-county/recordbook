import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorCommunicationsTabComponent } from './requestor-communications-tab.component';

describe('RequestorCommunicationsTabComponent', () => {
  let component: RequestorCommunicationsTabComponent;
  let fixture: ComponentFixture<RequestorCommunicationsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorCommunicationsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorCommunicationsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
