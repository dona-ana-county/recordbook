import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { Staff, User } from 'src/app/models/user';
import { CommunicationService } from 'src/app/services/communication.service';
import { StorageService } from 'src/app/services/storage.service';
import { Comment } from 'src/app/models/communication';
import { Request, RequestStatus } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { RequestService } from 'src/app/services/request.service';


@Component({
  selector: 'app-requestor-communications-tab',
  templateUrl: './requestor-communications-tab.component.html',
  styleUrls: ['./requestor-communications-tab.component.scss'],
})
export class RequestorCommunicationsTabComponent implements OnInit, AfterViewInit {
  @Input() request: Request;
  alerts = [];
  comments: Comment[];
  newComment: string;
  @Input() status: RequestStatus;
  @ViewChild('messageArea') messageAreaRef: ElementRef;
  staff: Staff;

  constructor(
    private commService: CommunicationService,
    private storageService: StorageService,
    private requestService: RequestService,
    public text: TextService,
  ) {}

  ngOnInit() {
    this.staff = this.storageService.getStaff();
    this.requestService
      .getRequestorComments(this.request.id, this.request.passcode)
      .subscribe((comments: Comment[]) => {
        this.comments = comments;
      });
  }

  ngAfterViewInit() {
    const messageArea: HTMLDivElement = this.messageAreaRef.nativeElement;
    messageArea.scrollTop = this.messageAreaRef.nativeElement.scrollHeight;
  }

  sendComment() {
    const comment = {} as Comment;
    comment.message = this.newComment;
    comment.commenter = {
      id: this.request.requestor.id,
      firstName: this.request.requestor.firstName,
      lastName: this.request.requestor.lastName,
    };
    this.commService
      .requestorNewComment(this.request.id, comment, this.request.passcode)
      .subscribe((res) => {
        this.comments.push(res);
        this.newComment = null;
      });
  }

  isRequestor(comment: Comment): boolean {
    if (comment.commenter.id === this.request.requestor.id) {
      return true;
    }
    return false;
  }
}
