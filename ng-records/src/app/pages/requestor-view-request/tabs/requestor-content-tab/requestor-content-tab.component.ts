import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReleaseType } from 'src/app/models/content';
import { Request } from 'src/app/models/request';
import { AnalyticsService } from 'src/app/services/analytics.service';
import { ConstantService } from 'src/app/services/constant.service';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-requestor-content-tab',
  templateUrl: './requestor-content-tab.component.html',
  styleUrls: ['./requestor-content-tab.component.scss'],
})
export class RequestorContentTabComponent implements OnInit {
  @Input() request: Request;
  active = 'documents';
  releaseType: ReleaseType;

  constructor(
    public text: TextService,
    private constantService: ConstantService,
    private analyticsService: AnalyticsService,
    private route: ActivatedRoute,
  ) {
    this.route.fragment.subscribe({next: (fragment) => {
      if (fragment && fragment.indexOf('.') !== -1) {
        fragment = fragment.split('.', 2)[1];
        setTimeout(() => {
          this.showTab(fragment);
        }, 0);
      }
    }});


  }

  ngOnInit() {
    this.releaseType = this.constantService.getReleaseType('REQ');
  }

  showTab(tab: string) {
    if (this.active !== tab) {
      this.active = tab;
      this.analyticsService.navigateAnchor('responseTab.' + tab);
    }
  }
}
