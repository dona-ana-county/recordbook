import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorLinksComponent } from './requestor-links.component';

describe('RequestorLinksComponent', () => {
  let component: RequestorLinksComponent;
  let fixture: ComponentFixture<RequestorLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
