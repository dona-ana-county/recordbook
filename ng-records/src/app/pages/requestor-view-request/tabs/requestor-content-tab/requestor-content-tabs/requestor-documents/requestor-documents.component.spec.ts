import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorDocumentsComponent } from './requestor-documents.component';

describe('RequestorDocumentsComponent', () => {
  let component: RequestorDocumentsComponent;
  let fixture: ComponentFixture<RequestorDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
