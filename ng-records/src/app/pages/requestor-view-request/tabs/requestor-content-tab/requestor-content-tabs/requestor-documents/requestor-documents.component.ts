import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Resource, ContentType, ReleaseType } from 'src/app/models/content';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { MatDialog } from '@angular/material/dialog';
import { ContentService } from 'src/app/services/content.service';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-requestor-documents',
  templateUrl: './requestor-documents.component.html',
  styleUrls: ['./requestor-documents.component.scss'],
})
export class RequestorDocumentsComponent implements OnInit {
  fileData: File = null;
  @Input() request: Request;
  subscriptions: Subscription[] = [];
  title: string;
  needsReview: boolean;
  documents: Resource[] = [];

  headers: SteeleHeader[] = [
    {
      name: 'title',
      key: 'title',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
    {
      name: 'date_uploaded',
      key: 'createDate',
      type: 'date',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
    {
      name: 'type',
      key: 'extension',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
    {
      name: 'download',
      key: 'download',
      type: 'icon',
      label: 'cloud_download',
      hasAction: true,
      hasLink: false,
      isEditable: false,
    },
  ];
  rows: any[] = [];
  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    public text: TextService,
    private constantService: ConstantService,
    private requestService: RequestService,
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService
      .getRequestorResources(this.request.id, this.request.passcode)
      .subscribe((r) => {
        this.documents = r;
      });
  }

  download(resource) {
    this.contentService.downloadRequestorFile(resource, this.request.passcode);
  }
}
