import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorInstructionsComponent } from './requestor-instructions.component';

describe('RequestorInstructionsComponent', () => {
  let component: RequestorInstructionsComponent;
  let fixture: ComponentFixture<RequestorInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
