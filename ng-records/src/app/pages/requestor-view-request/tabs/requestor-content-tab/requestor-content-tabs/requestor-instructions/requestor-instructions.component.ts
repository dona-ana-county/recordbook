import { Component, OnInit, Input } from '@angular/core';
import { Instruction} from 'src/app/models/content';
import { MatDialog } from '@angular/material/dialog';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-requestor-instructions',
  templateUrl: './requestor-instructions.component.html',
  styleUrls: ['./requestor-instructions.component.scss']
})
export class RequestorInstructionsComponent implements OnInit {
  @Input() request: Request;
  instructions: Instruction[] = [];

  constructor(
    public dialog: MatDialog,
    public text: TextService,
    private requestService: RequestService
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService
      .getRequestorInstructions(this.request.id, this.request.passcode)
      .subscribe((res: Instruction[]) => {
        this.instructions = res;
       // console.log('inst', this.instructions);
      });
  }
}
