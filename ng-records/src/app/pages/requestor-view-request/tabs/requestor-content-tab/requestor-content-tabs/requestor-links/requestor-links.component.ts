import { Component, OnInit, Input } from '@angular/core';
import { SteeleHeader } from 'src/app/components/steele-table/header';
import { Link, ContentType, ReleaseType } from 'src/app/models/content';
import { MatDialog } from '@angular/material/dialog';
import { ContentService } from 'src/app/services/content.service';
import { AddLinkDialogComponent } from 'src/app/dialogs/add-link-dialog/add-link-dialog.component';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { ConstantService } from 'src/app/services/constant.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-requestor-links',
  templateUrl: './requestor-links.component.html',
  styleUrls: ['./requestor-links.component.scss'],
})
export class RequestorLinksComponent implements OnInit {
  @Input() request: Request;
  headers: SteeleHeader[] = [
    {
      name: 'name',
      key: 'name',
      type: 'text',
      hasAction: false,
      hasLink: false,
      isEditable: false,
    },
    {
      name: 'link',
      key: 'link',
      type: 'link',
      hasAction: false,
      hasLink: true,
      isEditable: false,
    },
  ];
  rows: any[] = [];
  links: Link[] = [];

  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    public text: TextService,
    private constantService: ConstantService,
    private requestService: RequestService,
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService
      .getRequestorLinks(this.request.id, this.request.passcode)
      .subscribe((res: Link[]) => {
        this.links = res;
      });
  }

  goToUrl(link: string) {
    window.open(link, '_blank');
  }
}
