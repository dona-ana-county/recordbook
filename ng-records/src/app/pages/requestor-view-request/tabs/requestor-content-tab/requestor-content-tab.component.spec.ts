import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorContentTabComponent } from './requestor-content-tab.component';

describe('RequestorContentTabComponent', () => {
  let component: RequestorContentTabComponent;
  let fixture: ComponentFixture<RequestorContentTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorContentTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorContentTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
