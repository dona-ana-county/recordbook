import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorNotesComponent } from './requestor-notes.component';

describe('RequestorNotesComponent', () => {
  let component: RequestorNotesComponent;
  let fixture: ComponentFixture<RequestorNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
