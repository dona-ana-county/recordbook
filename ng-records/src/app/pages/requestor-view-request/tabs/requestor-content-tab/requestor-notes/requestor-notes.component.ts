import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Note} from 'src/app/models/content';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-requestor-notes',
  templateUrl: './requestor-notes.component.html',
  styleUrls: ['./requestor-notes.component.scss']
})
export class RequestorNotesComponent implements OnInit {
  @Input() request: Request;
  notes: Note[] = [];

  constructor(
    public dialog: MatDialog,
    public text: TextService,
    private requestService: RequestService
  ) {}

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.requestService
      .getRequestorNotes(this.request.id, this.request.passcode)
      .subscribe((res: Note[]) => {
        this.notes = res;
        // console.log(this.notes);
      });
  }
}
