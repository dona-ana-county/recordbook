import { Component, OnInit, Input } from '@angular/core';
import { Department } from 'src/app/models/department';
import { Request } from 'src/app/models/request';
import { MatDialog } from '@angular/material/dialog';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-requestor-info-tab',
  templateUrl: './requestor-info-tab.component.html',
  styleUrls: ['./requestor-info-tab.component.scss']
})
export class RequestorInfoTabComponent implements OnInit {
  @Input() request: Request;

  constructor(
    public dialog: MatDialog,
    public text: TextService
  ) {}

  ngOnInit() {}
}
