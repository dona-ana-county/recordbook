import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorInfoTabComponent } from './requestor-info-tab.component';

describe('RequestorInfoTabComponent', () => {
  let component: RequestorInfoTabComponent;
  let fixture: ComponentFixture<RequestorInfoTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorInfoTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorInfoTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
