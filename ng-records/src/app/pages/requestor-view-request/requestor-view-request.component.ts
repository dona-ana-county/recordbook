import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { Request } from 'src/app/models/request';
import { AnalyticsService } from 'src/app/services/analytics.service';
import { RequestService } from 'src/app/services/request.service';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-requestor-view-request',
  templateUrl: './requestor-view-request.component.html',
  styleUrls: ['./requestor-view-request.component.scss'],
})
export class RequestorViewRequestComponent implements OnInit {
  request: Request;

  hasEmail = false;

  @ViewChild(MatTabGroup) set setTabGroup(tabGroup: MatTabGroup) {
    if (tabGroup) {
      this.route.fragment.subscribe({next: ( fragment ) => {
        if (fragment ) {
          fragment = fragment.split('.', 1)[0];
          const tabs = tabGroup._tabs.toArray();
          for (let i = 0; i < tabs.length; i++) {
            if (tabs[i].textLabel === fragment) {
              setTimeout( () => {
                this.selectedIndex = i;
              }, 0);
              break;
            }
          }
        }
      }});
    }
  }

  selectedIndex: number;

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestService,
    private router: Router,
    public text: TextService,
    private analyticsService: AnalyticsService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const requestId = params['requestId'];
      const passCode = params['passCode'];
      this.requestService.getRequestorRequest(requestId, passCode).subscribe((res: any) => {
        if (res.error) {
          this.router.navigate(['viewRequest/', requestId], {replaceUrl: true});
        }
        this.request = res;

        if (this.request && this.request.requestor
          && this.request.requestor.primaryContact
          && this.request.requestor.primaryContact.code) {
          this.hasEmail = this.request.requestor.primaryContact.code ===  'EML';
        }
      });
    });
  }
  tabChange(event) {
    this.analyticsService.navigateAnchor(event.tab.textLabel);
  }
}
