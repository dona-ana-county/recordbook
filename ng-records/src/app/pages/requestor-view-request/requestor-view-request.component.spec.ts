import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestorViewRequestComponent } from './requestor-view-request.component';

describe('RequestorViewRequestComponent', () => {
  let component: RequestorViewRequestComponent;
  let fixture: ComponentFixture<RequestorViewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorViewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestorViewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
