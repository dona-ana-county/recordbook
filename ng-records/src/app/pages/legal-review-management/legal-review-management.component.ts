import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Resource } from 'src/app/models/content';
import { ContentService } from 'src/app/services/content.service';
import { RequestService } from 'src/app/services/request.service';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { Requestor } from 'src/app/models/user';

@Component({
  selector: 'app-legal-review-management',
  templateUrl: './legal-review-management.component.html',
  styleUrls: ['./legal-review-management.component.scss'],
})
export class LegalReviewManagementComponent implements OnInit {
  resourceId: string;
  resource: Resource;
  request: Request;

  constructor(
    private route: ActivatedRoute,
    private contentService: ContentService,
    private requestService: RequestService,
    public text: TextService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.resourceId = params['resourceId'];

      this.contentService.getResource(this.resourceId).subscribe((res: any) => {
        this.resource = res;
        this.requestService.getRequestInfo(this.resource.requestId).subscribe((r: any) => {
          this.request = r;
          this.requestService.getRequestor(this.request.id).subscribe((requestor: Requestor) => {
            this.request.requestor = requestor;
          });
        });
      });
    });
  }
}
