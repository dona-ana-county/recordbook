import { Component, OnInit, Input } from '@angular/core';
import { Request } from 'src/app/models/request';
import { TextService } from 'src/app/services/text.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request-info',
  templateUrl: './request-info.component.html',
  styleUrls: ['./request-info.component.scss']
})
export class RequestInfoComponent implements OnInit {
  @Input() request: Request;

  constructor(public text: TextService, private router: Router) {}

  ngOnInit() {}

  goToRequest(id: string) {
    this.router.navigate(['requestManagement/' + id]);
  }
}
