import { Component, OnInit, Input } from '@angular/core';
import { Resource, ReleaseType } from 'src/app/models/content';
import { MatDialog } from '@angular/material/dialog';
import { ApproveResourceDialogComponent } from 'src/app/dialogs/approve-resource-dialog/approve-resource-dialog.component';
import { ContentService } from 'src/app/services/content.service';
import { DenyResourceDialogComponent } from 'src/app/dialogs/deny-resource-dialog/deny-resource-dialog.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/services/storage.service';
import { Staff } from 'src/app/models/user';
import { UploadRevisedDialogComponent } from 'src/app/dialogs/upload-revised-dialog/upload-revised-dialog.component';
import { TextService } from 'src/app/services/text.service';

@Component({
  selector: 'app-resource-panel',
  templateUrl: './resource-panel.component.html',
  styleUrls: ['./resource-panel.component.scss']
})
export class ResourcePanelComponent implements OnInit {
  fileData: any;
  @Input() resource: Resource;
  releaseType: ReleaseType;
  underReview: boolean;

  constructor(
    public dialog: MatDialog,
    private contentService: ContentService,
    private router: Router,
    private toastrService: ToastrService,
    private storageService: StorageService,
    public text: TextService
  ) {}

  ngOnInit() {
    this.underReview = this.resource.underReview;
    this.releaseType = this.resource.releaseType;
    this.resource.releaseType = this.resource.releaseType;
  }

  openDenyDialog(): void {
    const dialogRef = this.dialog.open(DenyResourceDialogComponent, {
      width: '800px',
      height: 'auto'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const staff: Staff = this.storageService.getStaff();
        this.contentService
          .rejectResource(this.resource.id, result.reason, staff.id)
          .subscribe(res => {
            if (res) {
              this.toastrService.success('', 'Resource Successfully Rejected', {
                timeOut: 3000
              });
            }
            this.router.navigate(['dashboard']);
          });

      }
    });
  }

  openUploadDialog(): void {
    const dialogRef = this.dialog.open(UploadRevisedDialogComponent, {
      width: '800px',
      height: 'auto'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.fileData = {
        file: result.file,
        title: this.resource.title,
        status: 'Waiting',
        progress: 0
      };
      this.submitRevised();
    });
  }

  submitRevised() {
    const formData = new FormData();
    formData.append('file', this.fileData.file);
    this.fileData.status = 'Uploading';
    this.contentService.uploadRevisedFile(this.resource.id, formData).subscribe(res => {
      if (res.progress) {
        this.fileData.progress = res.progress;
        if (this.fileData.progress === 100) {
          this.fileData.status = this.text.get('finishing_up');
        }
      }
      if (res.response) {
        const response = res.response;
        if (response.error) {
          return;
        }
        this.resource = response;
        this.fileData.status = 'Done';
        this.toastrService.success(
          response.title,
          'File Successfully Updated',
          {
            timeOut: 3000
          }
        );
      }
    });
  }

  openApproveDialog(): void {
    const dialogData = {
      type: this.resource.releaseType
    };
    const dialogRef = this.dialog.open(ApproveResourceDialogComponent, {
      width: '800px',
      height: 'auto',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contentService.approveResource(this.resource.id, result.releaseType).subscribe(res => {
          if (res) {
            this.toastrService.success('', 'Document Approved', {
              timeOut: 3000
            });
          }
          this.router.navigate(['dashboard']);
        });
      }
    });
  }

  downloadResource() {
    this.contentService.downloadPrivateFile(this.resource);
  }

  toggleUnderReview() {
    this.underReview = !this.underReview;

    this.contentService.updateUnderReview(this.resource.id, this.underReview).subscribe(res => {
      if (res) {
        this.toastrService.success('', 'Success!', {
          timeOut: 3000
        });
      }
    });
  }
}
