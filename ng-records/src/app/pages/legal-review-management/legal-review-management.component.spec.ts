import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalReviewManagementComponent } from './legal-review-management.component';

describe('LegalReviewManagementComponent', () => {
  let component: LegalReviewManagementComponent;
  let fixture: ComponentFixture<LegalReviewManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalReviewManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalReviewManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
