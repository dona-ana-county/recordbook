import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TextService } from 'src/app/services/text.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    public textService: TextService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    if (this.storageService.getStaff()) {
      this.router.navigate(['dashboard']);
    }
  }

  changeRoute() {
    this.router.navigate(['newRequest']);
  }
}
