import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, ErrorHandler, Injectable, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatChipsModule } from '@angular/material/chips';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { AuthGuardService } from './services/auth-guard.service';
import { AuthInterceptor } from './services/api-interceptor';
import { RequestStoreService } from './services/request-store.service';
import { ConstantService } from './services/constant.service';
import { LocationService } from './services/location.service';

import { AppComponent } from './app.component';
import { SigninComponent } from './pages/signin/signin.component';
import { HomeComponent } from './pages/home/home.component';
import { PublicNewRequestComponent } from './pages/public-new-request/public-new-request.component';
import { StaffNewRequestComponent } from './pages/staff-new-request/staff-new-request.component';
import { StaffDashboardComponent } from './pages/staff-dashboard/staff-dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { StaffSidebarComponent } from './components/staff-sidebar/staff-sidebar.component';
import { AssignmentQueuePanelComponent } from './pages/staff-dashboard/widgets/assignment-queue-panel/assignment-queue-panel.component';
import { DepartmentQueuePanelComponent } from './pages/staff-dashboard/widgets/department-queue-panel/department-queue-panel.component';
import { RequestManagementComponent } from './pages/request-management/request-management.component';
import { DepartmentTabComponent } from './pages/request-management/tabs/department-tab/department-tab.component';
import { WorkerTabComponent } from './pages/request-management/tabs/worker-tab/worker-tab.component';
import { LogTabComponent } from './pages/request-management/tabs/log-tab/log-tab.component';
import { InfoTabComponent } from './pages/request-management/tabs/info-tab/info-tab.component';
import { SteeleTableComponent } from './components/steele-table/steele-table.component';
import { DenyRequestDialogComponent } from './dialogs/deny-request-dialog/deny-request-dialog.component';
import { AssignRequestDialogComponent } from './dialogs/assign-request-dialog/assign-request-dialog.component';
import { ExtendDueDateDialogComponent } from './dialogs/extend-due-date-dialog/extend-due-date-dialog.component';
import { UpdateDueDateDialogComponent } from './dialogs/update-due-date-dialog/update-due-date-dialog.component';
import { CloseRequestDialogComponent } from './dialogs/close-request-dialog/close-request-dialog.component';
import { StaffSearchRequestComponent } from './pages/staff-search-request/staff-search-request.component';
import { PublicSearchRequestComponent } from './pages/public-search-request/public-search-request.component';
import { ResponseTabComponent } from './pages/request-management/tabs/response-tab/response-tab.component';
import { AssignedRequestsPanelComponent } from './pages/staff-dashboard/widgets/assigned-requests-panel/assigned-requests-panel.component';
import { DocumentsComponent } from './pages/request-management/tabs/response-tab/content-tabs/documents/documents.component';
import { LinksComponent } from './pages/request-management/tabs/response-tab/content-tabs/links/links.component';
import { InstructionsComponent } from './pages/request-management/tabs/response-tab/content-tabs/instructions/instructions.component';
import { NotesComponent } from './pages/request-management/tabs/response-tab/content-tabs/notes/notes.component';
import { UploadDocumentDialogComponent } from './dialogs/upload-document-dialog/upload-document-dialog.component';
import { AddLinkDialogComponent } from './dialogs/add-link-dialog/add-link-dialog.component';
import { AddInstructionDialogComponent } from './dialogs/add-instruction-dialog/add-instruction-dialog.component';
import { AddNoteDialogComponent } from './dialogs/add-note-dialog/add-note-dialog.component';
import { CommunicationsTabComponent } from './pages/request-management/tabs/communications-tab/communications-tab.component';
import { PublicViewRequestComponent } from './pages/public-view-request/public-view-request.component';
import { PublicInfoTabComponent } from './pages/public-view-request/tabs/public-info-tab/public-info-tab.component';
import { PublicContentTabComponent } from './pages/public-view-request/tabs/public-content-tab/public-content-tab.component';
import { PublicDocumentsComponent } from './pages/public-view-request/tabs/public-content-tab/public-content-tabs/public-documents/public-documents.component';
import { PublicLinksComponent } from './pages/public-view-request/tabs/public-content-tab/public-content-tabs/public-links/public-links.component';
import { LegalQueuePanelComponent } from './pages/staff-dashboard/widgets/legal-queue-panel/legal-queue-panel.component';
import { LegalReviewManagementComponent } from './pages/legal-review-management/legal-review-management.component';
import { RequestorViewRequestComponent } from './pages/requestor-view-request/requestor-view-request.component';
import { RequestorInfoTabComponent } from './pages/requestor-view-request/tabs/requestor-info-tab/requestor-info-tab.component';
import { RequestorContentTabComponent } from './pages/requestor-view-request/tabs/requestor-content-tab/requestor-content-tab.component';
import { RequestorCommunicationsTabComponent } from './pages/requestor-view-request/tabs/requestor-communications-tab/requestor-communications-tab.component';
import { RequestorDocumentsComponent } from './pages/requestor-view-request/tabs/requestor-content-tab/requestor-content-tabs/requestor-documents/requestor-documents.component';
import { RequestorLinksComponent } from './pages/requestor-view-request/tabs/requestor-content-tab/requestor-content-tabs/requestor-links/requestor-links.component';
import { RequestorInstructionsComponent } from './pages/requestor-view-request/tabs/requestor-content-tab/requestor-content-tabs/requestor-instructions/requestor-instructions.component';
import { RequestInfoComponent } from './pages/legal-review-management/request-info/request-info.component';
import { ResourcePanelComponent } from './pages/legal-review-management/resource-panel/resource-panel.component';
import { ApproveResourceDialogComponent } from './dialogs/approve-resource-dialog/approve-resource-dialog.component';
import { DenyResourceDialogComponent } from './dialogs/deny-resource-dialog/deny-resource-dialog.component';
import { UploadRevisedDialogComponent } from './dialogs/upload-revised-dialog/upload-revised-dialog.component';
import { SuperDashboardComponent } from './pages/super-dashboard/super-dashboard.component';
import { SuperUserManagementComponent } from './pages/super-user-management/super-user-management.component';
import { SuperDepartmentManagementComponent } from './pages/super-department-management/super-department-management.component';
import { SuperSidebarComponent } from './components/super-sidebar/super-sidebar.component';
import { CreateDepartmentDialogComponent } from './dialogs/super-dialogs/create-department-dialog/create-department-dialog.component';
import { UpdateStaffDialogComponent } from './dialogs/super-dialogs/update-staff-dialog/update-staff-dialog.component';
import { CreateStaffDialogComponent } from './dialogs/super-dialogs/create-staff-dialog/create-staff-dialog.component';
import { QuickAddUserComponent } from './pages/super-dashboard/widgets/quick-add-user/quick-add-user.component';
import { QuickAddDepartmentComponent } from './pages/super-dashboard/widgets/quick-add-department/quick-add-department.component';
import { SuperGovernmentEntitiesManagementComponent } from './pages/super-government-entities-management/super-government-entities-management.component';
import { MultiChipSelectorComponent } from './components/multi-chip-selector/multi-chip-selector.component';
import { CreateGovernmentEntityDialogComponent } from './dialogs/super-dialogs/create-government-entity-dialog/create-government-entity-dialog.component';
import { UpdateGovernmentEntityDialogComponent } from './dialogs/super-dialogs/update-government-entity-dialog/update-government-entity-dialog.component';
import { RequestorEnterPasscodeComponent } from './dialogs/requestor-enter-passcode/requestor-enter-passcode.component';
import { BasicConfirmationDialogComponent } from './dialogs/basic-confirmation-dialog/basic-confirmation-dialog.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { UpdateNoteDialogComponent } from './dialogs/update-note-dialog/update-note-dialog.component';
import { UpdateInstructionDialogComponent } from './dialogs/update-instruction-dialog/update-instruction-dialog.component';
import { UpdateLinkDialogComponent } from './dialogs/update-link-dialog/update-link-dialog.component';
import { UpdateResourceDialogComponent } from './dialogs/update-resource-dialog/update-resource-dialog.component';
import { AlertTabComponent } from './pages/request-management/tabs/alert-tab/alert-tab.component';
import { MultiStackBoxSelectorComponent } from './components/multi-stack-box-selector/multi-stack-box-selector.component';
import { AboutRequestComponent } from './pages/staff-new-request/steps/about-request/about-request.component';
import { RequestorInformationComponent } from './pages/staff-new-request/steps/requestor-information/requestor-information.component';
import { DepartmentAssignmentStepComponent } from './pages/staff-new-request/steps/department-assignment-step/department-assignment-step.component';
import { ReOpenRequestDialogComponent } from './dialogs/re-open-request-dialog/re-open-request-dialog.component';
import { UpdateDepartmentDialogComponent } from './dialogs/super-dialogs/update-department-dialog/update-department-dialog.component';
import { DeleteContentDialogComponent } from './dialogs/delete-content-dialog/delete-content-dialog.component';
import { PublicInstructionsComponent } from './pages/public-view-request/tabs/public-content-tab/public-content-tabs/public-instructions/public-instructions.component';
import { RequestorNotesComponent } from './pages/requestor-view-request/tabs/requestor-content-tab/requestor-notes/requestor-notes.component';
import { PublicNotesComponent } from './pages/public-view-request/tabs/public-content-tab/public-content-tabs/public-notes/public-notes.component';
import { JL } from 'jsnlog';

@Injectable()
export class UncaughtExceptionHandler implements ErrorHandler {
  constructor(private injector: Injector) {}

  handleError(error: any) {
      const locationService = this.injector.get(LocationService);
      JL.setOptions({defaultAjaxUrl:  locationService.baseUrl + 'logging/newLogMessage'});
      const ajaxAppender = JL.createAjaxAppender('ajaxAppender');
      ajaxAppender.setOptions({
        bufferSize: 20,
        storeInBufferLevel: 1000,
        level: 4000,
        sendWithBufferLevel: 6000,
      });
      const consoleAppender = JL.createConsoleAppender('consoleAppender');

      JL().setOptions({
        appenders: [ajaxAppender, consoleAppender],
      });
      JL().fatalException('Uncaught Exception', error);
  }
}

// tslint:disable-next-line:max-line-length
import { DepartmentReadOnlyTabComponent } from './pages/request-management/tabs/department-read-only-tab/department-read-only-tab.component';
import { WorkerReadOnlyTabComponent } from './pages/request-management/tabs/worker-read-only-tab/worker-read-only-tab.component';
import { MarkCompleteDialogComponent } from './dialogs/mark-complete-dialog/mark-complete-dialog.component';
import { CloseWarningDialogComponent } from './dialogs/close-warning-dialog/close-warning-dialog.component';
import { WrongCustodianDialogComponent } from './dialogs/wrong-custodian-dialog/wrong-custodian-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    HomeComponent,
    PublicNewRequestComponent,
    StaffNewRequestComponent,
    StaffDashboardComponent,
    HeaderComponent,
    StaffSidebarComponent,
    AssignmentQueuePanelComponent,
    DepartmentQueuePanelComponent,
    RequestManagementComponent,
    DepartmentTabComponent,
    WorkerTabComponent,
    LogTabComponent,
    InfoTabComponent,
    SteeleTableComponent,
    DenyRequestDialogComponent,
    AssignRequestDialogComponent,
    ExtendDueDateDialogComponent,
    UpdateDueDateDialogComponent,
    CloseRequestDialogComponent,
    StaffSearchRequestComponent,
    PublicSearchRequestComponent,
    ResponseTabComponent,
    AssignedRequestsPanelComponent,
    DocumentsComponent,
    LinksComponent,
    InstructionsComponent,
    NotesComponent,
    UploadDocumentDialogComponent,
    AddLinkDialogComponent,
    AddInstructionDialogComponent,
    AddNoteDialogComponent,
    CommunicationsTabComponent,
    PublicViewRequestComponent,
    PublicInfoTabComponent,
    PublicContentTabComponent,
    PublicDocumentsComponent,
    PublicLinksComponent,
    LegalQueuePanelComponent,
    LegalReviewManagementComponent,
    RequestorViewRequestComponent,
    RequestorInfoTabComponent,
    RequestorContentTabComponent,
    RequestorCommunicationsTabComponent,
    RequestorDocumentsComponent,
    RequestorLinksComponent,
    RequestorInstructionsComponent,
    RequestInfoComponent,
    ResourcePanelComponent,
    ApproveResourceDialogComponent,
    DenyResourceDialogComponent,
    UploadRevisedDialogComponent,
    SuperDashboardComponent,
    SuperUserManagementComponent,
    SuperDepartmentManagementComponent,
    SuperSidebarComponent,
    CreateDepartmentDialogComponent,
    UpdateStaffDialogComponent,
    CreateStaffDialogComponent,
    QuickAddUserComponent,
    QuickAddDepartmentComponent,
    SuperGovernmentEntitiesManagementComponent,
    MultiChipSelectorComponent,
    CreateGovernmentEntityDialogComponent,
    UpdateGovernmentEntityDialogComponent,
    RequestorEnterPasscodeComponent,
    BasicConfirmationDialogComponent,
    UpdateNoteDialogComponent,
    UpdateInstructionDialogComponent,
    UpdateLinkDialogComponent,
    UpdateResourceDialogComponent,
    AlertTabComponent,
    MultiStackBoxSelectorComponent,
    AboutRequestComponent,
    RequestorInformationComponent,
    DepartmentAssignmentStepComponent,
    ReOpenRequestDialogComponent,
    UpdateDepartmentDialogComponent,
    DeleteContentDialogComponent,
    PublicInstructionsComponent,
    RequestorNotesComponent,
    PublicNotesComponent,
    DepartmentReadOnlyTabComponent,
    WorkerReadOnlyTabComponent,
    MarkCompleteDialogComponent,
    CloseWarningDialogComponent,
    WrongCustodianDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatButtonModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatChipsModule,
    MatPaginatorModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  entryComponents: [
    DenyRequestDialogComponent,
    AssignRequestDialogComponent,
    ExtendDueDateDialogComponent,
    UpdateDueDateDialogComponent,
    CloseRequestDialogComponent,
    ReOpenRequestDialogComponent,
    UploadDocumentDialogComponent,
    AddLinkDialogComponent,
    AddInstructionDialogComponent,
    AddNoteDialogComponent,
    ApproveResourceDialogComponent,
    DenyResourceDialogComponent,
    UploadRevisedDialogComponent,
    CreateDepartmentDialogComponent,
    UpdateStaffDialogComponent,
    CreateStaffDialogComponent,
    CreateGovernmentEntityDialogComponent,
    UpdateGovernmentEntityDialogComponent,
    RequestorEnterPasscodeComponent,
    BasicConfirmationDialogComponent,
    UpdateNoteDialogComponent,
    UpdateInstructionDialogComponent,
    UpdateLinkDialogComponent,
    UpdateResourceDialogComponent,
    UpdateDepartmentDialogComponent,
    DeleteContentDialogComponent,
    MarkCompleteDialogComponent
  ],
  providers: [
    // {
    //  provide: APP_INITIALIZER,
    //  useFactory: AppConfigurationFactory,
    //  deps: [AppConfiguration, HttpClient, LocationService],
    //  multi: true,
    // },
    Title,
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    RequestStoreService,
    ConstantService,
    { provide: ErrorHandler,
      useClass: UncaughtExceptionHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

//  export function AppConfigurationFactory(appConfig: AppConfiguration) {
//    return () => appConfig.ensureInit();
//  }
