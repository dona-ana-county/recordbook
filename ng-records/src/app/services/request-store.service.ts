import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, forkJoin } from 'rxjs';
import { Request, LogMessage } from 'src/app/models/request';
import { RequestService } from './request.service';
import { HttpClient } from '@angular/common/http';
import { Staff, Requestor } from 'src/app/models/user';
import { Department } from 'src/app/models/department';
import { Instruction, Note, Resource, Link } from '../models/content';
import { Alert, Comment } from 'src/app/models/communication';
import { DepartmentService } from './department.service';

@Injectable({
  providedIn: 'root'
})
export class RequestStoreService {
  private request: Request;
  private requestStore: BehaviorSubject<Request> = new BehaviorSubject<Request>(null);
  private requestId: string;
  private staffId: string;
  private departments: Department[];

  constructor(
    private requestService: RequestService,
    private http: HttpClient,
    private departmentService: DepartmentService
  ) { }

  initRequest(requestId: string, staffId: string) {
    this.requestId = requestId;
    this.staffId = staffId;
    forkJoin(
      this.requestService.getFullRequest(this.requestId),
      this.requestService.getAlerts(this.requestId, this.staffId),
      this.requestService.getResources(this.requestId),
      this.requestService.getNotes(this.requestId),
      this.requestService.getInstructions(this.requestId),
      this.requestService.getLinks(this.requestId),
      this.departmentService.getAllFullDepartments()
    ).subscribe(data => {
      this.request = data[0];
      this.request.alerts = data[1];
      this.request.resources = data[2];
      this.request.notes = data[3];
      this.request.instructions = data[4];
      this.request.links = data[5];
      this.departments = data[6];
      this.requestStore.next(this.request);
    });
  }

  getRequest(): Observable<Request> {
    return this.requestStore.asObservable();
  }

  getDepartments(): Department[] {
    return this.departments;
  }

  reset() {
    this.request = null;
    this.requestId = null;
    this.staffId = null;
    this.requestStore.next(this.request);
  }

  loadAll() {}

  loadInfo() {
    this.requestService.getRequestInfo(this.requestId).subscribe((res: Request) => {
      this.request.id = res.id;
      this.request.createDate = res.createDate;
      this.request.description = res.description;
      this.request.publicDescription = res.publicDescription;
      this.request.publicTitle = res.publicTitle;
      this.request.dueDate = res.dueDate;
      this.request.status = res.status;
      this.request.title = res.title;
      this.request.extensionStatus = res.extensionStatus;

      this.requestStore.next(this.request);
    });
  }

  loadRequestor() {
    this.requestService.getRequestor(this.requestId).subscribe((res: Requestor) => {
      this.request.requestor = res;
      this.requestStore.next(this.request);
    });
  }

  loadResources() {
    this.requestService.getResources(this.requestId).subscribe((res: Resource[]) => {
      this.request.resources = res;
      this.requestStore.next(this.request);
    });
  }

  loadNotes() {
    this.requestService.getNotes(this.requestId).subscribe((res: Note[]) => {
      this.request.notes = res;
      this.requestStore.next(this.request);
    });
  }

  loadInstructions() {
    this.requestService.getInstructions(this.requestId).subscribe((res: Instruction[]) => {
      this.request.instructions = res;
      this.requestStore.next(this.request);
    });
  }

  loadLinks() {
    this.requestService.getLinks(this.requestId).subscribe((res: Link[]) => {
      this.request.links = res;
      this.requestStore.next(this.request);
    });
  }

  loadDepartments() {
    this.requestService.getPrimaryDepartment(this.requestId).subscribe((res: Department) => {
      this.request.primaryDepartment = res;
      this.requestStore.next(this.request);
    });
    this.requestService.getSecondaryDepartments(this.requestId).subscribe((res: Department[]) => {
      this.request.secondaryDepartments = res;
      this.requestStore.next(this.request);
    });
  }

  loadSecondaryDepartments() {
    this.requestService.getSecondaryDepartments(this.requestId).subscribe((res: Department[]) => {
      this.request.secondaryDepartments = res;
      this.requestStore.next(this.request);
    });
  }

  loadAssignedStaff() {
    this.requestService.getAssignedStaff(this.requestId).subscribe((assigned: Staff[]) => {
      this.request.assignedStaff = assigned;
      this.requestStore.next(this.request);
    });
  }

  loadComments() {
    this.requestService.getStaffComments(this.requestId).subscribe((res: Comment[]) => {
      this.request.comments = res;
      this.requestStore.next(this.request);
    });
  }

  loadAlerts() {
    this.requestService.getAlerts(this.requestId, this.staffId).subscribe((res: Alert[]) => {
      this.request.alerts = res;
      this.requestStore.next(this.request);
    });
  }

  loadLog() {
    this.requestService.getLog(this.requestId).subscribe((res: LogMessage[]) => {
      this.request.log = res;
      this.requestStore.next(this.request);
    });
  }

  refreshContactInfoDisplay(): void {
    this.loadInfo();
    this.loadRequestor();
  }
}
