import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { LocationService } from './location.service';
import {
  Note,
  Instruction,
  Resource,
  ReleaseType,
  ContentType,
  Link,
  Content
} from '../models/content';
import { Staff } from '../models/user';

import HmacSHA256 from 'crypto-js/hmac-sha256';
import Base64 from 'crypto-js/enc-base64';
import { map } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ContentService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService, private storageService: StorageService) {
    this.apiUrl = locationService.baseUrl;
  }

  public getResource(contentId: string): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'content/getResource/' + contentId);
  }

  public getResourceUnderReview(contentId: string): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'content/getResourceUnderReview/' + contentId);
  }

  public updateContent(
    requestId: string,
    contentType: ContentType,
    updatedContent: Content
  ): Observable<any> {
    const data = {
      type: contentType.code,
      content: updatedContent
    };
    return this.http.post<any>(this.apiUrl + 'content/updateContent/' + requestId, data);
  }

  public uploadRequestFile(requestId: string, formData: FormData): Observable<any> {
    return this.http
      .post<any>(this.apiUrl + 'content/uploadRequestFile/' + requestId, formData, {
        reportProgress: true,
        observe: 'events'
      })
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const percent = Math.round((100 * event.loaded) / event.total);
              return { progress: percent };

            case HttpEventType.Response:
              return { response: event.body };
            default:
              return `Unhandled event: ${event.type}`;
          }
        })
      );
  }

  public uploadFile(requestId: string, formData: FormData): Observable<any> {
    return this.http
      .post<any>(this.apiUrl + 'content/uploadFile/' + requestId, formData, {
        reportProgress: true,
        observe: 'events'
      })
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const percent = Math.round((100 * event.loaded) / event.total);
              return { progress: percent };

            case HttpEventType.Response:
              return { response: event.body };
            default:
              return `Unhandled event: ${event.type}`;
          }
        })
      );
  }

  public downloadPrivateFile(resource: Resource): void {
    const now = new Date();
    const staff: Staff = this.storageService.getStaff();
    const token = staff.token.token;
    const mac = HmacSHA256(resource.id + staff.username + now.getTime(), token);
    // Base64 contains '/' and it messes up the url.
    const base64String = Base64.stringify(mac);
    const re = /\//gi;
    const editedBase64 = base64String.replace(re, '_');
    window.location.href =
      this.apiUrl +
      'content/downloadPrivateFile/' +
      resource.id +
      '/' +
      staff.username +
      '/' +
      now.getTime() +
      '/' +
      editedBase64;
  }

  public downloadFile(resource: Resource): void {
    const now = new Date();
    window.location.href =
      this.apiUrl + 'content/downloadFile/' + resource.id + '?time=' + now.getTime();
  }

  public downloadRequestorFile(resource: Resource, passcode: string): void {
    const now = new Date();
    window.location.href =
      this.apiUrl +
      'content/downloadFile/' +
      resource.id +
      '/' +
      passcode +
      '?time=' +
      now.getTime();
  }

  public createNote(requestId: string, newNote: Note): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'content/newNote/' + requestId, newNote);
  }


  public uploadRevisedFile(resourceId: string, formData: FormData): Observable<any> {
    return this.http
      .post<any>(this.apiUrl + 'content/uploadRevisedFile/' + resourceId, formData, {
        reportProgress: true,
        observe: 'events'
      })
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const percent = Math.round((100 * event.loaded) / event.total);
              return { progress: percent };

            case HttpEventType.Response:
              return { response: event.body };
            default:
              return `Unhandled event: ${event.type}`;
          }
        })
      );
  }

  public createInstruction(requestId: string, newInstruction: Instruction): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'content/newInstruction/' + requestId, newInstruction);
  }

  public createLink(requestId: string, newLink: Link): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'content/newLink/' + requestId, newLink);
  }

  public approveResource(resourceId: string, releaseType: ReleaseType): Observable<any> {
    const data = {
      type: releaseType.code
    };
    return this.http.post<any>(this.apiUrl + 'content/approveResource/' + resourceId, data);
  }

  public rejectResource(resourceId: string, reasonMsg: string, staffId: string): Observable<any> {
    const data = {
      reason: reasonMsg,
      staff: staffId
    };
    return this.http.post<any>(this.apiUrl + 'content/rejectResource/' + resourceId, data);
  }

  public deleteContent(contentId: string, contentType: ContentType, reasonMsg: string): Observable<any> {
    const data = {
      reason: reasonMsg,
      type: contentType.code
    };
    return this.http.post<any>(this.apiUrl + 'content/deleteContent/' + contentId, data);
  }

  public updateUnderReview(resourceId: string, underReview: boolean): Observable<any> {
    const data = {
      status: underReview
    };
    return this.http.post<any>(this.apiUrl + 'content/updateUnderReview/' + resourceId, data);
  }
}
