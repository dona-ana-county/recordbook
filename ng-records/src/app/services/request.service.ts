import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { LocationService } from './location.service';

import {
  PublicRequest,
  Request,
  LogMessage,
  StaffRequest,
  CompletionStatus,
} from 'src/app/models/request';
import { Staff, Requestor } from 'src/app/models/user';
import { Note, Link, Instruction, Resource } from 'src/app/models/content';
import { GovernmentEntity, Reason } from '../models/constant';
import { AlertedItem } from '../components/multi-stack-box-selector/multi-stack-box-selector.component';
import { Department } from '../models/department';
import { Alert, Comment } from '../models/communication';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public newPublicRequest(request: PublicRequest): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/newPublicRequest', request);
  }

  public newStaffRequest(request: StaffRequest): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/newStaffRequest', request);
  }

  public getFullRequest(requestId: string): Observable<Request> {
    return this.http.get<Request>(this.apiUrl + 'request/getFullRequest/' + requestId);
  }

  public getPublicRequest(requestId: string): Observable<Request> {
    return this.http.get<Request>(this.apiUrl + 'request/getPublicRequest/' + requestId);
  }

  public getRequestorRequest(requestID: string, passCode: number): Observable<Request> {
    return this.http.get<Request>(
      this.apiUrl + 'request/getRequestorRequest/' + requestID + '/' + passCode
    );
  }

  public getRequestInfo(requestId: string): Observable<Request> {
    return this.http.get<Request>(this.apiUrl + 'request/getRequestInfo/' + requestId);
  }

  public getRequestor(requestId: string): Observable<Requestor> {
    return this.http.get<Requestor>(this.apiUrl + 'request/getRequestor/' + requestId);
  }

  public getPrimaryDepartment(requestId: string): Observable<Department> {
    return this.http.get<Department>(this.apiUrl + 'request/getPrimaryDepartment/' + requestId);
  }

  public getSecondaryDepartments(requestId: string): Observable<Department[]> {
    return this.http.get<Department[]>(
      this.apiUrl + 'request/getSecondaryDepartments/' + requestId
    );
  }

  public getAssignedStaff(requestId: string): Observable<Staff[]> {
    return this.http.get<Staff[]>(this.apiUrl + 'request/getAssignedStaff/' + requestId);
  }

  public getResources(requestId: string): Observable<Resource[]> {
    return this.http.get<Resource[]>(this.apiUrl + 'request/getResources/' + requestId);
  }

  public getNotes(requestId: string): Observable<Note[]> {
    return this.http.get<Note[]>(this.apiUrl + 'request/getNotes/' + requestId);
  }

  public getLinks(requestId: string): Observable<Link[]> {
    return this.http.get<Link[]>(this.apiUrl + 'request/getLinks/' + requestId);
  }

  public getInstructions(requestId: string): Observable<Instruction[]> {
    return this.http.get<Instruction[]>(this.apiUrl + 'request/getInstructions/' + requestId);
  }

  public getLog(requestId: string): Observable<LogMessage[]> {
    return this.http.get<LogMessage[]>(this.apiUrl + 'request/getLog/' + requestId);
  }

  public getAlerts(requestId: string, staffId: string): Observable<Alert[]> {
    return this.http.get<Alert[]>(this.apiUrl + 'request/getAlerts/' + requestId + '/' + staffId);
  }

  public getStaffComments(requestId: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.apiUrl + 'request/getComments/' + requestId);
  }

  public getRequestorComments(requestId: string, passcode: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(
      this.apiUrl + 'request/getComments/' + requestId + '/' + passcode
    );
  }

  public getPublicResources(requestId: string): Observable<Resource[]> {
    return this.http.get<Resource[]>(this.apiUrl + 'request/getPublicResources/' + requestId);
  }

  public getPublicLinks(requestId: string): Observable<Link[]> {
    return this.http.get<Link[]>(this.apiUrl + 'request/getPublicLinks/' + requestId);
  }

  public getPublicInstructions(requestId: string): Observable<Instruction[]> {
    return this.http.get<Instruction[]>(
      this.apiUrl + 'request/getPublicInstructions/' + requestId);
  }

  public getPublicNotes(requestId: string): Observable<Note[]> {
    return this.http.get<Note[]>(
      this.apiUrl + 'request/getPublicNotes/' + requestId);
  }

  public getRequestorResources(requestId: string, passcode: string): Observable<Resource[]> {
    return this.http.get<Resource[]>(
      this.apiUrl + 'request/getRequestorResources/' + requestId + '/' + passcode
    );
  }

  public getRequestorLinks(requestId: string, passcode: string): Observable<Link[]> {
    return this.http.get<Link[]>(
      this.apiUrl + 'request/getRequestorLinks/' + requestId + '/' + passcode
    );
  }

  public getRequestorInstructions(requestId: string, passcode: string): Observable<Instruction[]> {
    return this.http.get<Instruction[]>(
      this.apiUrl + 'request/getRequestorInstructions/' + requestId + '/' + passcode
    );
  }

  public getRequestorNotes(requestId: string, passcode: string): Observable<Note[]> {
    return this.http.get<Note[]>(
      this.apiUrl + 'request/getRequestorNotes/' + requestId + '/' + passcode
    );
  }

  public updateDueDate(requestId: string, reason: string, dueDate: Date): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/updateDueDate/' + requestId, {
      reason,
      dueDate
    });
  }

  public extendDueDate(requestId: string, description: string, dueDate: Date = null): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/extendDueDate/' + requestId, {
      description,
      dueDate
    });
  }

  public updatePublicDescription(requestId: string, newDescription: string): Observable<any> {
    const data = {
      newDescription
    };
    return this.http.post<any>(this.apiUrl + 'request/updatePublicDescription/' + requestId, data);
  }

  public updatePublicTitle(requestId: string, newTitle: string): Observable<any> {
    const data = {
      newTitle
    };
    return this.http.post<any>(this.apiUrl + 'request/updatePublicTitle/' + requestId, data);
  }

  public updateDepartments(requestId: string, primary: AlertedItem, secondaries: AlertedItem[]): Observable<any> {
    const data = {
      primaryDepartments: primary,
      secondaryDepartments: secondaries
    };
    return this.http.post<any>(this.apiUrl + 'request/updateDepartments/' + requestId, data);
  }

  public updateCompletion(requestId: string, deptId: string, completionStatus: boolean, comment: string): Observable<any> {
    const data = {
      deptId,
      completionStatus,
      comment
    };
    return this.http.post<any>(this.apiUrl + 'request/updateCompletion/' + requestId, data);
  }

  public updateRequestStaff(requestId: string, staff: AlertedItem[]): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/updateStaff/' + requestId, staff);
  }

  public closeRequest(requestId: string, closeReason: Reason): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/closeRequest/' + requestId, closeReason);
  }

  public denyRequest(requestId: string, denialReason: Reason): Observable<Request> {
    return this.http.post<Request>(this.apiUrl + 'request/denyRequest/' + requestId, denialReason);
  }

  public denyRequestWrongGovernmentEntity(
    request: Request,
    reason: Reason,
    entity: GovernmentEntity
  ): Observable<any> {
    const data = {
      entity,
      reason,
      request: request.id
    };
    return this.http.post<any>(this.apiUrl + 'request/denyRequestWrongGovernmentEntity', data);
  }

  public forwardRequestWrongGovernmentEntity(
    request: Request,
    entity: GovernmentEntity
  ): Observable<any> {
    const data = {
      entity,
      request: request.id
    };
    return this.http.post<any>(this.apiUrl + 'request/forwardRequestWrongGovernmentEntity', data);
  }

  public reOpenRequest(requestId: string, reason: any): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'request/reOpenRequest/' + requestId, reason);
  }

  public updateRequestorContactInfo(requestId: string, contactInfo: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}request/updateRequestorContactInfoByRequestId/${requestId}`, contactInfo);
  }
}
