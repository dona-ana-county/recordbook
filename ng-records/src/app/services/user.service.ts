import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocationService } from './location.service';
import { Observable } from 'rxjs';
import { Requestor, Staff } from '../models/user';
import { Department } from '../models/department';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public getAssignedRequests(staffID: string): Observable<any> {
    return this.http.get<Request>(this.apiUrl + 'users/getAssignedRequests/' + staffID);
  }
}
