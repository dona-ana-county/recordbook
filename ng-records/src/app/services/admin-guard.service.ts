import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

import { Staff } from 'src/app/models/user';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router, private storageService: StorageService) {}

  canActivate(): boolean {
    if (!this.isAuthenticated()) {
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }

  isAuthenticated(): boolean {
    const staff: Staff = this.storageService.getStaff();
    if (staff !== null && staff.role.code === 'SUP') {
      return true;
    }
    return false;
  }
}
