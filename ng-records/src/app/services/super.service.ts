import { Injectable } from '@angular/core';
import { LocationService } from './location.service';
import { Staff } from '../models/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Department } from '../models/department';
import { GovernmentEntity } from '../models/constant';

@Injectable({
  providedIn: 'root'
})
export class SuperService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public newStaff(staff: Staff, department: Department): Observable<Staff> {
    const data = {
      staff,
      department
    };
    return this.http.post<Staff>(this.apiUrl + 'super/newStaff', data);
  }

  public updateStaff(staff: Staff, dept: Department): Observable<any> {
    const data = {
      staff,
      department: dept
    };
    return this.http.post<any>(this.apiUrl + 'super/updateStaff', data);
  }

  public deleteStaff(staff: Staff): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'super/deleteStaff', staff);
  }

  public getAllStaff(): Observable<Staff> {
    return this.http.get<Staff>(this.apiUrl + 'super/getStaff');
  }

  public disableStaff(staff: Staff): Observable<Staff> {
    return this.http.post<Staff>(this.apiUrl + 'super/disableStaff', staff);
  }

  public enableStaff(staff: Staff): Observable<Staff> {
    return this.http.post<Staff>(this.apiUrl + 'super/enableStaff', staff);
  }

  public newGovernmentEntity(entity: GovernmentEntity): Observable<GovernmentEntity> {
    return this.http.post<GovernmentEntity>(this.apiUrl + 'super/newGovernmentEntity', entity);
  }

  public updateGovernmentEntity(entity: GovernmentEntity): Observable<GovernmentEntity> {
    return this.http.post<GovernmentEntity>(this.apiUrl + 'super/updateGovernmentEntity', entity);
  }

  public deleteGovernmentEntity(entity: GovernmentEntity): Observable<GovernmentEntity> {
    return this.http.post<GovernmentEntity>(this.apiUrl + 'super/deleteGovernmentEntity', entity);
  }
}
