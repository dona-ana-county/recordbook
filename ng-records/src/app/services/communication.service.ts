import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocationService } from './location.service';

import { Comment, Alert } from 'src/app/models/communication';

@Injectable({
  providedIn: 'root',
})
export class CommunicationService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public staffNewComment(requestId: string, comment: Comment): Observable<any> {
    return this.http.post<Comment>(
      this.apiUrl + 'communication/staffNewComment/' + requestId,
      comment,
    );
  }

  public requestorNewComment(
    requestId: string,
    comment: Comment,
    passcode: string,
  ): Observable<any> {
    return this.http.post<Comment>(
      this.apiUrl + 'communication/requestorNewComment/' + requestId + '/' + passcode,
      comment,
    );
  }
}
