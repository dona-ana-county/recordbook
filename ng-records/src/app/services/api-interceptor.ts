import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { Staff } from '../models/user';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private storageService: StorageService,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const staff: Staff = this.storageService.getStaff();
    if (staff != null) {
      const token = staff.token.token;
      if (token) {
        req = req.clone({
          headers: req.headers.set('Authorization', token),
        });
      }
    }

    return next.handle(req).pipe(
      tap((event) => {
        if (event instanceof HttpResponse) {
          if (event.body && event.body.error && event.body.type) {
            if (!this.toastr.findDuplicate('', event.body.error, false, false)) {
              this.toastr.error(event.body.error, event.body.type, {
                timeOut: 3000,
              });
            }
            if (event.body.type === 'Unauthorized' &&  [ 'Invalid Permissions', 'Invalid Passcode'].indexOf(event.body.error) === -1) {
              this.storageService.clearStaffData();
              this.router.navigate(['home']);
            }
          }
        }
      }),
    );
  }
}
