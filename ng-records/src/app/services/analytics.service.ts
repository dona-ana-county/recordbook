import { Injectable } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { ConstantService } from './constant.service';

declare var gtag: (...args: any[]) => void;

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  trackingId = 'TrackingIDPlaceholder';
  inited = false;
  backlog = [];
  currentPage: string;

  constructor(private router: Router, private constantService: ConstantService) {
    this.listenForRouteChanges();
  }

  public event(eventName: string, params: {}): void {
    this.gtag('event', eventName, params);
  }

  public gtag(...args: any[]): void {
    if (this.trackingId) {
      if (!this.inited) {
        this.backlog.push(args);
      } else {
        console.log('gtag', args);
        gtag(...args);
      }
    }
  }

  public init(): void {
    this.trackingId = this.constantService.config.gtagTrackingId;
    if (this.trackingId) {

      try {

        const script1 = document.createElement('script');
        script1.async = true;
        script1.src = 'https://www.googletagmanager.com/gtag/js?id=' + this.trackingId;
        document.head.appendChild(script1);

        const script2 = document.createElement('script');
        script2.innerHTML = `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
        `; // 'sha256-0nFqR3yUugrQGAtIZponDb3/qaTV0yC5xf55Rcowd6g='
        document.head.appendChild(script2);

        gtag('js', new Date());
        gtag('config', this.trackingId, {send_page_view: false});

        this.inited = true;

        for (const item of this.backlog) {
          if (item[1] === 'TrackingIDPlaceholder' ) {
            item[1] = this.trackingId;
          }
          console.log('gtag', item);
          gtag(...item);
        }
        this.backlog = [];
      } catch (ex) {
        console.error('Error appending google analytics');
        console.error(ex);
      }
    }
  }

  public navigateAnchor(anchor: string): void {
    this.navigate(this.currentPage, anchor);
  }

  public navigate(page: string, anchor?: string): void {
    this.currentPage = page.replace(/#.*/, '');
    if (anchor) {
      page = page + '#' + anchor;
    }
    this.gtag('config', this.trackingId, {
      page_path: page
    });
    console.log('Analytics:', page);
  }

  private listenForRouteChanges(): void {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        let page = event.urlAfterRedirects;
        page = page.replace(/IPRA-\d+-\d+/, 'IPRA-XXXX');
        page = page.replace(/IPRA-XXXX\/[a-zA-Z0-9]+/, 'IPRA-XXXX/code/');
        this.navigate(page);
      }
    });
  }
}
