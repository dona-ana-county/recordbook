import { Injectable } from '@angular/core';
import { LocationService } from './location.service';
import { Observable } from 'rxjs';
import { LogMessage } from '../models/request';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public getLogMessages(requestId: string): Observable<LogMessage[]> {
    return this.http.get<LogMessage[]>(this.apiUrl + 'request/getLogMessages/' + requestId);
  }
}
