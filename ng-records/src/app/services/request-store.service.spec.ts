import { TestBed } from '@angular/core/testing';

import { RequestStoreService } from './request-store.service';

describe('RequestStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestStoreService = TestBed.get(RequestStoreService);
    expect(service).toBeTruthy();
  });
});
