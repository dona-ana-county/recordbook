import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { LocationService } from './location.service';

import { Observable } from 'rxjs';
import { SearchQuery, PaginationInfo } from '../models/search';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  apiUrl: string;


  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public publicRequestSearch(q: SearchQuery): Observable<PaginationInfo> {
    return this.http.post<PaginationInfo>(this.apiUrl + 'publicSearch/query', q);
  }

  public staffRequestSearch(q: SearchQuery): Observable<PaginationInfo> {
    return this.http.post<PaginationInfo>(this.apiUrl + 'staffSearch/query', q);
  }

  exportSearchResults(query: SearchQuery): Observable<any> {
    return this.http.post(this.apiUrl + '/staffSearch/export', query, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

}
