import { Injectable, Inject } from '@angular/core';
import { DOCUMENT} from '@angular/common';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  baseUrl: string;

  constructor(@Inject(DOCUMENT) private document: Document) {
    this.baseUrl = document.location.protocol +
                   '//' +
                   document.location.hostname +
                   environment.baseAppUrl;
  }
  public urlFor(path: string): string {
    return this.baseUrl + path;
  }
}
