import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Department } from '../models/department';

import { LocationService } from './location.service';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public newDepartment(newDepartment: Department): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'department/newDepartment', newDepartment);
  }

  public getAllFullDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.apiUrl + 'department/getAllFullDepartments');
  }

  public getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.apiUrl + 'department/getAllDepartments');
  }

  public getFullDepartment(id: string): Observable<Department> {
    return this.http.get<Department>(this.apiUrl + 'department/getFullDepartment/' + id);
  }

  public getDepartment(id: string): Observable<Department> {
    return this.http.get<Department>(this.apiUrl + 'department/getDepartment/' + id);
  }

  public getDepartmentByStaff(staffId: string): Observable<Department> {
    return this.http.get<Department>(this.apiUrl + 'department/getDepartmentByStaff/' + staffId);
  }

  public updateDepartment(department: Department): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'department/updateDepartment', department);
  }

  public deleteDepartment(department: Department): Observable<any> {
    // NOTE VT: Probably going to make this a get
    return this.http.post<any>(this.apiUrl + 'department/deleteDepartment', department);
  }
}
