import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

import { Staff } from 'src/app/models/user';
import { state } from '@angular/animations';
import { StorageService } from './storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router, private storageService: StorageService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.isAuthenticated()) {
      this.router.navigate(['signin'], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
    return true;
  }

  isAuthenticated(): boolean {
    const staff: Staff = this.storageService.getStaff();
    if (staff !== null) {
      if ( (new Date(staff.token.expirationDt)).getTime() > Date.now()
            && staff.token.revokedDt === null
      ) {
        if (staff.role.code !== 'SUP') {
          return true;
        } else if (staff.role.code === 'SUP') {
          this.router.navigate(['superUser']);
        }
      } else {
        this.storageService.clearStaffData();
      }
    }
    return false;
  }
}
