import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocationService } from './location.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private locationService: LocationService
  ) {
    this.apiUrl = locationService.baseUrl;
  }

  public login(credentials): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'auth/login', credentials);
  }

  public logout(): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'auth/logout', {});
  }
}
