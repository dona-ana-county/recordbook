import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

declare const require;

@Injectable({
  providedIn: 'root'
})
export class TextService {
  private textMap: object;
  finishedLoading: boolean;
  availableLanguages: string[];

  /* TODO default english when no other translation matches */
  constructor(private http: HttpClient) {
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.availableLanguages = [];
    this.finishedLoading = false;
    this.textMap = Object.assign({}, ...this.getJSON());
    const languages = new Set<string> ();
    for (const langs of Object.values(this.textMap)) {
      for (const lang of Object.keys(langs)) {
        languages.add(lang);
      }
    }
    this.availableLanguages = Array.from(languages);
    this.finishedLoading = true;
  }

  public selectLanguage(id: string): void {
    localStorage.setItem('language', id.trim());
  }

  public get(id: string, def?: string): string {
    if (!this.finishedLoading) {
      return 'Not finished loading yet.';
    }
    if (id === '') {
      return '';
    }
    const langMap = this.textMap[id];
    if (langMap) {
      const language = localStorage.getItem('language');
      if (!langMap[language]) {
        // return '((' + id + ')) Translation not available.';
        return langMap['en'];
      }
      return langMap[language];
    }
    if (def ) {
      return def;
    }
    return '((' + id + ')) Text does not exist';
  }

  public getJSON(): any[] {
    return [require('../text_ext.json'), require('../text_int.json'), require('../text.custom.json')];
  }
}
