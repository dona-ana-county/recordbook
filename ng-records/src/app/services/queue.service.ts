import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocationService } from './location.service';
import { RequestQueueItem, ResourceQueueItem } from 'src/app/models/queue-item';

@Injectable({
  providedIn: 'root'
})
export class QueueService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public getAssignmentQueue(): Observable<RequestQueueItem[]> {
    return this.http.get<RequestQueueItem[]>(this.apiUrl + 'queue/getAssignmentQueue');
  }

  public getDepartmentQueue(deptId: string): Observable<RequestQueueItem[]> {
    return this.http.get<RequestQueueItem[]>(this.apiUrl + 'queue/getDepartmentQueue/' + deptId);
  }

  public getAllDepartmentQueues(): Observable<RequestQueueItem[]> {
    return this.http.get<RequestQueueItem[]>(this.apiUrl + 'queue/getAllDepartmentQueues');
  }

  public getStaffAssignedQueue(staffId: string): Observable<RequestQueueItem[]> {
    return this.http.get<RequestQueueItem[]>(this.apiUrl + 'queue/getStaffAssignedQueue/' + staffId);
  }

  public getLegalQueue(): Observable<ResourceQueueItem[]> {
    return this.http.get<ResourceQueueItem[]>(this.apiUrl + 'queue/getLegalQueue');
  }
}
