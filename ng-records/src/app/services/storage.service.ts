import { Injectable } from '@angular/core';
import { Staff, User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {}

  public getStaff(): Staff {
    if (window.localStorage.staff) {
      const staff: Staff = JSON.parse(localStorage.getItem('staff'));
      return staff;
    }
    return null;
  }
  public setStaff(staff: Staff) {
    localStorage.setItem('staff', JSON.stringify(staff));
  }

  public getRequestorUser(): User {
    if (window.localStorage.requestorUser) {
      const user: User = JSON.parse(localStorage.getItem('requestorUser'));
      return user;
    }
    return null;
  }

  public updateSideToggle(toggle: boolean) {
    localStorage.setItem('sideToggle', toggle.toString());
  }

  public getSideToggle() {
    if (window.localStorage.sideToggle) {
      return JSON.parse(localStorage.getItem('sideToggle'));
    }
    return false;
  }

  public getLastTabIndex(requestId: string): string {
    let indexes: Map<string, string> = new Map();
    if (localStorage.getItem('tabIndexes')) {
      indexes = new Map(JSON.parse(localStorage.getItem('tabIndexes')));
      if (indexes.has(requestId)) {
        return indexes.get(requestId);
      } else {
        indexes.set(requestId, null);
        localStorage.setItem('tabIndexes', JSON.stringify(Array.from(indexes.entries())));
        return indexes.get(requestId);
      }
    } else {
      indexes.set(requestId, null);
      localStorage.setItem('tabIndexes', JSON.stringify(Array.from(indexes.entries())));
      return indexes.get(requestId);
    }
  }

  public setLastTabIndex(requestId: string, index: string): void {
    let indexes: Map<string, string> = new Map();
    if (localStorage.getItem('tabIndexes')) {
      indexes = new Map(JSON.parse(localStorage.getItem('tabIndexes')));
      indexes.set(requestId, index);
    } else {
      indexes.set(requestId, index);
    }
    localStorage.setItem('tabIndexes', JSON.stringify(Array.from(indexes.entries())));
  }

  public clearStaffData(): void {
    if (localStorage.getItem('staff')) {
      localStorage.removeItem('staff');
    }
    if (localStorage.getItem('tabIndexes')) {
      localStorage.removeItem('tabIndexes');
    }
    if (localStorage.getItem('departmentQueueSelected')) {
      localStorage.removeItem('departmentQueueSelected');
    }
  }

  public getLastDepartmentQueueSelected(defaultValue: string): string {
    if (localStorage.getItem('departmentQueueSelected')) {
      return localStorage.getItem('departmentQueueSelected');
    } else {
      this.setLastDepartmentQueueSelected(defaultValue);
      return localStorage.getItem('departmentQueueSelected');
    }
  }

  public setLastDepartmentQueueSelected(id: string): void {
    localStorage.setItem('departmentQueueSelected', id);
  }
}
