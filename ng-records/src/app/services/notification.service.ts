import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocationService } from './location.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  apiUrl: string;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }
  public getTemplate(templateName: string, requestId: string, params= {} ): Observable<any> {
    return this.http.post(this.apiUrl + 'notification/getTemplate/' + templateName + '/' + requestId, params );
  }
}
