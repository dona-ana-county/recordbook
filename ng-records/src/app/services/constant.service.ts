import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { LocationService } from './location.service';

import { GovernmentEntity, Reason, QueueType, QueueItemType, ConfigType} from '../models/constant';
import { ContactType, Group, Role } from '../models/user';
import { RequestStatus, EventCode } from '../models/request';
import { ContentType, ReleaseType } from '../models/content';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  @Output() loadingComplete = new EventEmitter();
  loaded: number;
  apiUrl: string;
  groups: Group[];
  roles: Role[];
  requestStatuses: RequestStatus[];
  contentTypes: ContentType[];
  releaseTypes: ReleaseType[];
  eventCodes: EventCode[];
  contactTypes: ContactType[];
  queueItemTypes: QueueItemType[];
  queueTypes: QueueType[];
  config: ConfigType;

  constructor(private http: HttpClient, private locationService: LocationService) {
    this.apiUrl = locationService.baseUrl;
  }

  public init(): void {
    forkJoin(
      this.getAllGroups(),
      this.getAllRoles(),
      this.getAllRequestStatuses(),
      this.getAllContentTypes(),
      this.getAllReleaseTypes(),
      this.getAllEventCodes(),
      this.getAllContactTypes(),
      this.getAllQueueItemTypes(),
      this.getAllQueueTypes(),
      this.getConfig()
    ).subscribe(data => {
      this.groups = data[0];
      this.roles = data[1];
      this.requestStatuses = data[2];
      this.contentTypes = data[3];
      this.releaseTypes = data[4];
      this.eventCodes = data[5];
      this.contactTypes = data[6];
      this.queueItemTypes = data[7];
      this.queueTypes = data[8];
      this.config = data[9];
      this.loadingComplete.emit();
    });
  }

  public getAllGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.apiUrl + 'constants/getAllGroups');
  }

  public getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.apiUrl + 'constants/getAllRoles');
  }

  public getAllRequestStatuses(): Observable<RequestStatus[]> {
    return this.http.get<RequestStatus[]>(this.apiUrl + 'constants/getAllRequestStatuses');
  }

  public getAllContentTypes(): Observable<ContentType[]> {
    return this.http.get<ContentType[]>(this.apiUrl + 'constants/getAllContentTypes');
  }

  public getAllReleaseTypes(): Observable<ReleaseType[]> {
    return this.http.get<ReleaseType[]>(this.apiUrl + 'constants/getAllReleaseTypes');
  }

  public getAllEventCodes(): Observable<EventCode[]> {
    return this.http.get<EventCode[]>(this.apiUrl + 'constants/getAllEventCodes');
  }

  public getAllContactTypes(): Observable<ContactType[]> {
    return this.http.get<ContactType[]>(this.apiUrl + 'constants/getAllContactTypes');
  }

  public getAllQueueItemTypes(): Observable<QueueItemType[]> {
    return this.http.get<QueueItemType[]>(this.apiUrl + 'constants/getAllQueueItems');
  }

  public getAllQueueTypes(): Observable<QueueType[]> {
    return this.http.get<QueueType[]>(this.apiUrl + 'constants/getAllQueueTypes');
  }

  public getGroup(code: string) {
    return this.groups.find(g => g.code === code);
  }

  public getRole(code: string) {
    return this.roles.find(r => r.code === code);
  }

  public getRequestStatus(code: string) {
    return this.requestStatuses.find(r => r.code === code);
  }

  public getContentType(code: string) {
    return this.contentTypes.find(c => c.code === code);
  }

  public getReleaseType(code: string) {
    return this.releaseTypes.find(r => r.code === code);
  }

  public getEventCode(code: string) {
    return this.eventCodes.find(e => e.code === code);
  }

  public getContactType(code: string) {
    return this.contactTypes.find(c => c.code === code);
  }

  public getQueueItemType(code: string) {
    return this.queueItemTypes.find(q => q.code === code);
  }

  public getQueueType(code: string) {
    return this.queueTypes.find(q => q.code === code);
  }

  public getAllCloseReasons(): Observable<Reason[]> {
    return this.http.get<Reason[]>(this.apiUrl + 'constants/getAllCloseReasons');
  }

  public getAllDenialReasons(): Observable<Reason[]> {
    return this.http.get<Reason[]>(this.apiUrl + 'constants/getAllDenialReasons');
  }

  public getGovernmentEntities(): Observable<GovernmentEntity[]> {
    return this.http.get<GovernmentEntity[]>(this.apiUrl + 'constants/getAllGovernmentEntities');
  }
  public getConfig(): Observable<ConfigType> {
    return this.http.get<ConfigType>(this.apiUrl + 'constants/getConfig');
  }
}
