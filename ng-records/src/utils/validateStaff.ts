import { Staff } from 'src/app/models/user';

export function validateAssignedStaff(
  staffId: string,
  assignedStaff: Staff[]
): boolean {
  let validated = false;
  for (const worker of assignedStaff) {
    if (worker.id === staffId) {
      validated = true;
    }
  }

  return validated;
}

export function validateAssignedManager(staffId: string, assignedStaff: Staff[]): boolean {
  let validated = false;
  for (const worker of assignedStaff) {
    if (worker.id === staffId && worker.role.code === 'MAN') {
      validated = true;
    }
  }
  return validated;
}

export function validatePDM(staffId: string, deptStaff: Staff[]): boolean {
  let validated = false;
  for (const worker of deptStaff) {
    if (worker.id === staffId && worker.role.code === 'MAN') {
      validated = true;
    }
  }
  return validated;
}

export function validateAdmin(staffArr: Staff) {
  let validated = false;
  const groups = staffArr.groups;
  for (const group of groups) {
    if (group.code === 'IPA') {
      validated = true;
    }
  }
  return validated;
}

export function validateClerk(staffArr: Staff) {
  let validated = false;
  const groups = staffArr.groups;
  for (const group of groups) {
    if (group.code === 'IPC') {
      validated = true;
    }
  }
  return validated;
}

export function validateLegal(staffArr: Staff) {
  let validated = false;
  const groups = staffArr.groups;
  for (const group of groups) {
    if (group.code === 'LEG') {
      validated = true;
    }
  }
  return validated;
}
