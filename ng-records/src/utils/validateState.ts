import { AbstractControl } from '@angular/forms';
import stateJSON from './stateList.json';

export function ValidateState(control: AbstractControl) {
  for (const state of stateJSON) {
    if (state.name === control.value) {
      return null;
    }
  }
  return { invalidState: true };
}
