export function textTransform(s: string): string {
  return s ? s.replace(new RegExp('\n', 'g'), '<br />') : '';
}

export function textTransformNewLines(s: string): string {
  return s.replace(new RegExp('<br />', 'g'), '\n');
}
