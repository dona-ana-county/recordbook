import {DateTime} from 'luxon';
import { AbstractControl, ValidatorFn } from '@angular/forms';


export function serializeDate(d: Date): string {
  return DateTime.fromJSDate(d).toISODate();
}

export function deserializeDate(s: string): Date {
  return DateTime.fromISO(s).toJSDate();
}

export function forbiddenDateValidator(dueDate: DateTime): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const current = DateTime.fromJSDate(control.value).toISODate();
    return current === dueDate.toISODate() ? {forbiddenDate: {value: current}} : null;
  };
}
