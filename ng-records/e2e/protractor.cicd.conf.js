// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const defaultConfig = require('./protractor.conf');
superOnPrepare = defaultConfig.config.onPrepare;
let stagingConfig = {
  directConnect: true,
  seleniumAddress: 'http://webdriver:4444/wd/hub',
  baseUrl: 'http://recordbook/',
  params: {
    baseApi: 'http://recordbook/api/'
  },
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': ['--headless', '--no-sandbox', '--window-size=1280,1024', '--start-maximized']
    }
  },
  onPrepare() {
    superOnPrepare();
    var JUnitXmlReporter = require('jasmine-reporters').JUnitXmlReporter;
    jasmine.getEnv().addReporter(new JUnitXmlReporter({
      savePath: 'target'
    }));
  }
}
exports.config = Object.assign(defaultConfig.config, stagingConfig);
