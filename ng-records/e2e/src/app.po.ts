import { browser, by, element, ExpectedConditions, ElementFinder } from 'protractor';
import * as path from 'path';
const EC = ExpectedConditions;

export class AppPage {
  async navigateTo(url: string): Promise<any> {
    await browser.get(this.urlOf(url));
  }
  urlOf(part: string): string {
    return browser.baseUrl + part;
  }

  async getTitleText(): Promise<string> {
    return await element(by.css('app-root h1')).getText();
  }

  async setLanguage(lang: string) {
    await element(by.css('app-root app-home app-header select')).element(by.cssContainingText('option', lang)).click();
  }

  async newRequestBegin() {
    await element(by.cssContainingText('app-header a', 'Begin New Request')).click();
    await browser.sleep(100);
  }
  async newRequestFillAbout(title: string, desc: string) {
    // TODO: Make more specific
    await element(by.formControlName('title')).sendKeys(title);
    await element(by.formControlName('description')).sendKeys(desc);
  }
  async newRequestNext() {
    // TODO: Make more specific
    const currentTab = element(by.css('div.mat-horizontal-stepper-content[aria-expanded="true"]'));
    const button = currentTab.element(by.cssContainingText('button', 'Next'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }
  // TODO:Make more specific
  async newRequestFillAboutYou(first: string,
                               last: string,
                               email: string,
                               phone: string,
                               address: string,
                               city: string,
                               state: string,
                               zip: string) {

    await browser.wait(EC.visibilityOf(element(by.formControlName('firstName'))));
    await element(by.formControlName('firstName')).sendKeys(first);
    await browser.wait(EC.visibilityOf(element(by.formControlName('lastName'))));
    await element(by.formControlName('lastName')).sendKeys(last);
    await element(by.formControlName('email')).sendKeys(email);
    await element(by.formControlName('phone')).sendKeys(phone);
    await element(by.formControlName('address')).sendKeys(address);
    await element(by.formControlName('city')).sendKeys(city);
    await element(by.formControlName('state')).sendKeys(state);
    await element(by.formControlName('zip')).sendKeys(zip);
  }
  async newRequestSubmit() {
    await browser.sleep(100);
    const frame = element(by.css('re-captcha iframe'));
    await browser.wait(EC.visibilityOf(frame));
    await browser.sleep(100);
    await browser.switchTo().frame(frame.getWebElement());
    await browser.waitForAngularEnabled(false);
    const recaptcha = element(by.css('span.recaptcha-checkbox'));
    await browser.actions().mouseMove(recaptcha).perform();
    await recaptcha.click();
    await browser.waitForAngularEnabled(true);
    await browser.switchTo().defaultContent();
    // TODO: Make more specific
    const button = element(by.cssContainingText('button', 'Submit'));
    await browser.wait(EC.elementToBeClickable(button), 5000);
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }

  async newRequestIsSubmitted(): Promise<boolean> {
    // TODO: Make more specific
    const label = element(by.cssContainingText('h1', 'Request Created'));
    await browser.wait(EC.visibilityOf(label), 5000);
    return true;
  }
  async newRequestHome() {
    // TODO: Make more specific
    const button = element(by.cssContainingText('button', 'Home'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }
  async search() {
    // TODO: Make more specific
    const link = element(by.cssContainingText('a', 'Search'));
    await browser.actions().mouseMove(link).perform();
    await link.click();
    await browser.sleep(100);
  }

  async searchGetIpraId(title: string): Promise<string> {
    // TODO: Make more specific
    const titleElement = element(by.cssContainingText('div.table-rows div.col div', title));
    await browser.wait(EC.visibilityOf(titleElement));
    const idElement = titleElement.element(by.xpath('../..')).element(by.css('div.isLink'));
    return await idElement.getText();
  }

  async searchGotoRequest(id: string) {
    // TODO: Make more specific
    const div = element(by.cssContainingText('div.table-rows div.col div', id));
    await browser.actions().mouseMove(div).perform();
    await div.click();
    await browser.sleep(100);
  }

  async publicRequestGetInfo(): Promise<any> {
    const about = element(by.css('app-public-info-tab>div'));
    return {
      title: await about.element(by.xpath('div[1]/div/h5')).getText(),
      desc: await about.element(by.xpath('div[1]/p')).getText(),
      name: (await about.element(by.xpath('div[2]/div/p')).getText()).replace('Name: ', '').trim(),
      status: (await about.element(by.xpath('div[3]/p[3]')).getText()).replace('Status: ', '').trim(),
      note: (await about.element(by.xpath('div[3]/p[4]')).getText()).replace('Status Note: ', '').trim(),
    };
  }
  async publicGetFiles(): Promise<any> {
    // const tab = element(by.cssContainingText('app-public-content-tab li', 'Documents'));
    // await tab.click();
    // await browser.sleep(100);
    const divs = element.all(by.css('app-public-documents>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText()
      };
    });
  }
  async publicGetLinks(): Promise<any> {
    const tab = element(by.cssContainingText('app-public-content-tab li', 'Links'));
    await browser.actions().mouseMove(tab).perform();
    tab.click();
    await browser.sleep(200);
    const divs = element.all(by.css('app-public-links>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText(),
        url: await e.element(by.css('span.link')).getText(),
      };
    });
  }
  async publicGetInstructions(): Promise<any> {
    const tab = element(by.cssContainingText('app-public-content-tab li', 'Instructions'));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    await browser.sleep(100);
    const divs = element.all(by.css('app-requestor-instructions>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      const message = e.element(by.cssContainingText('span', 'Message:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText(),
        message: await message.element(by.xpath('following-sibling::span')).getText()
      };
    });
  }


  async successToast(count: number = 1) {
    // TODO: make more specific
    const toast = by.css('div.toast-success');
    await browser.wait(EC.visibilityOf(element.all(toast).first()), 5000);
    await element.all(toast).each(async (e) => {
      await browser.actions().mouseMove(e).perform();
      await e.click();
    });
    await browser.wait(EC.invisibilityOf(element.all(toast).first()), 5000);
  }
  async login(username: string, password: string) {
    const link = element(by.cssContainingText('app-header a', 'Sign In'));
    await browser.actions().mouseMove(link).perform();
    await link.click();
    await browser.sleep(100);
    const userElement = element(by.name('Username'));
    const passElement = element(by.name('Password'));
    const signIn = element(by.cssContainingText('div.btn', 'Sign In')); // TODO: Make more specific
    await userElement.sendKeys(username);
    await passElement.sendKeys(password);
    await browser.actions().mouseMove(signIn).perform();
    await signIn.click();
    await browser.sleep(100);
  }
  async signout() {
    const link = element(by.cssContainingText('app-header span', 'Sign Out?'));
    if (await browser.isElementPresent(by.cssContainingText('app-header span', 'Sign Out?'))) {
      await browser.actions().mouseMove(link).perform();
      await link.click();
      await this.successToast();
    }
    // await browser.sleep(100);
    // await element(by.css('div.toast-success')).click();
    // await this.waitForToast();
  }
  async dashboardGetIpraId(title: string): Promise<string> {
    const titleElement = element(by.cssContainingText('app-assignment-queue-panel div.col div', title));
    const idElement = titleElement.element(by.xpath('../..')).element(by.css('div.isLink'));
    return await idElement.getText();
  }
  async dashboardOpenFromAssignment(id: string) {
    const link = element(by.cssContainingText('app-assignment-queue-panel div.isLink', id));
    await browser.actions().mouseMove(link).perform();
    await link.click();
    await browser.sleep(100);
  }
  async dashboardOpenFromDept(id: string) {
    const link = element(by.cssContainingText('app-department-queue-panel div.isLink', id));
    await browser.actions().mouseMove(link).perform();
    await link.click();
    await browser.sleep(100);
  }
  async dashboardOpenFromAssigned(id: string) {
    const link = element(by.cssContainingText('app-assigned-requests-panel div.isLink', id));
    await browser.actions().mouseMove(link).perform();
    await link.click();
    await browser.sleep(100);
  }
  async dashboardOpenFromLegal(fileTitle: string, id: string) {
    const ipraLinks = element.all(by.cssContainingText('app-legal-queue-panel div.isLink', id));
    await ipraLinks.each(async (ipraLink: ElementFinder) => {
      const link = ipraLink.element(by.xpath('../..')).element(by.cssContainingText('div.isLink', fileTitle));
      if (await link.isPresent()) {
        await browser.actions().mouseMove(link).perform();
        await link.click();
        await browser.sleep(100);
        return;
      }
    });
    expect(false);

  }
  async requestGetInfo(): Promise<any> {
    const div = element(by.css('app-info-tab>div'));
    return {
      title: await div.element(by.xpath('div[1]/div[1]/h5')).getText(),
      desc: await div.element(by.xpath('div[3]/p')).getText(),
      name: (await div.element(by.cssContainingText('span', 'Name: ')).element(by.xpath('..')).getText()).replace('Name: ', '').trim(),
      email: (await div.element(by.cssContainingText('span', 'Email: ')).element(by.xpath('..')).getText()).replace('Email: ', '').trim(),
      contact: (await div.element(by.cssContainingText('span', 'Preferred Contact: ')).element(by.xpath('..')).getText()).replace('Preferred Contact: ', '').trim(),
      passcode: (await div.element(by.cssContainingText('span', 'Passcode: ')).element(by.xpath('..'))
      .getText()).replace('Passcode: ', '').trim(),
      status: (await div.element(by.cssContainingText('span', 'Status: ')).element(by.xpath('..'))
      .getText()).replace('Status: ', '').trim(),
      note: (await div.element(by.cssContainingText('span', 'Status Note: ')).element(by.xpath('..'))
      .getText()).replace('Status Note: ', '').trim(),
    };
  }
  async requestAssign(primary: string, secondary: string[]) {
    const tab = element(by.tagName('app-info-tab'));
    const button = tab.element(by.buttonContainingText('Assign Department'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    await browser.sleep(100);
    const dialog = element(by.tagName('app-assign-request-dialog'));
    const primaryElem = dialog.element(by.label('Primary Department'));
    await browser.actions().mouseMove(primaryElem).perform();
    await primaryElem.click();
    const deptOption = element(by.cssContainingText('mat-option', primary));
    await browser.actions().mouseMove(deptOption).perform();
    await deptOption.click();
    const assign = dialog.element(by.buttonContainingText('Assign'));
    await browser.actions().mouseMove(assign).perform();
    await assign.click();
    await browser.sleep(500);
  }
  async requestDeny(entity: string, entityName: string, entityEmail: string) {
    const tab = element(by.tagName('app-info-tab'));
    const button = tab.element(by.buttonContainingText('Deny Request'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    const dialog = element(by.tagName('app-deny-request-dialog'));
    const reason = dialog.element(by.label('Reason for Denial'));
    await browser.actions().mouseMove(reason).perform();
    await reason.click();
    const wrongEntity = element(by.cssContainingText('mat-option', 'Wrong Government Entity'));
    await browser.actions().mouseMove(wrongEntity).perform();
    await wrongEntity.click();
    const selectEntity = dialog.element(by.label('Select Entity'));
    await browser.actions().mouseMove(selectEntity).perform();
    await selectEntity.click();
    const entityOption = element(by.cssContainingText('mat-option', entity));
    await browser.actions().mouseMove(entityOption).perform();
    await entityOption.click();
    const name = dialog.element(by.label('Name'));
    await name.clear();
    await name.sendKeys(entityName);
    const email = dialog.element(by.label('Email'));
    await email.clear();
    await email.sendKeys(entityEmail);
    const submit = dialog.element(by.buttonContainingText('Submit'));
    await browser.actions().mouseMove(submit).perform();
    await submit.click();
  }
  async requestClose(reason: string) {
    const tab = element(by.tagName('app-info-tab'));
    const close = tab.element(by.buttonContainingText('Close Request'));
    await browser.wait(EC.visibilityOf(close));
    await browser.actions().mouseMove(close).perform();
    await close.click();
    const dialog = element(by.css('app-close-request-dialog'));
    // const reasonElem = dialog.element(by.label('reason'));
    const reasonElem = dialog.element(by.tagName('mat-select'));
    await browser.actions().mouseMove(reasonElem).perform();
    await reasonElem.click();
    const reasonOption = element(by.cssContainingText('mat-option', reason));
    await browser.actions().mouseMove(reasonOption).perform();
    await reasonOption.click();
    const submit = dialog.element(by.buttonContainingText('Submit'));
    await browser.wait(EC.visibilityOf(submit));
    await browser.actions().mouseMove(submit).perform();
    await submit.click();
  }
  // TODO: make more specific
  async requestContentTab() {
    const button = element(by.cssContainingText('div.mat-tab-label', 'Response'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    await browser.sleep(500);
  }
  // TODO: make more specific
  async requestWorkersTab() {
    const button = element(by.cssContainingText('div.mat-tab-label', 'Workers'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    await browser.sleep(100);
  }
  // TODO: make more specific
  async requestCommunicationsTab() {
    const button = element(by.cssContainingText('div.mat-tab-label', 'Communications'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    await browser.sleep(100);
  }


  async workerAdd(name: string) {
    const tab = element(by.tagName('app-worker-tab'));
    const select = tab.element(by.tagName('mat-select'));
    await browser.actions().mouseMove(select).perform();
    await select.click();
    const workerOption = element(by.cssContainingText('mat-option', name));
    await browser.actions().mouseMove(workerOption).perform();
    await workerOption.click();
    const button = tab.element(by.buttonContainingText('Save'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }
  async contentAddNote(title: string, data: string) {
    const tab = element(by.cssContainingText('app-response-tab li', 'Notes'));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    await browser.sleep(200);
    const notes = element(by.tagName('app-notes'));
    const addButton = notes.element(by.buttonContainingText('Add Note'));
    await browser.actions().mouseMove(addButton).perform();
    await addButton.click();
    const dialog = element(by.tagName('app-add-note-dialog'));
    const titleElem = dialog.element(by.label('Title'));
    await titleElem.sendKeys(title);
    const noteElem = dialog.element(by.label('Note'));
    await noteElem.sendKeys(data);
    const okButton = dialog.element(by.buttonContainingText('Ok'));
    await browser.actions().mouseMove(okButton).perform();
    await okButton.click();
    await browser.sleep(100);
  }
  async contentGetLastNote(): Promise<any> {
    const tab = element(by.cssContainingText('app-response-tab li', 'Notes'));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    await browser.sleep(200);
    const note = element.all(by.css('app-notes div.notes-wrapper>div')).last();
    const title = note.element(by.cssContainingText('span', 'Title:'));
    const message = note.element(by.cssContainingText('span', 'Message:'));
    const date = note.element(by.css('div.instr-date'));
    return {
      sender: (await date.getText()).split(/by /)[1].trim(),
      title: await title.element(by.xpath('following-sibling::span')).getText(),
      message: await message.element(by.xpath('following-sibling::span')).getText()

    };


  }

  async contentAddDocument(filename: string, title: string, legalReview: string, release: string) {
    const documents = element(by.tagName('app-documents'));
    const uploadButton = documents.element(by.buttonContainingText('Upload Document'));
    await browser.wait(EC.elementToBeClickable(uploadButton));
    await browser.actions().mouseMove(uploadButton).perform();
    await uploadButton.click();
    await browser.sleep(100);
    const dialog = element(by.tagName('app-upload-document-dialog'));
    const absolute = path.resolve(__dirname, filename);
    const file = dialog.element(by.css('input[type="file"]'));
    await file.sendKeys(absolute);

    await dialog.element(by.label('Title')).clear();
    await dialog.element(by.label('Title')).sendKeys(title);
    if (legalReview !== null) {
      const legalCheck = dialog.element(by.label('Needs Legal Review'));
      await browser.actions().mouseMove(legalCheck).perform();
      await legalCheck.click();
      const reason = dialog.element(by.label('Reason / Comment'));
      await reason.sendKeys(legalReview);
    }
    const releaseElem = dialog.element(by.label('Release Type'));
    await browser.actions().mouseMove(releaseElem).perform();
    await releaseElem.click();
    const releaseOption = element(by.cssContainingText('mat-option', release));
    await browser.actions().mouseMove(releaseOption).perform();
    await releaseOption.click();
    const button = dialog.element(by.buttonContainingText('Upload File'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }
  async contentAddLink(title: string, url: string, release: string) {
    const tab = element(by.cssContainingText('app-response-tab li', 'Links'));
    await browser.wait(EC.elementToBeClickable(tab));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    const link = element(by.tagName('app-links'));
    const addButton = link.element(by.buttonContainingText('Add Link'));
    await browser.wait(EC.elementToBeClickable(addButton));
    await browser.actions().mouseMove(addButton).perform();
    await addButton.click();
    const dialog = element(by.tagName('app-add-link-dialog'));
    const titleElem = dialog.element(by.label('Title'));
    await titleElem.sendKeys(title);
    const noteElem = dialog.element(by.label('Link URL'));
    await noteElem.sendKeys(url);
    const releaseElem = dialog.element(by.label('Release Type'));
    await browser.actions().mouseMove(releaseElem).perform();
    await releaseElem.click();
    const releaseOption = element(by.cssContainingText('mat-option', release));
    await browser.actions().mouseMove(releaseOption).perform();
    await releaseOption.click();
    const okButton = dialog.element(by.buttonContainingText('Ok'));
    await browser.actions().mouseMove(okButton).perform();
    await okButton.click();
    await browser.sleep(100);
  }
  async contentGetLastLink(): Promise<any> {
    return {};
  }
  async contentAddInstruction(title: string, inst: string, release: string) {
    const tab = element(by.cssContainingText('app-response-tab li', 'Instructions'));
    await browser.wait(EC.elementToBeClickable(tab));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    const instructions = element(by.tagName('app-instructions'));
    const addButton = instructions.element(by.buttonContainingText('Add Instruction'));
    await browser.wait(EC.elementToBeClickable(addButton));
    await browser.actions().mouseMove(addButton).perform();
    await addButton.click();
    const dialog = element(by.tagName('app-add-instruction-dialog'));
    const titleElem = dialog.element(by.label('Title'));
    await titleElem.sendKeys(title);
    const noteElem = dialog.element(by.label('Instruction'));
    await noteElem.sendKeys(inst);
    const releaseElem = dialog.element(by.label('Release Type'));
    await browser.actions().mouseMove(releaseElem).perform();
    await releaseElem.click();
    const releaseOption = element(by.cssContainingText('mat-option', release));
    await browser.actions().mouseMove(releaseOption).perform();
    await releaseOption.click();
    const okButton = dialog.element(by.buttonContainingText('Ok'));
    await browser.actions().mouseMove(okButton).perform();
    await okButton.click();
    await browser.sleep(100);
  }
  async contentGetLastInstruction(): Promise<any> {
    return {};
  }
  async commentsSend(comment: string) {
    const tab = element(by.tagName('app-communications-tab'));
    const textbox = tab.element(by.label('Send Comment'));
    await textbox.sendKeys(comment);
    const send = tab.element(by.buttonContainingText('Send'));
    await browser.actions().mouseMove(send).perform();
    await send.click();
    await browser.sleep(100);
  }
  async commentGetLast(): Promise<any> {
    const comment = element.all(by.css('app-communications-tab div.staff-comment,div.user-comment')).last();
    return {
      user: await comment.element(by.className('user-name')).getText(),
      comment: await comment.element(by.className('comment-section')).getText()
    };
  }
  async legalUnderReview(state: boolean) {
    const panel = element(by.tagName('app-resource-panel'));
    const toggle = panel.element(by.label('Under Review'));
    await browser.actions().mouseMove(toggle).perform();
    await toggle.click();
  }
  async legalReject(reason: string) {
    const panel = element(by.tagName('app-resource-panel'));
    const button = panel.element(by.buttonContainingText('Reject'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
    const dialog = element(by.tagName('app-deny-resource-dialog'));
    const reasonElem = dialog.element(by.label('Reason for Denial'));
    await browser.wait(EC.elementToBeClickable(reasonElem));
    await reasonElem.sendKeys(reason);
    const ok = dialog.element(by.buttonContainingText('Ok'));
    await browser.wait(EC.elementToBeClickable(ok));
    await browser.actions().mouseMove(ok).perform();
    await ok.click();
    await browser.sleep(100);

  }
  async legalApprove(release: string) {
    const panel = element(by.tagName('app-resource-panel'));
    const approve = panel.element(by.buttonContainingText('Approve'));
    await browser.actions().mouseMove(approve).perform();
    await approve.click();
    const dialog = element(by.tagName('app-approve-resource-dialog'));
    const releaseElem = dialog.element(by.label('Release Type'));
    await browser.actions().mouseMove(releaseElem).perform();
    await releaseElem.click();
    const releaseOption = element(by.cssContainingText('mat-option', release));
    await browser.actions().mouseMove(releaseOption).perform();
    await releaseOption.click();
    const button = dialog.element(by.buttonContainingText('Ok'));
    await browser.actions().mouseMove(button).perform();
    await button.click();
  }
  async legalReplace(filename: string) {
    const panel = element(by.tagName('app-resource-panel'));
    const upload = panel.element(by.buttonContainingText('Upload Revised Document'));
    await browser.actions().mouseMove(upload).perform();
    await upload.click();
    const dialog = element(by.tagName('app-upload-revised-dialog'));
    const absolute = path.resolve(__dirname, filename);
    const file = dialog.element(by.css('input[type="file"]'));
    await file.sendKeys(absolute);
    const button = dialog.element(by.buttonContainingText('Ok'));
    await browser.actions().mouseMove(button).perform();
    await button.click();

  }
  async requestorGetInfo(): Promise<any> {
    const div = element(by.css('app-requestor-info-tab>div'));
    return {
      title: await div.element(by.xpath('div[1]/div/h5')).getText(),
      desc: await div.element(by.xpath('div[1]/p')).getText(),
      name: (await div.element(by.xpath('div[2]/div[1]/p[1]')).getText()).replace('Name: ', '').trim(),
      email: (await div.element(by.xpath('div[2]/div[1]/p[2]')).getText()).replace('Email: ', '').trim(),
      status: (await div.element(by.xpath('div[3]/p[3]')).getText()).replace('Status: ', '').trim(),
    };
  }
  async requestorCommunicationsTab() {

  }
  async requestorGetLastComment(): Promise<any> {
    const comment = element.all(by.css('app-requestor-communications-tab div.staff-comment,div.user-comment')).last();
    return {
      user: await comment.element(by.className('user-name')).getText(),
      comment: await comment.element(by.className('comment-section')).getText()
    };
  }
  async requestorSendComment(comment: string) {
    const tab = element(by.tagName('app-requestor-communications-tab'));
    const textbox = tab.element(by.label('Send Comment'));
    await textbox.sendKeys(comment);
    const send = tab.element(by.buttonContainingText('Send'));
    await browser.actions().mouseMove(send).perform();
    await send.click();
    await browser.sleep(100);
  }
  async requestorGetFiles(): Promise<any> {
    // const tab = element(by.cssContainingText('app-requestor-content-tab li', 'Documents'));
    // await tab.click();
    // await browser.sleep(100);
    const divs = element.all(by.css('app-requestor-documents>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText()
      };
    });
  }
  async requestorGetLinks(): Promise<any> {
    const tab = element(by.cssContainingText('app-requestor-content-tab li', 'Links'));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    await browser.sleep(200);
    const divs = element.all(by.css('app-requestor-links>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText(),
        url: await e.element(by.css('span.link')).getText()
      };
    });
  }
  async requestorGetInstructions(): Promise<any> {
    const tab = element(by.cssContainingText('app-requestor-content-tab li', 'Instructions'));
    await browser.actions().mouseMove(tab).perform();
    await tab.click();
    await browser.sleep(100);
    const divs = element.all(by.css('app-requestor-instructions>div>div'));
    return await divs.map(async (e, i) => {
      const title = e.element(by.cssContainingText('span', 'Title:'));
      const message = e.element(by.cssContainingText('span', 'Message:'));
      return {
        title: await title.element(by.xpath('following-sibling::span')).getText(),
        message: await message.element(by.xpath('following-sibling::span')).getText()
      };
    });
  }
}
