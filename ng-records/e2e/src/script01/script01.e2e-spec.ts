import { AppPage } from '../app.po';
import { checkLogs } from '../util';
import { browser } from 'protractor';

describe('Recordbook Script 01 - simple denial, wrong entity', () => {
  let page: AppPage;

  beforeAll(() => {
    page = new AppPage();
  });

  afterEach(checkLogs);

  const id = Math.random().toString(36).substring(2, 15);
  const data = {
    title: 'Script 01 ' + id,
    desc: 'Simple denial, wrong entity',
    first: 'Script 01',
    last: id,
    email: 'script01@test.com',
    phone: '5555555555',
    address: 'Over There',
    city: 'Las Cruces',
    state: 'NM',
    zip: '88001',
    ipra_id: null,
    passcode: null,
    link: null,
    reqLink: null,
  };

  const ipraAdmin = ['pi-man-ipa', 'pass'];

  const mailhog = require('mailhog')({
    host: process.env.SMTP_SERVER,
  });

  it('Requestor enters a request', async () => {
    await mailhog.deleteAll();
    await page.navigateTo('');
    await page.newRequestBegin();
    await page.newRequestFillAbout(data.title, data.desc);
    await page.newRequestNext();
    await page.newRequestFillAboutYou(
      data.first,
      data.last,
      data.email,
      data.phone,
      data.address,
      data.city,
      data.state,
      data.zip
    );
    await page.newRequestNext();
    await page.newRequestSubmit();
    expect(await page.newRequestIsSubmitted());
    await page.newRequestHome();
    expect(await browser.getCurrentUrl()).toEqual(page.urlOf('home'));
  });
  it('Requestor receives initial email', async () => {
    await mailhog.messages().then((result) => {
      const item = result.items[1];
      const mailtoData = item.To[0];
      expect(mailtoData.Mailbox).toEqual('script01');
      expect(mailtoData.Domain).toEqual('test.com');
    });
  });
  it('Request is visible to public', async () => {
    await page.navigateTo('');
    await page.search();
    data.ipra_id = await page.searchGetIpraId(data.title);
    await page.searchGotoRequest(data.ipra_id);
    const info = await page.publicRequestGetInfo();
    expect(info.title).toEqual(data.title + ' - ' + data.ipra_id);
    expect(info.desc).toEqual(data.desc);
    expect(info.name).toEqual(data.first + ' ' + data.last);
    expect(info.status).toEqual('Open');
  });
  it('IPRA Admin receive email', async () => {
    await mailhog.messages().then((result) => {
      const item = result.items[0];
      let mailtoData = item.To.find((elt) => elt.Mailbox === 'pi-man-ipa');
      expect(mailtoData.Mailbox).toEqual('pi-man-ipa');
      expect(mailtoData.Domain).toEqual('donaanacounty.org');
      mailtoData = item.To.find((elt) => elt.Mailbox === 'lg-man-ipa');
      expect(mailtoData.Mailbox).toEqual('lg-man-ipa');
      expect(mailtoData.Domain).toEqual('donaanacounty.org');
    });
  });
  it('IPRA Admin can see request', async () => {
    await page.navigateTo('');
    await page.login.apply(null, ipraAdmin);
    await page.dashboardOpenFromAssignment(data.ipra_id);
    const info = await page.requestGetInfo();
    expect(info.title).toEqual(data.title + ' - ' + data.ipra_id);
    expect(info.desc).toEqual(data.desc);
    expect(info.name).toEqual(data.first + ' ' + data.last);
    expect(info.email).toEqual(data.email);
    expect(info.contact).toEqual('Email');
    data.passcode = info.passcode;
    data.link = 'viewRequest/' + data.ipra_id;
    data.reqLink = 'viewRequest/' + data.ipra_id + '/' + data.passcode;
    await page.signout();
  });
  it('Request is visible to requestor with pass', async () => {
    await page.navigateTo(data.reqLink);
    const info = await page.requestorGetInfo();
    expect(info.title).toEqual(data.title + ' - ' + data.ipra_id);
  });
  it('IPRA Admin rejects request, wrong entity', async () => {
    await page.navigateTo('');
    await page.login.apply(null, ipraAdmin);
    await page.dashboardOpenFromAssignment(data.ipra_id);
    await page.requestDeny(
      'Gov. Entity 2',
      'Test Gov. Entity',
      'overthere@gov.org'
    );
    await page.successToast();
    await page.signout();
  });
  it('sends an email forwarding request to correct governmental entity', async () => {
    await mailhog.messages().then((result) => {
      const item = result.items[0];
      const mailtoData = item.To.find((elt) => elt.Mailbox === 'overthere');
      expect(mailtoData.Mailbox).toEqual('overthere');
      expect(mailtoData.Domain).toEqual('gov.org');
    });
  });
  it('also sends the forwarded email to the requestor', async () => {
    await mailhog.messages().then((result) => {
      const item = result.items[0];
      const mailtoData = item.To.find((elt) => elt.Mailbox === 'script01');
      expect(mailtoData.Mailbox).toEqual('script01');
      expect(mailtoData.Domain).toEqual('test.com');
    });
  });
  it('Requestor receives deny email', async () => {
    await mailhog.messages().then((result) => {
      const item = result.items[1];
      const mailtoData = item.To.find((elt) => elt.Mailbox === 'script01');
      expect(mailtoData.Mailbox).toEqual('script01');
      expect(mailtoData.Domain).toEqual('test.com');
    });
   });
  it('Request is denied', async () => {
    await page.navigateTo('');
    await page.search();
    await page.searchGotoRequest(data.ipra_id);
    const info = await page.publicRequestGetInfo();
    expect(info.status).toEqual('Denied');
    expect(info.note).toEqual('Denied: Wrong Government Entity');
  });
});
