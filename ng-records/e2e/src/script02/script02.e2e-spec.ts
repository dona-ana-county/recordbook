import { AppPage } from '../app.po';
import { checkLogs, validateUsers } from '../util';
import { browser } from 'protractor';
import * as path from 'path';



describe('Recordbook Script 02 - three files to legal review', () => {

  let page: AppPage;



  afterEach( checkLogs);

  const id = Math.random().toString(36).substring(2, 15);
  const data = {
    title: 'Script 02 ' + id,
    desc: '3 files uploaded, 2 to legal review',
    first: 'Script 02',
    last: id,
    email: 'script02@test.com',
    phone: '5555555555',
    address: 'Over There',
    city: 'Las Cruces',
    state: 'AZ',
    zip: '88001',
    ipra_id: null,
    passcode: null,
    link: null,
    reqLink: null,
    filename: '../upload.txt',
    redactedFilename: '../upload-redacted.txt',
    comment: 'I need some more info',
    commentResponse: 'Sure thing: more info',
    noteTitle: 'Instructions',
    noteData: 'We need to give these types of files',
    closeReason: 'All Response Documents Added',
    linkTitle: 'This is a link',
    linkUrl: 'http://Overthere.google.com',
    instTitle: 'Pick up DVD',
    inst: 'To pick up the DVD, please go to the Clerk\'s office. The DVD will be $5',


  };

  const ipraAdmin = ['pi-man-ipa', 'pass'];
  const deptMan = ['it-man', 'pass'];
  const deptManName = 'IT Manager';
  const deptWorker = ['it-wrk', 'pass'];
  const deptWorkerName = 'IT Worker';
  const legal = ['lg-wrk-leg', 'pass'];
  const primaryDept = 'Information Technology';

  beforeAll( () => {
    page = new AppPage();
    browser.wait(validateUsers([
      ipraAdmin,
      deptMan,
      deptWorker,
      legal
    ]));
  });

  // TODO: verify emails
  it('Requestor enters a request', async () => {
    await page.navigateTo('');
    await page.signout();
    await page.newRequestBegin();
    await page.newRequestFillAbout(data.title, data.desc);
    await page.newRequestNext();
    await page.newRequestFillAboutYou(data.first, data.last, data.email, data.phone, data.address, data.city, data.state, data.zip);
    await page.newRequestNext();
    await page.newRequestSubmit();
    expect(await page.newRequestIsSubmitted());
    await page.newRequestHome();
    expect(await browser.getCurrentUrl()).toEqual(page.urlOf('home'));
  });
  describe('Request is assigned', () => {
    it('IPRA Admin assignes request to department as primary', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, ipraAdmin);
      data.ipra_id = await page.dashboardGetIpraId(data.title);
      await page.dashboardOpenFromAssignment(data.ipra_id);
      const info = await page.requestGetInfo();
      data.passcode = info.passcode;
      data.link = 'viewRequest/' + data.ipra_id;
      data.reqLink = 'viewRequest/' + data.ipra_id + '/' + data.passcode;
      await page.requestAssign(primaryDept, []);
      await page.signout();
    });
    it('Dept Manager assigns IT Worker to request', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptMan);
      await page.dashboardOpenFromDept(data.ipra_id);
      await page.requestWorkersTab();
      await page.workerAdd(deptWorkerName);
      await page.successToast();
      await page.signout();

    });
  });
  describe('Dept manager sends note to dept worker', () => {
    it('Dept Manager adds note instructing document types', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptMan);
      await page.dashboardOpenFromDept(data.ipra_id);
      await page.requestContentTab();
      await page.contentAddNote(data.noteTitle, data.noteData);
      await page.signout();
    });
    it('Dept Worker sees note', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptWorker);
      await page.dashboardOpenFromAssigned(data.ipra_id);
      await page.requestContentTab();
      const note = await page.contentGetLastNote();
      expect(note.title).toEqual(data.noteTitle);
      expect(note.message).toEqual(data.noteData);
      expect(note.sender).toEqual(deptManName);
      await page.signout();
    });
  });
  describe('Worker and requester send messages', () => {
    it('Dept Worker sends a message to requestor', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptWorker);
      await page.dashboardOpenFromAssigned(data.ipra_id);
      await page.requestCommunicationsTab();
      await page.commentsSend(data.comment);
      await page.signout();

    });
    it('Requestor responds', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.navigateTo(data.reqLink);
      await page.requestCommunicationsTab();
      const comment = await page.requestorGetLastComment();
      expect(comment.comment).toEqual(data.comment);
      await page.requestorSendComment(data.commentResponse);

    });
    it('Dept worker verifies comment', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptWorker);
      await page.dashboardOpenFromAssigned(data.ipra_id);
      await page.requestCommunicationsTab();
      const commentResponse = await page.commentGetLast();
      expect(commentResponse.comment).toEqual(data.commentResponse);
      await page.signout();
    });
  });
  describe('Files are added', () => {
    describe('Dept Worker adds 3 files', async () => {
      beforeAll(async () => {
        await page.navigateTo('');
        await page.signout();
        await page.login.apply(null, deptWorker);
        await page.dashboardOpenFromAssigned(data.ipra_id);
        await page.requestContentTab();
      });
      it('Add File1', async () => {
        await page.contentAddDocument(data.filename, 'File1', null, 'Requestor Only');
        await page.successToast();
      });
      it('Add File2', async () => {
        await page.contentAddDocument(data.filename, 'File2', 'Needs Review', 'Requestor Only');
        await page.successToast();
      });
      it('Add File3', async () => {
        await page.contentAddDocument(data.filename, 'File3', 'Needs Review', 'Requestor Only');
        await page.successToast();
      });
      afterAll(async () => {
        await page.signout();
      });
    });
    it('Legal rejects file2 from legal review', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, legal);
      await page.dashboardOpenFromLegal('File2', data.ipra_id);
      await page.legalUnderReview(true);
      await page.successToast();
      await page.legalReject('I do not like it');
      await page.successToast();
      // TODO: verify rejected in logs
      // TODO: Verify rejected email
    });
    it('Legal replaces file3 with replacement file', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, legal);
      await page.dashboardOpenFromLegal('File3', data.ipra_id);
      await page.legalUnderReview(true);
      await page.successToast();
      await page.legalReplace(data.redactedFilename);
      await page.successToast();
      await page.legalApprove('Public');
      await page.successToast(); // ?? Two toasts?
      // TODO: verfiy file replaced
    });
  });
  describe('Manager adds content', () => {
    it('Dept Manager adds a link', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptMan);
      await page.dashboardOpenFromDept(data.ipra_id);
      await page.requestContentTab();
      await page.contentAddLink(data.linkTitle, data.linkUrl, 'Public');
      await browser.sleep(100);
      await page.signout();
    });
    it('Dept Manager adds an instruction', async () => {
      await page.navigateTo('');
      await page.signout();
      await page.login.apply(null, deptMan);
      await page.dashboardOpenFromDept(data.ipra_id);
      await page.requestContentTab();
      await page.contentAddInstruction(data.instTitle, data.inst, 'Requestor Only');
      await browser.sleep(100);
      await page.signout();
    });
  });
  it('Dept Manager closes request', async () => {
    await page.navigateTo('');
    await page.signout();
    await page.login.apply(null, deptMan);
    await page.dashboardOpenFromDept(data.ipra_id);
    await page.requestClose(data.closeReason);
    await page.successToast();
  });
  it('requester gets close email');
  describe('Requestor can see response', () => {
    beforeAll(async () => {
      await page.navigateTo('');
      await page.signout();
      await page.navigateTo(data.reqLink);
    });
    it('info is correct', async () => {
      const info = await page.requestorGetInfo();
      expect(info.title).toEqual(data.title + ' - ' + data.ipra_id);
      expect(info.status).toEqual('Closed');
    });
    it('files are visible', async () => {
      await page.requestContentTab();
      const files = await page.requestorGetFiles();
      expect(files.length).toEqual(2);
      if (files.length >= 2) {
        // TODO: Check download
        // expect(files[1].title).toEqual(path.basename(data.redactedFilename));
        expect(files[0].title).toEqual('File3.txt');
        // expect(files[0].filename).toEqual(path.basename(data.filename));
        expect(files[1].title).toEqual('File1.txt');
      }
    });
    it('file3 is redacted'); // TODO: File3 is redacted
    it('links are visible', async () => {
      const links = await page.requestorGetLinks();
      expect(links.length).toEqual(1);
      if (links.length >= 1) {
        expect(links[0].title).toEqual(data.linkTitle);
        expect(links[0].url).toEqual(data.linkUrl);
      }
    });
    it('Instruments are visible', async () => {
      const instrs = await page.requestorGetInstructions();
      expect(instrs.length).toEqual(1);
      if (instrs.length >= 1) {
        expect(instrs[0].title).toEqual(data.instTitle);
        expect(instrs[0].message).toEqual(data.inst);
      }
    });
    it('Notes are visible');
  });
  describe('Public can see response', () => {
    beforeAll(async () => {
      await page.navigateTo('');
      await page.signout();
      await page.search();
      await page.searchGotoRequest(data.ipra_id);
    });
    it('info is correct', async () => {
      const info = await page.publicRequestGetInfo();
      expect(info.status).toEqual('Closed');
      expect(info.note).toEqual(data.closeReason);
    });
    it('files are visible', async () => {
      await page.requestContentTab();
      const files = await page.publicGetFiles();
      expect(files.length).toEqual(1);
      if (files.length >= 1) {
        // TODO: Check download
        // expect(files[0].title).toEqual(path.basename(data.redactedFilename));
        expect(files[0].title).toEqual('File3.txt');
      }
    });
    it('file3 is redacted'); // TODO: File3 is redacted
    it('links are visible', async () => {
      const links = await page.publicGetLinks();
      expect(links.length).toEqual(1);
      if (links.length >= 1) {
        expect(links[0].title).toEqual(data.linkTitle);
        expect(links[0].url).toEqual(data.linkUrl);
      }
    });
    it('Instruments are visible', async () => {
      const instrs = await page.publicGetInstructions();
      expect(instrs.length).toEqual(0);
    });
    it('Notes are visible');
  });
});
