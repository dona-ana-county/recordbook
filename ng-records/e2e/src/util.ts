import { browser, logging } from 'protractor';
import { post} from 'request-promise-native';

const baseApi = browser.params.baseApi;

export async function checkLogs() {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
}

export async function validateUsers(users: string[][]) {
  for (const user of users) {
    const loginOptions = {
      uri: baseApi + 'auth/login',
      body: {
        username: user[0],
        password: user[1]
      },
      json: true
    };
    let result = await post(loginOptions);
    const token = result.token.token;
    const logoutOptions = {
      uri: baseApi + 'auth/logout',
      body: { },
      json: true,
      headers: {
        authorization: token
      }
    };
    result = await post(logoutOptions);
  }
}
