import { AppPage } from '../app.po';
import { checkLogs } from '../util';

describe('Recordbook basic tests', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', async () => {
    await page.navigateTo('');
    expect(await page.getTitleText()).toEqual('Notice of Right to Inspect Public Records');
  });

  it('should display welcome message in spanish', async () => {
    await page.navigateTo('');
    await page.setLanguage('es');
    expect(await page.getTitleText()).toEqual('Aviso de Derecho a Inspeccionar Registros Públicos');
    await page.setLanguage('en');
  });

  afterEach( checkLogs);

});
