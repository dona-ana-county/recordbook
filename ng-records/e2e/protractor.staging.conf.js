// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const defaultConfig = require('./protractor.conf');
let stagingConfig = {
  baseUrl: 'http://localhost/',
  params: {
    baseApi: 'http://localhost/api/'
  }
}
exports.config = Object.assign(defaultConfig.config, stagingConfig);
