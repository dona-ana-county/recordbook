#!/bin/bash

#SHA=2be83c2cb45685ee87a4eac38c965ed410d0e8e1
#CI_REGISTRY_IMAGE=registry.gitlab.com/dona-ana-county/recordbook
#CI_PROJECT_DIR=/home/matthewh/ipra/DonaAnaRecords/

docker network create e2e
docker run --network e2e -d --network-alias postgres --env-file common.e2e.env --env POSTGRES_PASSWORD=ShaiKahsheetha9 postgres:latest
docker run --network e2e -d --network-alias elasticsearch --env-file common.e2e.env --env discovery.type=single-node docker.elastic.co/elasticsearch/elasticsearch:7.17.21
docker run --network e2e -d --network-alias recordbook-smtp $CI_REGISTRY_IMAGE/mailhog:latest
docker run --network e2e  --rm -d --env-file common.e2e.env --env-file backend.e2e.env $CI_REGISTRY_IMAGE/api:$SHA flask manage db_init --testdata --superpass pass

docker pull $CI_REGISTRY_IMAGE/frontend:$SHA
docker pull $CI_REGISTRY_IMAGE/protractor:latest

docker run --network e2e -d --network-alias recordbook-api  --env-file common.e2e.env --env-file backend.e2e.env  --name recordbook-api $CI_REGISTRY_IMAGE/api:$SHA
docker exec recordbook-api flask manage verify --wait --no-upgrade
docker run --network e2e -d --network-alias recordbook --env-file common.e2e.env  $CI_REGISTRY_IMAGE/frontend:$SHA
docker run --network e2e -d --network-alias e2e --env-file backend.e2e.env --mount type=bind,source=$CI_PROJECT_DIR,destination=/project --name e2e $CI_REGISTRY_IMAGE/protractor:latest
docker exec e2e /project/ng-records/e2e/e2e.sh
