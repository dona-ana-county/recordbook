// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  suites: {
    basic: './src/basic/*.e2e-spec.ts',
    script01: './src/script01/*.e2e-spec.ts',
    script02: './src/script02/*.e2e-spec.ts',
    other: './src/*.e2e-spec.ts'
  },
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:5000/',
  params: {
    baseApi: 'http://localhost:5001/'
  },
  chromeDriver: require(`chromedriver/lib/chromedriver`).path,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  SELENIUM_PROMISE_MANAGER: false,
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    by.addLocator('formControlName', (value, opt_parentElement, opt_rootSelector) => {
      const using = opt_parentElement || document;
      return using.querySelectorAll('[formControlName="' + value + '"]');
    });
    by.addLocator('label', (text, opt_parentElement, opt_rootSelector) => {
      const using = opt_parentElement || document;
      const labels = using.querySelectorAll('label');
      const goodLabels = Array.prototype.filter.call(labels, function (label) {
        return label.textContent.indexOf(text) !== -1;
      });
      return Array.prototype.map.call(goodLabels, function(label) {
        const id = label.getAttribute('for');
        element = using.querySelector('#' + id);
        while (element.getAttribute('class').indexOf('hidden') !== -1) {
          element = element.parentElement;
        }
        return element;
      });
    });
    by.addLocator('buttonContainingText', (text, opt_parentElement, opt_rootSelector) => {
      const using = opt_parentElement || document;
      const buttons = using.querySelectorAll('button');
      return Array.prototype.filter.call(buttons, function(button) {
        return button.textContent.indexOf(text) !== -1;
      })
    })
  }
};
