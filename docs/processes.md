# Processes for working on the repo

## Dev Tools

- VS code
- Docker and docker-compose
- Pipenv for python virtual environment management
- Angular cli
- Postman

### Useful VS Code Extensions

- Angular Essentials by John Papa
  - Angular inline
  - Angular language service
  - Angular Snippets
  - Chrome Debugger
  - Editor Config
  - Material Icon Theme
  - npm
  - Peacock
  - Prettier
  - tslint
  - Winter is coming theme
- Docker by Microsoft
- GitLens by Eric Amodio
- Live share by Microsoft
- Markdownlint by David Anson
- Path Intellisense by Christian Kohler
- Python by Microsoft
- Visual Studio IntelliCode
