# API Endpoints

## Auth

## Communications

## Config (Should probably be removed)

## Constants

## Content

## Departments

## Queues

## Requests

- newStaffRequest
- newPublicRequest
- getPublicRequest
- getRequestorRequest
- getStaffRequest
- getAllRequests: This should probably be filtered some how (maybe have a get by status)
- getAssignedStaff
- getPrimaryDepartment
- getSecondaryDepartments
- getCommunications: Could probably use this for the alerts and for the comments
- getLog
- closeRequest
- denyRequest
- denyRequestWrongGovernmentEntity
- reopenRequest
- updateStaff
- assignStaff
- unassignStaff
- assignPrimaryDepartment
- assignSecondaryDepartment
- updateStatus: Don't think we should have this one
- updateDueDate
- updatePublicDescription

## Search

## Users
