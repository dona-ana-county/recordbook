# Developer Notes

## Test Users
1. pi-man-ipa - Public Information Manger w/ Ipra Admin
2. lg-man-ipa - Legal Manager w/Ipra Admin and Legal Queue
3. lg-wrk-leg - Legal Worker w/ Legal Queue
4. it-man - Information Technology Manager
5. it-wrk - Information Technology Worker
6. super - Superuser

## To (re)initialize the database

From the commandline with docker:
    docker-compose exec api flask manage db_init

From the commandline with docker in staging
    docker-compose -f docker-compose.staging.yml exec api-staging flask manage db_init

from the commandline with docker With test data
    docker-compose exec api flask manage db_init --testdata

from the commandline with docker With local data
    docker-compose exec api flask manage db_init  --localdata



## to (re) initialize to index

From the commandline:
    docker-compose exec api flask manage es_init

From the commandline in staging
    docker-compose -f docker-compose.staging.yml exec api-staging flask manage es_init

From the url
    http://localhost:5001/reset/es

From the url in staging
    http://staging/api/reset/es


## Upgrading the database

from the commandline:
    docker-compose exec api flask db upgrade

On start, set environtment Variable (ex in .env-docker-prod or .env-docker-dev)
    UPGRADE_DB=dangerous


## End-to-end (e2e) testing

Testing is done through protractor.

Initializtion (from ng-records):
    npm install --no-save protractor

Running against a docker setup (from ng-records):
    npm run e2e

Run a single test-suite (from ng-records)
    npm run e2e -- --suite=script02

Run against a staging docker setup
    npm run e2e:staging

## Unit testing

Unit testing on the python side is done through pytest.  The python app is 
run locally, but it needs to connect to the database and elasticsearch
servers

Ensure you have a .env setup with database and elasticsearch uris (see sample.env)

You must have a full python environment setup ( from flask-records):
    pipenv install --dev

To run tests from commandline, you must first activate the python environment (from flask-records):
    pipenv shell

Run tests from activated commandline (from diretory root):
    pytest

From vscode
1. Open any python (.py) file under flask-records to init python extension
2. Select "Test" from left hand side
3. Ensure tests are discovered, if not click "Discover Tests" along bar on top
4. Click "run all tests" along bar on top
