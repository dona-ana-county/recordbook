# Domain Model

| Request |  |
| --- | --- |
| id | string |
| title | string |
| description | string |
| create date | date |
| due date | date |
| status | Status (enum) |
| requestor | Requestor |
| primary department | Department |
| subordinate departments | Department[] |
| assigned staff | Staff[] |
| content | Content[] |
| log | LogMessage |
---

| Requestor |  |
| --- | --- |
| id | uuid |
| first name | string |
| last name | string |
| email | string |
| phone | string |
| fax | string |
| address line 1 | string |
| address line 2 | string |
| city | string |
| state | string |
| zip | int |
---

| Staff |  |
| --- | --- |
| id | uuid |
| username | string |
| password | string |
| first name | string |
| last name | string |
| email | string |
| token | Auth Token |
| role | Role (Enum) |
| groups | Group[] (Enum) |
---

| Department |  |
| --- | --- |
| id | uuid |
| name | string |
| category | string |
| staff | Staff[] |
---

| Auth Token |  |
| --- | --- |
|  |  |
---

| Queue |  |
| --- | --- |
| items | QueueItem[] |
---

| Queue Item |  |
| --- | --- |
| enqueue date | date |
| dequeue date | date |
---

| Assignment Queue | Queue |
| --- | --- |
| items | AssignmentQueueItem[] |
---

| Department Queue | Queue |
| --- | --- |
| items | DepartmentQueueItem[] |
---

| Legal Queue | Queue |
| --- | --- |
| items | LegalQueueItem[] |
---

| Assignment Queue Item | Queue Item |
| --- | --- |
| item | Request |
---

| Department Queue Item | Queue Item |
| --- | --- |
| item | Request |
---

| Legal Queue Item | Queue |
| --- | --- |
| item | Document |
---

This class is an abstract base class that we extend to implement the other
kinds of events.

| Event |  |
| --- | --- |
|  |  |
---

| Log Message |  |
| --- | --- |
| date | date |
| message | string |
| event code | Event code (Enum) |
---

| Notification |  |
| --- | --- |
| message | string |
| recipient | string |
| email | string |
---

Not 100% sure how we are going to implement these content classes yet. I just
wanted to have them here.

| Content |  |
| --- | --- |
| id | uuid |
| release type | Release Type (Enum) |
---

| Document/File | Content |
| --- | --- |
|  |  |
---

| Link | Content |
| --- | --- |
|  |  |
---

| Instruction | Content |
| --- | --- |
|  |  |
---

| Note | Content |
| --- | --- |
|  |  |
---

| Government Entity |  |
| --- | --- |
|  |  |
---

## Enums

| Status |  |
| --- | --- |
| Open |  |
| Closed |  |
| Denied |  |
---

| Group |  |
| --- | --- |
| IPRA Admin |  |
| IPRA Clerk |  |
| Legal |  |
| Super user |  |
---

| Role |  |
| --- | --- |
| Worker |  |
| Manager |  |
---

| Event Code |  |
| --- | --- |
|  |  |
---

| Release Type |  |
| --- | --- |
| Public |  |
| Private |  |
| Requestor |  |
---