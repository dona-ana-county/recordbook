# Domain Dictionary

This file will outline common terms we use in stories and explain what we mean
when we use those terms. If we are using them incorrectly we can adjust those
definitions here so everyone is speaking the same language.
