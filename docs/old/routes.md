# Routes

## Current Routes

/auth/login [POST]
/auth/logout [POST]

/department/getAllDepartments [GET]
/department/getDepartmentById/<department-id> [GET]
/department/getDepartmentByStaff/<staff-id> [GET]

/governmentEntity/getAllGovernmentEntities [GET]

/queue/getAssignmentQueue [GET]
/queue/getDepartmentQueue/<department-id> [GET]
/queue/removeItemFromQueue/<item-id> [GET]

/request/newRequest [POST]
/request/newStaffRequest [POST]
/request/getRequest/<request-id> [GET]
/request/openRequest/<request-id> [GET]
/request/closeRequest/<request-id> [GET]
/request/updateRequestDueDate/<request-id>/<due-dt> [GET]
/request/denyRequest/<denial-id> [POST]     -- Not sure why post
/request/denyRequestWrongGovernmentEntity/<entity-id> [POST]
/request/assignPrimaryDepartment [POST]
/request/assignSubordinateDepartment [POST]
/request/getAssignedStaff/<request-id>  [GET]
/request/assignStaff/<request-id>/<staff-id> [GET]

## Updated/new routes

/auth/login [POST]
/auth/logout [POST]

/department/getAllDepartments [GET]
/department/getDepartmentById/<department-id> [GET]
/department/getDepartmentByStaff/<staff-id> [GET]

/governmentEntity/getAllGovernmentEntities [GET]

/queue/getAssignmentQueue [GET]
/queue/getDepartmentQueue/<department-id> [GET]
/queue/removeItemFromQueue/<item-id> [GET]

/request/newPublic [POST]
/request/newStaff [POST]
/request/get/<request-id> [GET]
/request/getAssignedStaff/<request-id>  [GET]
/request/getAssignedDepartments/<request-id>
/request/updateStatus/<request-id>/<status> [GET]
/request/updateDueDate/<request-id>/<due-dt> [GET]
/request/deny/<request-id>/<reason> [GET]
/request/denyWrongGovernmentEntity/<request-id>/<entity-id> [GET]
/request/assignDepartment/<request-id>/<department-id> [GET]
/request/assignStaff/<request-id>/<staff-id> [GET]

