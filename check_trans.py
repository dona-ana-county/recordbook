import re
import json

from collections import defaultdict

id_pattern = re.compile(r"text(?:Service)?\.get\('([^']+)'\)")

grep_all = defaultdict(list)
grep_external = defaultdict(list)

prefix_pattern = re.compile(r'ng-records/src/app/[^/]+/[^/]+/')

prefixes = set()

external_whitelist = {
 'ng-records/src/app/components/header/',
 'ng-records/src/app/components/multi-chip-selector/',
 'ng-records/src/app/components/steele-table/',
 'ng-records/src/app/dialogs/upload-document-dialog/',
 'ng-records/src/app/pages/home/',
 'ng-records/src/app/pages/public-new-request/',
 'ng-records/src/app/pages/public-search-request/',
 'ng-records/src/app/pages/public-view-request/',
 'ng-records/src/app/pages/requestor-view-request/',
 'ng-records/src/app/pages/signin/',
}

dynamic_ext_keys = {
    'id',
    'title',
    'status',
    'department',
    'create_date',
    'due_date',
    'date_uploaded',
    'type',
    'download',
    'open',
    'closed',
    'denied',
    'unassigned',
}

dynamic_int_keys = {
    'event',
    'date',
    'message',
    'title',
    'date_uploaded',
    'type',
    'release_type',
    'download',
    'name',
    'role',
    'department',
    'id',
    'create_date',
    'due_date',
    'extension_status',
    'status',
    'request_id',
    'upload_date',
    'open',
    'closed',
    'denied',   
    'unassigned' 
}

import subprocess
import sys

process = subprocess.run(["grep","-ER", r"text(Service)?\.get\('[^']+'\)", "ng-records/src/app"],  text=True, capture_output=True, check=True)


for line_no, line in enumerate(process.stdout.splitlines()) :
    id_match = id_pattern.search(line)
    assert id_match and id_match[1], f"error reading line {line_no} of grep output: {line}"

    prefix_match = prefix_pattern.match(line)
    if prefix_match[0] in external_whitelist:
        grep_external[id_match[1]].append(line)
    grep_all[id_match[1]].append(line)

for k in dynamic_ext_keys:
    grep_all[k].append('dynamic')      
    grep_external[k].append('dynamic')
for k in dynamic_int_keys:
    grep_all[k].append('dynamic')    



with open("ng-records/src/app/text_ext.json") as f:
    external_dict = json.load(f)

with open("ng-records/src/app/text_int.json") as f:
    internal_dict = json.load(f)

errors = 0

def error(message):
    global errors
    print(message, file=sys.stderr)
    errors += 1

for k in grep_external.keys():
    if k not in external_dict:
        error(f"Grepped Key not found in external json: {k}")
    if k in internal_dict:
        error(f"Grepped key in internal json and external json: {k}")

for k in grep_all.keys():
    if k not in internal_dict and k not in external_dict:
        error(f"Grepped key not in external or internal json: {k}")

for k,langs in external_dict.items():
    if k not in grep_external:
        print(f"Warning: Extra external key: {k}")
    for lang in ('es', 'en'):
        if lang not in langs:
            error(f"{lang} is missing for key: {k}" )

    if langs['en'].strip():
        for lang, value in langs.items():
            if not value.strip():
                error(f"{lang} is empty for key : {k}")
    else:
        for lang, value in langs.items():
            if value.strip():
                error(f"{lang} is not empty for empty key: {k}")

for k,langs in internal_dict.items():
    if k not in grep_all:
        print(f"Warning: Extra internal key: {k}")    
    for lang in ('en',):
        if lang not in langs or  langs[lang].strip() == '':
            error(f"{lang} is missing or empty for key: {k}" )

if errors>0:
    print(f"Errors Found: {errors}")
else:
    print("No Errors Found")