#!/usr/bin/env bash
set -eu

if [[ -n "${TAKE_FILE_OWNERSHIP-}" ]]; then
    chown -R appuser:0 /uploads
fi
exec pysu appuser:appuser "$@"