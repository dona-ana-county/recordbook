import os
import time

from records_api import create_app
from records_api.manage.alembic import verify_schema

app = create_app(os.getenv("FLASK_CONFIG") or "default")

if __name__ == "__main__":
    if not app.debug or os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
        if not verify_schema(app, os.environ.get("UPGRADE_DB") == "dangerous"):
            app.logger.error("App not started")
            while True:
                time.sleep(1)
        if False:
            try:
                import ptvsd
                app.logger.info("Enabling Remote Debugger")
                ptvsd.enable_attach(address=("0.0.0.0", 5678))
                app.logger.info("Remote Debugger active")
                #ptvsd.wait_for_attach()
                #app.logger.info("Remote Debugger Attached")
        
            except Exception as ex:
                app.logger.error("Remote Debugger Not working: ")

    app.logger.info("Running App")
    app.run(host="0.0.0.0", port=8080, debug=True)
