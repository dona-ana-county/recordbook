from typing import List
from uuid import UUID


from records_api.service import Service

from .repo import QueueRepo
from .model import RequestQueueItem, ResourceQueueItem


class QueueService(Service):
    def __init__(self, app):
        self.repo = QueueRepo()

    def get_assignment_queue(self) -> List[RequestQueueItem]:
        return self.repo.get_assignment_queue()

    def get_department_queue(self, dept_id: UUID):
        return self.repo.get_department_queue(dept_id)

    def get_all_department_queues(self):
        return self.repo.get_all_department_queues()

    def get_assigned_requests(self, staff_id: UUID):
        return self.repo.get_assigned_requests(staff_id)

    def get_legal_queue(self) -> List[ResourceQueueItem]:
        return self.repo.get_legal_queue()