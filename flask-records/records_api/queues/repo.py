# import logging
# logging.basicConfig()
# logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)
# logging.getLogger("sqlalchemy.pool").setLevel(logging.DEBUG)

from typing import List
from uuid import UUID

from records_api import db
from records_api.constant import (extension_statuses, request_statuses,
                                  resource_statuses)
from records_api.queues.model import RequestQueueItem, ResourceQueueItem
from records_api.serializer.db_schema import Content as DbContent
from records_api.serializer.db_schema import Request as DbRequest
from records_api.serializer.db_schema import \
    RequestAssignedDepartment as DbRequestAssignedDepartment
from records_api.serializer.db_schema import \
    RequestAssignedStaff as DbRequestAssignedStaff
from records_api.serializer.db_schema import RequestContent as DbRequestContent
from records_api.serializer.db_schema import Resource as DbResource
from records_api.serializer.db_schema import \
    ResourceMessage as DbResourceMessage
from records_api.serializer.db_schema import Requestor as DbRequestor
from records_api.serializer.db_schema import User as DbUser

# from flask import current_app

class QueueRepo:
    def __init__(self):
        pass
    def get_assignment_queue(self) -> List[RequestQueueItem]:
        requests: List[RequestQueueItem] = []
        db_requests = (
            (
                db.session.query(DbRequest, DbUser).join(
                    DbRequestor, 
                    DbRequest.id_ == DbRequestor.request_id
                ).join(
                    DbUser, 
                    DbRequestor.id_ == DbUser.id_
                ).outerjoin(
                    DbRequestAssignedDepartment,
                    DbRequestAssignedDepartment.request_id == DbRequest.id_,
                )
            )
            .filter(DbRequestAssignedDepartment.request_id == None, DbRequest.status == "OPN")
            .order_by(DbRequest.due_dt)
            .all()
        )
        for db_req in db_requests:
            requests.append(
                RequestQueueItem(
                    id_=db_req[0].id_,
                    requestor_name=f"{db_req[1].first_name} {db_req[1].last_name }",
                    title=db_req[0].title,
                    create_dt=db_req[0].create_dt,
                    due_dt=db_req[0].due_dt,
                    # status=request_statuses[db_req.status],
                    extension_status=extension_statuses[db_req[0].extension_status],
                    completion_status=None
                )
            )
        return requests

    def get_department_queue(self, dept_id: UUID) -> List[RequestQueueItem]:
        requests: List[RequestQueueItem] = []
        db_requests = (
            (
                db.session.query(DbRequest, DbUser, DbRequestAssignedDepartment).join(
                    DbRequestAssignedDepartment,
                    DbRequestAssignedDepartment.request_id == DbRequest.id_,
                ).join(
                    DbRequestor, 
                    DbRequest.id_ == DbRequestor.request_id
                ).join(
                    DbUser, 
                    DbRequestor.id_ == DbUser.id_
                )
            )
            .filter(
                DbRequestAssignedDepartment.department_id == dept_id,
                DbRequestAssignedDepartment.unassigned_date == None,
                DbRequest.status == 'OPN'
            )
            .order_by(DbRequestAssignedDepartment.completed, DbRequest.due_dt)
            .all()
        )
        for db_req in db_requests:
            requests.append(
                RequestQueueItem(
                    id_=db_req[0].id_,
                    requestor_name=f"{db_req[1].first_name} {db_req[1].last_name }",
                    title=db_req[0].title,
                    create_dt=db_req[0].create_dt,
                    due_dt=db_req[0].due_dt,
                    # status=request_statuses[db_req.status],
                    extension_status=extension_statuses[db_req[0].extension_status],
                    completion_status=db_req[2].completed
                )
            )
        return requests

    def get_all_department_queues(self) -> List[RequestQueueItem]:
        requests: List[RequestQueueItem] = []
        db_requests = (
            (
                db.session.query(DbRequest, DbUser).join(
                    DbRequestAssignedDepartment,
                    DbRequestAssignedDepartment.request_id == DbRequest.id_,
                ).join(
                    DbRequestor, 
                    DbRequest.id_ == DbRequestor.request_id
                ).join(
                    DbUser, 
                    DbRequestor.id_ == DbUser.id_
                )
            )
            .filter(DbRequest.status == 'OPN')
            # .filter(DbRequestAssignedDepartment.department_id)
            .order_by(DbRequest.due_dt)
            .all()
        )
        for db_req in db_requests:
            requests.append(
                RequestQueueItem(
                    id_=db_req[0].id_,
                    requestor_name=f"{db_req[1].first_name} {db_req[1].last_name }",
                    title=db_req[0].title,
                    create_dt=db_req[0].create_dt,
                    due_dt=db_req[0].due_dt,
                    # status=request_statuses[db_req.status],
                    extension_status=extension_statuses[db_req[0].extension_status],
                    completion_status=None
                )
            )
        return requests

    def get_assigned_requests(self, staff_id: UUID) -> List[RequestQueueItem]:
        requests: List[RequestQueueItem] = []
        db_requests = (
            (
                db.session.query(DbRequest, DbUser).join(
                    DbRequestAssignedStaff, DbRequestAssignedStaff.request_id == DbRequest.id_
                ).join(
                    DbRequestor, 
                    DbRequest.id_ == DbRequestor.request_id
                ).join(
                    DbUser, 
                    DbRequestor.id_ == DbUser.id_
                )
            )
            .filter(
                DbRequestAssignedStaff.staff_id == staff_id,
                DbRequestAssignedStaff.unassigned_date == None,
                DbRequest.status == 'OPN'
            )
            .order_by(DbRequest.due_dt)
            .all()
        )
        for db_req in db_requests:
            requests.append(
                RequestQueueItem(
                    id_=db_req[0].id_,
                    requestor_name=f"{db_req[1].first_name} {db_req[1].last_name }",
                    title=db_req[0].title,
                    create_dt=db_req[0].create_dt,
                    due_dt=db_req[0].due_dt,
                    # status=request_statuses[db_req.status],
                    extension_status=extension_statuses[db_req[0].extension_status],
                    completion_status=None
                )
            )
        return requests

    def get_legal_queue(self) -> List[ResourceQueueItem]:
        resources = []
        db_resources = (
            db.session.query(DbResource, DbContent, DbRequestContent, DbResourceMessage)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .outerjoin(DbResourceMessage, DbResourceMessage.resource_id == DbResource.id_)
            .filter(DbResource.needs_review)
            .order_by(DbRequest.due_dt)
            .all()
        )
        for db_res in db_resources:
            res_item = ResourceQueueItem(
                id_=db_res[1].id_,
                title=db_res[1].title,
                request_id=db_res[2].request_id,
                upload_dt=db_res[1].create_dt,
            )
            if db_res[0].under_review:
                res_item.status = resource_statuses["UND"]
            else:
                res_item.status = resource_statuses["NUR"]
            resources.append(res_item)
        return resources
