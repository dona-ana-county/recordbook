from dataclasses import dataclass
from datetime import date, datetime
from uuid import UUID

from records_api.constants.model import Lookup
from records_api.requests.model import (ExtensionStatus, RequestStatus,
                                        ResourceStatus)


@dataclass
class QueueType(Lookup):
    def __repr__(self):
        return f"QueueType(Code: {self.code}, Value: {self.value})"


@dataclass
class QueueItemType(Lookup):
    def __repr__(self):
        return f"QueueItemType(Code: {self.code}, Value: {self.value})"


@dataclass
class RequestQueueItem:
    id_: str
    requestor_name: str
    title: str
    create_dt: date
    due_dt: date
    # status: RequestStatus
    extension_status: ExtensionStatus
    completion_status: bool


@dataclass
class ResourceQueueItem:
    id_: UUID
    title: str
    request_id: str
    upload_dt: datetime
    status: ResourceStatus = None
