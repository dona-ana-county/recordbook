from marshmallow import Schema, fields
from records_api.constants.schema import LookupSchema


class RequestQueueItemSchema(Schema):
    id_ = fields.Str(data_key="id")
    requestor_name = fields.Str(data_key="requestorName", allow_none=False)
    title = fields.Str(allow_none=False)
    create_dt = fields.Date(data_key="createDate", allow_none=False)
    due_dt = fields.Date(data_key="dueDate", allow_none=False)
    # status = fields.Nested(LookupSchema, allow_none=False)
    extension_status = fields.Nested(LookupSchema, data_key="extensionStatus", allow_None=False)
    completion_status = fields.Bool(data_key="completed", allow_none=True)


class ResourceQueueItemSchema(Schema):
    id_ = fields.Str(data_key="id")
    title = fields.Str(allow_none=False)
    request_id = fields.Str(data_key="requestId", allow_none=False)
    upload_dt = fields.DateTime(data_key="uploadDate", allow_none=False)
    status = fields.Nested(LookupSchema, allow_none=False)
