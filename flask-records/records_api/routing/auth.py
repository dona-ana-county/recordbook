from flask import jsonify, request

from records_api import pass_limiter
from records_api.auth.decorators import validate
from records_api.auth.service import AuthService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service
from records_api.users.schema import StaffSchema

from . import routing

auth: AuthService = Service.get_service(AuthService)


@routing.route("/auth/login", methods=["POST"])
@pass_limiter
def login():
    credentials = request.get_json()
    staff = auth.login(credentials)
    if staff is None:
        return jsonify({"success": 0, "msg": "Username or password was wrong"})
    else:
        return ds.to_json(StaffSchema(), staff)


@routing.route("/auth/logout", methods=["POST"])
@validate(include_token=True)
def logout(token):
    return auth.logout(token)


@routing.route("/auth/validateToken", methods=["POST"])
@validate()
def validate_token():
    return jsonify({"success": 1})
