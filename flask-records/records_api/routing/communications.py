from flask import request

from records_api.auth.decorators import (assigned, ipra_admin, validate,
                                         validate_code)
from records_api.communications.model import Comment
from records_api.communications.schema import CommentSchema
from records_api.communications.service import CommunicationService
from records_api.notifications.service import NotificationService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service
from records_api.users.model import Staff

from . import routing

comm_service: CommunicationService = Service.get_service(CommunicationService)
notification_service: NotificationService = Service.get_service(NotificationService)


@routing.route("/communication/staffNewComment/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def staff_new_comment(request_id, staff: Staff) -> Comment:
    comment: Comment = ds.to_model(CommentSchema(), request.get_json())
    comment.commenter = staff
    comment = comm_service.new_comment(request_id, comment)
    notification_service.staff_message_event(request_id, comment, staff=staff)
    return ds.to_json(CommentSchema(), comment)


@routing.route("/communication/requestorNewComment/<request_id>/<passcode>", methods=["POST"])
@validate_code()
def requestor_new_comment(request_id, passcode) -> Comment:
    comment = ds.to_model(CommentSchema(), request.get_json())
    comment = comm_service.new_comment(request_id, comment)
    notification_service.requestor_message_event(request_id, comment)
    return ds.to_json(CommentSchema(), comment)
