from flask import request

from records_api.auth.decorators import super_user, validate
from records_api.departments.schema import DepartmentSchema
from records_api.departments.service import DepartmentService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service

from . import routing

dept_service:DepartmentService = Service.get_service(DepartmentService)


@routing.route("/department/newDepartment", methods=["POST"])
@validate([super_user])
def new_department():
    department = ds.to_model(DepartmentSchema(), request.get_json())
    department = dept_service.new_department(department)
    return ds.to_json(DepartmentSchema(), department)


@routing.route("/department/getAllFullDepartments", methods=["GET"])
@validate()
def get_all_full_departments():
    departments = dept_service.get_all_full_departments()
    return ds.to_json(DepartmentSchema(many=True), departments)


@routing.route("/department/getAllDepartments", methods=["GET"])
def get_all_departments():
    departments = dept_service.get_all_departments()
    return ds.to_json(DepartmentSchema(many=True), departments)


@routing.route("/department/getFullDepartment/<department_id>", methods=["GET"])
@validate()
def get_full_department(department_id):
    department = dept_service.get_full_departments([department_id])
    return ds.to_json(DepartmentSchema(), department[0])


@routing.route("/department/getDepartment/<department_id>", methods=["GET"])
def get_department(department_id):
    department = dept_service.get_departments([department_id])
    return ds.to_json(DepartmentSchema(), department[0])


@routing.route("/department/getDepartmentByStaff/<staff_id>", methods=["GET"])
@validate()
def get_department_by_staff(staff_id):
    department = dept_service.get_department_by_staff(staff_id)
    department.staff = dept_service.get_department_staff(department.id_)
    return ds.to_json(DepartmentSchema(), department)


@routing.route("/department/updateDepartment", methods=["POST"])
@validate([super_user])
def update_department():
    department = ds.to_model(DepartmentSchema(), request.get_json())
    department = dept_service.update_department(department)
    return ds.to_json(DepartmentSchema(), department)


@routing.route("/department/deleteDepartment", methods=["POST"])
@validate([super_user])
def delete_department():
    # NOTE VT: Idk if we really need to be sending the whole request here
    department = ds.to_model(DepartmentSchema(), request.get_json())
    department = dept_service.delete_department(department)
    return ds.to_json(DepartmentSchema(), department)
