from flask import current_app, jsonify, request
from . import routing

@routing.route("/logging/newLogMessage", methods=['POST'])
def log_js_message():
    data = request.get_json()
    for item in data['lg']:
        current_app.logger.info(item['m'])
    return jsonify({"success": 1, "msg": "Logged message."})