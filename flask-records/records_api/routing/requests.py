import datetime
import uuid

from flask import current_app, json, jsonify, request
from marshmallow import ValidationError
from records_api.auth.decorators import (
    assigned,
    ipra_admin,
    ipra_clerk,
    primary_department_manager,
    secondary_department_manager,
    validate,
    validate_code,
)
from records_api.auth.service import AuthService
from records_api.communications.schema import (
    AlertedItemSchema,
    AlertSchema,
    CommentSchema,
)
from records_api.communications.service import CommunicationService
from records_api.constant import content_types, release_types
from records_api.constants.schema import GovernmentEntitySchema, LookupSchema
from records_api.content.service import ContentService
from records_api.departments.schema import DepartmentSchema
from records_api.departments.service import DepartmentService
from records_api.notifications.schema import LogMessageSchema
from records_api.notifications.service import NotificationService
from records_api.requests.model import RequestOrReqError
from records_api.searching.service import SearchService
from records_api.requests.schema import (
    PublicRequestSchema,
    ReqErrorSchema,
    RequestOrReqErrorSchema,
    RequestorRequestSchema,
    RequestSchema,
)
from records_api.requests.service import RequestService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.serializer.domain_serializer import from_iso_date
from records_api.service import Service
from records_api.users.model import Staff
from records_api.users.schema import RequestorSchema, StaffSchema
from records_api.users.service import UserService

from . import routing

request_service: RequestService = Service.get_service(RequestService)
auth_service: AuthService = Service.get_service(AuthService)
user_service: UserService = Service.get_service(UserService)
notification_service: NotificationService = Service.get_service(NotificationService)
search_service: SearchService = Service.get_service(SearchService)
content_service: ContentService = Service.get_service(ContentService)
communication_service: CommunicationService = Service.get_service(CommunicationService)
dept_service: DepartmentService = Service.get_service(DepartmentService)

# TODO VT: Need to look over all the routes and stuff with Emmanuel so we can add the new decorators

# NOTE VT: It would be nice to be able to turn off the recaptcha some how for testing.


# NOTE VT: To include the staff id in the log, we might need to make the logging a decorator
# That way we can access which token sent the request and we can put it in the log message
@routing.route("/request/newPublicRequest", methods=["POST"])
def new_public_request():
    reCaptchaResponse = auth_service.validate_reCaptcha(
        request.get_json()["reCaptchaToken"]
    )
    if reCaptchaResponse:
        try:
            new_request = ds.to_model(RequestSchema(), request.get_json()["request"])

        except ValidationError as err:
            if (
                err.messages["requestor"]["contactInfo"]["email"][0]
                == "Not a valid email address."
            ):
                return jsonify({"success": 0, "msg": "Not a valid email address."})
            else:
                current_app.logger.debug(err)
                raise

        new_request.create_dt = None
        req = request_service.new_request(new_request)

        search_service.add_request(req.id_)
        
        notification_service.create_request_event(req.id_)
        notification_service.assignment_queue_event(req.id_)
        return jsonify({"success": 1, "msg": f"Created request with id {req.id_}"})

    else:
        return "", "400, ReCaptcha token expired or invalid."


@routing.route("/request/newStaffRequest", methods=["POST"])
@validate(include_staff=True)
def new_staff_request(staff: Staff):
    try:
        request_json = request.get_json()
        new_request = ds.to_model(RequestSchema(), request_json["request"])

        req = request_service.new_request(new_request)

        if "assignments" in request_json:
            alerts = ds.to_model(
                AlertedItemSchema(many=True), request_json["assignments"]
            )
            for alert in alerts:
                if alert.alert:
                    communication_service.alert_department(
                        req.id_, alert.id_, alert.alert
                    )

        notification_service.create_request_event(req.id_, staff=staff)
        if new_request.primary_department:
            notification_service.departments_assigned(
                req.id_, [new_request.primary_department.id_], staff=staff
            )
            if new_request.secondary_departments:
                secondary_ids = [d.id_ for d in new_request.secondary_departments]
                notification_service.departments_assigned(
                    req.id_, secondary_ids, staff=staff
                )
        else:
            notification_service.assignment_queue_event(req.id_, staff=staff)

        req_or_err = RequestOrReqError(request=req)
        return ds.to_json(RequestOrReqErrorSchema(), req_or_err)

    except ValidationError as err:
        if (
            err.messages["requestor"]["contactInfo"]["email"][0]
            == "Not a valid email address."
        ):
            req_err = ds.to_model(
                ReqErrorSchema(),
                {"error": True, "message": "Not a valid email address."},
            )

            req_or_err = RequestOrReqError(req_error=req_err)
            return ds.to_json(RequestOrReqErrorSchema(), req_or_err)

        else:
            current_app.logger.debug(err)
            raise


@routing.route("/request/getPublicRequest/<request_id>", methods=["GET"])
def get_public_request(request_id):
    req = request_service.get_info(request_id)
    req.requestor = request_service.get_requestor(request_id)
    return ds.to_json(PublicRequestSchema(), req)


@routing.route("/request/getRequestorRequest/<request_id>/<passcode>", methods=["GET"])
@validate_code()
def get_requestor_request(request_id, passcode):
    req = request_service.get_info(request_id)
    req.requestor = request_service.get_requestor(request_id)
    req.requestor.contact_info = user_service.get_requestor_contact_info(
        req.requestor.id_
    )
    return ds.to_json(RequestorRequestSchema(), req)


@routing.route("/request/getFullRequest/<request_id>", methods=["GET"])
@validate()
def get_full_request(request_id):
    req = request_service.get_info(request_id)
    req.requestor = request_service.get_requestor(request_id)
    req.requestor.contact_info = user_service.get_requestor_contact_info(
        req.requestor.id_
    )
    req.primary_department = request_service.get_primary_department(request_id)
    req.secondary_departments = request_service.get_secondary_departments(request_id)
    req.assigned_staff = request_service.get_assigned_staff(request_id)
    req.log = request_service.get_log(request_id)
    req.comments = request_service.get_comments(request_id)
    return ds.to_json(RequestSchema(), req)


@routing.route("/request/getRequestInfo/<request_id>", methods=["GET"])
@validate()
def get_info(request_id):
    req = request_service.get_info(request_id)
    return ds.to_json(RequestSchema(), req)


@routing.route("/request/getRequestor/<request_id>", methods=["GET"])
@validate()
def get_requestor(request_id):
    requestor = request_service.get_requestor(request_id)
    requestor.contact_info = user_service.get_requestor_contact_info(requestor.id_)
    return ds.to_json(RequestorSchema(), requestor)


@routing.route("/request/getPrimaryDepartment/<request_id>", methods=["GET"])
@validate()
def get_primary_department(request_id):
    primary_department = request_service.get_primary_department(request_id)
    return ds.to_json(DepartmentSchema(), primary_department)


@routing.route("/request/getSecondaryDepartments/<request_id>", methods=["GET"])
@validate()
def get_secondary_departments(request_id):
    secondary_departments = request_service.get_secondary_departments(request_id)
    return ds.to_json(DepartmentSchema(many=True), secondary_departments)


@routing.route("/request/getAssignedStaff/<request_id>", methods=["GET"])
@validate()
def get_assigned_staff(request_id):
    staff = request_service.get_assigned_staff(request_id)
    return ds.to_json(StaffSchema(many=True), staff)


@routing.route("/request/getLog/<request_id>", methods=["GET"])
@validate()
def get_log_messages(request_id: str):
    log_messages = request_service.get_log(request_id)
    return ds.to_json(LogMessageSchema(many=True), log_messages)


@routing.route("/request/getResources/<release_type>/<request_id>", methods=["GET"])
@validate()
def get_request_docs(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["RES"], release_types["DOC"]
    )
    return resources


@routing.route("/request/getResources/<request_id>", methods=["GET"])
@validate()
def get_resources(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["RES"], release_types["PRV"]
    )
    return resources


@routing.route("/request/getNotes/<request_id>", methods=["GET"])
@validate()
def get_notes(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["NOT"], release_types["PRV"]
    )
    return resources


@routing.route("/request/getLinks/<request_id>", methods=["GET"])
@validate()
def get_links(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["LNK"], release_types["PRV"]
    )
    return resources


@routing.route("/request/getInstructions/<request_id>", methods=["GET"])
@validate()
def get_instructions(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["INS"], release_types["PRV"]
    )
    return resources


@routing.route("/request/getPublicResources/<request_id>", methods=["GET"])
def get_public_resources(request_id: str):
    resources = request_service.get_content(
        request_id, content_types["RES"], release_types["PUB"]
    )
    return resources


@routing.route("/request/getPublicLinks/<request_id>", methods=["GET"])
def get_public_links(request_id: str):
    links = request_service.get_content(
        request_id, content_types["LNK"], release_types["PUB"]
    )
    return links


@routing.route("/request/getPublicInstructions/<request_id>", methods=["GET"])
def get_public_instructions(request_id: str):
    instructions = request_service.get_content(
        request_id, content_types["INS"], release_types["PUB"]
    )
    return instructions


@routing.route("/request/getPublicNotes/<request_id>", methods=["GET"])
def get_public_notes(request_id: str):
    notes = request_service.get_content(
        request_id, content_types["NOT"], release_types["PUB"]
    )
    return notes


@routing.route(
    "/request/getRequestorResources/<request_id>/<passcode>", methods=["GET"]
)
@validate_code()
def get_requestor_resources(request_id: str, passcode: str):
    resources = request_service.get_content(
        request_id, content_types["RES"], release_types["REQ"]
    )
    return resources


@routing.route("/request/getRequestorLinks/<request_id>/<passcode>", methods=["GET"])
@validate_code()
def get_requestor_links(request_id: str, passcode: str):
    links = request_service.get_content(
        request_id, content_types["LNK"], release_types["REQ"]
    )
    return links


@routing.route(
    "/request/getRequestorInstructions/<request_id>/<passcode>", methods=["GET"]
)
@validate_code()
def get_requestor_instructions(request_id: str, passcode: str):
    instructions = request_service.get_content(
        request_id, content_types["INS"], release_types["REQ"]
    )
    return instructions


@routing.route("/request/getRequestorNotes/<request_id>/<passcode>", methods=["GET"])
@validate_code()
def get_requestor_notes(request_id: str, passcode: str):
    notes = request_service.get_content(
        request_id, content_types["NOT"], release_types["REQ"]
    )
    return notes


@routing.route("/request/getComments/<request_id>", methods=["GET"])
@validate()
def get_staff_comments(request_id: str):
    comments = request_service.get_comments(request_id)
    return ds.to_json(CommentSchema(many=True), comments)


@routing.route("/request/getComments/<request_id>/<passcode>", methods=["GET"])
@validate_code()
def get_requestor_comments(request_id: str, passcode: str):
    comments = request_service.get_comments(request_id)
    return ds.to_json(CommentSchema(many=True), comments)


@routing.route("/request/getAlerts/<request_id>/<staff_id>", methods=["GET"])
@validate()
def get_alerts(request_id: str, staff_id: str):
    alerts = request_service.get_alerts(request_id, staff_id)
    return ds.to_json(AlertSchema(many=True), alerts)


@routing.route("/request/updateDueDate/<request_id>", methods=["POST"])
@validate([ipra_admin, primary_department_manager], include_staff=True)
def update_due_date(request_id, staff: Staff):
    due_dt = from_iso_date(request.get_json()["dueDate"])
    reason = request.get_json()["reason"]
    request_service.update_due_date(request_id, reason, due_dt, staff)

    notification_service.request_due_date_updated_event(request_id, reason, staff=staff)
    return jsonify({"success": 1, "msg": f"Updated {request_id} due date"})


@routing.route("/request/updatePublicDescription/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def update_public_description(request_id, staff: Staff):
    old_description = request_service.get_info(request_id).public_description
    new_description = request.get_json()["newDescription"]
    request_service.update_public_description(
        request_id, new_description
    )  
    notification_service.request_updated(
        request_id,
        f"Public Description: from '{old_description}' to '{new_description}'",
        staff=staff
    )
    return jsonify({"success": 1, "msg": f"Updated {request_id} public description"})


@routing.route(("/request/updatePublicTitle/<request_id>"), methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def update_public_title(request_id, staff: Staff):
    old_title = request_service.get_info(request_id).public_title
    new_title = request.get_json()["newTitle"]
    request_service.update_public_title(request_id, new_title)
    notification_service.request_updated(
        request_id,
        f"Public Title: from '{old_title}' to '{new_title}'",
        staff=staff)
    return jsonify({"success": 1, "msg": f"Updated {request_id} public title"})


@routing.route("/request/updateDepartments/<request_id>", methods=["POST"])
@validate([ipra_admin, ipra_clerk], include_staff=True)
def update_departments(request_id, staff: Staff):
    req = request.get_json()
    primary_id = uuid.UUID(req["primaryDepartments"]["id"])
    secondary_ids = [uuid.UUID(d["id"]) for d in req["secondaryDepartments"]]

    dept_changes = request_service.get_department_changes(
        request_id, primary_id, secondary_ids
    )
    request_service.update_departments(
        request_id, primary_id, secondary_ids, dept_changes
    )

    alerts = ds.to_model(AlertedItemSchema(many=True), req["secondaryDepartments"])
    for alert in alerts:
        if alert.alert:
            communication_service.alert_department(request_id, alert.id_, alert.alert)

    notification_service.departments_assigned(
        request_id, dept_changes["assign"], staff=staff
    )
    notification_service.departments_unassigned(
        request_id, dept_changes["unassign"], staff=staff
    )
    return jsonify({"success": 1, "msg": f"Updated {request_id} departments"})


@routing.route("/request/updateCompletion/<request_id>", methods=["POST"])
@validate([ipra_admin, secondary_department_manager], include_staff=True)
def update_completion(request_id, staff: Staff):
    req = request.get_json()
    dept_id = uuid.UUID(req["deptId"])
    completion_status = req["completionStatus"]
    comment = req["comment"]
    request_service.assign_department_completion_status(
        request_id, dept_id, completion_status, comment
    )

    if completion_status:
        notification_service.secondary_department_task_completed(
            request_id, dept_id, staff=staff
        )
    else:
        notification_service.secondary_department_task_not_completed(
            request_id, dept_id, staff=staff
        )

    return jsonify({"success": 1, "msg": f"Updated {request_id} completion status."})


@routing.route("/request/updateStaff/<request_id>", methods=["POST"])
@validate(
    [primary_department_manager, secondary_department_manager], include_staff=True
)
def update_request_staff(request_id, staff: Staff):
    staff_assignments = ds.to_model(AlertedItemSchema(many=True), request.get_json())
    staff_assignments_ids = [s_a.id_ for s_a in staff_assignments]
    staff_changes = request_service.get_staff_changes(request_id, staff_assignments_ids)
    dept = dept_service.get_department_by_staff(staff.id_)
    dept_staff = frozenset(user_service.get_staff_by_departments([dept.id_]))
    if staff_changes["assign"] - dept_staff or staff_changes["unassign"] - dept_staff:
        return jsonify(
            {
                "type": "Validation Error",
                "error": "Cannot assign staff outside of department",
            }
        )
    request_service.update_staff(request_id, staff_changes)
    for da in staff_assignments:
        if da.alert:
            communication_service.alert_staff(request_id, da.id_, da.alert)

    notification_service.workers_assigned(
        request_id, staff_changes["assign"], staff=staff
    )
    notification_service.workers_unassigned(
        request_id, staff_changes["unassign"], staff=staff
    )
    return jsonify({"success": 1, "msg": f"Updated the staff for {request_id}"})


@routing.route("/request/closeRequest/<request_id>", methods=["POST"])
@validate([ipra_admin, primary_department_manager], include_staff=True)
def close_request(request_id: str, staff: Staff):
    close_reason = ds.to_model(LookupSchema(), request.get_json())
    if content_service.has_legal_review_resources(request_id):
        return jsonify({"type": "Update Error", "error": "Documents in legal review"})

    notification_service.request_closed_event(request_id, close_reason, staff=staff)

    if not request_service.close_request(request_id, close_reason):
        return jsonify({"type": "Update Error", "error": "Error closing request"})
    return jsonify({"success": 1, "msg": "Request has been closed"})


@routing.route("/request/denyRequest/<request_id>", methods=["POST"])
@validate([ipra_admin], include_staff=True)
def deny_request(request_id, staff: Staff):
    denial_reason = ds.to_model(LookupSchema(), request.get_json())
    notification_service.request_denied_event(
        request_id, reason=denial_reason, staff=staff
    )
    request_service.deny_request(request_id, denial_reason)

    return jsonify(
        {
            "success": 1,
            "msg": f"Request has been denied with reason {denial_reason.value}",
        }
    )


@routing.route("/request/denyRequestWrongGovernmentEntity", methods=["POST"])
@validate([ipra_admin], include_staff=True)
def deny_request_wrong_government_entity(staff: Staff):
    denial_reason = ds.to_model(LookupSchema(), request.get_json()["reason"])
    entity = ds.to_model(GovernmentEntitySchema(), request.get_json()["entity"])
    request_id = request.get_json()["request"]
    notification_service.request_denied_event(
        request_id, reason=denial_reason, entity=entity, staff=staff
    )
    request_service.deny_request_wrong_government_entity(request_id, entity)
    return jsonify(
        {"success": 1, "msg": f"Request has been denied for wrong government entity"}
    )


@routing.route("/request/forwardRequestWrongGovernmentEntity", methods=["POST"])
@validate([ipra_admin], include_staff=True)
def forward_request_wrong_government_entity(staff: Staff):
    entity = ds.to_model(GovernmentEntitySchema(), request.get_json()["entity"])
    request_id = request.get_json()["request"]
    notification_service.request_forwarded_event(request_id, entity=entity, staff=staff)
    return jsonify({"success": 1, "msg": f"Request forwarded to {entity.name}"})


@routing.route("/request/reOpenRequest/<request_id>", methods=["POST"])
@validate([ipra_admin], include_staff=True)
def reopen_request(request_id: str, staff: Staff):
    reason = request.get_json()["reason"]
    request_service.reopen_request(request_id, reason)
    notification_service.request_reopened(request_id, staff=staff)
    return jsonify(
        {"success": 0, "msg": f"NYI: This will reopen the request with id {request_id}"}
    )


@routing.route("/request/extendDueDate/<request_id>", methods=["POST"])
@validate([ipra_admin, primary_department_manager], include_staff=True)
def extend_due_date(request_id: str, staff: Staff):
    description = request.get_json()["description"]
    due_dt = request.get_json().get("dueDate", None)
    if due_dt:
        due_dt = from_iso_date(due_dt)
    if not request_service.extend_due_date(request_id, description, due_dt, staff):
        return jsonify({"type": "Update Error", "error": "Error updating request"})
    notification_service.request_extended_event(request_id, description, staff)
    return jsonify({"success": 1, "msg": "Due date extended"})


@routing.route(
    "/request/updateRequestorContactInfoByRequestId/<request_id>", methods=["POST"]
)
@validate([ipra_admin], include_staff=True)
def update_requestor_contact_info_by_request_id(request_id: str, staff: Staff):
    try:
        new_contact_info_payload = request.get_json()
        primary_contact_code = new_contact_info_payload.pop("primary_contact")
        do_update = request_service.update_requestor_contact_info(
            request_id, new_contact_info_payload, primary_contact_code
        )
        if do_update["status"] == 200 and "changed_fields" in do_update:
            notification_service.request_updated(
                request_id,
                {k: f"from {v['old']} to {v['new']}" for k, v in do_update["changed_fields"].items()},
                staff
            )
        return jsonify(do_update["response"]), do_update["status"]
    except Exception as e:
        return jsonify({"error": str(e)}), 500