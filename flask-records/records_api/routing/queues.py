from records_api.auth.decorators import ipra_admin, ipra_clerk, legal, validate
from records_api.queues.schema import (RequestQueueItemSchema,
                                       ResourceQueueItemSchema)
from records_api.queues.service import QueueService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service

from . import routing

queue_service: QueueService = Service.get_service(QueueService)


@routing.route("/queue/getAssignmentQueue", methods=["GET"])
@validate([ipra_admin, ipra_clerk])
def get_assignment_queue():
    queue_items = queue_service.get_assignment_queue()
    return ds.to_json(RequestQueueItemSchema(many=True), queue_items)


@routing.route("/queue/getDepartmentQueue/<dept_id>", methods=["GET"])
@validate()
def get_department_queue(dept_id):
    queue_items = queue_service.get_department_queue(dept_id)
    return ds.to_json(RequestQueueItemSchema(many=True), queue_items)


@routing.route("/queue/getAllDepartmentQueues", methods=["GET"])
@validate([ipra_admin, ipra_clerk])
def get_all_department_queues():
    queue_items = queue_service.get_all_department_queues()
    return ds.to_json(RequestQueueItemSchema(many=True), queue_items)


@routing.route("/queue/getStaffAssignedQueue/<staff_id>", methods=["GET"])
@validate()
def get_assigned_requests(staff_id):
    queue_items = queue_service.get_assigned_requests(staff_id)
    return ds.to_json(RequestQueueItemSchema(many=True), queue_items)


@routing.route("/queue/getLegalQueue", methods=["GET"])
@validate([legal])
def get_legal_queue():
    queue_items = queue_service.get_legal_queue()
    return ds.to_json(ResourceQueueItemSchema(many=True), queue_items)
