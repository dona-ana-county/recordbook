from flask import current_app

from records_api import constant
from records_api.auth.decorators import validate
from records_api.constants.schema import (ConfigSchema, GovernmentEntitySchema,
                                          LookupSchema)
from records_api.constants.service import ConstantService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service

from . import routing

constant_service: ConstantService = Service.get_service(ConstantService)


@routing.route("/constants/getAllGroups", methods=["GET"])
def get_all_groups():
    lookup_list = []
    for value in constant.groups.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllRoles", methods=["GET"])
def get_all_roles():
    lookup_list = []
    for value in constant.roles.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllRequestStatuses", methods=["GET"])
def get_all_request_statuses():
    lookup_list = []
    for value in constant.request_statuses.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllContentTypes", methods=["GET"])
def get_all_content_types():
    lookup_list = []
    for value in constant.content_types.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllReleaseTypes", methods=["GET"])
def get_all_release_types():
    lookup_list = []
    for key, value in constant.release_types.items():
        if key == 'DOC':
            continue
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllContactTypes", methods=["GET"])
def get_all_contact_types():
    lookup_list = []
    for value in constant.contact_types.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllEventCodes", methods=["GET"])
def get_all_event_codes():
    lookup_list = []
    for value in constant.event_codes.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllQueueItems", methods=["GET"])
def get_all_queue_items():
    lookup_list = []
    for value in constant.queue_items.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllQueueTypes", methods=["GET"])
def get_all_queue_types():
    lookup_list = []
    for value in constant.queue_types.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)


@routing.route("/constants/getAllGovernmentEntities", methods=["GET"])
@validate()
def get_all_government_entities():
    entities = constant_service.get_all_government_entities()
    return ds.to_json(GovernmentEntitySchema(many=True), entities)


@routing.route("/constants/getAllDenialReasons", methods=["GET"])
def get_all_denial_reasons():
    reasons = constant_service.get_all_denial_reasons()
    return ds.to_json(LookupSchema(many=True), reasons)


# TODO: Finish CRUD ops for denial reasons


@routing.route("/constants/getAllCloseReasons", methods=["GET"])
def get_all_close_reasons():
    reasons = constant_service.get_all_close_reasons()
    return ds.to_json(LookupSchema(many=True), reasons)


# TODO: Finish CRUD ops for close reasons

@routing.route("/constants/getAllExtensionStatuses", methods=["GET"])
def get_all_extension_types():
    lookup_list = []
    for value in constant.extension_statuses.values():
        lookup_list.append(value)
    return ds.to_json(LookupSchema(many=True), lookup_list)

@routing.route("/constants/getConfig", methods=["GET"])    
def get_config():
    return ds.to_json(ConfigSchema(), current_app.config)
