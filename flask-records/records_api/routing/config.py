# from flask import current_app

#from records_api import db
from records_api.auth.decorators import super_user_basic_required
#from records_api.manage.db_init import db_init, insert_test_data
from records_api.manage.es_init import es_init

from . import routing


#@routing.route("/reset/all")
#@super_user_basic_required
#def reset_all():
#    db_init(db)
#    es_init()
#    return "Database and Elasticsearch reset"

#@routing.route("/reset/all/test")
#@super_user_basic_required
#def reset_all_test():
#    db_init(db)
#    insert_test_data(db)
#    es_init()
#    return "Database and Elasticsearch reset"


@routing.route("/reset/es")
@super_user_basic_required
def reset_es():
    es_init()
    return "Elastic Search Requests Index Reset"
