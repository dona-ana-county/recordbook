import json

from flask import jsonify, request

from records_api.auth.decorators import (assigned, creator, ipra_admin, legal,
                                         primary_department_manager, validate,
                                         validate_code, validate_mac)
from records_api.constant import content_types, release_types
from records_api.content.schema import (InstructionSchema, LinkSchema,
                                        NoteSchema, ResourceSchema)
from records_api.content.service import ContentService
from records_api.notifications.service import NotificationService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service
from records_api.users.model import Staff
from records_api.users.service import UserService

from . import routing

content_service: ContentService = Service.get_service(ContentService)
user_service: UserService = Service.get_service(UserService)
notification_service: NotificationService = Service.get_service(NotificationService)


@routing.route("/content/getResource/<content_id>", methods=["GET"])
@validate()
def get_resource(content_id):
    content = content_service.get_resource_by_id(content_id)
    return ds.to_json(ResourceSchema(), content)


@routing.route("/content/newNote/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def new_note(request_id, staff):
    note = ds.to_model(NoteSchema(), request.get_json())
    note = content_service.new_note(request_id, note)
    notification_service.new_content(request_id, note, content_types["NOT"], staff=staff)
    return ds.to_json(NoteSchema(), note)


@routing.route("/content/newInstruction/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def new_instruction(request_id, staff):
    instruction = ds.to_model(InstructionSchema(), request.get_json())
    instruction = content_service.new_instruction(request_id, instruction)
    notification_service.new_content(
        request_id, instruction, content_types["INS"], staff=staff
    )
    return ds.to_json(InstructionSchema(), instruction)


@routing.route("/content/newLink/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def new_link(request_id, staff):
    link = ds.to_model(LinkSchema(), request.get_json())
    link = content_service.new_link(request_id, link)
    notification_service.new_content(request_id, link, content_types['LNK'], staff=staff)
    return ds.to_json(LinkSchema(), link)



@routing.route("/content/updateContent/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def update_content(request_id, staff):
    content_type = content_types[request.get_json()["type"]]
    content = ds.to_model(  # Seems a little weird but I understand.
        content_service.get_content_schema(content_type), request.get_json()["content"]
    )
    content, old_release_type = content_service.update_content(request_id, content_type, content)
    notification_service.content_updated_event(
        content, content_type, old_release_type, staff=staff
    )
    # TODO VT: Maybe make this the same as the other ones and only return a success.
    return ds.to_json(content_service.get_content_schema(content_type), content)


@routing.route("/content/updateUnderReview/<content_id>", methods=["POST"])
@validate([legal], include_staff=True)
def update_review_status(content_id, staff):
    resource = content_service.update_under_review(content_id, request.get_json()["status"])
    notification_service.review_status_updated(content_id, staff=staff)
    if resource:
        return jsonify({"success": 1})
    else:
        return jsonify({"type": "Update Error", "error": "Error updating under review status"})


@routing.route("/content/approveResource/<content_id>", methods=["POST"])
@validate([legal], include_staff=True)
def approve_resource(content_id, staff):
    release_type = release_types[request.get_json()["type"]]
    resource = content_service.approve_resource(content_id, release_type)
    if resource:
        notification_service.legal_approved_event(resource, staff=staff)
        return jsonify({"success": 1})
    else:
        return jsonify({"type": "Update Error", "error": "Error approving resource"})


@routing.route("/content/rejectResource/<content_id>", methods=["POST"])
@validate([legal], include_staff=True)
def reject_resource(content_id, staff: Staff):
    staff_id = request.get_json()["staff"]
    if staff_id != str(staff.id_):
        return jsonify({"type": "Validation Error", "error": "Staff_id did not match requesting user"})
    reason = request.get_json()["reason"]
    resource = content_service.reject_resource(content_id, staff_id, reason)
    if resource:
        notification_service.legal_denied_event(resource, reason, staff=staff)
        return jsonify({"success": 1})
    else:
        return jsonify({"type": "Update Error", "error": "Error rejecting resource"})

@routing.route("/content/deleteContent/<content_id>", methods=["POST"])
@validate([ipra_admin, primary_department_manager, creator], include_staff=True)
def delete_content(content_id, staff):
    content_type = content_types[request.get_json()["type"]]
    reason = request.get_json()["reason"]

    content = content_service.delete_content(content_id, content_type, staff.id_ , reason)
    if content:
        notification_service.delete_content_event(content, content_type, reason, staff=staff)
        return jsonify({"success": 1})
    else:
        return jsonify({"type": "Update Error", "error": "Error deleting content"})

@routing.route("/content/uploadFile/<request_id>", methods=["POST"])
@validate([assigned, ipra_admin], include_staff=True)
def upload_file(request_id, staff):
    resource_update = ds.to_model(ResourceSchema(), json.loads(request.form["resource"]))
    resource_update.request_id = request_id
    resource = content_service.upload_file(request, resource_update)
    if resource is None:
        data = {"type": "Upload Failed", "error": "The file failed to upload."}
        return jsonify(data)
    else:
        notification_service.new_content(request_id, resource, content_types["RES"], staff=staff)
        if resource.needs_review:
            notification_service.legal_queue_event(resource.id_, staff)
        return ds.to_json(ResourceSchema(), resource)

@routing.route("/content/uploadRequestFile/<request_id>", methods=["POST"])
@validate(include_staff=True)
def upload_request_file(request_id, staff):
    resource_update = ds.to_model(ResourceSchema(), json.loads(request.form["resource"]))
    resource_update.request_id = request_id
    if resource_update.release_type != release_types['DOC']:
        return jsonify({"type": "Validation Error", "error": "File must be a request document"})
    resource = content_service.upload_file(request, resource_update)
    if resource is None:
        data = {"type": "Upload Failed", "error": "The file failed to upload."}
        return jsonify(data)
    else:
        return ds.to_json(ResourceSchema(), resource)
    # TODO: Notification


@routing.route("/content/uploadRevisedFile/<resource_id>", methods=["POST"])
@validate([legal], include_staff=True)
def upload_revised_file(resource_id, staff):
    updated_resource = content_service.upload_revised_file(request, resource_id)
    if not updated_resource:
        data = {"type": "Upload Revised Failed", "error": "The file failed to upload."}
        return jsonify(data)
    else:
        notification_service.legal_revised_event(
            resource_id, staff=staff
        )
    return ds.to_json(ResourceSchema(), updated_resource)


@routing.route("/content/downloadFile/<resource_id>", methods=["GET"])
def download_public_file(resource_id):
    response = content_service.download_file(resource_id, release_types["PUB"])
    if response is None:
        data = {"type": "Download Failed", "error": "The file failed to download."}
        return jsonify(data)
    return response


@routing.route("/content/downloadFile/<resource_id>/<passcode>", methods=["GET"])
@validate_code()
def download_requestor_file(resource_id, passcode):
    response = content_service.download_file(resource_id, release_types["REQ"])
    if response is None:
        data = {"type": "Download Failed", "error": "The file failed to download."}
        return jsonify(data)
    return response


@routing.route(
    "/content/downloadPrivateFile/<resource_id>/<username>/<int:timestamp>/<mac>", methods=["GET"]
)
@validate_mac("resource_id", "username", "timestamp", "mac")
def download_private_file(resource_id, username, timestamp, mac):
    response = content_service.download_private_file(resource_id)
    if response is None:
        data = {"type": "Download Failed", "error": "The file failed to download."}
        return jsonify(data)
    return response
