from flask import request

from records_api.auth.decorators import super_user, validate
from records_api.auth.service import AuthService
from records_api.constants.schema import GovernmentEntitySchema
from records_api.constants.service import ConstantService
from records_api.departments.schema import DepartmentSchema
from records_api.requests.service import RequestService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service
from records_api.users.schema import StaffSchema
from records_api.users.service import UserService

from . import routing

user_service: UserService = Service.get_service(UserService)
constant_service: ConstantService = Service.get_service(ConstantService)
auth_service: AuthService = Service.get_service(AuthService)
request_service: RequestService = Service.get_service(RequestService)


@routing.route("/super/newStaff", methods=["POST"])
@validate([super_user])
def new_staff():
    staff = ds.to_model(StaffSchema(), request.get_json()["staff"])
    staff.disabled = False
    department = ds.to_model(DepartmentSchema(), request.get_json()["department"])
    staff = user_service.new_staff(staff, department)
    return ds.to_json(StaffSchema(), staff)


@routing.route("/super/updateStaff", methods=["POST"])
@validate([super_user])
def update_staff():
    staff = ds.to_model(StaffSchema(), request.get_json()["staff"])
    department = ds.to_model(DepartmentSchema(), request.get_json()["department"])
    staff = user_service.update_staff(staff, department)
    return ds.to_json(StaffSchema(), staff)


@routing.route("/super/getStaff", methods=["GET"])
@validate([super_user])
def get_staff():
    staff = user_service.get_staff()
    return ds.to_json(StaffSchema(many=True), staff)


@routing.route("/super/deleteStaff", methods=["POST"])
@validate([super_user])
def delete_user_staff():
    staff = ds.to_model(StaffSchema(), request.get_json())
    staff = user_service.delete_staff(staff)
    auth_service.revoke_tokens(staff.id_)
    return ds.to_json(StaffSchema(), staff)


@routing.route("/super/enableStaff", methods=["POST"])
@validate([super_user])
def enable_staff():
    staff = ds.to_model(StaffSchema(), request.get_json())
    staff = user_service.enable_staff(staff)
    return ds.to_json(StaffSchema(), staff)


@routing.route("/super/disableStaff", methods=["POST"])
@validate([super_user])
def disable_staff():
    staff = ds.to_model(StaffSchema(), request.get_json())
    staff = user_service.disable_staff(staff)
    auth_service.revoke_tokens(staff.id_)
    return ds.to_json(StaffSchema(), staff)


@routing.route("/super/newGovernmentEntity", methods=["POST"])
@validate([super_user])
def new_government_entity():
    gov_entity = ds.to_model(GovernmentEntitySchema(), request.get_json())
    gov_entity = constant_service.new_government_entity(gov_entity)
    return ds.to_json(GovernmentEntitySchema(), gov_entity)


@routing.route("/super/updateGovernmentEntity", methods=["POST"])
@validate([super_user])
def update_government_entity():
    gov_entity = ds.to_model(GovernmentEntitySchema(), request.get_json())
    gov_entity = constant_service.update_government_entity(gov_entity)
    return ds.to_json(GovernmentEntitySchema(), gov_entity)


@routing.route("/super/deleteGovernmentEntity", methods=["POST"])
@validate([super_user])
def delete_government_entity():
    gov_entity = ds.to_model(GovernmentEntitySchema(), request.get_json())
    gov_entity = constant_service.delete_government_entity(gov_entity)
    return ds.to_json(GovernmentEntitySchema(), gov_entity)
