from flask import jsonify, request

from records_api.auth.decorators import validate
from records_api.notifications.schema import NotificationParamsSchema
from records_api.notifications.service import NotificationService
from records_api.requests.service import RequestService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service

from . import routing

notification_service: NotificationService = Service.get_service(NotificationService)
request_service: RequestService = Service.get_service(RequestService)

@routing.route("/notification/getTemplate/<template_name>/<request_id>", methods=["POST"])
@validate()
def get_template(request_id, template_name):
    # Sanity check
    params = ds.to_model(NotificationParamsSchema(), request.get_json())
    # filter out Nones
    params = {k:v for k,v in params.items() if v is not None}

    ipra_request = request_service.get_info(request_id)
    res = notification_service.get_template(template_name, ipra_request, params)
    return jsonify(res)
