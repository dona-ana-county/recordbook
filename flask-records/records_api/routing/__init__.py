from flask import Blueprint

routing = Blueprint("routing", __name__)

from . import auth
from . import content
from . import departments
from . import constants
from . import queues
from . import requests
from . import config
from . import communications
from . import search
from . import super_user
from . import notifications
from . import js_logging
