from flask import jsonify, request

from records_api.auth.decorators import super_user_basic_required, validate
from records_api.departments.service import DepartmentService
from records_api.requests.service import RequestService
from records_api.searching.schema import PaginationInfoSchema, RequestResultSchema
from records_api.searching.service import SearchService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service

from . import routing

request_service: RequestService = Service.get_service(RequestService)
search_service: SearchService = Service.get_service(SearchService)
department_service: DepartmentService = Service.get_service(DepartmentService)


@routing.route("/publicSearch/query", methods=["POST"])
def public_search():
    query = request.get_json()
    search_result = search_service.search_query(query, isPublic=True)
    json = ds.to_json(PaginationInfoSchema(many=False), search_result)
    return json


@routing.route("/staffSearch/query", methods=["POST"])
@validate()
def staff_search():
    query = request.get_json()
    search_result = search_service.search_query(query, isPublic=False)
    json = ds.to_json(PaginationInfoSchema(many=False), search_result)
    return json


@routing.route("/search/all", methods=["GET"])
@super_user_basic_required
def search_all():
    results = search_service.get_all()
    json = ds.to_json(RequestResultSchema(many=True), results)
    return json


@routing.route("/search/addAll", methods=["GET"])
@super_user_basic_required
def add_all():
    search_service.add_all_requests()
    return jsonify({"success": 1, "msg": "All Requests added"})


@routing.route("/search/query", methods=["POST"])
@super_user_basic_required
def send_query():
    query = request.get_json()
    requests = search_service.send_bare_query(query)
    return jsonify(requests)


@routing.route("/staffSearch/export", methods=["POST"])
@validate()
def export_search_results():
    query = request.get_json()
    return search_service.export_search_results_to_csv(query)
