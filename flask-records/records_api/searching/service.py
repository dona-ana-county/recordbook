import csv
import re
from io import StringIO
from typing import Dict, List

from flask import Response, current_app, request
from records_api.constant import extension_statuses, request_statuses
from records_api.service import Service

from .model import PaginationInfo, RequestIndex, RequestResult
from .repo import SearchRepo


class SearchService(Service):
    def __init__(self, app) -> None:
        self.repo = SearchRepo()

    def add_all_requests(self) -> None:
        requests = self.repo.get_all_request_indexes()
        for req in requests:
            index = RequestIndex(meta={"id": req.id_})
            index.id_ = req.id_
            index.title = req.title
            index.public_title = req.public_title
            index.requestor_name = req.requestor_name
            index.requestor_email = req.requestor_email
            index.description = req.description
            index.public_description = req.public_description
            index.status = req.status
            index.department = req.department
            index.create_dt = req.create_dt
            index.due_dt = req.due_dt
            index.closing_date = req.closing_date
            index.extension_status = req.extension_status

            res = index.save()
            current_app.logger.debug(res)

    def add_request(self, request_id: str) -> None:
        request = self.repo.get_request_index(request_id)

        index = RequestIndex(meta={"id": request.id_})
        index.id_ = request.id_
        index.title = request.title
        index.public_title = request.public_title
        index.requestor_name = request.requestor_name
        index.requestor_email = request.requestor_email
        index.description = request.description
        index.public_description = request.public_description
        index.status = request.status
        index.department = request.department
        index.create_dt = request.create_dt
        index.due_dt = request.due_dt
        index.closing_date = request.closing_date
        index.extension_status = request.extension_status

        index.save()

    def get_all(self) -> List[RequestResult]:
        s = RequestIndex.search()
        res = s.execute()
        return self.parse_request_results(res, isPublic=False)

    def send_bare_query(self, query, isPublic=False):
        if isPublic:
            fields = ["public_title", "public_description"]
        else:
            fields = ["title", "description"]

        s = RequestIndex.search()
        if "search" in query and query["search"] != "":
            s = s.query("multi_match", query=query["search"], fields=fields)

        for f in query["filters"]:
            if f["type"] == "by_value":
                if f["field"] == "department":
                    s = s.filter("term", department=f["value"])
                elif f["field"] == "status":
                    s = s.filter("term", status=f["value"])
            elif f["type"] == "by_range":
                if "from" not in f:
                    range_filter = {"range": {f["field"]: {"lte": f["to"]}}}
                elif "to" not in f:
                    range_filter = {"range": {f["field"]: {"gte": f["from"]}}}
                else:
                    range_filter = {
                        "range": {f["field"]: {"gte": f["from"], "lte": f["to"]}}
                    }
                s = s.filter(range_filter)

        if "sort" in query and query["sort"] != "":
            s = s.sort(query["sort"])

        page_start = (query["page"] - 1) * query["perPage"]
        page_end = page_start + query["perPage"]
        s = s[page_start:page_end]

        res = s.execute()
        return res.to_dict()

    def search_query(self, query: Dict, isPublic: bool) -> List[RequestResult]:
        if isPublic:
            fields = ["public_title", "public_description", "requestor_name"]
        else:
            fields = ["title", "description", "requestor_name", "requestor_email.raw"]

        s = RequestIndex.search()
        if "search" in query and query["search"] != "":
            search_text = query["search"].strip()
            #! Might need to use an email validator or check with the frontend later
            if isPublic and "@" in search_text:
                return PaginationInfo(num_items=0, req_list=[])
            elif search_text.startswith("IPRA-"):
                s = s.query("term", id_=search_text)
            else:
                s = s.query("multi_match", query=search_text, fields=fields)

        for f in query["filters"]:
            if f["type"] == "by_value":
                if f["field"] == "department" and f["value"] != "all":
                    s = s.filter("term", department=f["value"])
                elif f["field"] == "status":
                    s = s.filter("term", status=f["value"])
                elif f["field"] == "extension_status":
                    s = s.filter("term", extension_status=f["value"])
            elif f["type"] == "by_range":
                if "from" not in f:
                    range_filter = {"range": {f["field"]: {"lte": f["to"]}}}
                elif "to" not in f:
                    range_filter = {"range": {f["field"]: {"gte": f["from"]}}}
                else:
                    range_filter = {
                        "range": {f["field"]: {"gte": f["from"], "lte": f["to"]}}
                    }
                s = s.filter(range_filter)

        if "sort" in query and query["sort"] != "":
            s = s.sort(query["sort"])
        else:
            s = s.sort({"create_dt": {"order": "desc"}})

        page_start = (query["page"] - 1) * query["perPage"]
        page_end = page_start + query["perPage"]
        s = s[page_start:page_end]

        res = s.execute()
        num_items = s.count()
        reqs = self.parse_request_results(res, isPublic)

        return PaginationInfo(num_items=num_items, req_list=reqs)

    def parse_request_results(self, res, isPublic: bool) -> List[RequestResult]:
        results = []
        for r in res:
            if isPublic:
                r.requestor_email = ""
                title = r.public_title
            else:
                title = r.title

            result = RequestResult(
                requestor_email=r.requestor_email,
                request_id=r.meta.id,
                requestor_name=r.requestor_name,
                title=title,
                department=r.department,
                create_dt=r.create_dt,
                due_dt=r.due_dt,
                closing_date=r.closing_date,
                status=request_statuses[r.status],
                extension_status=extension_statuses[r.extension_status],
            )
            results.append(result)
        return results

    def export_search_results_to_csv(self, query_parameters: dict) -> Response:
        search_results = self.search_query(query_parameters, isPublic=False)
        string_buffer = StringIO()
        csv_writer = csv.writer(string_buffer)
        csv_writer.writerow(
            [
                "Request ID",
                "Requestor Name",
                "Requestor Email",
                "Title",
                "Create Date",
                "Due Date",
                "Closing Date",
                "Department",
                "Status",
                "Extension Status",
            ]
        )
        for request_result in search_results.req_list:
            csv_writer.writerow(
                [
                    request_result.request_id,
                    request_result.requestor_name,
                    request_result.requestor_email,
                    request_result.title,
                    request_result.create_dt.strftime('%Y-%m-%d')
                    if request_result.create_dt else 'None',
                    request_result.due_dt.strftime('%Y-%m-%d')
                    if request_result.due_dt else 'None',
                    request_result.closing_date.strftime('%Y-%m-%d')
                    if request_result.closing_date else 'None',
                    request_result.department,
                    request_result.status.value,
                    request_result.extension_status.value,
                ]
            )
        csv_output = string_buffer.getvalue()
        return Response(
            csv_output,
            mimetype="text/ppt",
            headers={"Content-disposition": "attachment; filename=search_results.csv"},
        )