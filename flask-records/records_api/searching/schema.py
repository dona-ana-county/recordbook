from marshmallow import Schema, fields
from records_api.constants.schema import LookupSchema


class RequestResultSchema(Schema):
    request_id = fields.Str(data_key="requestId")
    requestor_name = fields.Str(data_key="requestorName")
    requestor_email = fields.Str(data_key="requestorEmail")
    title = fields.Str()
    create_dt = fields.Date(data_key="createDate")
    due_dt = fields.Date(data_key="dueDate")
    closing_date = fields.Date(data_key="closingDate", allow_none=True)
    department = fields.Str()
    status = fields.Nested(LookupSchema)
    extension_status = fields.Nested(LookupSchema, data_key="extensionStatus")


class PaginationInfoSchema(Schema):
    num_items = fields.Integer(data_key="numItems")
    req_list = fields.Nested(RequestResultSchema, data_key="reqList", many=True)
