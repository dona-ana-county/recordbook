
import re
from dataclasses import dataclass
from datetime import date
from typing import List

from elasticsearch_dsl import Date, Document, Keyword, Text
from records_api.requests.model import ExtensionStatus, RequestStatus

query = {
    "search": "Hi",
    "sort": "create_dt",
    "filter": [
        {"type": "by_value", "field": "department", "value": "Airport"},
        {"type": "by_value", "field": "status", "value": "Open"},
    ],
    "page": 1,
    "per_page": 3,
}


@dataclass
class SearchRequest:
    id_: str
    create_dt: date
    due_dt: date
    closing_date: date
    title: str = ""
    public_title: str = ""
    description: str = ""
    public_description: str = ""
    requestor_name: str = ""
    requestor_email: str = ""
    department: str = ""
    status: str = ""
    extension_status: str = ""


class RequestIndex(Document):
    title = Text(analyzer="snowball", fields={"raw": Keyword(normalizer="lowercase")})
    public_title = Text(analyzer="snowball", fields={"raw": Keyword(normalizer="lowercase")})
    description = Text(analyzer="snowball")
    public_description = Text(analyzer="snowball")
    requestor_name = Text(
        analyzer="snowball", fields={"raw": Keyword(normalizer="lowercase")}
    )
    requestor_email = Text(
        analyzer="snowball", fields={"raw": Keyword(normalizer="lowercase")}
    )
    id_ = Keyword()
    department = Keyword()
    status = Keyword()
    create_dt = Date()
    due_dt = Date()
    closing_date = Date()
    extension_status = Keyword()

    class Index:
        name = "requests"


# TODO VT: Add Document/resource index
# TODO VT: Add User index


@dataclass
class RequestResult:
    request_id: str
    requestor_name: str
    requestor_email: str
    title: str
    create_dt: date
    due_dt: date
    closing_date: date
    department: str
    status: RequestStatus
    extension_status: ExtensionStatus


@dataclass
class PaginationInfo:
    num_items: int
    req_list: List[RequestResult]
