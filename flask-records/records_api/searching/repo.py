from typing import Iterator

from records_api import db
from records_api.constant import extension_statuses, request_statuses
from records_api.serializer.db_schema import ContactInfo as DbContact
from records_api.serializer.db_schema import Department as DbDepartment
from records_api.serializer.db_schema import Request as DbRequest
from records_api.serializer.db_schema import (
    RequestAssignedDepartment as DbRequestDepartment,
)
from records_api.serializer.db_schema import Requestor as DbRequestor
from records_api.serializer.db_schema import User as DbUser
from sqlalchemy import and_

from .model import SearchRequest


class SearchRepo:
    def __init__(self):
        pass

    def get_request_index(self, request_id: str) -> SearchRequest:
        db_index = (
            db.session.query(DbRequest, DbUser, DbDepartment, DbContact)
            .join(DbRequestor, DbRequest.id_ == DbRequestor.request_id)
            .join(DbUser, DbRequestor.id_ == DbUser.id_)
            .join(DbContact, DbRequestor.id_ == DbContact.requestor_id)
            .outerjoin(
                DbRequestDepartment,
                and_(
                    DbRequest.id_ == DbRequestDepartment.request_id,
                    DbRequestDepartment.unassigned_date == None,
                    DbRequestDepartment.level == "Primary",
                ),
            )
            .outerjoin(
                DbDepartment, DbRequestDepartment.department_id == DbDepartment.id_
            )
            .filter(DbRequest.id_ == request_id)
            .first()
        )

        index = SearchRequest(
            id_=db_index[0].id_,
            create_dt=db_index[0].create_dt,
            due_dt=db_index[0].due_dt,
            closing_date=db_index[0].closing_date,
        )
        index.title = db_index[0].title
        index.public_title = db_index[0].public_title
        index.description = db_index[0].description
        index.public_description = db_index[0].public_description
        index.requestor_name = f"{db_index[1].first_name} {db_index[1].last_name }"
        index.requestor_email = db_index[3].email
        index.status = request_statuses[db_index[0].status].code
        index.extension_status = extension_statuses[db_index[0].extension_status].code
        if db_index[2]:
            index.department = db_index[2].name
        else:
            index.department = "Unassigned"
        return index

    def get_all_request_indexes(self) -> Iterator[SearchRequest]:
        db_indexes = (
            db.session.query(DbRequest, DbUser, DbDepartment, DbContact)
            .join(DbRequestor, DbRequest.id_ == DbRequestor.request_id)
            .join(DbUser, DbRequestor.id_ == DbUser.id_)
            .join(DbContact, DbRequestor.id_ == DbContact.requestor_id)
            .outerjoin(
                DbRequestDepartment,
                and_(
                    DbRequest.id_ == DbRequestDepartment.request_id,
                    DbRequestDepartment.unassigned_date == None,
                    DbRequestDepartment.level == "Primary",
                ),
            )
            .outerjoin(
                DbDepartment, DbRequestDepartment.department_id == DbDepartment.id_
            )
            .order_by(DbRequest.create_dt.desc())
            .yield_per(1000)
        )

        for i in db_indexes:
            new_index = SearchRequest(
                id_=i[0].id_,
                create_dt=i[0].create_dt,
                due_dt=i[0].due_dt,
                closing_date=i[0].closing_date,
            )
            new_index.title = i[0].title
            new_index.public_title = i[0].public_title
            new_index.description = i[0].description
            new_index.public_description = i[0].public_description
            new_index.requestor_name = f"{i[1].first_name} {i[1].last_name}"
            new_index.requestor_email = i[3].email
            new_index.status = request_statuses[i[0].status].code
            new_index.extension_status = extension_statuses[i[0].extension_status].code
            if i[2]:
                new_index.department = i[2].name
            else:
                new_index.department = "Unassigned"

            yield new_index
