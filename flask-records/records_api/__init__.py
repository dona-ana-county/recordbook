import os
from urllib.parse import urlparse

from flask import Flask
from flask.logging import default_handler
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_mail import Mail
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_alembic import Alembic
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections

from records_api.config import config

from records_api.service import Service
from records_api.notifications import strftime

mail = Mail()
db = SQLAlchemy()
limiter = Limiter(key_func=get_remote_address)
pass_limiter = limiter.limit("5/minute")
code_limiter = limiter.limit("1/second")
alembic = Alembic()

def create_app(config_name):
    app = Flask(__name__, template_folder="notifications/mail_templates")

    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    app.logger.setLevel(app.config['LOG_LEVEL'])

    limiter.init_app(app)
    if app.config['DEBUG'] or config_name == 'test':
        limiter.enabled = False
        CORS(app, resources={r"*": {"origins": "*"}})
        app.logger.setLevel('DEBUG')

    mail.init_app(app)
    db.init_app(app)
    alembic.init_app(app)
    Service.init_app(app)

    # Mask password from uri
    parsed = urlparse(app.config['SQLALCHEMY_DATABASE_URI'])
    replaced = parsed._replace(netloc="{}:{}@{}".format(parsed.username, "****", parsed.hostname))

    app.logger.info("Database: %s", replaced.geturl())

    if app.config['LDAP_ENABLE']:
        from flask_ldap3_login import LDAP3LoginManager, log as ldap_log
        
        app.logger.info("LDAP Enabled: "+ app.config['LDAP_HOST']) #pylint: disable=no-member
        LDAP3LoginManager(app)

        ldap_log.disabled = False
        ldap_log.setLevel(app.config['LDAP_DEBUG'])
        ldap_log.addHandler(default_handler)

    app.elasticsearch = Elasticsearch([app.config["ELASTICSEARCH_URL"]])
    connections.add_connection("default", app.elasticsearch)

    app.context_processor(lambda: {
        'app_name': app.config['APP_NAME'],
        'entity_name': app.config['ENTITY_NAME'],
        'entity_email': app.config['ENTITY_EMAIL'],
        'ipra_admin_email': app.config['IPRA_ADMIN_EMAIL'],
        'strftime': strftime,
    })

    from records_api.routing import routing as routing_blueprint

    app.register_blueprint(routing_blueprint)

    from records_api.scheduler import scheduler as scheduler_blueprint
    app.register_blueprint(scheduler_blueprint)

    from records_api.manage import blueprint as manage_blueprint
    app.register_blueprint(manage_blueprint)

    return app