'''Handles dependency injection for services'''
import sys
from typing import Dict, Union

TypeOrString = Union[type, str]
ServiceOrProxy = Union["Service", "ServiceProxy"]
class Service:

    @classmethod
    def init_app(cls, app):
        cls._app = app
    

    @classmethod
    def get_service(cls, service_name: TypeOrString) -> ServiceOrProxy:
        if isinstance(service_name, type):
            service_name = service_name.__module__ + "." + service_name.__name__
        service = cls._services.get(service_name)
        if service is not None:
            return service
        
        if service_name in cls._service_cls and cls._app is not None:
            cls._services[service_name] = ServiceProxy(service_name)
            service = cls._service_cls[service_name](cls._app)
            cls._services[service_name] = service
            return service
        return ServiceProxy(service_name)

    @classmethod
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        name = cls.__module__ + "." + cls.__name__
        cls._service_cls[name] = cls
       
    @classmethod
    def get_real(cls, service_name) -> "Service":
        service = cls._services.get(service_name)
        if service is not None and not isinstance(service, ServiceProxy):
            return service
        if service_name in cls._service_cls:
            assert cls._app is not None
            service = cls._service_cls[service_name](cls._app)
            cls._services[service_name] = service
            return service
        assert False

    _app = None
    _services: Dict[str, ServiceOrProxy] = {}
    _service_cls: Dict[str, type] = {}

class ServiceProxy:
    __slots__ = ["_service", "_service_name"]
    def __init__(self, service_name: str):
        self._service_name: str = service_name
        self._service: Service = None

    def __getattr__(self, name):
        if self._service is None:
            self._service = Service.get_real(self._service_name)
        return getattr(self._service, name)
