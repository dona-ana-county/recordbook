import os
from time import sleep

import click
from flask import Blueprint, current_app

from .alembic import verify_schema
from .db_init import db_init, insert_test_data, set_super_password
from .es_init import es_init

blueprint = Blueprint("manage", __name__)

@blueprint.cli.command("verify")
@click.option("--wait", default=False, is_flag=True, help="Stop and wait for database to be brought up to date")
@click.option("--upgrade", "upgrade", flag_value="upgrade", help="Upgrade the database")
@click.option("--no-upgrade", "upgrade", flag_value="no-upgrade", default=True, help="Prevent upgrade of the database")
@click.option("--upgrade-by-environment", "upgrade", flag_value="environment", help="Upgrade depending on $UPGRADE_DB" )
def verify(wait, upgrade):
    '''Verify database is up to date. 
    
    Upgrade the database depending on flags
    '''
    if upgrade == "environment":
        upgrade = os.environ.get("UPGRADE_DB") == "dangerous"
    elif upgrade == "upgrade":
        upgrade = True
    elif upgrade == "no-upgrade":
        upgrade = False
    else:
        assert False

    if verify_schema(current_app, upgrade):
        exit(0)
    else:
        if wait:
            current_app.logger.error("Waiting on schema upgrade...")
            while not verify_schema(current_app, False):
                sleep(5)
        else:
            exit(1)

@blueprint.cli.command("db_init")
@click.option("--testdata", default=False, is_flag=True, help="Insert test data")
@click.option("--localdata", default=False, is_flag=True, help="Insert local data")
@click.option("--superpass", required=True, help="Superuser password")
def cli_db_init(testdata, localdata, superpass):
    '''Initialize or reinitialize the database.'''
    from records_api import db as _db
    db_init(_db, superpass)
    current_app.logger.info("Tables Reset")
    if testdata:
        insert_test_data(_db)
        current_app.logger.info("Test data inserted")

    if localdata:
        from local_db_init import run
        run(_db)
        current_app.logger.info("Local data inserted")

    es_init()
    current_app.logger.info("Indicies Reset")

@blueprint.cli.command("es_init")
def cli_es_init():
    '''Initialize or recreate the search index.'''
    es_init()
    current_app.logger.info("Indicies Reset")

@blueprint.cli.command("set_super_password")
@click.option("--superpass", required=True, help="Superuser password")
def cli_set_super_pass(superpass):
    from records_api import db as _db
    set_super_password(_db, superpass)
    current_app.logger.info("Super user password reset")
