
from time import sleep

from sqlalchemy.exc import OperationalError

from records_api import alembic


def verify_schema(app, do_upgrade=False):
    '''Verifies the schema in the database matches provided migrations.
    
    Will perform an upgrade if environment variable set or throw error
    '''

    with app.app_context():
        app.logger.info("Checking schema version...")
        
        ver_head = alembic.heads()
        app.logger.info("Head: %s", ver_head)
        
        try:
            ver_curr = alembic.current()
        except OperationalError:
            app.logger.error("Cannot connect to db. Waiting 10 seconds to retry...")
            sleep(10)
            ver_curr = alembic.current()
        
        app.logger.info("current: %s", ver_curr)
        while ver_head != ver_curr:
            if  do_upgrade:
                app.logger.info("Schema not up to date. Upgrading...")
                
                # enable alembic Logs
                #alembic_log = logging.getLogger("alembic")
                #alembic_log.disabled = False
                #alembic_log.setLevel("INFO")
                #alembic_log.addHandler(default_handler)

                # perform upgrade
                alembic.upgrade()
                do_upgrade = False # only try upgrade once

                app.logger.info("Finished Upgrading")
                ver_curr = alembic.current()
                
            else:
                app.logger.error("Schema version mismatch: expected %s. Got %s", ver_head, ver_curr)
                return False 
        app.logger.info("Found correct version: %s", ver_curr)
        return True
