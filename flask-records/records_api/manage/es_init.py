
from elasticsearch.exceptions import NotFoundError
from elasticsearch_dsl import Index
from flask import Blueprint

from records_api.searching.model import RequestIndex
from records_api.searching.service import SearchService
from records_api.service import Service

blueprint = Blueprint("es_init", __name__)

def delete_index(index):
    i = Index(index)
    try:
        i.delete()
    except NotFoundError:
        pass


def create_requests():
    RequestIndex.init()


def es_init():
    delete_index("requests")
    RequestIndex.init()
    Service.get_service(SearchService).add_all_requests()

if __name__ == "__main__":
    print("Direct calling is depricated. Please use 'flask es_init init'")
