import os
import shutil
from datetime import datetime, timedelta, timezone
from uuid import UUID, uuid4

from flask import current_app

from records_api.constant import groups, roles
from records_api.serializer import db_schema as dbs
from records_api.users.model import Staff
from records_api.users.service import hash_password

# def insert_initial_staff(db, department):
#     # service = AuthService()

#     user = Staff(
#         id_=uuid4(),
#         username="super",
#         first_name="Super",
#         last_name="User",
#         email="super@test.com",
#         role=roles["SUP"],
#         disabled=False,
#         external_auth=False,
#     )
#     user.groups = []

#     db_user = dbs.User(id_=user.id_, first_name=user.first_name, last_name=user.last_name)
#     db.session.add(db_user)
#     db.session.commit()


#     db_staff = dbs.Staff(
#         id_=db_user.id_,
#         username=user.username,
#         password=hash_password("pass"),
#         email=user.email,
#         role=user.role.code,
#         department=department,
#         disabled=False,
#         delete_date=None,
#         external_auth=user.external_auth,
#     )
#     db.session.add(db_staff)
#     db.session.commit()



def insert_test_staff(db) -> UUID:
    # service = AuthService()
    departments = []
    departments.append(
        db.session.query(dbs.Department).filter(dbs.Department.name == "Public Information").first()
    )
    departments.append(
        db.session.query(dbs.Department).filter(dbs.Department.name == "Legal").first()
    )
    departments.append(
        db.session.query(dbs.Department)
        .filter(dbs.Department.name == "Information Technology")
        .first()
    )
    departments.append(
        db.session.query(dbs.Department)
        .filter(dbs.Department.name == "Assessor")
        .first()
    )

    staff = []
    one_id = uuid4()
    staff.append(Staff(
        id_=one_id,
        username="test",
        first_name="Test",
        last_name="Test",
        email="test@test.com",
        role=roles["SUP"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = [groups["IPA"], groups["IPC"], groups["LEG"]]

    staff.append(Staff(
        id_=uuid4(),
        username="pi-man-ipa",
        first_name="Public Information",
        last_name="Manager - Ipra Admin",
        email="pi-man-ipa@donaanacounty.org",
        role=roles["MAN"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = [groups["IPA"]]

    staff.append(Staff(
        id_=uuid4(),
        username="lg-man-ipa",
        first_name="Legal",
        last_name="Manager - Ipra Admin/Legal Queue",
        email="lg-man-ipa@donaanacounty.org",
        role=roles["MAN"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = [groups["IPA"], groups["LEG"]]

    staff.append(Staff(
        id_=uuid4(),
        username="lg-wrk-leg",
        first_name="Legal",
        last_name="Worker - Legal Queue",
        email="lg-wrk-leg@donaanacounty.org",
        role=roles["WRK"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = [groups["LEG"]]

    staff.append(Staff(
        id_=uuid4(),
        username="it-man",
        first_name="IT",
        last_name="Manager",
        email="it-man@donaanacounty.org",
        role=roles["MAN"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = []

    staff.append(Staff(
        id_=uuid4(),
        username="it-wrk",
        first_name="IT",
        last_name="Worker",
        email="it-wrk@donaanacounty.org",
        role=roles["WRK"],
        disabled=False,
        external_auth=False,
    ))
    staff[-1].groups = []

    for user in staff:
        db_user = dbs.User(id_=user.id_, first_name=user.first_name, last_name=user.last_name)
        db.session.add(db_user)
        db.session.commit()

        if user.username == "test" or user.username == 'pi-man-ipa':
            db_staff = dbs.Staff(
                id_=db_user.id_,
                username=user.username,
                password=hash_password("pass"),
                email=user.email,
                role=user.role.code,
                department=departments[0].id_,
                disabled=False,
                delete_date=None,
                external_auth=user.external_auth,
            )
            db.session.add(db_staff)
            db.session.commit()

        if user.username == "lg-man-ipa" or user.username == "lg-wrk-leg":
            db_staff = dbs.Staff(
                id_=db_user.id_,
                username=user.username,
                password=hash_password("pass"),
                email=user.email,
                role=user.role.code,
                department=departments[1].id_,
                disabled=False,
                delete_date=None,
                external_auth=user.external_auth,
            )
            db.session.add(db_staff)
            db.session.commit()

        if user.username == "it-man" or user.username == "it-wrk":
            db_staff = dbs.Staff(
                id_=db_user.id_,
                username=user.username,
                password=hash_password("pass"),
                email=user.email,
                role=user.role.code,
                department=departments[2].id_,
                disabled=False,
                delete_date=None,
                external_auth=user.external_auth,
            )
            db.session.add(db_staff)
            db.session.commit()

        db_user_groups = []
        for group in user.groups:
            db_user_groups.append(dbs.StaffGroup(staff_id=user.id_, group_code=group.code))
        db.session.add_all(db_user_groups)

    db.session.commit()
    return one_id


# def insert_initial_department(db) -> UUID:
#     id_ = uuid4()
#     department = dbs.Department(id_=id_, name="Human Resources")
#     db.session.add(department)
#     db.session.commit()
#     return id_

def insert_test_departments(db):
    departments = [
        dbs.Department(id_=uuid4(), name="Legal"),
        dbs.Department(id_=uuid4(), name="Public Information"),
        dbs.Department(id_=uuid4(), name="Assessor"),
    ]
    db.session.add_all(departments)
    db.session.commit()


def insert_token(db, one_id: UUID):
    token = dbs.AuthToken(
        staff_id=one_id,
        token="auth_token",
        issued_dt=datetime.now(tz=timezone.utc),
        expiration_dt=datetime.now(tz=timezone.utc) + timedelta(days=9999),
        revoked_dt=None,
    )
    db.session.add(token)
    db.session.commit()


def insert_denial_reasons(db):
    reasons = [
        dbs.DenialReason(code="WGE", reason="Wrong Government Entity"),
        dbs.DenialReason(code="NPA", reason="Information Not Publicly Available"),
        dbs.DenialReason(code="OTH", reason="Other"),
    ]
    db.session.add_all(reasons)
    db.session.commit()


def insert_close_reasons(db):
    reasons = [
        dbs.CloseReason(code="RES", reason="All Response Documents Added"),
        dbs.CloseReason(code="OTH", reason="Other"),
    ]
    db.session.add_all(reasons)
    db.session.commit()


def insert_gov_entities(db):
    entities = [
        dbs.GovernmentEntity(id_=uuid4(), name="Gov. Entity 1", email="email@entity1.gov"),
        dbs.GovernmentEntity(id_=uuid4(), name="Gov. Entity 2", email="email@entity2.gov"),
        dbs.GovernmentEntity(id_=uuid4(), name="Gov. Entity 3", email="email@entity3.gov"),
    ]
    db.session.add_all(entities)
    db.session.commit()


def remove_resources():
    folder = current_app.config["UPLOAD_FOLDER"]
    if os.path.isdir(folder):
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                # uncomment to remove subdirectories
                elif os.path.isdir(file_path): 
                    shutil.rmtree(file_path)
            except Exception as e:
                print(e)

def insert_test_data(db):
    insert_test_departments(db)
    one_id = insert_test_staff(db)
    insert_token(db, one_id)
    insert_gov_entities(db)
    insert_close_reasons(db)
    insert_denial_reasons(db)
    #print("Test data inserted")


def set_super_password(db, password:str):
    super_:dbs.Staff = db.session.query(dbs.Staff).filter(dbs.Staff.username == 'super').first()
    super_.password = hash_password(password)
    db.session.add(super_)
    db.session.commit()

def db_init(db, password:str) -> None:
    from records_api.manage.alembic import verify_schema
    db.session.commit()

    current_app.logger.info("Dropping All Tables")
    for name in [row[0] for row in db.engine.execute("SELECT tablename from pg_tables where schemaname='public'")]:
        db.engine.execute('DROP TABLE IF EXISTS public."' + name + '" CASCADE')
    #for name in [row[0] for row in db.engine.execute("SELECT sequencename from pg_sequences where schemaname='public'")]:
    for name in [row[0] for row in db.engine.execute("SELECT c.relname from pg_class c where c.relkind='S'")]:
        db.engine.execute('DROP SEQUENCE IF EXISTS public."' +  name + '" CASCADE')

    remove_resources()
    verify_schema(current_app, do_upgrade=True)
    set_super_password(db, password)

if __name__ == "__main__":
    print("Direct calling is depricated. Please use 'flask manage db_init [--testdata]'")
