from datetime import datetime

def suffix(d: int) -> str:
    return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')

def strftime(format_: str, t: datetime) -> str:
    return t.strftime(format_).format(th=str(t.day) + suffix(t.day))
