from email import charset
from typing import List

from flask import current_app
from flask_mail import Message

from records_api import mail

# ensure emails are sent using content-transfer-encoding of quoted-printable
charset.add_charset('utf-8', charset.SHORTEST, charset.QP, 'utf-8')

def send_email(title: str, body: str, html: str, recipients: List[str]):
    if recipients:
        msg = Message(title, recipients=recipients)
        msg.body = body
        msg.html = html
        mail.send(msg)
    else:
        current_app.logger.info("No recipients added")
