from marshmallow import Schema, fields

from records_api.constants.schema import LookupSchema


class LogMessageSchema(Schema):
    id_ = fields.UUID(data_key="id")
    message = fields.Str(allow_none=False)
    message_date = fields.DateTime(data_key="messageDate", allow_none=False)
    event_code = fields.Nested(LookupSchema, data_key="eventCode", allow_none=False)

class NotificationParamsSchema(Schema):
    due_date = fields.Date(data_key="dueDt", allow_none=True)
