from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from records_api.constants.model import Lookup
from records_api.users.model import User


@dataclass
class Notification:
    message: str
    recipient: User


@dataclass
class EventCode(Lookup):
    def __repr__(self):
        return f"EventCode(Code: {self.code}, Value: {self.value})"


class Event(metaclass=ABCMeta):
    @abstractmethod
    def action(self):
        pass

@dataclass
class LogMessage:
    id_: UUID
    message_date: datetime
    message: str
    event_code: EventCode
    request_id: str

    def __repr__(self):
        return f"LogMessage(Event: {self.event_code.value}, Request: {self.request_id})"
