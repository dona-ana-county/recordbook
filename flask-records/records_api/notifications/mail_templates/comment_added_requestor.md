subject: {{app_name}}: A COMMENT Was Added to Your Request {{ ipra_request.id_ }}
## A New COMMENT has been added to your request. 

Please click [view the request]({{ links.direct_requestor }}) to read and respond through the website.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Title:**              |{{ ipra_request.public_title}}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt) }}| 

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)