subject: {{app_name}}: A new request {{ ipra_request.id_ }} has been ASSIGNED to {{ department }}.
## {{ department }} Has Been ASSIGNED to a Request

{{ department }} has been assigned to the request "{{ ipra_request.public_title }}".  Please [view the request]({{ links.staff }}) to begin.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 