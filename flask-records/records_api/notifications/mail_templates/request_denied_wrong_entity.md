subject: {{app_name}}: Your request {{ ipra_request.id_ }} has been DENIED
## Your request has been DENIED: Wrong Governmental Entity

Your request {{ ipra_request.id_ }} has been DENIED because {{ entity_name }} is the wrong governmental entity to respond to your request.  This is because the County does not 
maintain the records you requested.  

Based on a review of your request we believe you will need to request this of {{ entity.name }}.  They may be contacted at {{ entity.email }}.  We have forwarded your request to them so they may respond to you directly but we recommend you reach out to them for more information on your request.   

The content of your request is below.

|Field          | Value                                                                                                                                                 |
|:---           |:---                                                                                                                                                   |
|Request Title  |{{ ipra_request.title }}                                                                                                                               |
|Description    |{{ ipra_request.description }}                                                                                                                         |
|Requestor Name |{{ ipra_request.requestor.first_name }} {{ ipra_request.requestor.last_name }}                                                                         |
|Email          |{{ ipra_request.requestor.contact_info.email }}                                                                                                        |
|Phone          |{{ ipra_request.requestor.contact_info.phone }}                                                                                                        |
|Address        |{{ ipra_request.requestor.contact_info.address_line_1 }}                                                                                               |
|               |{{ ipra_request.requestor.contact_info.address_line_2 }}                                                                                               |
|               |{{ ipra_request.requestor.contact_info.city }}, {{ ipra_request.requestor.contact_info.state }} {{ ipra_request.requestor.contact_info.postal_code }}  |
|Fax            |{{ ipra_request.requestor.contact_info.fax }}                                                                                                          |
   
If you feel this request has been denied in error, please email {{ ipra_admin_email }} and provide the reason you believe the request should be reopened.

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)