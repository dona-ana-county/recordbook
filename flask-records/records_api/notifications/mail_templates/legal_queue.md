subject: {{app_name}}: A new document has been added for LEGAL REVIEW.
## A document has been uploaded to the LEGAL REVIEW Queue

A document titled "{{ resource.title }}" has been uploaded to the Legal Review Queue.

|                   |             |
|:---               |:---         |  
|**Document:**      |{{ resource.title }}|  
|**Creation Date:** |{{ strftime('%A, %B {th}, %Y', resource.create_dt )}}|  
|**Release:**       |{{ resource.release_type.value }}| 
|**Request Title:**              |{{ ipra_request.public_title}}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 