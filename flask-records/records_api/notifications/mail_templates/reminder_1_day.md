subject: {{app_name}}: Request {{ ipra_request.id_ }} will BREACH in 1 day
## Request {{ ipra_request.id_ }} will BREACH in 1 day

The request {{ ipra_request.id_ }} is nearly at its required time to complete, and must 
be completed by tomorrow (even if tomorrow is a weekend or holiday.)  If you cannot 
complete the request in the allotted time, please speak with our Legal
Department immediately to determine next steps.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 