subject: {{app_name}}: Your request {{ ipra_request.id_ }} has been CREATED
## Request {{ ipra_request.id_ }} has been CREATED

Thank you for submitting your request for public records to {{entity_name}}.  
County staff will begin working on this request immediately and you will be notified by email of updates to your request.  Please check your email (including your spam or junk folders) regularly to view notifications.   
  
The link below will allow you to view your request and provide access to more information.   
  
The Pass Code below may also be used to access your request through the main {{app_name}} website by entering the Pass Code where you see "view as requestor."

If you have questions or concerns about your request, please click the [View Request]({{ links.requestor }}) link below and use the "Communications" page to send a message to the staff working on your request. 

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)