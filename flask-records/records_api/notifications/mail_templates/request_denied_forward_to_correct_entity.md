subject: Forwarded IPRA request: {{ ipra_request.id_ }}

{{ entity_name }} received IPRA request {{ ipra_request.id_ }}, detailed below, but has determined that the County is the wrong governmental entity to respond to the request. 

We believe {{ entity.name }} is the correct entity to respond to the request; thus, we have forwarded it to you at {{ entity.email }}. 

|Field          | Value                                                                                                                                                 |
|:---           |:---                                                                                                                                                   |
|Request Title  |{{ ipra_request.title }}                                                                                                                               |
|Description    |{{ ipra_request.description }}                                                                                                                         |
|Requestor Name |{{ ipra_request.requestor.first_name }} {{ ipra_request.requestor.last_name }}                                                                         |
|Email          |{{ ipra_request.requestor.contact_info.email }}                                                                                                        |
|Phone          |{{ ipra_request.requestor.contact_info.phone }}                                                                                                        |
|Address        |{{ ipra_request.requestor.contact_info.address_line_1 }}                                                                                               |
|               |{{ ipra_request.requestor.contact_info.address_line_2 }}                                                                                               |
|               |{{ ipra_request.requestor.contact_info.city }}, {{ ipra_request.requestor.contact_info.state }} {{ ipra_request.requestor.contact_info.postal_code }}  |
|Fax            |{{ ipra_request.requestor.contact_info.fax }}                                                                                                          |

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.public }})|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)