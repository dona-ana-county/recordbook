subject: {{app_name}}: A COMMENT Was Added to Request {{ ipra_request.id_ }}
## A New COMMENT has been added to a request

Please [view the request]({{ links.direct_staff }}) to read and respond to the comment.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt) }}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt) }}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 