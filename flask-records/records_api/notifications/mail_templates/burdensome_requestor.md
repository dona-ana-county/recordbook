subject: {{app_name}}: Your request {{ ipra_request.id_ }} has been deemed BURDENSOME
## Your request {{ ipra_request.id_ }} has been deemed BURDENSOME and the County will need more time to respond. 
On {{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}, the County received your request to inspect public records. We believe that your request is excessively burdensome or overly broad and the County will need additional time to respond to your request, on or before {{ strftime('%A, %B {th}, %Y', due_date | default(ipra_request.due_dt) )}}. If the County needs additional time beyond that, we will promptly inform you.   

{{ middle |e }}

If you have questions or concerns about your request, please click the [View Request]({{ links.requestor }}) link below and use the "Communications" page to send a message to the staff working on your request.


|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title}}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)