subject: {{app_name}}: A new request is in the ASSIGNMENT QUEUE
## A new request has been entered, and requires ASSIGNMENT.

A new request has been received with the title "{{ ipra_request.public_title }}".  Please [view the request]({{ links.staff }}) to assign it to department(s).   

This email is an update on the following request:   

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 