subject: {{app_name}}: Your request {{ ipra_request.id_ }} needs additional time
## Your request {{ ipra_request.id_ }} needs additional time
On {{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}, the County received your request to inspect public records. The County will need additional time to respond to your request and will respond on or before fifteen (15) calendar days as allowed under the Inspection of Public Records Act, NMSA 1978 Sec. 14-2-8(D). The County will promptly inform you if your request will take additional time.   

{{ middle |e }}

If you have questions or concerns about your request, please click the [View Request]({{ links.requestor }}) link below and use the "Communications" page to send a message to the staff working on your request. 

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)