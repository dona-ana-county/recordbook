subject: {{app_name}}: Your Document "{{ resource.public_title }}" has been RELEASED
## A document you uploaded has been RELEASED from the Legal Queue

{% if resource.revised %}
The document you uploaded titled "{{ resource.public_title }}" has 
been revised by legal review, and was released.
{% else %}
The document you uploaded titled "{{ resource.public_title }}" has 
passed legal review, and was released.
{% endif %}

|                   |             |
|:---               |:---         |  
|**Document:**      |{{ resource.public_title }}|  
|**Creation Date:** |{{ strftime('%A, %B {th}, %Y', resource.create_dt )}}|  
|**Release:**       |{{ resource.release_type.value }}| 

The document is now visible according to its release type: {{ resource.release_type.value }}

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt) }}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 