subject: {{app_name}}: Your Document "{{ resource.public_title}}" has been REJECTED
## A document you uploaded for Legal review has been REJECTED

The document you uploaded titled "{{ resource.public_title }}" has
been rejected for the following reason:

{{ reason }}

|                   |             |
|:---               |:---         |  
|**Document:**      |{{ resource.public_title }}|  
|**Creation Date:** |{{ strftime('%A, %B {th}, %Y', resource.create_dt )}}|  
|**Release:**       |{{ resource.release_type.value }}| 

The document is no longer accessable in the system.  If you feel this is in error, you 
must upload a new copy of the document.

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 