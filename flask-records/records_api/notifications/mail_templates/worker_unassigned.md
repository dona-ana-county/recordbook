subject: {{app_name}}: You have been UNASSIGNED from request {{ ipra_request.id_ }}
## You have been UNASSIGNED from request {{ ipra_request.id_ }}

You have been unassigned from the request {{ ipra_request.id_ }} "{{ ipra_request.public_titlee }}".

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt) }}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt) }}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 