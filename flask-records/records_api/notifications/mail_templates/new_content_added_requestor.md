subject: {{app_name}}: A new {{ content_type.value.upper() }} is available for inspection

## A new {{ content_type.value.upper() }} has been added to the County's response to your request

A new {{ content_type.value.upper() }} is available for inspection. Please click the link below to access the record(s).

Content: {{ content.title }}
Content Type: {{ content_type.value.title() }}
Added: {{ strftime('%A, %B {th}, %Y', content.create_dt )}}

Please [view the request]({{ links.direct_requestor }}) to read or download the new content.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title}}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)