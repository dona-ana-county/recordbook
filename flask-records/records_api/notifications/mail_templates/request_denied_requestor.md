subject: {{app_name}}: Your request {{ ipra_request.id_ }} has been DENIED
## Request {{ ipra_request.id_ }} has been DENIED

Request {{ ipra_request.id_ }} has been denied for the following reason:

- {{ reason.value }}

If you feel this is in error, please email {{ ipra_admin_email }} and provide the reason you believe the 
request should be reopened.

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.requestor }})|  
|**Pass Code:**                  |{{ ipra_request.passcode }}|  
|**Request Title:**              |{{ ipra_request.public_title }}| 
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}|  

----------------------------------------------------------------------------------------
(For technical support with the application, please contact <{{ entity_email }}>.)