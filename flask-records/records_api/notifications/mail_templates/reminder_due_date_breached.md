subject: {{app_name}}: Request {{ ipra_request.id_ }} has BREACHED
## Request {{ ipra_request.id_ }} has BREACHED

The request {{ ipra_request.id_ }} is was required to be completed 
on {{ ipra_request.due_dt }}.  Action MUST be taken immediately as
we are out of legal compliance as long as this request remains incomplete.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title}}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 