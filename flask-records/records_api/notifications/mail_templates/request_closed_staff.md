subject: {{app_name}}: Request {{ ipra_request.id_ }} has been CLOSED
## Request {{ ipra_request.id_ }} has been CLOSED

The request {{ ipra_request.id_ }} with the title "{{ ipra_request.public_title }}" has been closed.  You may still [view the request]({{ links.staff }}) but can no longer modify or add content.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 