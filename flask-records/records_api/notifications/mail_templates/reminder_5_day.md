subject: {{app_name}}: Request {{ ipra_request.id_ }} will BREACH in 5 days
## Request {{ ipra_request.id_ }} will BREACH in 5 days

The is a reminder that the request {{ ipra_request.id_ }} is has five CALENDAR
days before you reach the due date.  Please find time to finish the request when
possible.

This email is an update on the following request:

|                                |             |
|:---                            |:---         |  
|**Request Link:**               |[View Request]({{ links.staff }})|  
|**Request Title:**              |{{ ipra_request.public_title }}|  
|**Request ID:**                 |{{ ipra_request.id_ }}|  
|**Request Submission Date:**    |{{ strftime('%A, %B {th}, %Y', ipra_request.create_dt )}}| 
|**Request Due Date:**           |{{ strftime('%A, %B {th}, %Y', ipra_request.due_dt )}}| 
|**Request Extension Status:**   |{{ ipra_request.extension_status.value }}| 