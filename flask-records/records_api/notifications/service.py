from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime, timezone
from typing import List
from uuid import UUID, uuid4

from flask import current_app, render_template
from markdown import markdown
from records_api.communications.model import Comment
from records_api.constant import content_types, event_codes, groups, release_types
from records_api.constants.model import GovernmentEntity, Reason
from records_api.content.model import Content, ContentType, Note, ReleaseType, Resource
from records_api.content.service import ContentService
from records_api.departments.service import DepartmentService
from records_api.requests.model import Request
from records_api.requests.service import RequestService
from records_api.service import Service
from records_api.users.model import Requestor, Staff
from records_api.users.service import UserService

from .emailer import send_email
from .model import EventCode, LogMessage
from .repo import NotificationRepo

Message = namedtuple("Message", ["subject", "html", "text"])

@dataclass
class RequestUrls:
    public: str
    requestor: str
    staff: str
    direct_public: str
    direct_requestor: str
    direct_staff: str

CONTENT_TYPE_TO_SUBTAB = {
    'RES': "documents",
    'NOT': "notes",
    'LNK': "links",
    'INS': "instructions"
}                

class NotificationService(Service):
    def __init__(self, app):
        self.notification_repo = NotificationRepo()
        self.user_service: UserService = Service.get_service(UserService)
        self.departments_service: DepartmentService = Service.get_service(DepartmentService)
        self.content_service: ContentService = Service.get_service(ContentService)
        self.request_service: RequestService = Service.get_service(RequestService)

    def __new_log_message(self, request_id: str, message: str, event_code: EventCode) -> None:
        current_app.logger.info("new log message")
        log = LogMessage(
            id_=uuid4(),
            message_date=datetime.now(tz=timezone.utc),
            message=message,
            event_code=event_code,
            request_id=request_id,
        )
        self.notification_repo.new_log(log)

    def __new_note(self, request_id: str, title: str, message:str, release_type: ReleaseType, creator: Staff):
        note = Note(
            id_=None,
            create_dt=None,
            request_id=None,
            title=title,
            release_type=release_type,
            creator=creator,
            message=message,
            editable=False,
        )
        return self.content_service.new_note(request_id, note)

    def __new_note_from_email(self, request_id: str, title:str, msg:Message, staff: Staff, recipient: str = 'requestor', release_type: str = 'REQ'):
        return self.__new_note(
            request_id,
            title,
            f"The following email was sent to the {recipient}\n{msg.html}",
            release_types[release_type],
            staff,
        )

    def __check_contact(self, requestor: Requestor) -> bool:
        if requestor.primary_contact.code == "EML":
            return True
        else:
            return False

    def __get_request(self, request_id: str) -> Request:
        r = self.request_service.get_info(request_id)
        r.requestor = self.request_service.get_requestor(request_id)
        r.requestor.contact_info = self.user_service.get_requestor_contact_info(r.requestor.id_)
        return r

    def __get_content(self, content_id: UUID) -> Content:
        content = self.content_service.get_content_by_id(content_id)
        return content

    def __get_staff(self, token: str) -> Staff:
        return self.user_service.get_staff_by_token(token)

    def send_notification(self, msg: Message, recipient: str):
        recipients = [recipient]
        self.send_notifications(msg, recipients)

    def send_notifications(self, msg: Message, recipients: List[str]):
        recipients = [email for sublist in recipients for email in (
            sublist if isinstance(sublist, list) else [sublist])]
        send_email(msg.subject, msg.text, msg.html, recipients)

    def _markdown(self, md):
        return markdown(md, extensions=['tables', 'nl2br'])

    def _markdown_doctype(self, md):
        return "<!doctype html>" + self._markdown(md)

    def render(self, filename:str, subject:str = None,**kwargs):
        data = render_template(filename, **kwargs)

        # Extract subject from data
        first_line, _, rest = data.partition('\n')
        tag, _, line = first_line.partition(": ")
        if tag.strip() == "subject":
            subject = line.strip()
            data = rest.lstrip()

        if subject is None:
            return None

        if filename.endswith(".md"):
            return Message(                
                subject = subject,
                html = self._markdown_doctype(data), 
                text = data
            )
        else:
            return Message(
                subject = subject,
                html = data, 
                text = ''
            )
    def _links(self, request: Request, tab: str = None, content_type: ContentType = None):
        suffix = ""
        if tab:
            suffix = '#' + tab
            if content_type and CONTENT_TYPE_TO_SUBTAB.get(content_type.code):
                suffix=suffix+'.'+CONTENT_TYPE_TO_SUBTAB.get(content_type.code)

        return RequestUrls(
            public=f"{current_app.config['BASE_URL']}/viewRequest/{request.id_}",
            requestor=f"{current_app.config['BASE_URL']}/viewRequest/{request.id_}/{request.passcode}",
            staff=f"{current_app.config['BASE_URL']}/requestManagement/{request.id_}",
            direct_public=f"{current_app.config['BASE_URL']}/viewRequest/{request.id_}{suffix}",
            direct_requestor=f"{current_app.config['BASE_URL']}/viewRequest/{request.id_}/{request.passcode}{suffix}",
            direct_staff=f"{current_app.config['BASE_URL']}/requestManagement/{request.id_}{suffix}",
        )
    
    def _send_notifications_to_all_emails(self, msg: Message, requestor: Requestor):
        secondary_emails = requestor.contact_info.secondary_emails or []
        recipients = [requestor.contact_info.email] + secondary_emails
        self.send_notifications(msg, recipients)


    def create_request_event(self, request_id: str, staff: Staff = None) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has created request {request_id} with due date {request.due_dt.isoformat()}: {request.public_title}."
        else:
            message = f"Request {request_id}: {request.public_title} has been created with due date {request.due_dt.isoformat()}."
        self.__new_log_message(request_id, message, event_codes["CRE"])

        if self.__check_contact(request.requestor):
            msg = self.render(
                subject = "Request Created",
                filename = "requestor_notification.md",
                title=request.public_title,
                date=request.create_dt,
                links=self._links(request),
                code=request.passcode,
                ipra_request=request,
            )
            self._send_notifications_to_all_emails(msg, request.requestor)
            # self.__new_note_from_email(
            #     request_id, 
            #     "Request Created Email",
            #     msg,
            #     staff,
            # )
        else:
            # NOTE VT: Not sure what we are going to do if the requestor isn't going to be getting
            # emails.
            return

    def assignment_queue_event(self, request_id: str, staff: Staff = None) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has added request {request_id} to the assignment queue."
        else:
            message = (
                f"Request {request_id}: {request.public_title} has been added to the assignment queue."
            )
        self.__new_log_message(request_id, message, event_codes["AQE"])

        emails = self.notification_repo.get_emails_by_groups([groups["IPA"], groups["IPC"]])
        msg = self.render(
                subject = "New Request in Assignment Queue",
                filename = "request_in_assignment_queue.md", 
                title=request.public_title, 
                date=datetime.now(tz=timezone.utc),
                links=self._links(request),
                ipra_request = request
        )
        self.send_notifications(msg, emails)

    def departments_assigned(
        self, request_id: str, departments: List[UUID], staff: Staff = None
    ) -> None:
        request = self.__get_request(request_id)

        for dept_id in departments:
            dept = self.departments_service.get_departments([dept_id])[0]
            if staff:
                message = f"{staff.username} has assigned request {request_id} to {dept.name}."
            else:
                message = f"Request {request_id}: {request.public_title} has been assigned to {dept.name}."
            self.__new_log_message(request_id, message, event_codes["DAE"])

            emails = self.notification_repo.get_department_manager_emails(dept)
            msg = self.render(
                subject = "New Request in your Department Queue",
                filename="request_in_department_queue.md",
                title=request.public_title,
                date=datetime.now(tz=timezone.utc),
                department=dept.name,
                links=self._links(request),
                ipra_request=request,
            )
            self.send_notifications(msg, emails)

    def departments_unassigned(
        self, request_id: str, departments: List[UUID], staff: Staff = None
    ) -> None:
        request = self.__get_request(request_id)

        for dept_id in departments:
            dept = self.departments_service.get_departments([dept_id])[0]
            if staff:
                message = f"{staff.username} has unassigned request {request_id} from {dept.name}."
            else:
                message = (
                    f"Request {request_id}: {request.public_title} has been unassigned from {dept.name}."
                )
            self.__new_log_message(request_id, message, event_codes["DUE"])


    def secondary_department_task_completed(
        self, request_id: str, department: UUID, staff: Staff = None
        ) -> None:
        current_app.logger.info("completed")
        request = self.__get_request(request_id)
        dept = self.departments_service.get_departments([department])[0]
        if staff:
            message = f"{staff.username} has marked the response of {dept.name} to request {request_id} complete."
        else:
            message = (
                f"Request {request_id}: The response of {dept.name} to {request.public_title} has been marked complete."
                )
        self.__new_log_message(request_id, message, event_codes["DCT"])

    
    def secondary_department_task_not_completed(
        self, request_id: str, department: UUID, staff: Staff = None
    ) -> None:
        current_app.logger.info("not completed")
        request = self.__get_request(request_id)    
        dept = self.departments_service.get_departments([department])[0]
        if staff:
            message = f"{staff.username} has marked the response of {dept.name} to request {request_id} incomplete."
        else:
            message = (
                f"Request {request_id}: The response of {dept.name} to {request.public_title} has been marked incomplete."
                )
        self.__new_log_message(request_id, message, event_codes["DIT"])

    
    # def request_updated(self, request_id: str, message: str, staff: Staff = None) -> None:
    #     request = self.__get_request(request_id)
    #     if staff:
    #         message = f"{staff.username} has updated {request_id}: {message}."
    #     else:
    #         message = f"Request {request_id}: {request.title} has been updated, {message}."
    #     self.__new_log_message(request_id, message, event_codes["RUE"])

    def request_due_date_updated_event(self, request_id: str, reason:str, staff: Staff) -> None: 
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} updated the due-date of {request_id} to {request.due_dt.isoformat()}: {reason}"
        else:
            message = f"The due-date of Request {request_id} has been updated to {request.due_dt.isoformat()}: {reason}"
        self.__new_log_message(request_id, message, event_codes["RDU"])

    def request_extended_event(self, request_id: str, description: str, staff: Staff ) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} extended the due-date of {request_id} to {request.due_dt.isoformat()}: {request.extension_status.value}"
        else:
            message = f"The due-date of Request {request_id} has been updated to {request.due_dt.isoformat()}: {request.extension_status.value}"
        self.__new_log_message(request_id, message, event_codes["RDU"])

        description = description.strip()
        if description:
            description = f"\n{description}   \n"

        if self.__check_contact(request.requestor):
            if request.extension_status.code == "INI":
                filename = "extension_requestor"
            else:
                filename = "burdensome_requestor"
            
            msg = self.render(
                middle = description,
                filename = filename + ".md",
                title=request.public_title, 
                date=datetime.now(tz=timezone.utc),
                links=self._links(request),
                ipra_request=request,
            )
            self._send_notifications_to_all_emails(msg, request.requestor)
            self.__new_note_from_email(
                request_id, 
                "Due Date Extended Email", 
                msg,
                staff,
            )
        else:
            message = f"{staff.username} confirmed they sent an {request.extension_status.value} document for {request_id} to the requestor by {request.requestor.primary_contact.value}"
            self.__new_log_message(request_id, message, event_codes["SDR"])


        emails = self.notification_repo.get_assigned_staff_emails(request.id_)
        msg = self.render(
            subject = "Due date Extended",
            filename = "request_extended_staff.md", 
            title=request.public_title, 
            date=datetime.now(tz=timezone.utc),
            links=self._links(request),
            ipra_request=request,
        )
        self.send_notifications(msg, emails)


    def request_closed_event(self, request_id: str, reason: Reason, staff: Staff) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has closed request {request_id} for reason {reason.value}"
        else:
            message = (
                f"Request {request_id}: {request.public_title} has been closed for reason {reason.value}"
            )
        self.__new_log_message(request_id, message, event_codes["RCE"])

        emails = self.notification_repo.get_assigned_staff_emails(request.id_)
        msg = self.render(
            subject = "Request Closed",
            filename = "request_closed_staff.md", 
            title=request.public_title, 
            date=datetime.now(tz=timezone.utc),
            links=self._links(request),
            reason=reason,
            ipra_request=request,
        )
        self.send_notifications(msg, emails)

        if self.__check_contact(request.requestor):
            msg = self.render(
                subject = "Your Request Has Been Closed",
                filename = "request_closed_requestor.md", 
                title=request.public_title, 
                date=datetime.now(tz=timezone.utc),
                links=self._links(request),
                reason=reason,
                ipra_request=request

            )
            self._send_notifications_to_all_emails(msg, request.requestor)
            self.__new_note_from_email(request_id, "Request Closed Email",msg, staff)
        else:
            # NOTE VT: Not sure what we are going to do if the requestor isn't going to be getting
            # emails.
            return

    def request_reopened(self, request_id: str, staff: Staff) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has re-opened {request_id}."
        else:
            message = f"Request {request_id}: {request.public_title} has been re-opened."
        self.__new_log_message(request_id, message, event_codes["RRE"])
    
    def request_forwarded_event(
        self,
        request_id: str,
        entity: GovernmentEntity = None,
        staff: Staff = None,
    ) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has forwarded request {request_id} to {entity.name}"
        else:
            message = (
                f"Request {request_id}: {request.public_title} has been forwarded to {entity.name}"
            )
        self.__new_log_message(request_id, message, event_codes["RFE"])

        msg = self.render(
            subject = "Forwarded IPRA Request",
            filename="request_denied_forward_to_correct_entity.md",
            title=request.public_title,
            date=datetime.now(tz=timezone.utc),
            entity=entity,
            links=self._links(request),
            ipra_request=request
            )
        
        if self.__check_contact(request.requestor):
            secondary_emails = request.requestor.contact_info.secondary_emails or []
            recipients = [request.requestor.contact_info.email] + secondary_emails
            self.send_notifications(msg, [entity.email, recipients])
            self.__new_note_from_email(request_id, "Request Forwarded Email", msg, staff,
                entity.name + ' and the requestor', 'REQ')
        else:
            # NOTE VT: Not sure what we are going to do if the requestor isn't going to be getting
            # emails.
            self.send_notification(msg, entity.email)
            self.__new_note_from_email(request_id, "Request Forwarded Email", msg, staff, entity.name, 'PRV')
        return


    def request_denied_event(
        self,
        request_id: str,
        reason: Reason,
        entity: GovernmentEntity = None,
        staff: Staff = None,
    ) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has denied request {request_id} for reason {reason.value}"
        else:
            message = (
                f"Request {request_id}: {request.public_title} has been closed for reason {reason.value}"
            )
        self.__new_log_message(request_id, message, event_codes["RDE"])
        if entity:
            # request deny with entity information
            msg = self.render(
                subject = "Your Request Has Request Denied (Wrong Government Entity)",
                filename="request_denied_wrong_entity.md",
                title=request.public_title,
                date=datetime.now(tz=timezone.utc),
                entity=entity,
                reason=reason,
                links=self._links(request),
                ipra_request=request
            )
            msg2 = self.render(
                subject = "Forwarded IPRA Request",
                filename="request_denied_forward_to_correct_entity.md",
                title=request.public_title,
                date=datetime.now(tz=timezone.utc),
                entity=entity,
                reason=reason,
                links=self._links(request),
                ipra_request=request
            )
        else:
            msg = self.render(
                subject = "Your Request Has Request Denied",
                filename="request_denied_requestor.md", 
                title=request.public_title,
                date=datetime.now(tz=timezone.utc),
                reason=reason,
                links=self._links(request),
                ipra_request=request

            )

        if self.__check_contact(request.requestor):
            secondary_emails = request.requestor.contact_info.secondary_emails or []
            recipients = [request.requestor.contact_info.email] + secondary_emails
            if entity:   
                self.send_notification(msg, recipients)
                self.send_notifications(msg2, [entity.email, recipients])
            else:
                self.send_notification(msg, recipients)
            self.__new_note_from_email(request_id, "Request Denied Email", msg, staff)
        else:
            # NOTE VT: Not sure what we are going to do if the requestor isn't going to be getting
            # emails.
            if entity:
                self.send_notification(msg2, entity.email)
            return

    def staff_message_event(self, request_id: str, comment: Comment, staff: Staff = None) -> None:
        request = self.__get_request(request_id)
        if staff:
            message = f"{staff.username} has made a comment on {request_id}."
        else:
            message = f"Request {request_id}: {request.public_title}, a comment has been added by staff."
        self.__new_log_message(request_id, message, event_codes["SME"])

        if self.__check_contact(request.requestor):
            msg = self.render(
                subject = "Comment Was Added to Your Request",
                filename = "comment_added_requestor.md", 
                title=request.public_title,
                date=datetime.now(tz=timezone.utc),
                links=self._links(request, "communicationsTab"),
                ipra_request=request,
                comment=comment,
            )
            self._send_notifications_to_all_emails(msg, request.requestor)
        else:
            # NOTE VT: Not sure what we are going to do if the requestor isn't going to be getting
            # emails.
            return

    def requestor_message_event(self, request_id: str, comment: Comment) -> None:
        request = self.__get_request(request_id)
        message = f"Requestor made a comment on {request.id_}."
        self.__new_log_message(request_id, message, event_codes["RME"])

        emails = self.notification_repo.get_assigned_staff_emails(request.id_)
        msg = self.render(
            subject = "Comment Was Added to a Request you are assigned",
            filename="comment_added_staff.md", 
            title=request.public_title, 
            date=datetime.now(tz=timezone.utc),
            links=self._links(request, "communicationsTab"),
            ipra_request=request,
            comment=comment,
        )
        self.send_notifications(msg, emails)

    def new_content(self, request_id: str, content: Content, content_type: ContentType, staff: Staff):
        request = self.__get_request(request_id)

        if staff:
            message = f"{staff.username} has added a new {content_type.value} to {request_id}."
        else:
            message = (
                f"Request {request_id}: {request.public_title} has had a new {content_type.value} added to it."
            )
        self.__new_log_message(request_id, message, event_codes["CAE"])

        self.release_type_updated(request, content, content_type, None, staff)

    def content_updated_event(self, content: Content, content_type: ContentType, old_release_type: ReleaseType, staff: Staff = None) -> None:
        request = self.__get_request(content.request_id)
        if staff:
            message = f"{staff.username} has updated {content.title} on {request.id_}."
        else:
            message = (
                f"Request {request.id_}: {request.public_title}, content {content.title} has been updated."
            )
        self.__new_log_message(request.id_, message, event_codes["CUE"])
        self.release_type_updated(request, content, content_type, old_release_type, staff)
            
    def workers_assigned(self, request_id: str, workers: List[UUID], staff: Staff = None) -> None:
        request = self.__get_request(request_id)

        for worker_id in workers:
            worker = self.user_service.get_staff_by_id(worker_id)
            if staff:
                message = (
                    f"{staff.username} has assigned request {request_id} to {worker.username}."
                )
            else:
                message = (
                    f"Request {request_id}: {request.public_title} has been assigned to {worker.username}."
                )
            self.__new_log_message(request_id, message, event_codes["DAE"])

            email = worker.email
            msg = self.render(
                subject = "A new request has been assigned to you.",
                filename="worker_assigned.md", 
                request_id=request.id_, 
                date=datetime.now(tz=timezone.utc),
                links=self._links(request),
                ipra_request=request,
            )
            self.send_notification(msg, email)

    def workers_unassigned(self, request_id: str, workers: List[UUID],staff: Staff = None) -> None:
        request = self.__get_request(request_id)

        for worker_id in workers:
            worker = self.user_service.get_staff_by_id(worker_id)
            if staff:
                message = (
                    f"{staff.username} has assigned request {request_id} to {worker.username}."
                )
            else:
                message = (
                    f"Request {request_id}: {request.public_title} has been assigned to {worker.username}."
                )
            self.__new_log_message(request_id, message, event_codes["DAE"])

            email = worker.email
            msg = self.render(
                subject = "You have been unassigned from a request",
                filename = "worker_unassigned.md", 
                request_id=request.id_, 
                date=datetime.now(tz=timezone.utc),
                links=self._links(request),
                ipra_request = request,
            )
            self.send_notification(msg, email)

    def legal_queue_event(self, resource_id: UUID, staff: Staff = None) -> None:
        res = self.content_service.get_resource_by_id(resource_id)
        request = self.__get_request(res.request_id)
        if staff:
            message = (
                f"{staff.username} has added a new resource to {res.request_id} for legal review."
            )
        else:
            message = f"Request {res.request_id}: has a new resource that needs legal review."
        self.__new_log_message(res.request_id, message, event_codes["LQE"])
        emails = self.notification_repo.get_emails_by_groups([groups["LEG"]])
        msg = self.render(
            subject = "New resource added for review.",
            filename = "legal_queue.md", 
            resource_id=resource_id, 
            date=datetime.now(tz=timezone.utc),
            links=self._links(request),
            ipra_request=request,
            resource=res,
        )
        self.send_notifications(msg, emails)

    def legal_approved_event(self, res: Resource, staff: Staff = None) -> None:
        request = self.__get_request(res.request_id)
        if staff:
            message = f"{staff.username} has approved resource {res.title} on {res.request_id}."
        else:
            message = f"Resource {res.title} has been approved by legal."
        self.__new_log_message(res.request_id, message, event_codes["LAE"])

        emails = self.notification_repo.get_assigned_staff_emails(res.request_id)
        msg = self.render(
            subject = "Resource has been approved",
            filename = "legal_approved.md", 
            resource_id=res.id_, 
            date=datetime.now(tz=timezone.utc),
            links=self._links(request),
            ipra_request=request,
            resource=res,
        )
        self.send_notifications(msg, emails)
        # TODO: A redudant email may be sent if the release type is also updated on approval
        self.release_type_updated(request, res, content_types['RES'], None, staff)

    def legal_denied_event(self, res: Resource, reason: str, staff: Staff = None) -> None:
        request = self.__get_request(res.request_id)
        if staff:
            message = f"{staff.username} has denied resource {res.title} with reason {reason}"
        else:
            message = f"Resource {res.title} has been denied by legal for reason {reason}"
        self.__new_log_message(res.request_id, message, event_codes["LDE"])

        emails = self.notification_repo.get_assigned_staff_emails(res.request_id)
        msg = self.render(
            subject="Resource has been denied for review",
            filename = "legal_denied.md", 
            resource_id=res.id_,
            date=datetime.now(tz=timezone.utc),
            reason=reason,
            links=self._links(request),
            ipra_request=request,
            resource=res,
        )
        self.send_notifications(msg, emails)

        self.__new_note(res.request_id, 
            title=f"{res.title} was REJECTED.",
            message=reason,
            release_type=release_types["PRV"],
            creator=staff,
        )

    def legal_revised_event(self, resource_id: UUID, staff: Staff) -> None:
        res = self.content_service.get_resource_by_id(resource_id)
        if staff:
            message = f"{staff.username} has revised resource {res.title} on {res.request_id} that is under review."
        else:
            message = f"Resource {res.title} on {res.request_id} has been revised under review."
        self.__new_log_message(res.request_id, message, event_codes["LRE"])

    def release_type_updated(
        self, request: Request, content: Content, content_type: ContentType, old_release_type: ReleaseType,  staff: Staff
    ) -> None:
        if old_release_type is None or old_release_type != content.release_type:
            if staff:
                message = f"{staff.username} has set the release type of {content_type.value} {content.title} to {content.release_type.value}"
            else:
                message = f"The release type of {content_type.value} {content.title} has been set to {content.release_type.value}"
            self.__new_log_message(request.id_, message, event_codes["REE"])
            needs_review = False
            if content_type is content_types["RES"]:
                res: Resource = content
                needs_review = res.needs_review
            if (
                self.__check_contact(request.requestor)
                and (
                    content.release_type == release_types["REQ"] or content.release_type == release_types["PUB"]
                )
                and not (
                    old_release_type == release_types["REQ"] or old_release_type == release_types["PUB"]
                )
                and not needs_review
            ):
                msg = self.render(
                    subject = "A New {content_type.value.title()} Was Added to Your Request",
                    filename = "new_content_added_requestor.md", 
                    title=request.public_title, 
                    date=datetime.now(tz=timezone.utc),
                    links=self._links(request, "responseTab", content_type),
                    ipra_request=request,
                    content = content,
                    content_type = content_type,
                )
                self._send_notifications_to_all_emails(msg, request.requestor)

    def review_status_updated(self, resource_id: UUID, staff: Staff) -> None:
        res = self.content_service.get_resource_by_id(resource_id)
        if staff:
            message = f"{staff.username} has updated the review status of resource {res.title}."
        else:
            message = f"Resource {res.title} has had the review status updated."
        self.__new_log_message(res.request_id, message, event_codes["LSE"])

    def delete_content_event(self, content: Content, content_type: ContentType, reason: str, staff: Staff = None) -> None:
        if staff:
            message = f"{staff.username} has deleted {content_type.value} {content.title} with reason {reason}"
        else:
            message = f"{content_type.value} {content.title} has been deleted for reason {reason}"
        self.__new_log_message(content.request_id, message, event_codes["CDE"])
        # Add Note??

        # emails = self.notification_repo.get_assigned_staff_emails(res.request_id)
        # title = "Resource has been denied for review"
        # body = ""
        # html = render_template("legal_denied.html", resource_id=resource_id, date=datetime.now(tz=timezone.utc))
        # self.send_notifications(title, body, html, emails)


    def reminder(self, form: str, request: Request, emails: List[str] = None, ipra: bool = False) -> None:
        if form == '1day':
            filename = "reminder_1_day.md"
        elif form == '5day':
            filename = "reminder_5_day.md"
        elif form == 'dueday':
            filename = "reminder_due_day.md"
        elif form == 'breach':
            filename = "reminder_due_date_breached.md"

        if ipra:
            emails = self.notification_repo.get_emails_by_groups([groups['IPA']])

        msg = self.render(
            subject = f"IPRA Request {request.id_} Due",
            filename = filename,
            title=request.public_title,
            date=datetime.now(tz=timezone.utc),
            links=self._links(request),
            ipra_request=request
        )
        self.send_notifications(msg, emails)

    def get_template(self, template: str, request: Request, params):
        if template == 'burdensome':
            file_prefix = 'burdensome_requestor'
        elif template == 'extension':
            file_prefix = "extension_requestor"
        else:
            return None

        split = "-----split-----"
        msg = self.render(
            **params,
            filename = file_prefix + ".md", 
            title = request.public_title,
            date = datetime.now(tz=timezone.utc),
            links = self._links(request),
            ipra_request = request,
            middle = split,
        )
        if msg is None:
            return {}
        result = msg.html.split(split, 1)
        if len(result) > 1:
            return {
                "header": result[0] +"</p>",
                "footer": "<p>" + result[1]
            }
        else:
            return {
                "template": msg.html
            }

    def request_updated(self, request_id: str, update_details, staff: Staff = None) -> None: 
        request = self.__get_request(request_id) 
        if isinstance(update_details, dict): 
            update_details_fields = [f"{k}: {v}" for k, v in update_details.items()] 
            update_details_str = ", ".join(update_details_fields)
        elif isinstance(update_details, str):
            update_details_str = update_details
        else:
            raise ValueError("Unhandled update_details type")

        if staff:
            message = f"{staff.username} has updated {request_id}.Changes made: {update_details_str}."
        else:
            message = f"Request {request_id}: {request.title} has been updated.Changes made: {update_details_str}."
        self.__new_log_message(request_id, message, event_codes["RUE"])
