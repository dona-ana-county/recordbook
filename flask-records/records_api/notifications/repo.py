from typing import List

from records_api import db
from records_api.constant import roles
from records_api.departments.model import Department
from records_api.serializer import db_schema as dbs
from records_api.serializer.db_schema import LogMessage as DbLogMessage
from records_api.serializer.db_schema import \
    RequestAssignedStaff as DbRequestStaff
from records_api.serializer.db_schema import Staff as DbStaff
from records_api.serializer.db_schema import StaffGroup as DbStaffGroup
from records_api.users.model import Group, Role

from .model import LogMessage


class NotificationRepo:
    def __init__(self):
        pass

    def new_log(self, log: LogMessage) -> None:
        log = DbLogMessage(
            id_=log.id_,
            message_date=log.message_date,
            message=log.message,
            event_code=log.event_code.code,
            request_id=log.request_id,
        )
        db.session.add(log)
        db.session.commit()

    def get_emails_by_request_assigned(self, request_id: str) -> List[str]:
        emails = []
        for staff in (
            db.session.query(dbs.Staff)
            .join(dbs.RequestAssignedStaff, dbs.RequestAssignedStaff.staff_id == dbs.Staff.id_)
            .filter(dbs.RequestAssignedStaff.request_id == request_id)
            .filter(dbs.Staff.delete_date == None)
            .all()
        ):
            emails.append(staff.email)
        return emails

    def get_department_manager_emails(self, dept: Department) -> List[str]:
        emails = []
        managers = (
            dbs.Staff.query
            .filter_by(department=dept.id_, role=roles["MAN"].code)
            .filter(dbs.Staff.delete_date == None)
            .all()
        )
        for manager in managers:
            emails.append(manager.email)
        return emails

    def get_assigned_staff_emails(self, request_id: str) -> List[str]:
        emails = []
        staff_assigned = (
            db.session.query(DbStaff)
            .join(DbRequestStaff, DbStaff.id_ == DbRequestStaff.staff_id)
            .filter(DbRequestStaff.request_id == request_id)
            .filter(DbStaff.delete_date == None)
            .all()
        )
        for staff in staff_assigned:
            emails.append(staff.email)
        return emails

    def get_emails_by_groups(self, user_groups: List[Group]) -> List[str]:
        emails = []
        groups_str = []
        for group in user_groups:
            groups_str.append(group.code)

        for staff in (
            db.session.query(dbs.Staff)
            .join(dbs.StaffGroup)
            .filter(DbStaffGroup.group_code.in_(groups_str))
            .filter(dbs.Staff.delete_date == None)
            .all()
        ):
            emails.append(staff.email)
        return emails

    def get_emails_by_roles(self, user_roles: List[Role]) -> List[str]:
        emails = []
        role_codes = []
        for role in user_roles:
            role_codes.append(role.code)

        for staff in (
            db.session.query(dbs.Staff)
            .filter(DbStaffGroup.group_code.in_(role_codes))
            .filter(dbs.Staff.delete_date == None)
            .all()
        ):
            emails.append(staff.email)
        return emails
