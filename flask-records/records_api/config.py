import os

from dotenv import find_dotenv, load_dotenv
from sqlalchemy.pool import NullPool

base_dir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(find_dotenv())


class Config:
    APP_NAME = os.getenv("APP_NAME", "RecordBook")
    ENTITY_NAME = os.getenv("ENTITY_NAME")
    ENTITY_EMAIL = os.getenv("ENTITY_EMAIL")
    IPRA_ADMIN_EMAIL = os.getenv("IPRA_ADMIN_EMAIL")
    REQUEST_ID_FORMAT = os.getenv("REQUEST_ID_FORMAT", "IPRA-{year}-{inc:05d}")
    DB_USER = os.getenv("DB_USER", "postgres")
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # NOTE VT: We should check if this folder exists and if it doesn't we should create it.
    UPLOAD_FOLDER = "/uploads"
    BASE_URL = os.getenv("BASE_URL")
    ELASTICSEARCH_URL = os.getenv("ELASTICSEARCH_URL")
    RECAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify"
    RECAPTCHA_SECRET_KEY = os.getenv("RECAPTCHA_SECRET_KEY")
    RECAPTCHA_SITE_KEY = os.getenv("RECAPTCHA_SITE_KEY")
    GTAG_TRACKING_ID = os.getenv("GTAG_TRACKING_ID", None)
    ALEMBIC = {
        'script_location': 'migrations'
    }

    # Using sink
    MAIL_SERVER = os.getenv("SMTP_SERVER", "smtp")
    MAIL_PORT = int(os.getenv("SMTP_PORT", "25"))
    MAIL_USE_TLS = False
    MAIL_USE_SSL = False
    MAIL_DEFAULT_SENDER = os.getenv("SMTP_SENDER", ENTITY_EMAIL)
    # Using gmail
    # MAIL_SERVER = "smtp.gmail.com"
    # MAIL_PORT = 465
    # MAIL_USERNAME = "emailtestsender04@gmail.com"
    # MAIL_PASSWORD = "Appl3s12!"
    # MAIL_USE_TLS = False
    # MAIL_USE_SSL = True
    # MAIL_SUPPRESS_SEND = True
    # MAIL_DEFAULT_SENDER = ("DARecords", "emailtestsender04@gmail.com")

    # Example
    # MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.googlemail.com')
    # MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    # MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
    # ['true', 'on', '1']
    # MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    # MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    # FLASKY_MAIL_SUBJECT_PREFIX = '[Flasky]'
    # FLASKY_MAIL_SENDER = 'Flasky Admin <flasky@example.com>'
    # FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')
    # SQLALCHEMY_TRACK_MODIFICATIONS = False

    LDAP_ENABLE = os.environ.get("LDAP_ENABLE", "FALSE") == "TRUE"
    LDAP_DEBUG = os.environ.get("LDAP_DEBUG", "INFO")
    if LDAP_ENABLE:
        LDAP_HOST = os.environ.get("LDAP_HOST")
        LDAP_BIND_DIRECT_SUFFIX = os.environ.get("LDAP_BIND_DIRECT_SUFFIX", "")
        LDAP_BIND_DIRECT_CREDENTIALS = True
        LDAP_SEARCH_FOR_GROUPS = False
        LDAP_BIND_DIRECT_GET_USER_INFO = False

    LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")

    @staticmethod
    def init_app(app):
        pass


class ProdConfig(Config):
    pass


class StagingConfig(Config):
    pass


class LocalStagingConfig(Config):
    pass


class DonaAnaStagingConfig(Config):
    pass


class TestingConfig(Config):
    TESTING = True
    UPLOAD_FOLDER = "tmp/uploads-test"
    SQLALCHEMY_ENGINE_OPTIONS = {'poolclass': NullPool}

class DevelopmentConfig(Config):
    DEBUG = True


config = {
    "development": DevelopmentConfig,
    "staging": StagingConfig,
    "localStaging": LocalStagingConfig,
    "daStaging": DonaAnaStagingConfig,
    "test": TestingConfig,
    "prod": ProdConfig,
    "default": DevelopmentConfig,
}
