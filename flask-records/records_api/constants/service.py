from typing import List

from records_api.service import Service

from .model import GovernmentEntity, Reason
from .repo import ConstantRepo


class ConstantService(Service):
    def __init__(self, app) -> None:
        self.repo: ConstantRepo = ConstantRepo()

    def get_all_government_entities(self) -> List[GovernmentEntity]:
        return self.repo.get_all_government_entities()

    def new_government_entity(self, entity: GovernmentEntity) -> GovernmentEntity:
        return self.repo.new_government_entity(entity)

    def update_government_entity(self, entity: GovernmentEntity) -> GovernmentEntity:
        return self.repo.update_government_entity(entity)

    def delete_government_entity(self, entity: GovernmentEntity) -> None:
        return self.repo.delete_government_entity(entity)

    def get_denial_reason(self, code: str) -> Reason:
        return self.repo.get_denial_reason(code)

    def get_all_denial_reasons(self) -> List[Reason]:
        return self.repo.get_all_denial_reasons()

    def add_denial_reason(self, reason: Reason) -> Reason:
        return self.repo.add_denial_reason(reason)

    def update_denial_reason(self, reason: Reason) -> Reason:
        return self.repo.update_denial_reason(reason)

    def delete_denial_reason(self, reason_code: str) -> None:
        return self.repo.delete_denial_reason(reason_code)

    def get_all_close_reasons(self) -> List[Reason]:
        return self.repo.get_all_close_reasons()

    def add_close_reason(self, reason: Reason) -> Reason:
        return self.repo.add_close_reason(reason)

    def update_close_reason(self, reason: Reason) -> Reason:
        return self.repo.update_close_reason(reason)

    def delete_close_reason(self, reason_code: str) -> None:
        return self.repo.delete_close_reason(reason_code)
