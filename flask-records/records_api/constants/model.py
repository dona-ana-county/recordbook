from dataclasses import dataclass
from uuid import UUID


@dataclass
class Lookup:
    code: str
    value: str
    def __eq__(self, other):
        return isinstance(other, Lookup) and self.code == other.code
    def __repr__(self):
        return f"{type(self).__name__}(Code: {self.code}, Value: {self.value})"

@dataclass
class GovernmentEntity:
    id_: UUID
    name: str
    email: str


@dataclass
class Reason(Lookup):
    pass
