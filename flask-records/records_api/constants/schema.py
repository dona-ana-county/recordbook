from marshmallow import Schema, fields, post_load

from .model import GovernmentEntity, Lookup


class LookupSchema(Schema):
    code = fields.Str()
    value = fields.Str()

    @post_load
    def make_lookup(self, data, **kwargs):
        return Lookup(**data)


class GovernmentEntitySchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    name = fields.Str()
    email = fields.Email()

    @post_load
    def make_government_entity(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        return GovernmentEntity(**data)


class ConfigSchema(Schema):
    APP_NAME = fields.Str(data_key="appName")
    ENTITY_NAME = fields.Str(data_key="entityName")
    ENTITY_EMAIL = fields.Str(data_key="entityEmail")
    RECAPTCHA_SITE_KEY = fields.Str(data_key="recaptchaSiteKey")
    GTAG_TRACKING_ID = fields.Str(data_key="gtagTrackingId", allow_none=True)
