from typing import List
from uuid import uuid4

from flask import current_app

from records_api import db
from records_api.serializer import db_schema as dbs
from records_api.serializer.db_schema import CloseReason as DbCloseReason
from records_api.serializer.db_schema import DenialReason as DbDenialReason

from .model import GovernmentEntity, Reason


class ConstantRepo:
    def __init__(self) -> None:
        pass

    def get_all_government_entities(self) -> List[GovernmentEntity]:
        government_entities = []
        db_gov_entities = dbs.GovernmentEntity.query.order_by(dbs.GovernmentEntity.name).all()
        for entity in db_gov_entities:
            government_entities.append(
                GovernmentEntity(id_=entity.id_, name=entity.name, email=entity.email)
            )
        return government_entities

    def new_government_entity(self, entity: GovernmentEntity) -> GovernmentEntity:
        db_entity = dbs.GovernmentEntity(id_=uuid4(), name=entity.name, email=entity.email)
        db.session.add(db_entity)
        db.session.commit()
        return GovernmentEntity(id_=db_entity.id_, name=db_entity.name, email=db_entity.email)

    def update_government_entity(self, entity: GovernmentEntity) -> GovernmentEntity:
        db_entity = (
            db.session.query(dbs.GovernmentEntity)
            .filter(dbs.GovernmentEntity.id_ == entity.id_)
            .first()
        )
        if db_entity:
            db_entity.name = entity.name
            db_entity.email = entity.email
            db.session.commit()
            return GovernmentEntity(id_=db_entity.id_, name=db_entity.name, email=db_entity.email)
        return None

    def delete_government_entity(self, entity: GovernmentEntity) -> GovernmentEntity:
        db_entity = (
            db.session.query(dbs.GovernmentEntity)
            .filter(dbs.GovernmentEntity.id_ == entity.id_)
            .first()
        )
        if db_entity:
            db.session.delete(db_entity)
            db.session.commit()
            return GovernmentEntity(id_=db_entity.id_, name=db_entity.name, email=db_entity.email)
        return None

    def get_denial_reason(self, code: str) -> Reason:
        db_reason = db.session.query(DbDenialReason).filter(DbDenialReason.code == code).first()
        return Reason(db_reason.code, db_reason.reason)

    def get_all_denial_reasons(self) -> List[Reason]:
        reasons = []
        for r in db.session.query(DbDenialReason).order_by(DbDenialReason.reason).all():
            reasons.append(Reason(r.code, r.reason))
        return reasons

    def add_denial_reason(self, reason: Reason) -> Reason:
        db_reason = DbDenialReason(reason.code, reason.reason)
        db.session.add(db_reason)
        db.session.commit()
        return self.get_denial_reason(reason.code)

    def update_denial_reason(self, reason: Reason) -> Reason:
        db_reason = self.get_denial_reason(reason.code)
        if db_reason:
            db_reason.code = reason.code
            db_reason.reason = reason.reason
            db.session.commit()
            return self.get_denial_reason(reason.code)
        else:
            current_app.logger.debug(f"There was no denial reason with code {reason.code}.")
            return None

    def delete_denial_reason(self, reason_code: str) -> None:
        reason = self.get_denial_reason(reason_code)
        if reason:
            db.session.delete(reason)
            db.session.commit()
            current_app.logger.debug(f"Denial reason with code {reason_code} was deleted.")

    def get_close_reason(self, code: str) -> Reason:
        db_reason = db.session.query(DbCloseReason).filter(DbCloseReason.code == code).first()
        return Reason(db_reason.code, db_reason.reason)

    def get_all_close_reasons(self) -> List[Reason]:
        reasons = []
        for r in db.session.query(DbCloseReason).order_by(DbCloseReason.reason).all():
            reasons.append(Reason(r.code, r.reason))
        return reasons

    def add_close_reason(self, reason: Reason) -> Reason:
        db_reason = DbCloseReason(reason.code, reason.reason)
        db.session.add(db_reason)
        db.session.commit()
        return self.get_close_reason(reason.code)

    def update_close_reason(self, reason: Reason) -> Reason:
        db_reason = self.get_close_reason(reason.code)
        if db_reason:
            db_reason.code = reason.code
            db_reason.reason = reason.reason
            db.session.commit()
            return self.get_close_reason(reason.code)
        else:
            current_app.logger.debug(f"There was no denial reason with code {reason.code}.")
            return None

    def delete_close_reason(self, reason_code: str) -> None:
        reason = self.get_close_reason(reason_code)
        if reason:
            db.session.delete(reason)
            db.session.commit()
            current_app.logger.debug(f"Close reason with code {reason_code} was deleted")
