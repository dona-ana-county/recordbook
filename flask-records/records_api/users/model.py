from dataclasses import dataclass, field
from typing import List
from uuid import UUID

from records_api.auth.model import AuthToken
from records_api.constants.model import Lookup


@dataclass
class ContactType(Lookup):
    def __repr__(self):
        return f"ContactType(Code: {self.code}, Value: {self.value})"


@dataclass
class Role(Lookup):
    def __repr__(self):
        return f"Role(Code: {self.code}, Value: {self.value})"


@dataclass
class Group(Lookup):
    def __repr__(self):
        return f"Group(Code: {self.code}, Value: {self.value})"


@dataclass
class ContactInfo:
    id_: UUID
    email: str = None
    secondary_emails: List[str]= None
    phone: str = None
    fax: str = None
    address_line_1: str = None
    address_line_2: str = None
    city: str = None
    state: str = None
    postal_code: str = None

    def to_dict(self):
        return {field: getattr(self, field) for field in self.__annotations__}


@dataclass
class User:
    id_: UUID
    first_name: str
    last_name: str

    def __repr__(self):
        return f"User(Id: {self.id_}, Name: {self.first_name} {self.last_name})"


class Staff(User):
    def __init__(
        self,
        id_: UUID,
        username: str,
        first_name: str,
        last_name: str,
        email: str,
        role: Role,
        disabled: bool,
        external_auth: bool,
        groups: List[Group] = field(),
    ) -> None:
        User.__init__(self, id_, first_name, last_name)
        self.username = username
        self.email = email
        self.token: AuthToken
        self.groups: List[Group] = groups
        self.role: Role = role
        self.disabled = disabled
        self.external_auth = external_auth

    def __repr__(self):
        return f"Staff(Username: {self.username}, Role: {self.role}, Email: {self.email})"


class Requestor(User):
    def __init__(
        self,
        id_: UUID,
        first_name: str,
        last_name: str,
        primary_contact: ContactType = None,
        contact_info: ContactInfo = None,
    ):
        User.__init__(self, id_, first_name, last_name)
        self.primary_contact = primary_contact
        self.contact_info = contact_info

    def __repr__(self):
        return f"Requestor(Name: {self.first_name} {self.last_name})"
