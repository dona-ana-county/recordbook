from datetime import datetime, timezone
from typing import Dict, List
from uuid import UUID, uuid4

from records_api import constant, db
from records_api.departments.model import Department
from records_api.serializer import db_schema as dbs
from records_api.serializer.db_schema import ContactInfo as DbContact

from records_api.serializer.db_schema import Requestor as DbRequestor
from records_api.serializer.db_schema import StaffGroup as DbStaffGroup
from records_api.serializer.db_schema import User as DbUser

from .model import (ContactInfo, ContactType, Group, Requestor, Role, Staff,
                    User)


class UserRepo:
    def __init__(self):
        self.contact_types: Dict[str, ContactType] = constant.contact_types
        self.roles: Dict[str, Role] = constant.roles
        self.groups: Dict[str, Group] = constant.groups

    def new_requestor(self, requestor: Requestor, request_id: str):
        user_id = self.new_user(requestor)

        db_requestor = DbRequestor(
            id_=user_id, request_id=request_id, primary_contact=requestor.primary_contact.code
        )
        db.session.add(db_requestor)
        db.session.commit()
        self.new_contact_info(requestor.contact_info, user_id)

    def new_staff(self, s: Staff, password: str, department: Department) -> Staff:
        db_user = dbs.User(id_=s.id_, first_name=s.first_name, last_name=s.last_name)
        db_staff = dbs.Staff(
            id_=s.id_,
            email=s.email,
            username=s.username,
            password=password,
            role=s.role.code,
            department=department.id_,
            disabled=s.disabled,
            external_auth=s.external_auth,
        )
        db.session.add(db_user)
        db.session.commit()
        db.session.add(db_staff)
        db.session.commit()
        self.add_staff_groups(s.id_, s.groups)
        return s

    def new_user(self, new_user: User):
        db_user = DbUser(id_=uuid4(), first_name=new_user.first_name, last_name=new_user.last_name)
        db.session.add(db_user)
        db.session.commit()
        return db_user.id_

    def new_contact_info(self, contact_info: ContactInfo, user_id: UUID):
        db_contact = DbContact(
            id_=uuid4(),
            requestor_id=user_id,
            email=contact_info.email,
            secondary_emails=contact_info.secondary_emails,
            phone=contact_info.phone,
            fax=contact_info.fax,
            address_line_1=contact_info.address_line_1,
            address_line_2=contact_info.address_line_2,
            city=contact_info.city,
            state=contact_info.state,
            postal_code=contact_info.postal_code,
        )
        db.session.add(db_contact)
        db.session.commit()

    def get_requestor(self, requestor_id: UUID) -> Requestor:
        db_r = (
            db.session.query(dbs.Requestor, dbs.User)
            .join(dbs.User, dbs.User.id_ == dbs.Requestor.id_)
            .filter(dbs.Requestor.id_ == requestor_id)
            .first()
        )
        requestor = Requestor(
            id_=db_r[0].id_,
            first_name=db_r[1].first_name,
            last_name=db_r[1].last_name,
            primary_contact=self.contact_types[db_r[0].primary_contact],
        )
        return requestor

    def get_contact_info(self, requestor_id: UUID) -> ContactInfo:
        db_info = (
            db.session.query(dbs.ContactInfo)
            .filter(dbs.ContactInfo.requestor_id == requestor_id)
            .first()
        )
        contact_info = ContactInfo(
            id_=db_info.id_,
            email=db_info.email,
            secondary_emails=db_info.secondary_emails,
            phone=db_info.phone,
            fax=db_info.fax,
            address_line_1=db_info.address_line_1,
            address_line_2=db_info.address_line_2,
            city=db_info.city,
            state=db_info.state,
            postal_code=db_info.postal_code,
        )
        return contact_info

    def get_users(self, ids: List[UUID]) -> List[User]:
        db_users = db.session.query(DbUser).filter(DbUser.id_.in_(ids)).order_by(DbUser.last_name, DbUser.first_name).all()

        users = []
        for user in db_users:
            users.append(User(user.id_, user.first_name, user.last_name))

        return users

    def get_groups_by_token(self, token: str) -> List[Group]:
        db_staff = db.session.query(dbs.Staff).join(dbs.AuthToken).filter_by(token=token).first()
        return self.get_groups(db_staff.id_)

    def get_staff_by_token(self, token: str) -> Staff:
        db_staff = (
            db.session.query(dbs.Staff, dbs.User)
            .join(dbs.User, dbs.User.id_ == dbs.Staff.id_)
            .join(dbs.AuthToken, dbs.AuthToken.staff_id == dbs.Staff.id_)
            .filter(dbs.AuthToken.token == token)
            .first()
        )
        if db_staff is None:
            return None

        staff = Staff(
            id_=db_staff[1].id_,
            username=db_staff[0].username,
            first_name=db_staff[1].first_name,
            last_name=db_staff[1].last_name,
            email=db_staff[0].email,
            role=self.roles[db_staff[0].role],
            disabled=db_staff[0].disabled,
            external_auth=db_staff[0].external_auth,
        )

        staff.groups = self.get_groups(staff.id_)
        return staff

    def add_staff_groups(self, staff_id: UUID, groups: List[Group]) -> None:
        for group in groups:
            staff_group = DbStaffGroup(staff_id=staff_id, group_code=group.code)
            db.session.add(staff_group)
        db.session.commit()

    def update_staff(self, s: Staff, department: Department) -> Staff:
        db_staff = (
            db.session.query(dbs.User, dbs.Staff)
            .join(dbs.Staff, dbs.Staff.id_ == dbs.User.id_)
            .filter(dbs.User.id_ == s.id_)
            .first()
        )
        if db_staff[0]:
            db_staff[0].first_name = s.first_name
            db_staff[0].last_name = s.last_name
        if db_staff[1]:
            db_staff[1].username = s.username
            db_staff[1].email = s.email
            db_staff[1].role = s.role.code
            db_staff[1].department = department.id_
            db_staff[1].external_auth = s.external_auth
        db.session.commit()
        self.update_groups(s.id_, s.groups)
        return s

    def update_groups(self, staff_id: UUID, groups: List[Group]):
        db_groups = (
            db.session.query(dbs.StaffGroup).filter(dbs.StaffGroup.staff_id == staff_id).all()
        )
        if db_groups:
            for db_group in db_groups:
                db.session.delete(db_group)
            db.session.commit()

        new_db_groups = []
        for group in groups:
            new_db_groups.append(dbs.StaffGroup(staff_id=staff_id, group_code=group.code))
        db.session.add_all(new_db_groups)
        db.session.commit()

    def delete_staff(self, s: Staff) -> Staff:
        self.unassign_staff_from_requests(s.id_)
        db_staff = db.session.query(dbs.Staff).filter(dbs.Staff.id_ == s.id_).first()
        if db_staff:
            db_staff.delete_date = datetime.now(tz=timezone.utc)
            db_staff.disabled = True
            db.session.commit()
        return s

    def update_disable_staff(self, s: Staff, b: bool) -> Staff:
        # NOTE VT: We could probably just use the update staff thing and set the disable field
        if b:
            self.unassign_staff_from_requests(s.id_)
        db_staff = db.session.query(dbs.Staff).filter(dbs.Staff.id_ == s.id_).first()
        if db_staff:
            db_staff.disabled = b
            db.session.commit()
            s.disabled = b
            return s
        else:
            return None

    def unassign_staff_from_requests(self, staff_id: UUID):
        # NOTE: We should probably move this to the queue service
        db_assignments = (
            db.session.query(dbs.RequestAssignedStaff)
            .filter(dbs.RequestAssignedStaff.staff_id == staff_id)
        )
        db_assignments.update({
            dbs.RequestAssignedStaff.unassigned_date: datetime.now(tz=timezone.utc)
        })
        db.session.commit()

    def get_by_username(self, username: str) -> Staff:
        db_staff = (
            db.session.query(dbs.Staff, dbs.User)
            .join(dbs.User, dbs.User.id_ == dbs.Staff.id_)
            .filter(dbs.Staff.username == username)
            .first()
        )
        if db_staff is None:
            return None

        staff = Staff(
            id_=db_staff[1].id_,
            username=db_staff[0].username,
            first_name=db_staff[1].first_name,
            last_name=db_staff[1].last_name,
            email=db_staff[0].email,
            role=self.roles[db_staff[0].role],
            disabled=db_staff[0].disabled,
            external_auth=db_staff[0].external_auth,
        )
        staff.groups = self.get_groups(staff.id_)
        return staff

    def get_password_by_staff_id(self, staff_id: UUID) -> str:
        db_staff = (
            db.session.query(dbs.Staff)
            .filter(dbs.Staff.id_ == staff_id)
            .first()
        )
        if db_staff is None:
            return None
        return db_staff.password

    def get_groups(self, staff_id: UUID) -> List[Group]:
        groups = []
        for group in (
            db.session.query(dbs.StaffGroup).join(dbs.Staff).filter(dbs.Staff.id_ == staff_id).all()
        ):
            groups.append(self.groups[group.group_code])
        return groups

    def get_manager_ids_by_departments(self, dept_ids: List[UUID]) -> List[str]:
        # TODO: This should be in the department repo
        # It's also kinda weird that we are getting multiple departments
        manager_ids = []
        managers = (
            dbs.Staff.query
            .filter( dbs.Staff.department.in_(dept_ids), dbs.Staff.role == self.roles["MAN"].code)
            .filter( dbs.Staff.delete_date == None)
            .all()
        )
        for manager in managers:
            manager_ids.append(manager.id_)

        return manager_ids

    def get_staff_by_ids(self, staff_ids: List[UUID]) -> List[Staff]:
        db_staff = (
            db.session.query(dbs.Staff, dbs.User, dbs.Department)
            .join(dbs.User, dbs.User.id_ == dbs.Staff.id_)
            .join(dbs.Department, dbs.Department.id_ == dbs.Staff.department)
            .filter(dbs.Staff.id_.in_(staff_ids))
            .order_by(dbs.User.last_name.asc(), dbs.User.first_name.asc())
            .all()
        )
        staff_list = []
        for db_s in db_staff:
            new_staff = Staff(
                id_=db_s[0].id_,
                username=db_s[0].username,
                first_name=db_s[1].first_name,
                last_name=db_s[1].last_name,
                email=db_s[0].email,
                role=self.roles[db_s[0].role],
                disabled=db_s[0].disabled,
                external_auth=db_s[0].external_auth,
            )
            new_staff.groups = self.get_groups(new_staff.id_)
            staff_list.append(new_staff)

        return staff_list

    def get_staff_by_departments(self, dept_ids: List[UUID], role: Role = None) -> List[UUID]:
        if role:
            db_staff = (
                db.session.query(dbs.Staff)
                .filter(dbs.Staff.department.in_(dept_ids))
                .filter(dbs.Staff.role == role.code)
                .filter(dbs.Staff.delete_date == None)
                .all()
            )
        else:
            db_staff = (
                db.session.query(dbs.Staff)
                .filter(dbs.Staff.department.in_(dept_ids))
                .filter(dbs.Staff.delete_date == None)
                .all()
            )
        return [s.id_ for s in db_staff]

    def get_staff(self) -> List[Staff]:
        staff = []
        db_staff = (
            db.session.query(dbs.Staff, dbs.User)
            .join(dbs.User, dbs.User.id_ == dbs.Staff.id_)
            .filter(dbs.Staff.delete_date == None)
            .order_by(dbs.User.last_name.asc(), dbs.User.first_name.asc())
            .all()
        )
        for s in db_staff:
            domain_staff = Staff(
                id_=s[0].id_,
                username=s[0].username,
                first_name=s[1].first_name,
                last_name=s[1].last_name,
                email=s[0].email,
                role=self.roles[s[0].role],
                disabled=s[0].disabled,
                external_auth=s[0].external_auth,
            )
            domain_staff.groups = self.get_groups(domain_staff.id_)
            staff.append(domain_staff)
        return staff
