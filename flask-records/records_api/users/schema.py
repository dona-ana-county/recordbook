from uuid import uuid4

from marshmallow import Schema, fields, post_load

from records_api.auth.schema import AuthTokenSchema
from records_api.constants.schema import LookupSchema
from records_api.departments.model import Department

from .model import ContactInfo, Requestor, Staff, User


class ContactInfoSchema(Schema):
    id_ = fields.UUID(data_key="id")
    email = fields.Email(allow_none=True)
    secondary_emails = fields.List(fields.Email(), allow_none=True)
    phone = fields.Str()
    fax = fields.Str(allow_none=True)
    address_line_1 = fields.Str(data_key="address1")
    address_line_2 = fields.Str(data_key="address2", allow_none=True)
    city = fields.Str()
    state = fields.Str()
    postal_code = fields.Str(data_key="postalCode")

    @post_load
    def make_contact(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        if "fax" not in data:
            data["fax"] = None
        return ContactInfo(**data)


class UserSchema(Schema):
    id_ = fields.UUID(data_key="id")
    first_name = fields.Str(data_key="firstName")
    last_name = fields.Str(data_key="lastName")

    @post_load
    def make_user(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        return User(**data)


# NOTE VT: Why don't we just use the regular department schema
class AssignedDepartmentSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    name = fields.Str()

    @post_load
    def make_department(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        return Department(**data)


class StaffSchema(Schema):
    id_ = fields.UUID(data_key="id")
    first_name = fields.Str(data_key="firstName")
    last_name = fields.Str(data_key="lastName")
    email = fields.Str()
    username = fields.Str()
    token = fields.Nested(AuthTokenSchema, allow_none=True)
    groups = fields.List(fields.Nested(LookupSchema), allow_none=True)
    role = fields.Nested(LookupSchema)
    disabled = fields.Boolean()
    external_auth = fields.Boolean(data_key="externalAuth")
    department = fields.Nested(AssignedDepartmentSchema, allow_none=True)

    @post_load
    def make_staff(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        if "disabled" not in data:
            data["disabled"] = None
        if "groups" not in data:
            data["groups"] = []
        return Staff(**data)


class RequestorSchema(Schema):
    id_ = fields.UUID(data_key="id")
    first_name = fields.Str(data_key="firstName")
    last_name = fields.Str(data_key="lastName")
    primary_contact = fields.Nested(LookupSchema, data_key="primaryContact", allow_none=True)
    contact_info = fields.Nested(ContactInfoSchema, data_key="contactInfo", allow_none=True)

    @post_load
    def make_requestor(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        return Requestor(**data)
