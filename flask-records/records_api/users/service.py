import os
from binascii import hexlify
from hashlib import scrypt, sha256
from hmac import compare_digest
from typing import List
from uuid import UUID, uuid4

from records_api.departments.model import Department
from records_api.service import Service
from records_api.users.model import ContactInfo, Requestor, Role, Staff, User, Group
from records_api.users.repo import UserRepo


class UserService(Service):
    def __init__(self, app):
        self.repo: UserRepo = UserRepo()

    def new_requestor(self, requestor: Requestor, request_id: str):
        self.repo.new_requestor(requestor, request_id)

    def new_staff(self, staff: Staff, department: Department, password: str = "pass") -> Staff:
        staff.id_ = uuid4()
        return self.repo.new_staff(staff, hash_password(password), department)

    def get_requestor(self, requestor_id: UUID) -> Requestor:
        return self.repo.get_requestor(requestor_id)

    def get_requestor_contact_info(self, requestor_id: UUID) -> ContactInfo:
        return self.repo.get_contact_info(requestor_id)

    def get_users(self, ids: List[UUID]) -> List[User]:
        return self.repo.get_users(ids)

    def get_staff_by_ids(self, ids: List[UUID]) -> List[Staff]:
        return self.repo.get_staff_by_ids(ids)

    def get_staff_by_id(self, staff_id: UUID) -> Staff:
        ids = [staff_id]
        staff = self.get_staff_by_ids(ids)
        if staff:
            return staff.pop(0)
        return None

    def update_staff(self, staff: Staff, department: Department) -> Staff:
        return self.repo.update_staff(staff, department)

    def delete_staff(self, staff: Staff) -> Staff:
        return self.repo.delete_staff(staff)

    def disable_staff(self, staff: Staff) -> Staff:
        return self.repo.update_disable_staff(staff, True)

    def enable_staff(self, staff: Staff) -> Staff:
        return self.repo.update_disable_staff(staff, False)

    def get_groups_by_token(self, token: str) -> List[Group]:
        return self.repo.get_groups_by_token(token)

    def get_staff_by_token(self, token: str) -> Staff:
        return self.repo.get_staff_by_token(token)

    def get_by_username(self, username: str) -> Staff:
        return self.repo.get_by_username(username)

    def get_manager_ids_by_departments(self, dept_ids: List[UUID]) -> List[str]:
        return self.repo.get_manager_ids_by_departments(dept_ids)

    def get_staff_by_departments(self, dept_ids: List[UUID], role: Role = None) -> List[UUID]:
        return self.repo.get_staff_by_departments(dept_ids, role)

    def get_staff(self) -> List[Staff]:
        return self.repo.get_staff()

    def verify_password(self, staff_id: UUID, password: str) -> bool:
        return verify_password(self.repo.get_password_by_staff_id(staff_id), password)

def hash_password(password: str):
    salt = sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = scrypt(password.encode('utf-8'), 
                     salt=salt, 
                     n=32768, 
                     r=8, 
                     p=1,
                     maxmem=64*2**20)
    pwdhash = hexlify(pwdhash)
    return (salt+pwdhash).decode('ascii')

def verify_password(stored_hash: str, password:str):
    salt = stored_hash[:64]
    stored_hash = stored_hash[64:]
    pwdhash = scrypt(password.encode('utf-8'), 
                     salt=salt.encode('ascii'), 
                     n=32768, 
                     r=8, 
                     p=1,
                     maxmem=64*2**20)
    pwdhash = hexlify(pwdhash).decode('ascii')
    return compare_digest(pwdhash, stored_hash)
