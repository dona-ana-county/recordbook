from marshmallow import Schema, fields, post_load

from records_api.users.schema import StaffSchema
from records_api.completion.schema import CompletionStatusSchema

from .model import Department


class DepartmentSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    name = fields.Str()
    staff = fields.List(fields.Nested(StaffSchema), allow_none=True)
    completion_status = fields.Nested(CompletionStatusSchema, data_key="completionStatus", allow_none=True)

    @post_load
    def make_department(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        return Department(**data)