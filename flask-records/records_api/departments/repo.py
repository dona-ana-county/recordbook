from typing import List
from uuid import UUID

from records_api import db
from records_api.serializer import db_schema as dbs
from records_api.service import Service
from records_api.users.service import UserService

from .model import Department


class DepartmentRepo:
    def __init__(self) -> None:
        self.user_service: UserService = Service.get_service(UserService)

    def new_department(self, department: Department) -> Department:
        db_department = dbs.Department(id_=department.id_, name=department.name)
        db.session.add(db_department)
        db.session.commit()
        return department

    def get_all_departments(self) -> List[Department]:
        departments = []
        db_departments = dbs.Department.query.order_by(dbs.Department.name).all()

        for dept in db_departments:
            department = Department(id_=dept.id_, name=dept.name)
            departments.append(department)

        return departments

    def get_departments(self, department_ids: List[UUID]) -> List[Department]:
        db_depts = (
            db.session.query(dbs.Department).filter(dbs.Department.id_.in_(department_ids)).order_by(dbs.Department.name).all()
        )
        departments = []
        for db_dept in db_depts:
            department = Department(id_=db_dept.id_, name=db_dept.name)
            departments.append(department)

        return departments

    def get_department_by_staff(self, staff_id: str) -> Department:
        db_dept = (
            db.session.query(dbs.Department)
            .join(dbs.Staff)
            .filter(dbs.Staff.id_ == staff_id)
            .first()
        )

        department = Department(id_=db_dept.id_, name=db_dept.name)
        return department

    def get_department_staff_ids(self, department_id: UUID) -> List[UUID]:
        staff_ids = (
            db.session.query(dbs.Staff.id_)
            .join(dbs.Department)
            .filter(dbs.Department.id_ == department_id)
            .filter(dbs.Staff.delete_date == None)
            .all()
        )
        return [s.id_ for s in staff_ids]

    def update_department(self, department: Department) -> Department:
        db_department = (
            db.session.query(dbs.Department).filter(dbs.Department.id_ == department.id_).first()
        )
        db_department.name = department.name
        db.session.commit()
        return department

    def delete_department(self, department: Department) -> Department:
        db_department = (
            db.session.query(dbs.Department).filter(dbs.Department.id_ == department.id_).first()
        )
        db.session.delete(db_department)
        db.session.commit()
        return department
