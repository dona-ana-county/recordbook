from typing import List
from uuid import UUID, uuid4

from records_api.service import Service
from records_api.users.model import Staff
from records_api.users.service import UserService

from .model import Department
from .repo import DepartmentRepo


class DepartmentService(Service):
    def __init__(self, app) -> None:
        self.repo: DepartmentRepo = DepartmentRepo()
        self.user_service: UserService = Service.get_service(UserService)

    def new_department(self, department: Department) -> Department:
        department.id_ = uuid4()
        return self.repo.new_department(department)

    def get_all_full_departments(self) -> List[Department]:
        departments = self.repo.get_all_departments()
        for dept in departments:
            staff_ids = self.repo.get_department_staff_ids(dept.id_)
            dept.staff = self.user_service.get_staff_by_ids(staff_ids)
        return departments

    def get_full_departments(self, ids: List[UUID]) -> List[Department]:
        departments = self.repo.get_departments(ids)
        for dept in departments:
            staff_ids = self.repo.get_department_staff_ids(dept.id_)
            dept.staff = self.user_service.get_staff_by_ids(staff_ids)
        return departments

    def get_all_departments(self) -> List[Department]:
        departments = self.repo.get_all_departments()
        return departments

    def get_departments(self, ids: List[UUID]) -> List[Department]:
        departments = self.repo.get_departments(ids)
        return departments

    def get_full_department_by_staff(self, staff_id: str) -> Department:
        dept = self.repo.get_department_by_staff(staff_id)
        staff_ids = self.repo.get_department_staff_ids(dept.id_)
        dept.staff = self.user_service.get_staff_by_ids(staff_ids)
        return dept

    def get_department_by_staff(self, staff_id: str) -> Department:
        dept = self.repo.get_department_by_staff(staff_id)
        return dept

    def get_department_staff(self, department_id: UUID) -> List[Staff]:
        staff_ids = self.repo.get_department_staff_ids(department_id)
        return self.user_service.get_staff_by_ids(staff_ids)

    def update_department(self, department: Department) -> Department:
        return self.repo.update_department(department)

    def delete_department(self, department: Department) -> Department:
        return self.repo.delete_department(department)
