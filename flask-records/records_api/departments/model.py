from dataclasses import dataclass
from typing import List
from uuid import UUID

from records_api.users.model import Staff
from records_api.completion.model import CompletionStatus


@dataclass
class Department:
    id_: UUID
    name: str
    staff: List[Staff] = None
    completion_status: CompletionStatus = None

    def __repr__(self):
        return f"Department(Name: {self.name})"
