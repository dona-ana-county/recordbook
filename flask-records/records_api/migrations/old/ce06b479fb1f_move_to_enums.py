"""Move to enums

Revision ID: ce06b479fb1f
Revises: 6e83663b17d5
Create Date: 2019-07-08 15:31:00.293462

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'ce06b479fb1f'
down_revision = 'afa79ac2c20d'
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    op.add_column('requestor', sa.Column('primary_contact', sa.String(length=3), nullable=True))
    requestor=sa.sql.table('requestor', sa.sql.column('id', type_=postgresql.UUID),
                                        sa.sql.column('primary_contact', type_= sa.String(length=3)))
    contact_info=sa.sql.table('contact_info', sa.sql.column('requestor_id', type_= postgresql.UUID),
                                              sa.sql.column('contact_type', type_= sa.String(length=3) ))
    op.execute(requestor.update().values(
        primary_contact=sa.sql.select( [contact_info.c.contact_type])
                         .where(contact_info.c.requestor_id == requestor.c.id )
    ))
    op.alter_column('requestor', 'primary_contact', nullable=False)
    op.drop_column('contact_info', 'contact_type')
    op.drop_table('contact_type')

    op.drop_constraint('content_release_type_fkey', 'content', type_='foreignkey')
    op.drop_table('release_type')

    op.drop_constraint('request_status_fkey', 'request', type_='foreignkey')
    op.drop_table('request_status')

    op.drop_constraint('staff_group_group_code_fkey', 'staff_group', type_='foreignkey')
    op.drop_table('group')
    op.alter_column('staff_group', 'group_code', type_=sa.String(3))

    op.drop_constraint('log_message_event_code_fkey', 'log_message', type_='foreignkey')
    op.drop_table('event')
    op.alter_column('log_message', 'event_code', type_=sa.String(3))

    op.drop_table('queue_item')

    #op.drop_constraint('queue_item_queue_code_fkey', 'queue_item', type_='foreignkey')
    op.drop_table('queue')

    op.drop_constraint('content_content_type_fkey', 'content', type_='foreignkey')
    op.drop_table('content_type')

    op.drop_constraint('staff_role_fkey', 'staff', type_='foreignkey')
    op.drop_table('role')


    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    t = op.create_table('role',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('role_name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='role_pkey')
    )
    op.bulk_insert(t, [
        {'code':'SUP', 'role_name':'Super User'},
        {'code':'WRK', 'role_name':'Worker'},
        {'code':'MAN', 'role_name':'Manager'},
    ])    
    op.create_foreign_key('staff_role_fkey', 'staff', 'role', ['role'], ['code'])

    t = op.create_table('content_type',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('content_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='content_type_pkey')
    )
    op.bulk_insert(t, [
        {'code':'NOT', 'content_type':'Note'},
        {'code':'RES', 'content_type':'Resource'},
        {'code':'LNK', 'content_type':'Link'},
        {'code':'INS', 'content_type':'Instruction'},
    ])
    op.create_foreign_key('content_content_type_fkey', 'content', 'content_type', ['content_type'], ['code'])    

    t = op.create_table('queue',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('queue_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='queue_pkey')
    )
    op.bulk_insert(t, [
        {'code':"ASQ", 'queue_type':"Assignment Queue"},
        {'code':"DPQ", 'queue_type':"Department Queue"},
        {'code':"SAQ", 'queue_type':"Staff Assigned Queue"},
        {'code':"LRQ", 'queue_type':"Legal Review Queue"},
    ])

    op.create_table('queue_item',
    sa.Column('id', postgresql.UUID(), autoincrement=False, nullable=False),
    sa.Column('enqueue_date', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('dequeue_date', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('item_id', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('item_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('queue_code', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['queue_code'], ['queue.code'], name='queue_item_queue_code_fkey'),
    sa.PrimaryKeyConstraint('id', name='queue_item_pkey')
    )

    t = op.create_table('event',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('event_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='event_pkey')
    )
    t = op.bulk_insert(t, [
        {'code':"CRE", 'event_type':"Created Event"},
        {'code':"AQE", 'event_type':"Assignment Queue Event"},
        {'code':"DQE", 'event_type':"Department Queue Event"},
        {'code':"RCE", 'event_type':"Request Closed Event"},
        {'code':"RDE", 'event_type':"Request Denied Event"},
        {'code':"SME", 'event_type':"Staff Message Event"},
        {'code':"RME", 'event_type':"Requestor Message Event"},
        {'code':"CAE", 'event_type':"Content Added Event"},
        {'code':"CUE", 'event_type':"Content Updated Event"},
        {'code':"WAE", 'event_type':"Worker Assigned Event"},
        {'code':"WUE", 'event_type':"Worker Unassigned Event"},
        {'code':"LQE", 'event_type':"Legal Queue Event"},
        {'code':"LAE", 'event_type':"Legal Approved Event"},
        {'code':"LRE", 'event_type':"Legal Revised Event"},
        {'code':"LDE", 'event_type':"Legal Denied Event"},
    ])    
    op.alter_column('log_message', 'event_code', type_=sa.String)
    op.create_foreign_key('log_message_event_code_fkey', 'log_message', 'event', ['event_code'], ['code'])
    


    t = op.create_table('group',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('group_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='group_pkey')
    )
    op.bulk_insert(t, [
        {'code':"IPA", 'group_type':"IPRA Admin"},
        {'code':"IPC", 'group_type':"IPRA Clerk"},
        {'code':"LEG", 'group_type':"Legal"},
    ])
    op.alter_column('staff_group', 'group_code', type_=sa.String)
    op.create_foreign_key('staff_group_group_code_fkey', 'staff_group', 'group', ['group_code'], ['code'])

    t = op.create_table('request_status',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('status', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='request_status_pkey')
    )
    op.bulk_insert(t, [
        {'code': 'OPN', 'status':'open'},
        {'code': 'CLS', 'status':'closed'},
        {'code': 'DNY', 'status':'denied'},
    ])
    op.create_foreign_key('request_status_fkey', 'request', 'request_status', ['status'], ['code'])

    t = op.create_table('release_type',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('release_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='release_type_pkey')
    )
    op.bulk_insert(t, [
        {'code':'PUB', 'release_type':'Release and Public'},
        {'code':'REQ', 'release_type':'Release and Private'},
        {'code':'PRV', 'release_type':'Private'},
    ])
    op.create_foreign_key('content_release_type_fkey', 'content', 'release_type', ['release_type'], ['code'])    

    t = op.create_table('contact_type',
    sa.Column('code', sa.VARCHAR(length=3), autoincrement=False, nullable=False),
    sa.Column('contact_type', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('code', name='contact_type_pkey')
    )
    op.bulk_insert(t, [
        {'code': 'EML', 'contact_type':'Email'},
        {'code': 'PHN', 'contact_type':'Phone'},
        {'code': 'FAX', 'contact_type':'Fax'},
        {'code': 'ADD', 'contact_type':'Address'},
    ])

    op.add_column('contact_info', sa.Column('contact_type', sa.VARCHAR(length=3), autoincrement=False, nullable=True))
    requestor=sa.sql.table('requestor', sa.sql.column('id', type_=postgresql.UUID),
                                        sa.sql.column('primary_contact', type_=sa.String(length=3)))
    contact_info=sa.sql.table('contact_info', sa.sql.column('requestor_id', type_=postgresql.UUID),
                                              sa.sql.column('contact_type', type_=sa.String(length=3) ))
    op.execute(contact_info.update().values(
        contact_type=sa.sql.select( [requestor.c.primary_contact])
                         .where(contact_info.c.requestor_id == requestor.c.id )
    ))
    op.alter_column('contact_info', 'contact_type', nullable=False)
    op.create_foreign_key('contact_info_contact_type_fkey', 'contact_info', 'contact_type', ['contact_type'], ['code'])
    op.drop_column('requestor', 'primary_contact')

    # ### end Alembic commands ###
