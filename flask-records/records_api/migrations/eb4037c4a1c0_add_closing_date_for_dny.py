"""Add closing_date for DNY

Revision ID: eb4037c4a1c0
Revises: 3f2e9d83b166
Create Date: 2021-07-23 13:50:34.236140

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eb4037c4a1c0'
down_revision = '3f2e9d83b166'
branch_labels = ()
depends_on = None


def upgrade():
    op. execute("""update request r
                    set closing_date = (
                                        select max(message_date)
                                        from log_message m
                                        where event_code in ('RCE', 'RDE') and m.request_id = r.id_
                                        )
                    where closing_date is null and status = 'DNY';
                """)


def downgrade():
    pass
    # ### end Alembic commands ###
