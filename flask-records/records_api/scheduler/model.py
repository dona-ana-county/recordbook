from dataclasses import dataclass
from datetime import date
from uuid import UUID


@dataclass
class Reminder:
    request_id: str
    staff_id: UUID
    staff_email: str
    staff_reminder: date
