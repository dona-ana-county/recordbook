from datetime import date
from typing import Dict, List

from records_api import db
from records_api.constant import request_statuses
from records_api.serializer.db_schema import Request as DbRequest
from records_api.serializer.db_schema import \
    RequestAssignedStaff as DbRequestAssignedStaff
from records_api.serializer.db_schema import Staff as DbStaff

from .model import Reminder


class ReminderRepo:
    def __init__(self):
        pass

    def get_reminders(self, due:date, reminder:date) -> Dict[str, List[Reminder]]:

        db_reminders = (
            db.session.query(DbRequestAssignedStaff, DbStaff)
            .join(DbRequest)
            .filter(DbRequest.id_ == DbRequestAssignedStaff.request_id)
            .filter(DbRequestAssignedStaff.staff_id == DbStaff.id_)
            .filter(DbRequest.due_dt <= due)
            .filter(DbRequest.status == request_statuses["OPN"].code)
            .filter(DbRequestAssignedStaff.reminder_date < reminder)
            .filter(DbRequestAssignedStaff.unassigned_date == None)
            .all()
        )
        reminder_dict = {}
        for reminder in db_reminders:
            reminder_dict.setdefault(reminder[0].request_id, []).append(
                Reminder(
                    request_id=reminder[0].request_id,
                    staff_id=reminder[0].staff_id,
                    staff_email=reminder[1].email,
                    staff_reminder=reminder[0].reminder_date,
                )
            )
        return reminder_dict

    def get_ipra_reminders(self, due:date, grace: date) -> List[str]:
        db_requests = (
            db.session.query(DbRequest.id_)
            .filter(DbRequest.due_dt <= due)
            .filter(DbRequest.reminder_date < grace)
            .filter(DbRequest.status == request_statuses["OPN"].code)
            .all()
        )
        return [r.id_ for r in db_requests]
    def get_ipra_assigned_reminders(self, due:date, reminder: date, assigned: bool) -> List[str]:
        db_requests = (
            db.session.query(DbRequest.id_)
            .filter(DbRequest.due_dt <= due)
            .filter(DbRequest.reminder_date < reminder)
            .filter(DbRequest.status == request_statuses["OPN"].code)
            .filter(
                db.session.query(DbRequestAssignedStaff.staff_id)
                .filter(DbRequestAssignedStaff.request_id == DbRequest.id_)
                .exists() == assigned 
            )
            .all()
        )
        return [r.id_ for r in db_requests]


    def set_reminders_sent(self, request_id, reminders: List[Reminder], reminder_date: date):
        db_assigned = (
            db.session.query(DbRequestAssignedStaff)
            .filter(DbRequestAssignedStaff.request_id == request_id)
            .filter(DbRequestAssignedStaff.staff_id.in_([reminder.staff_id for reminder in reminders]))
            .filter(DbRequestAssignedStaff.unassigned_date == None)
            .all()
        )
        if db_assigned:
            for assigned in db_assigned:
                assigned.reminder_date = reminder_date
        db.session.commit()

    def update_reminder_date(self, request_id: str, reminder_date: date) -> bool:
        db_request = (
            db.session.query(DbRequest)
            .filter(DbRequest.id_ == request_id)
            .first()
        )
        if db_request:
            db_request.reminder_date = reminder_date
            db.session.commit()
