from datetime import date

from apscheduler.schedulers.blocking import BlockingScheduler
from flask import current_app

from records_api.auth.service import AuthService
from records_api.notifications.service import NotificationService
from records_api.requests.service import RequestService
from records_api.service import Service

from . import scheduler
from .repo import ReminderRepo


@scheduler.cli.command('cron')
def cron():
    reminder = ReminderScheduler()
    reminder.init_app(current_app._get_current_object()) #pylint: disable=protected-access
    reminder.start()


class ReminderScheduler:
    def __init__(self):
        self.app = None
        self.reminder_repo = ReminderRepo()
        self.request_service: RequestService = Service.get_service(RequestService)
        self.notification_service: NotificationService = Service.get_service(NotificationService)
        self.auth_service: AuthService = Service.get_service(AuthService)
        self.schedule = None

    def init_app(self, app):
        self.app = app
        self.app.apscheduler = self

    def start(self):
        self.schedule = BlockingScheduler()
        self.schedule.add_job(func=self.send_all_reminders, trigger="cron", hour=6)  
        self.schedule.add_job(func=self.clean_sessions, trigger="cron", hour=2)      
        self.schedule.start()


    def clean_sessions(self):
        self.auth_service.clean_tokens()

    def send_all_reminders(self):
        with self.app.app_context():
            self.send_reminders( 1, 3, 'breach')
            self.send_reminders( 0, 0, 'duedate')
            self.send_reminders(-1, 0, '1day')
            self.send_reminders(-5, 0, '5day')
            
            self.send_ipra_reminders( 1, 3, 'breach',  assigned=True)
            self.send_ipra_reminders( 0, 0, 'duedate', assigned=True)

            self.send_ipra_reminders( 1, 3, 'breach',  assigned=False)
            self.send_ipra_reminders( 0, 0, 'duedate', assigned=False)
            self.send_ipra_reminders(-1, 0, '1day',    assigned=False)
            
            

    def send_reminders(self, delta:int, grace:int, form:str):
        today = date.today()
        due_date = self.request_service.calculator.business_days(today, -1 * delta)
        reminder_date = self.request_service.calculator.business_days(today, delta)
        grace_date = self.request_service.calculator.business_days(today, grace+1)

        current_app.logger.debug("Sending reminders: " + form)
        reminder_dict = self.reminder_repo.get_reminders(due_date, reminder_date)

        for request_id, reminders in reminder_dict.items():
            request = self.request_service.get_info(request_id)
            self.notification_service.reminder(form, request, emails=[r.staff_email for r in reminders])
            self.reminder_repo.set_reminders_sent(request_id, reminders, grace_date)
    
    def send_ipra_reminders(self, delta: int, grace: int, form: str, assigned: bool):
        today = date.today()
        due_date = self.request_service.calculator.business_days(today, -1 * delta)
        reminder_date = self.request_service.calculator.business_days(today, delta)
        grace_date = self.request_service.calculator.business_days(today, grace+1)

        requests = self.reminder_repo.get_ipra_assigned_reminders(due_date, reminder_date, assigned=assigned)
        for request_id in requests:
            request = self.request_service.get_info(request_id)
            self.notification_service.reminder(form, request, ipra=True)
            self.reminder_repo.update_reminder_date(request.id_, grace_date)
