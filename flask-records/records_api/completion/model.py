from dataclasses import dataclass

@dataclass
class CompletionStatus:
    request_id: str
    completion_status: bool
    comment: str

    def __repr__(self):
        return f"CompletionStatus(RequestId: {self.request_id}, CompletionStatus: {self.completion_status}, Comment: {self.comment})"