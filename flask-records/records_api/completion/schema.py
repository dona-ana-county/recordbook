from marshmallow import Schema, fields

class CompletionStatusSchema(Schema):
    request_id = fields.Str(data_key="requestId")
    completion_status = fields.Bool(data_key="completed", allow_none=True)
    comment = fields.Str(allow_none=True)
