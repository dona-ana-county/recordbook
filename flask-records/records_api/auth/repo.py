from datetime import datetime, timedelta, timezone
from typing import List
from uuid import UUID

from records_api import db
from records_api.serializer import db_schema as dbs

from .model import AuthToken


class AuthRepo:
    def __init__(self) -> None:
        pass

    def get_tokens_by_staff(self, staff_id: UUID) -> List[AuthToken]:
        db_tokens = (
            db.session
            .query(dbs.AuthToken)
            .filter(dbs.AuthToken.staff_id == staff_id)
            .filter(dbs.AuthToken.expiration_dt > datetime.now(tz=timezone.utc))
            .filter(dbs.AuthToken.revoked_dt == None)
            .all()
        )
        if db_tokens:
            return [AuthToken(
                staff_id=db_token.staff_id,
                token=db_token.token,
                issued_dt=db_token.issued_dt,
                expiration_dt=db_token.expiration_dt,
                revoked_dt=db_token.revoked_dt,
            ) for db_token in db_tokens]
        else:
            return []

    def add_token(self, auth_token: AuthToken) -> AuthToken:
        token = dbs.AuthToken(
            staff_id=auth_token.staff_id,
            token=auth_token.token,
            issued_dt=auth_token.issued_dt,
            expiration_dt=auth_token.expiration_dt,
            revoked_dt=auth_token.revoked_dt,
        )
        db.session.add(token)
        db.session.commit()
        return auth_token


    def get_token(self, token: str) -> AuthToken:
        db_token = db.session.query(dbs.AuthToken).filter(dbs.AuthToken.token == token).first()
        if db_token:
            return AuthToken(
                staff_id=db_token.staff_id,
                token=db_token.token,
                issued_dt=db_token.issued_dt,
                expiration_dt=db_token.expiration_dt,
                revoked_dt=db_token.revoked_dt,
            )
        else:
            return None

    def revoke_token(self, auth_token: AuthToken) -> bool:
        if auth_token:
            token = dbs.AuthToken.query.filter(
                dbs.AuthToken.token == auth_token.token
            ).first()
            token.revoked_dt = datetime.now(tz=timezone.utc)
            db.session.commit()
            return True
        return False
    def revoke_tokens(self, staff_id: UUID) -> bool:
        dbs.AuthToken.query.filter(
            dbs.AuthToken.staff_id == str(staff_id)
        ).update({dbs.AuthToken.revoked_dt: datetime.now(tz=timezone.utc)})
        db.session.commit()
        return True
    def clean_tokens(self, days: int):
        dbs.AuthToken.query.filter(
            dbs.AuthToken.expiration_dt < datetime.now(tz=timezone.utc) - timedelta(days=days)
        ).delete()
        db.session.commit()
    def update_token_expiration(self, auth_token: AuthToken, expiration: datetime):
        token = dbs.AuthToken.query.filter(dbs.AuthToken.token == auth_token.token).first()
        token.expiration_dt = expiration
        db.session.add(token)
        db.session.commit()
