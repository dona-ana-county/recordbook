import os
from datetime import datetime, timedelta, timezone
from hashlib import sha256
from typing import List
from uuid import UUID

import requests
from flask import current_app, jsonify
from flask_ldap3_login import AuthenticationResponseStatus

from records_api.service import Service
from records_api.users.model import Staff
from records_api.users.service import UserService

from .model import AuthToken
from .repo import AuthRepo


class AuthService(Service):
    def __init__(self, app):
        self.auth_repo = AuthRepo()
        self.user_service: UserService = Service.get_service(UserService)
    def login(self, credentials) -> Staff:
        user = self.user_service.get_by_username(credentials["username"])
        if user is not None:
            if user.disabled:
                current_app.logger.info("User disabled: " + credentials["username"])
                return None
            if user.external_auth:
                if current_app.config["LDAP_ENABLE"]:
                    response = current_app.ldap3_login_manager.authenticate(
                        credentials["username"], credentials["password"]
                    )
                    if not response.status == AuthenticationResponseStatus.success:
                        current_app.logger.info("ldap failed: " + credentials["username"])
                        return None
                else:
                    current_app.logger.error("No external auth enabled")
                    return None
            else:
                if not (
                    user.username == credentials["username"]
                    and self.user_service.verify_password(user.id_, credentials["password"])
                ):
                    current_app.logger.info("pass failed: " + credentials["username"])
                    return None

            user.token = self.generate_token(user.id_)
            self.auth_repo.add_token(
                user.token
            )  # NOTE VT: I guess so we don't have to pass the id too
        else:
            current_app.logger.info("User not found: " + credentials["username"])
        return user

    def logout(self, token: AuthToken):
        if self.auth_repo.revoke_token(token):
            return jsonify({"success": 1, "msg": "Signed out successfully."})
        else:
            return jsonify({"msg": "Something went wrong signing out"})

    def get_token(self, token: str) -> str:
        return self.auth_repo.get_token(token)

    def get_tokens_by_staff(self, staff_id: UUID) -> List[AuthToken]:
        return self.auth_repo.get_tokens_by_staff(staff_id)

    def revoke_token(self, auth_token: AuthToken):
        return self.auth_repo.revoke_token(auth_token)
    def revoke_tokens(self, staff_id: UUID):
        return self.auth_repo.revoke_tokens(staff_id)        

    def validate_reCaptcha(self, token: str) -> bool:

        # defining the api-endpoint
        googleReCaptchaUrl = current_app.config["RECAPTCHA_URL"]

        # your API key here
        googleSecretKey = current_app.config["RECAPTCHA_SECRET_KEY"]

        # data to be sent to api
        data = {"secret": googleSecretKey, "response": token}

        # sending post request and saving response as response object
        response = requests.post(url=googleReCaptchaUrl, data=data)
        # extracting response text
        return response.json()["success"]

    def generate_token(self, staff_id: UUID) -> AuthToken:
        token = AuthToken(
            staff_id=staff_id,
            token=sha256(os.urandom(60)).hexdigest(),
            issued_dt=datetime.now(tz=timezone.utc),
            expiration_dt=datetime.now(tz=timezone.utc) + timedelta(hours=10),
            revoked_dt=None,
        )
        return token
    def refresh_token(self, auth_token: AuthToken):
        now = datetime.now(tz=timezone.utc)
        if (
            auth_token.expiration_dt > now
            and auth_token.expiration_dt < now + timedelta(hours=9)
        ):
            self.auth_repo.update_token_expiration(auth_token,now+ timedelta(hours=10))

    def clean_tokens(self):
        self.auth_repo.clean_tokens( 1)
