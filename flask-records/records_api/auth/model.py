from dataclasses import dataclass
from datetime import datetime
from uuid import UUID


@dataclass
class AuthToken:
    token: str
    staff_id: UUID
    issued_dt: datetime
    revoked_dt: datetime
    expiration_dt: datetime
