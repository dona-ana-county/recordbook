import hmac
import time
from base64 import b64encode
from datetime import datetime, timezone
from functools import wraps
from typing import Dict, List

from flask import Response, jsonify, request

from records_api import code_limiter, constant, pass_limiter
from records_api.auth.model import AuthToken
from records_api.auth.service import AuthService
from records_api.content.model import ReleaseType
from records_api.content.service import ContentService
from records_api.departments.service import DepartmentService
from records_api.requests.service import RequestService
from records_api.service import Service
from records_api.users.model import Group, Role, Staff
from records_api.users.service import UserService

ipra_admin = "IPRA ADMIN"
ipra_clerk = "IPRA CLERK"
legal = "LEGAL"
manager = "MANAGER"
worker = "WORKER"
super_user = "SUPER USER"
primary_department_manager = "PRIMARY DEPARTMENT MANAGER"
secondary_department_manager = "SECONDARY DEPARTMENT MANAGER"
assigned = "ASSIGNED"
creator = "CREATOR"

auth_service: AuthService = Service.get_service(AuthService)
user_service: UserService = Service.get_service(UserService)
content_service: ContentService = Service.get_service(ContentService)
request_service: RequestService = Service.get_service(RequestService)
department_service: DepartmentService = Service.get_service(DepartmentService)

groups: Dict[str, Group] = constant.groups
roles: Dict[str, Role] = constant.roles
release_types: Dict[str, ReleaseType] = constant.release_types


def is_ipra_admin(staff: Staff, cache, **kwargs) -> bool:
    for group in staff.groups:
        if group == groups["IPA"]:
            return True
    return False


def is_ipra_clerk(staff: Staff, cache, **kwargs) -> bool:
    for group in staff.groups:
        if group == groups["IPC"]:
            return True
    return False


def is_legal(staff: Staff, cache, **kwargs) -> bool:
    for group in staff.groups:
        if group == groups["LEG"]:
            return True
    return False


def is_manager(staff: Staff, cache, **kwargs) -> bool:
    if staff.role == roles["MAN"]:
        return True
    return False


def is_worker(staff: Staff, cache, **kwargs) -> bool:
    if staff.role == roles["WRK"]:
        return True
    return False


def is_super_user(staff: Staff, cache, **kwargs) -> bool:
    if staff.role == roles["SUP"]:
        return True
    return False


def is_primary_department_manager(staff: Staff, cache, **kwargs) -> bool:
    # TODO VT: We can just use the get_full_departments maybe
    request_id = kwargs.get('request_id')
    content_id = kwargs.get('content_id')
    if not request_id and content_id:
        content = cache.get('content')
        if not content:
            content = cache['content'] = content_service.get_content_by_id(content_id)
        request_id = content.request_id
    if request_id:
        primary_dept_staff_ids = [
            s.id_
            for s in department_service.get_department_staff(
                request_service.get_primary_department(request_id).id_
            )
        ]
        if staff.id_ in primary_dept_staff_ids and staff.role == roles["MAN"]:
            return True

    return False


def is_secondary_department_manager(staff: Staff, cache, **kwargs) -> bool:
    request_id = kwargs.get('request_id')
    if request_id:
        secondary_department_staff_ids = []
        for dept in request_service.get_secondary_departments(request_id):
            dept_staff = department_service.get_department_staff(dept.id_)
            for s in dept_staff:
                secondary_department_staff_ids.append(s.id_)

        if staff.id_ in secondary_department_staff_ids and staff.role == roles["MAN"]:
            return True

    return False


def is_assigned(staff: Staff, cache, **kwargs) -> bool:
    request_id = kwargs.get('request_id')
    if request_id:
        assigned_staff_ids = [s.id_ for s in request_service.get_assigned_staff(request_id)]
        if staff.id_ in assigned_staff_ids:
            return True

    return False

def is_creator(staff: Staff, cache, **kwargs) -> bool:
    content_id = kwargs.get('content_id')
    if content_id:
        content = cache.get('content')
        if not content:
            content = content['content'] = content_service.get_content_by_id(content_id)
        if content:
            return staff.id_ == content.creator.id_
    return False

switcher = {
    ipra_admin: is_ipra_admin,
    ipra_clerk: is_ipra_clerk,
    legal: is_legal,
    manager: is_manager,
    worker: is_worker,
    super_user: is_super_user,
    primary_department_manager: is_primary_department_manager,
    secondary_department_manager: is_secondary_department_manager,
    assigned: is_assigned,
    creator: is_creator,
}


def get_token() -> AuthToken:
    if "Authorization" in request.headers:
        return auth_service.get_token(request.headers["Authorization"])
    else:
        return None


def validate(auth_params: List[str] = None, include_staff:bool=False, include_token:bool=False):
    def decorator_function(func):
        @wraps(func)
        def wrapper_function(*args, **kwargs):
            # Validate token
            auth_token = get_token()
            token_validation_error = do_validate_token(auth_token)
            if token_validation_error:
                return token_validation_error
            # Get staff and request objects
            staff = user_service.get_staff_by_id(auth_token.staff_id)

            # If staff disabled then throw error
            if staff.disabled:
                data = {"type": "Unauthorized", "error": "User Disabled"}
                return jsonify(data)

            # For each group or role call the validating function
            if auth_params:
                cache = {}
                for auth_param in auth_params:
                    # If one passes then continue normal execution
                    if switcher[auth_param](staff, cache, **kwargs):
                        break

                else:  # did not break, no auth_param returned true
                    # If none passed then the user doesn't have the appropriate permissions
                    data = {"type": "Unauthorized", "error": "Invalid Permissions"}
                    return jsonify(data)

            auth_service.refresh_token(auth_token)

            # If no groups then continue the normal function execution
            if include_staff:
                kwargs['staff'] = staff
            if include_token:
                kwargs['token'] = auth_token
            return func(*args, **kwargs)

        return wrapper_function

    return decorator_function

def super_user_basic_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if not do_validate_super_user_basic(request.authorization):
            return Response(
                "Please login", 401, {"WWW-Authenticate": 'Basic realm="Login Required"'}
            )
        return func(*args, **kwargs)

    return pass_limiter(wrap)


def validate_code(passcode_param: str = 'passcode',
                  request_id_param: str = 'request_id',
                  resource_id_param: str = 'resource_id'):
    def wrapper(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            if request_id_param in kwargs:
                if do_validate_code(kwargs[request_id_param], kwargs[passcode_param]):
                    return func(*args, **kwargs)
                data = {"type": "Unauthorized", "error": "Invalid Passcode"}
            elif resource_id_param in kwargs:
                resource = content_service.get_resource_by_release_type(kwargs[resource_id_param], 
                                                                        release_types['REQ'])
                if (resource and 
                       do_validate_code(resource.request_id, kwargs[passcode_param])):
                    return func(*args, **kwargs)
                data = {"type": "Unauthorized", "error": "Invalid Passcode"}                                                                     
            else:
                data = {"type": "Unauthorized", "error": "Invalid Parameters"}
            
            return jsonify(data)                
        return code_limiter(wrap)

    return wrapper


def validate_mac(resource_id_param: str = "resource_id", 
                 username_param: str = "username", 
                 timestamp_param: str = "timestamp", 
                 mac_param: str = "mac"):
    def wrapper(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            kwargs[mac_param] = kwargs[mac_param].replace("_", "/")
            if not do_validate_mac(
                kwargs[resource_id_param],
                kwargs[username_param],
                kwargs[timestamp_param],
                kwargs[mac_param],
            ):
                data = {"type": "Unauthorized", "error": "Invalid Mac"}
                return jsonify(data)
            return func(*args, **kwargs)

        return code_limiter(wrap)

    return wrapper


# -------------------------------------------------------------------


def do_validate_token(auth_token: AuthToken) -> str:
    if not auth_token:
        data = {"type": "Unauthorized", "error": "Authorization Error"}
        return jsonify(data)
    if auth_token.expiration_dt < datetime.now(tz=timezone.utc):
        auth_service.revoke_token(auth_token)
        data = {"type": "Unauthorized", "error": "Token Expired"}
        return jsonify(data)
    if auth_token.revoked_dt:
        data = {"type": "Unauthorized", "error": "Must Be Signed In"}
        return jsonify(data)
    return None


def do_validate_super_user_basic(auth) -> bool:
    if not auth:
        return False
    staff = user_service.get_by_username(auth.username)
    if not staff or staff.disabled or staff.external_auth or staff.role != roles["SUP"]:
        return False
    return user_service.verify_password(staff.id_, auth.password)


def do_validate_release_type(release_type: ReleaseType, request_id: str, passcode: int) -> str:
    if release_type == release_types["PUB"]:
        return None

    if release_type == release_types["REQ"]:
        if (
            passcode is not None
            and request_id is not None
            and do_validate_code(request_id, passcode)
        ):
            return None
        else:
            return "Error Validating Passcode"

    if release_type == release_types["PRV"]:
        auth_token = get_token()
        token_validation_error = do_validate_token(auth_token)
        if token_validation_error:
            return token_validation_error


def do_validate_code(request_id: str, passcode: int) -> bool:
    return request_service.get_info(request_id).passcode == passcode


def do_validate_mac(resource_id: str, username: str, timestamp: int, mac: str) -> bool:
    if time.time() > timestamp + 2 * 60 * 60:
        return False
    staff = user_service.get_by_username(username)
    tokens = auth_service.get_tokens_by_staff(staff.id_)
    for token in tokens:
        if (staff 
            and not  staff.disabled 
            and do_validate_token(token) is None
        ):
            digester = hmac.new(token.token.encode("ascii"), digestmod="SHA256")
            digester.update(resource_id.encode("ascii"))
            digester.update(username.encode("ascii"))
            digester.update(str(timestamp).encode("ascii"))
            mac_ = b64encode(digester.digest())
            if hmac.compare_digest(mac_, mac.encode("ascii")):
                return True
    return False
