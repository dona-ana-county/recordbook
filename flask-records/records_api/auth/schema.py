from marshmallow import Schema, fields


class AuthTokenSchema(Schema):
    token = fields.Str()
    staff_id = fields.UUID()
    issued_dt = fields.DateTime(data_key="issuedDt", allow_none=True)
    expiration_dt = fields.DateTime(data_key="expirationDt", allow_none=True)
    revoked_dt = fields.DateTime(data_key="revokedDt", allow_none=True)
