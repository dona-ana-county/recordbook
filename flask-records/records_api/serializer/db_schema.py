from records_api import db
from sqlalchemy.dialects.postgresql import UUID


class StaffGroup(db.Model):
    __tablename__ = "staff_group"

    staff_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("staff.id_"), primary_key=True
    )
    group_code = db.Column(db.String(3), primary_key=True)


class User(db.Model):
    __tablename__ = "user"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    first_name = db.Column(db.String(), nullable=False)
    last_name = db.Column(db.String(), nullable=False)


class Staff(db.Model):
    __tablename__ = "staff"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("user.id_"), primary_key=True)
    email = db.Column(db.String(), nullable=False)
    username = db.Column(db.String(), unique=True, nullable=False)
    password = db.Column(db.String(), nullable=True)
    role = db.Column(db.String(3), nullable=False)
    department = db.Column(
        UUID(as_uuid=True), db.ForeignKey("department.id_"), nullable=False
    )
    disabled = db.Column(db.Boolean, nullable=False)
    delete_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    external_auth = db.Column(db.Boolean, nullable=False)


class AuthToken(db.Model):
    __tablename__ = "auth_token"

    staff_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("staff.id_"), primary_key=True
    )
    token = db.Column(db.String(), primary_key=True)
    issued_dt = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    expiration_dt = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    revoked_dt = db.Column(db.TIMESTAMP(timezone=True), nullable=True)


class Requestor(db.Model):
    __tablename__ = "requestor"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("user.id_"), primary_key=True)
    request_id = db.Column(db.String(), db.ForeignKey("request.id_"), nullable=False)
    primary_contact = db.Column(db.String(3), nullable=False)


class ContactInfo(db.Model):
    __tablename__ = "contact_info"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    requestor_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("requestor.id_"), nullable=False
    )
    email = db.Column(db.String(), nullable=True)
    secondary_emails = db.Column(db.ARRAY(db.String), nullable=True) 
    phone = db.Column(db.String(), nullable=False)
    fax = db.Column(db.String(), nullable=True)
    address_line_1 = db.Column(db.String(), nullable=False)
    address_line_2 = db.Column(db.String(), nullable=True)
    city = db.Column(db.String(), nullable=False)
    state = db.Column(db.String(), nullable=False)
    postal_code = db.Column(db.String(), nullable=False)


class Request(db.Model):
    __tablename__ = "request"

    id_ = db.Column(db.String(), primary_key=True)
    title = db.Column(db.String(), nullable=False)
    public_title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    public_description = db.Column(db.String(), nullable=False)
    create_dt = db.Column(db.Date(), nullable=False)
    due_dt = db.Column(db.Date(), nullable=False)
    status = db.Column(db.String(3), nullable=False)
    status_note = db.Column(db.String(), nullable=False)
    passcode = db.Column(db.String(), nullable=False)
    extension_status = db.Column(db.String(), nullable=False)
    reminder_date = db.Column(db.Date, nullable=False)
    closing_date = db.Column(db.Date, nullable=True)


class RequestStart(db.Model):
    __tablename__ = "request_start"

    year = db.Column(db.String(), primary_key=True)
    start = db.Column(db.BigInteger(), nullable=False)


RequestId = db.Sequence("request_id", metadata=db.metadata)


class LogMessage(db.Model):
    __tablename__ = "log_message"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    message_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    message = db.Column(db.String(), nullable=False)
    event_code = db.Column(db.String(3), nullable=False)
    request_id = db.Column(db.String(), db.ForeignKey("request.id_"), nullable=False)


class Department(db.Model):
    __tablename__ = "department"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(), nullable=False)


class RequestAssignedDepartment(db.Model):
    __tablename__ = "request_assigned_department"

    request_id = db.Column(
        db.String(), db.ForeignKey("request.id_"), primary_key=True, nullable=False
    )
    department_id = db.Column(
        UUID(as_uuid=True),
        db.ForeignKey("department.id_"),
        primary_key=True,
        nullable=False,
    )
    assigned_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    unassigned_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    level = db.Column(db.String(), nullable=False)
    completed = db.Column(db.Boolean(), nullable=True, default=False)
    comment = db.Column(db.Text(), nullable=True)


class RequestAssignedStaff(db.Model):
    __tablename__ = "request_assigned_staff"

    request_id = db.Column(
        db.String(), db.ForeignKey("request.id_"), primary_key=True, nullable=False
    )
    staff_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("staff.id_"), primary_key=True, nullable=False
    )
    assigned_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    unassigned_date = db.Column(db.TIMESTAMP(timezone=True), nullable=True)
    reminder_date = db.Column(db.Date(), nullable=False)


class Content(db.Model):
    __tablename__ = "content"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    title = db.Column(db.String(), nullable=False)
    create_dt = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    content_type = db.Column(db.String(3), nullable=False)
    release_type = db.Column(db.String(3), nullable=False)
    creator = db.Column(UUID(as_uuid=True), db.ForeignKey("staff.id_"), nullable=False)
    editable = db.Column(db.Boolean, nullable=False)


class Resource(db.Model):
    __tablename__ = "resource"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("content.id_"), primary_key=True)
    extension = db.Column(db.String(), nullable=False)
    file_path = db.Column(db.String(), nullable=False)
    needs_review = db.Column(db.Boolean, nullable=False)
    under_review = db.Column(db.Boolean, nullable=False)
    description = db.Column(db.String(), nullable=True)
    revised = db.Column(db.Boolean, nullable=False)


class Note(db.Model):
    __tablename__ = "note"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("content.id_"), primary_key=True)
    message = db.Column(db.String(), nullable=False)


class Link(db.Model):
    __tablename__ = "link"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("content.id_"), primary_key=True)
    description = db.Column(db.String(), nullable=False)
    url = db.Column(db.String(), nullable=False)


class Instruction(db.Model):
    __tablename__ = "instruction"

    id_ = db.Column(UUID(as_uuid=True), db.ForeignKey("content.id_"), primary_key=True)
    message = db.Column(db.String(), nullable=False)


class RequestContent(db.Model):
    __tablename__ = "request_content"

    request_id = db.Column(db.String(), db.ForeignKey("request.id_"), primary_key=True)
    content_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("content.id_"), primary_key=True
    )


class Communication(db.Model):
    __tablename__ = "communication"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    create_dt = db.Column(db.TIMESTAMP(timezone=True), nullable=False)


class Alert(db.Model):
    __tablename__ = "alert"

    id_ = db.Column(
        UUID(as_uuid=True), db.ForeignKey("communication.id_"), primary_key=True
    )
    request_id = db.Column(db.String(), db.ForeignKey("request.id_"), nullable=False)
    sender_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("staff.id_"), nullable=False
    )
    recipient_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("staff.id_"), nullable=False
    )
    message = db.Column(db.String(), nullable=False)


class Comment(db.Model):
    __tablename__ = "comment"

    id_ = db.Column(
        UUID(as_uuid=True), db.ForeignKey("communication.id_"), primary_key=True
    )
    commenter_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("user.id_"), nullable=False
    )
    request_id = db.Column(db.String(), db.ForeignKey("request.id_"), nullable=False)
    message = db.Column(db.String(), nullable=False)


class ResourceMessage(db.Model):
    __tablename__ = "resource_message"

    resource_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("resource.id_"), primary_key=True
    )
    message = db.Column(db.String(), nullable=False)


class DenialReason(db.Model):
    __tablename__ = "denial_reason"

    code = db.Column(db.String(), primary_key=True)
    reason = db.Column(db.String(), nullable=False)


class CloseReason(db.Model):
    __tablename__ = "close_reason"

    code = db.Column(db.String(), primary_key=True)
    reason = db.Column(db.String(), nullable=False)


class GovernmentEntity(db.Model):
    __tablename__ = "government_entity"

    id_ = db.Column(UUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
