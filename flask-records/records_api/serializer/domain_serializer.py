from typing import Dict

from flask import current_app
from marshmallow import Schema, ValidationError
from marshmallow.utils import from_iso_date

class DomainSerializer:
    @staticmethod
    def to_json(schema: Schema, model) -> str:
        try:
            json = schema.dumps(model)
        except ValidationError as err:
            current_app.logger.debug(err)
        return current_app.response_class(
            response=json,
            status=200,
            mimetype='application/json'
        )

    @staticmethod
    def to_model(schema: Schema, data: Dict):
        return schema.load(data)

__all__ = ['DomainSerializer', 'from_iso_date']