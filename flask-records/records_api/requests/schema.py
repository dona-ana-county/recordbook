from marshmallow import Schema, fields, post_load

from records_api.communications.schema import AlertSchema, CommentSchema
from records_api.constants.schema import LookupSchema
from records_api.departments.schema import DepartmentSchema
from records_api.notifications.schema import LogMessageSchema
from records_api.users.schema import RequestorSchema, StaffSchema

from .model import Request

class RequestSchema(Schema):
    id_ = fields.Str(data_key="id", allow_none=True)
    title = fields.Str()
    public_title = fields.Str(data_key="publicTitle", allow_none=True)
    description = fields.Str()
    public_description = fields.Str(data_key="publicDescription", allow_none=True)
    create_dt = fields.Date(data_key="createDate", allow_none=True)
    due_dt = fields.Date(data_key="dueDate", allow_none=True)
    status = fields.Nested(LookupSchema, allow_none=True)
    status_note = fields.Str(data_key="statusNote", allow_none=True)
    passcode = fields.Str(allow_none=True)
    requestor = fields.Nested(RequestorSchema)
    primary_department = fields.Nested(
        DepartmentSchema, data_key="primaryDepartment", allow_none=True
    )
    secondary_departments = fields.List(
        fields.Nested(DepartmentSchema), data_key="secondaryDepartments", allow_none=True
    )
    assigned_staff = fields.List(
        fields.Nested(StaffSchema), data_key="assignedStaff", allow_none=True
    )
    comments = fields.List(fields.Nested(CommentSchema, allow_none=True))
    alerts = fields.List(fields.Nested(AlertSchema, allow_none=True))
    log = fields.List(fields.Nested(LogMessageSchema), allow_none=True)
    extension_status = fields.Nested(LookupSchema, data_key="extensionStatus", allow_none=True)

    @post_load
    def make_request(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        if "public_title" not in data:
            data["public_title"] = data["title"]
        if "public_description" not in data:
            data["public_description"] = data["description"]
        if "status" not in data:
            data["status"] = None
        if "status_note" not in data:
            data["status_note"] = ""
        if "create_dt" not in data:
            data["create_dt"] = None
        if "due_dt" not in data:
            data["due_dt"] = None
        if "passcode" not in data:
            data["passcode"] = None
        if "extension_status" not in data:
            data["extension_status"] = None
        return Request(**data)


class ReqErrorSchema(Schema):
    error = fields.Bool()
    message = fields.Str()

class RequestOrReqErrorSchema(Schema):
    req_error = fields.Nested(ReqErrorSchema, data_key="reqError", allow_none=True)
    request = fields.Nested(RequestSchema, allow_none=True)

class PublicRequestSchema(Schema):
    id_ = fields.Str(data_key="id", allow_none=True)
    # title = fields.Str()
    public_title = fields.Str(data_key="publicTitle")
    public_description = fields.Str(data_key="publicDescription")
    create_dt = fields.Date(data_key="createDate")
    due_dt = fields.Date(data_key="dueDate")
    status = fields.Nested(LookupSchema)
    status_note = fields.Str(data_key="statusNote")
    requestor = fields.Nested(RequestorSchema, only=["first_name", "last_name"])


class RequestorRequestSchema(Schema):
    id_ = fields.Str(data_key="id", allow_none=True)
    title = fields.Str()
    description = fields.Str(data_key="description")
    create_dt = fields.Date(data_key="createDate")
    due_dt = fields.Date(data_key="dueDate")
    status = fields.Nested(LookupSchema)
    status_note = fields.Str(data_key="statusNote")
    passcode = fields.Str()
    requestor = fields.Nested(RequestorSchema)
