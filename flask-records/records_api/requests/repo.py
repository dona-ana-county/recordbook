from datetime import date, datetime, timezone
from typing import List
from uuid import UUID

from flask import current_app
from sqlalchemy.exc import IntegrityError

from records_api import db
from records_api.constant import (event_codes, extension_statuses,
                                  request_statuses)
from records_api.completion.model import CompletionStatus
from records_api.notifications.model import LogMessage
from records_api.serializer.db_schema import LogMessage as DbLogMessage
from records_api.serializer.db_schema import Request as DbRequest
from records_api.serializer.db_schema import \
    RequestAssignedDepartment as DbRequestDepartment
from records_api.serializer.db_schema import \
    RequestAssignedStaff as DbRequestStaff
from records_api.serializer.db_schema import RequestId as DbRequestId
from records_api.serializer.db_schema import Requestor as DbRequestor
from records_api.users.model import ContactInfo, Requestor
from records_api.serializer.db_schema import RequestStart as DbRequestStart
from records_api.serializer import db_schema as dbs

from .model import Request


class RequestRepo:
    def __init__(self):
        pass


    def generate_id(self, year) -> str:
        request_id_format = current_app.config['REQUEST_ID_FORMAT']

        for _ in range(2):
            year_id = (
                db.session
                .query(DbRequestStart.start )
                .filter(DbRequestStart.year == year)
                .first()
            )
            if not year_id or not year_id[0]:
                try:
                    # create new record
                    start = DbRequestStart(year=year, start=DbRequestId.next_value())
                    db.session.add(start)
                    db.session.commit()
                    
                except IntegrityError:
                    continue
            else:
                break
        else:
            raise RuntimeError("Cannot get year id")
        new_id = db.session.execute(DbRequestId)
        new_id = new_id - year_id[0]
        return request_id_format.format(year=year, inc=new_id)

    def new_request(self, request: Request) -> Request:

        year = str(request.create_dt.year)

        #db.session.execute('LOCK TABLE request IN SHARE MODE;') # releaed by session.commit
        request.id_ = self.generate_id(year)

        db_request = DbRequest(
            id_=request.id_,
            title=request.title,
            public_title=request.public_title,
            description=request.description,
            public_description=request.public_description,
            create_dt=request.create_dt,
            due_dt=request.due_dt,
            status=request.status.code,
            status_note=request.status_note,
            passcode=request.passcode,
            extension_status=request.extension_status.code,
            reminder_date=request.reminder_date,
            closing_date=request.closing_date
        )
        db.session.add(db_request)
        db.session.commit()
        return self.db_to_domain(db_request)

    def get_request(self, request_id: str) -> Request:
        db_req = db.session.query(DbRequest).filter(DbRequest.id_ == request_id).first()
        request = Request(
            id_=db_req.id_,
            title=db_req.title,
            public_title=db_req.public_title,
            description=db_req.description,
            public_description=db_req.public_description,
            create_dt=db_req.create_dt,
            due_dt=db_req.due_dt,
            passcode=db_req.passcode,
            status=request_statuses[db_req.status],
            status_note=db_req.status_note,
            extension_status=extension_statuses[db_req.extension_status],
            reminder_date=db_req.reminder_date,
            closing_date=db_req.closing_date
        )
        return request

    def get_requestor_id(self, request_id: str) -> UUID:
        db_req = db.session.query(DbRequestor).filter(DbRequestor.request_id == request_id).first()
        return db_req.id_

    def get_staff_ids(self, request_id: str) -> List[UUID]:
        staff_ids = (
            db.session.query(DbRequestStaff)
            .filter(DbRequestStaff.request_id == request_id)
            .filter(DbRequestStaff.unassigned_date.is_(None))
            .all()
        )
        return [s.staff_id for s in staff_ids]

    def get_log_messages(self, request_id: str) -> List[LogMessage]:
        db_log_messages = (
            db.session.query(DbLogMessage)
            .filter(DbLogMessage.request_id == request_id)
            .order_by(DbLogMessage.message_date)
            .all()
        )
        log_messages = []
        for msg in db_log_messages:
            log_messages.append(
                LogMessage(
                    id_=msg.id_,
                    message_date=msg.message_date,
                    message=msg.message,
                    event_code=event_codes[msg.event_code],
                    request_id=request_id,
                )
            )
        return log_messages

    def get_assigned_department_ids(self, request_id: str, level: str) -> List[UUID]:
        db_department_ids = (
            db.session.query(DbRequestDepartment.department_id)
            .filter(
                DbRequestDepartment.request_id == request_id,
                DbRequestDepartment.unassigned_date == None,
                DbRequestDepartment.level == level,
            )
            .all()
        )
        return [d.department_id for d in db_department_ids]

    def get_completion_status(self, dept_id: UUID, request_id: str) -> CompletionStatus:
        db_secondary_department_completion_status = (
            db.session.query(DbRequestDepartment)
            .filter(
                DbRequestDepartment.department_id == dept_id,
                DbRequestDepartment.request_id == request_id,
            )
            .first()
        )
        completion_status = CompletionStatus(
            request_id=request_id, 
            completion_status=db_secondary_department_completion_status.completed, 
            comment=db_secondary_department_completion_status.comment
        )
            
        return completion_status
    
    def update_request(self, request_id: str, request: Request) -> None:
        req_to_update = DbRequest.query.filter_by(id_=request.id_).first()
        req_to_update.title = request.title
        req_to_update.public_title = request.public_title
        req_to_update.description = request.description
        req_to_update.public_description = request.public_description
        req_to_update.create_dt = request.create_dt
        req_to_update.due_dt = request.due_dt
        req_to_update.status = request.status.code
        req_to_update.status_note = request.status_note
        req_to_update.passcode = request.passcode
        req_to_update.extension_status = request.extension_status.code
        req_to_update.closing_date = request.closing_date
        db.session.commit()

    def assign_department(self, request_id: str, dept_id: UUID, level: str) -> None:
        return self.assign_departments(request_id, [dept_id], level)

    def unassign_department(self, request_id: str, dept_id: UUID) -> None:
        db_to_unassign = (
            db.session.query(DbRequestDepartment)
            .filter(
                DbRequestDepartment.request_id == request_id,
                DbRequestDepartment.department_id == dept_id,
                DbRequestDepartment.unassigned_date == None,
            )
            .first()
        )
        if db_to_unassign:
            db_to_unassign.unassigned_date = datetime.now(tz=timezone.utc)
            db.session.commit()

    def assign_departments(self, request_id: str, dept_ids: List[UUID], level: str) -> None:
        db_request_depts = (
            db.session.query(DbRequestDepartment)
            .filter(
                DbRequestDepartment.request_id == request_id,
                DbRequestDepartment.department_id.in_(dept_ids)
            )
            .all()
        )
        dept_ids = set(dept_ids)      
        for db_request_dept in db_request_depts:
            db_request_dept.assigned_date = datetime.now(tz=timezone.utc)
            db_request_dept.unassigned_date = None
            db_request_dept.level = level
            dept_ids.remove(db_request_dept.department_id)

        for dept_id in dept_ids:
            db.session.add(
                DbRequestDepartment(
                    request_id=request_id,
                    department_id=dept_id,
                    assigned_date=datetime.now(tz=timezone.utc),
                    unassigned_date=None,
                    level=level,
                )
            )
        db.session.commit()

    def update_department_assignment(self, request_id: str, dept_id: UUID, level: str) -> None:
        db_dept_update = (
            db.session.query(DbRequestDepartment)
            .filter(
                DbRequestDepartment.request_id == request_id,
                DbRequestDepartment.department_id == dept_id,
                DbRequestDepartment.unassigned_date == None,
            )
            .first()
        )

        #@TODO: Ask for confirmation to delete existing status when moving from Primary to Secondary.
        if db_dept_update:
            db_dept_update.level = level
            if level == 'Secondary':
                db_dept_update.completed = False
                db_dept_update.comment = ''
            db.session.commit()
    
    def assign_department_completion_status(self, request_id: str, dept_id: UUID, status: bool, comment: str) -> None:
        db_dept_completion_status = (
            db.session.query(DbRequestDepartment)
            .filter(
                DbRequestDepartment.request_id == request_id,
                DbRequestDepartment.department_id == dept_id,
                DbRequestDepartment.unassigned_date == None,
                DbRequestDepartment.level == 'Secondary'
            )
            .first()
        )
        if db_dept_completion_status:
            db_dept_completion_status.completed = status
            db_dept_completion_status.comment = comment
            db.session.commit()

    def assign_request_staff(self, request: Request, staff_ids: List[UUID], reminder_date:date) -> None:
        db_request_staffs = (
            db.session.query(DbRequestStaff)
            .filter(
                DbRequestStaff.request_id == request.id_,
                DbRequestStaff.staff_id.in_(staff_ids)
            )
            .all()
        )
        staff_ids = set(staff_ids)
        for db_request_staff in db_request_staffs:
            db_request_staff.assigned_date=datetime.now(tz=timezone.utc)
            db_request_staff.unassigned_date = None
            db_request_staff.reminder_date=reminder_date
            staff_ids.remove(db_request_staff.staff_id)

        for staff in staff_ids:
            db.session.add(
                DbRequestStaff(
                    request_id=request.id_, 
                    staff_id=staff, 
                    assigned_date=datetime.now(tz=timezone.utc), 
                    unassigned_date=None, 
                    reminder_date=reminder_date
                )
            )

        db.session.commit()

    def unassign_request_staff(self, request_id: str, staff_ids: List[UUID]) -> None:
        for staff in db.session.query(DbRequestStaff).filter(
            DbRequestStaff.request_id == request_id,
            DbRequestStaff.staff_id.in_(staff_ids)
        ):
            staff.unassigned_date = datetime.now(tz=timezone.utc)
            db.session.commit()

    def update_requestor_contact_info_by_request_id(
        self, request_id: str, new_contact_info: ContactInfo
    ) -> bool:
        requestor_id = self.get_requestor_id(request_id)
        db_info = (
            db.session.query(dbs.ContactInfo)
            .filter(dbs.ContactInfo.requestor_id == requestor_id)
            .first()
        )

        if db_info:
            contact_info_dict = new_contact_info.to_dict()
            for key, value in contact_info_dict.items():
                setattr(db_info, key, value)

            db.session.commit()
            return True
        return False

    def update_primary_contact_code_by_request_id(
        self, request_id: str, primary_contact_code: str
    ) -> bool:
        requestor_id = self.get_requestor_id(request_id)
        requestor: Requestor = (
            db.session.query(dbs.Requestor)
            .filter(dbs.Requestor.id_ == requestor_id)
            .first()
        )
        if requestor:
            requestor.primary_contact = primary_contact_code
            db.session.commit()
            return True
        return False

    def get_requestor_contact_info_by_request_id(self, request_id: str):
        requestor_id = self.get_requestor_id(request_id)
        contact_info_query = (
            db.session.query(dbs.ContactInfo, dbs.Requestor.primary_contact)
            .join(dbs.Requestor, dbs.Requestor.id_ == dbs.ContactInfo.requestor_id)
            .filter(dbs.ContactInfo.requestor_id == requestor_id)
            .first()
        )

        if contact_info_query:
            contact_info, primary_contact = contact_info_query
            return {
                "email": contact_info.email,
                "secondary_emails": contact_info.secondary_emails,
                "phone": contact_info.phone,
                "fax": contact_info.fax,
                "address_line_1": contact_info.address_line_1,
                "address_line_2": contact_info.address_line_2,
                "city": contact_info.city,
                "state": contact_info.state,
                "postal_code": contact_info.postal_code,
                "primary_contact": primary_contact,
            }
        return None

    def db_to_domain(self, db_request: DbRequest) -> Request:
        return Request(
            id_=db_request.id_,
            title=db_request.title,
            public_title=db_request.public_title,
            description=db_request.description,
            public_description=db_request.public_description,
            create_dt=db_request.create_dt,
            due_dt=db_request.due_dt,
            passcode=db_request.passcode,
            status=request_statuses[db_request.status],
            status_note=db_request.status_note,
            extension_status=extension_statuses[db_request.extension_status],
            closing_date=db_request.closing_date
        )
