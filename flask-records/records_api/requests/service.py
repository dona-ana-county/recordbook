import random
import string
from datetime import date, timedelta
from typing import Dict, FrozenSet, List
from uuid import UUID

import business_calendar

from records_api.communications.model import Alert, Comment
from records_api.communications.service import CommunicationService
from records_api.completion.model import CompletionStatus
from records_api.constant import (
    content_types,
    extension_statuses,
    groups,
    request_statuses,
    roles,
)
from records_api.constants.model import GovernmentEntity, Reason
from records_api.content.model import Content, ContentType, ReleaseType
from records_api.content.service import ContentService
from records_api.departments.model import Department
from records_api.departments.service import DepartmentService
from records_api.notifications.model import LogMessage
from records_api.searching.service import SearchService
from records_api.service import Service
from records_api.users.model import ContactInfo, Requestor, Staff
from records_api.users.service import UserService
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.users.schema import ContactInfoSchema

from .model import Request, RequestStatus
from .repo import RequestRepo


class RequestService(Service):
    def __init__(self, app):
        self.repo = RequestRepo()
        self.user_service: UserService = Service.get_service(UserService)
        self.department_service: DepartmentService = Service.get_service(DepartmentService)
        self.content_service: ContentService = Service.get_service(ContentService)
        self.communication_service: CommunicationService = Service.get_service(CommunicationService)
        self.search_service: SearchService = Service.get_service(SearchService)
        self.calculator = DateCalculator()
        
    def is_ipra_admin(self, staff:Staff):
        for group in staff.groups:
            if group == groups["IPA"]:
                return True
        return False
    def new_request(self, new_request: Request) -> Request:
        if not new_request.create_dt:
            new_request.create_dt = date.today()
        
        new_request.reminder_date = date.today()

        if not new_request.primary_department:
            new_request.status_note = "Awaiting assignment"

        new_request.id_ = ""
        new_request.status = request_statuses["OPN"]
        new_request.due_dt = self.calculator.initial_due_date(new_request.create_dt)
        new_request.passcode = self.generate_passcode()
        new_request.extension_status = extension_statuses['NON']
        request = self.repo.new_request(new_request)

        self.user_service.new_requestor(new_request.requestor, new_request.id_)

        if new_request.primary_department:
            self.assign_department(new_request, new_request.primary_department.id_, "Primary")
        if new_request.secondary_departments:
            for secondary_dept in new_request.secondary_departments:
                self.assign_department(new_request, secondary_dept.id_, "Secondary")
        self.search_service.add_request(request.id_)

        return request

    def get_info(self, request_id: str) -> Request:
        return self.repo.get_request(request_id)

    def get_requestor(self, request_id: str) -> Requestor:
        requestor_id = self.repo.get_requestor_id(request_id)
        requestor = self.user_service.get_requestor(requestor_id)
        return requestor

    def get_assigned_staff(self, request_id: str) -> List[Staff]:
        staff_ids = self.repo.get_staff_ids(request_id)
        return self.user_service.get_staff_by_ids(staff_ids)

    def get_primary_department(self, request_id: str) -> Department:
        dept_ids = self.repo.get_assigned_department_ids(request_id, "Primary")
        dept = self.department_service.get_full_departments(dept_ids)
        if dept:
            return dept[0]
        else:
            return None

    def get_secondary_departments(self, request_id: str) -> List[Department]:
        dept_ids = self.repo.get_assigned_department_ids(request_id, "Secondary")
        depts = self.department_service.get_full_departments(dept_ids)
        for dept in depts:
            dept.completion_status = self.get_completion_status(dept.id_, request_id)
        return depts

    def get_completion_status(self, dept_id: UUID, request_id: str)->CompletionStatus:
        return self.repo.get_completion_status(dept_id, request_id)
    
    def get_content(
        self, request_id: str, content_type: ContentType, release_type: ReleaseType
    ) -> List[Content]:
        if content_type == content_types["NOT"]:
            return self.content_service.get_notes(request_id, release_type)
        elif content_type == content_types["RES"]:
            return self.content_service.get_resources(request_id, release_type)
        elif content_type == content_types["LNK"]:
            return self.content_service.get_links(request_id, release_type)
        elif content_type == content_types["INS"]:
            return self.content_service.get_instructions(request_id, release_type)

    def get_comments(self, request_id: str) -> List[Comment]:
        return self.communication_service.get_request_comments(request_id)

    def get_alerts(self, request_id: str, staff_id: str) -> List[Alert]:
        return self.communication_service.get_alerts(request_id, staff_id)

    def get_log(self, request_id: str) -> List[LogMessage]:
        return self.repo.get_log_messages(request_id)

    def can_update(self, request: Request) -> bool:
        return request.status == request_statuses['OPN']

    def update_public_description(self, request_id: str, new_public_description: str) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False
        request.public_description = new_public_description
        self.repo.update_request(request_id, request)
        self.search_service.add_request(request_id)
        return True
    
    def update_public_title(self, request_id: str, new_public_title: str) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False
        request.public_title = new_public_title
        self.repo.update_request(request_id, request)
        self.search_service.add_request(request_id)
        return True

    def update_due_date(self, request_id: str, reason: str, due_dt: date, staff: Staff) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False
        # check for authorized date range
        if not self.is_ipra_admin(staff):
            min_dt = request.due_dt - timedelta(days=3)
            max_dt = request.due_dt + timedelta(days=3)
            if due_dt < min_dt or due_dt > max_dt:
                return False

        request.due_dt = due_dt
        self.repo.update_request(request_id, request)
        self.search_service.add_request(request_id)
        return True

    def _update_status(self, request: Request, status: RequestStatus) -> bool:
        request.status = status
        if status.code == 'CLS' or status.code == 'DNY':
            request.closing_date = date.today()
        elif status.code == 'OPN':
            request.closing_date = None
        self.repo.update_request(request.id_, request)
        return True

    def _update_status_note(self, request: Request, note: str) -> bool:
        if not self.can_update(request):
            return False
        request.status_note = note
        self.repo.update_request(request.id_, request)
        return True

    def update_departments(
        self,
        request_id: str,
        primary_id: UUID,
        secondary_ids: List[UUID],
        dept_changes: Dict[str, List[UUID]],
    ) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False    
        for unassign_id in dept_changes["unassign"]:
            self.unassign_department(request, unassign_id)

        for assign_id in dept_changes["assign"]:
            if assign_id == primary_id:
                self.assign_department(request, assign_id, "Primary")
            if assign_id in secondary_ids:
                self.assign_department(request, assign_id, "Secondary")
                self.assign_department_completion_status(request_id, assign_id, False, '')

        for update_id in dept_changes["update"]:
            if update_id == primary_id:
                self.update_department_assignment(request_id, update_id, "Primary")
            if update_id in secondary_ids:
                self.update_department_assignment(request_id, update_id, "Secondary")
        self.search_service.add_request(request_id)
        return True

    def unassign_department(self, request: Request, dept_id: UUID) -> bool:
        if not self.can_update(request):
            return False    
        department = self.department_service.get_full_departments([dept_id])[0]
        assigned_staff_ids = self.repo.get_staff_ids(request.id_)

        department_staff_ids = [s.id_ for s in department.staff]
        unassign_ids = [a for a in assigned_staff_ids for d in department_staff_ids if a == d]

        self.repo.unassign_department(request.id_, dept_id)
        self.repo.unassign_request_staff(request.id_, unassign_ids)
        return True

    def assign_department(self, request: Request, dept_id: UUID, level: str) -> bool:
        if not self.can_update(request):
            return False    
        department = self.department_service.get_full_departments([dept_id])[0]
        self.repo.assign_department(request.id_, dept_id, level)

        manager_ids = [s.id_ for s in department.staff if s.role is roles["MAN"]]
        self.assign_request_staff(request, manager_ids)
        if request.status_note == "Awaiting assignment":
            self._update_status_note(request, "Assigned")       
        return True

    def update_department_assignment(self, request_id: str, dept_id: UUID, level: str) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False    
        self.repo.update_department_assignment(request_id, dept_id, level)
        return True

    def assign_department_completion_status(self, request_id: str, dept_id: UUID, status: bool, comment: str)->bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False 
        self.repo.assign_department_completion_status(request_id, dept_id, status, comment)       
        return True

    def update_staff(self, request_id: str, staff_changes: Dict[str, FrozenSet[UUID]]) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False    
        self.repo.unassign_request_staff(request.id_, staff_changes["unassign"])

        self.assign_request_staff(request, staff_changes["assign"])
        return True

    def assign_request_staff(self, request: Request, add_staff: FrozenSet[UUID]) -> bool:
        if not self.can_update(request):
            return False
        reminder_date = date.today()
        self.repo.assign_request_staff(request, add_staff, reminder_date)
        return True

    def unassign_request_staff(self, request: Request, remove_staff: FrozenSet[UUID]) -> bool:
        if not self.can_update(request):
            return False    
        self.repo.unassign_request_staff(request.id_, remove_staff)
        return True

    def get_department_changes(
        self, request_id: str, primary_id: UUID, secondary_ids: List[UUID]
    ) -> Dict[str, List[UUID]]:
        departments_assigned = True
        department_changes = {"unassign": [], "assign": [], "update": []}

        current_primary_department = self.get_primary_department(request_id)
        current_secondary_departments = self.get_secondary_departments(request_id)
        current_secondary_ids = [d.id_ for d in current_secondary_departments]

        if not current_primary_department:
            departments_assigned = False
            department_changes["assign"].append(primary_id)
            if secondary_ids:
                department_changes["assign"].extend(secondary_ids)

        if departments_assigned:
            if (
                primary_id != current_primary_department.id_
                and primary_id not in current_secondary_ids
            ):
                department_changes["assign"].append(primary_id)
                if current_primary_department.id_ not in secondary_ids:
                    department_changes["unassign"].append(current_primary_department.id_)

            if primary_id in current_secondary_ids:
                department_changes["update"].append(primary_id)
                if current_primary_department.id_ not in secondary_ids:
                    department_changes["unassign"].append(current_primary_department.id_)
            if current_primary_department.id_ in secondary_ids:
                department_changes["update"].append(current_primary_department.id_)

            for dept in secondary_ids:
                if dept not in current_secondary_ids:
                    department_changes["assign"].append(dept)
            for dept in current_secondary_ids:
                if dept not in secondary_ids:
                    department_changes["unassign"].append(dept)

        for dept in department_changes["update"]:
            if dept in department_changes["assign"]:
                department_changes["assign"].remove(dept)
            if dept in department_changes["unassign"]:
                department_changes["unassign"].remove(dept)

        return department_changes

    def get_staff_changes(self, request_id: str, staff_ids: List[UUID]) -> Dict[str, FrozenSet[UUID]]:
        staff_ids_set = frozenset(staff_ids)
        all_staff_set = frozenset((s.id_ for s in self.get_assigned_staff(request_id) if s.role== roles['WRK']))
        return {
            "assign": staff_ids_set - all_staff_set,
            "unassign": all_staff_set - staff_ids_set       
        }

    def close_request(self, request_id: str, close_reason: Reason) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False

        self._update_status_note(request, close_reason.value)
        self._update_status(request, request_statuses["CLS"])
        dept_ids = self.repo.get_assigned_department_ids(request_id, "Primary")
        dept_ids.extend(self.repo.get_assigned_department_ids(request_id, "Secondary"))
        for dept in dept_ids:
            self.unassign_department(request, dept)
        self.search_service.add_request(request_id)            
        return True

    def deny_request(self, request_id: str, denial_reason: Reason) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False

        self._update_status_note(request, denial_reason.value)
        self._update_status(request, request_statuses['DNY'])
        self.search_service.add_request(request_id)
        return True

    def reopen_request(self, request_id: str, reason: str) -> bool:
        request = self.repo.get_request(request_id)

        self._update_status(request, request_statuses["OPN"])
        self._update_status_note(request, f"Request Re-opened: {reason}")
        self.search_service.add_request(request_id)
        return True

    def deny_request_wrong_government_entity(
        self, request_id: str, entity: GovernmentEntity
    ) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False        
        self._update_status_note(request, "Denied: Wrong Government Entity")            
        self._update_status(request, request_statuses['DNY'])
        self.search_service.add_request(request_id)
        return True

    def extend_due_date(self, request_id: str, reason: str, due_dt: date, staff: Staff) -> bool:
        request = self.repo.get_request(request_id)
        if not self.can_update(request):
            return False

        if request.extension_status.code == 'NON':
            request.due_dt = self.calculator.extended_due_date(request.create_dt)
            request.extension_status = extension_statuses['INI']
            # Notice sent and note added by parent call to notification service
        elif (
            request.extension_status.code == 'INI'
            or request.extension_status.code == 'BUR'
        ):
            # only IPRA Admin can do burdensome
            if not self.is_ipra_admin(staff):
                return False
            request.due_dt = due_dt
            request.extension_status = extension_statuses['BUR']
            # # Notice sent and note added by parent call to notification service
        else:
            return False

        
        self.repo.update_request(request.id_, request)    
        self.search_service.add_request(request_id)
        return True    
    
    def generate_passcode(self) -> str:
        passcode = "".join(random.choices(string.ascii_letters + string.digits, k=10))
        return passcode

    def update_requestor_contact_info(
        self, request_id: str, new_contact_info_payload: dict, primary_contact_code: str
    ) -> dict:
        new_contact_info = ds.to_model(ContactInfoSchema(), new_contact_info_payload)
        new_contact_info_dict = new_contact_info.to_dict()
        new_contact_info_dict["primary_contact"] = primary_contact_code
        new_contact_info_dict.pop("id_")

        old_contact_info = self.get_requestor_contact_info(request_id)
        changed_contact_info_fields = {}
        for k, v in new_contact_info_dict.items():
            old_value = str(old_contact_info.get(k, ""))
            if old_value != str(v):
                changed_contact_info_fields[k] = {"old": old_value, "new": v}

        if changed_contact_info_fields:
            is_update_contact_info = self.repo.update_requestor_contact_info_by_request_id(
                request_id, new_contact_info
            )
            is_update_primary_contact = self.repo.update_primary_contact_code_by_request_id(
                request_id, primary_contact_code
            )
            if is_update_contact_info or is_update_primary_contact:
                return {
                    "response": {"message": "Contact info updated successfully"},
                    "status": 200,
                    "changed_fields": changed_contact_info_fields
                }
            else:
                return {"response": {"error": "Failed to update contact info"}, "status": 500}
        else:
            return {"response": {"message": "No contact info changes detected"}, "status": 200}

    def get_requestor_contact_info(
            self, request_id: str
    ) -> ContactInfo:
        return self.repo.get_requestor_contact_info_by_request_id(request_id)


class DateCalculator:
    def __init__(self):
        self.calendar = business_calendar.Calendar()

    def business_days(self, start:date, days:int):
        return self.calendar.addbusdays(start, days)

    def next_business(self, start: date) -> date:
        return self.business_days(start, 1)
    
    def initial_due_date(self, create_dt: date) -> date:
        next_business = self.next_business(create_dt)
        return self.business_days(next_business, 2)
        
    def extended_due_date(self, create_dt: date) -> date:
        next_business = self.next_business(create_dt)
        return next_business + timedelta(days=14)
