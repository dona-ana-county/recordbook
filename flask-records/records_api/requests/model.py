from dataclasses import dataclass
from datetime import date
from typing import List

from records_api.communications.model import Alert, Comment
from records_api.constants.model import Lookup
from records_api.content.model import Content
from records_api.departments.model import Department
from records_api.notifications.model import LogMessage
from records_api.users.model import Requestor, Staff
from uuid import UUID


@dataclass
class RequestStatus(Lookup):
    pass

@dataclass
class ResourceStatus(Lookup):
    pass

@dataclass
class ExtensionStatus(Lookup):
    pass

@dataclass
class ReqError:
    error: bool
    message: str

    def __repr__(self) -> str:
        return f"ReqErr(Error: {self.error}, Message: {self.message})"


@dataclass
class Request:
    id_: str
    title: str
    public_title: str
    description: str
    public_description: str
    create_dt: date
    due_dt: date
    status: RequestStatus
    status_note: str
    passcode: str
    extension_status: ExtensionStatus
    requestor: Requestor = None
    primary_department: Department = None
    secondary_departments: List[Department] = None
    assigned_staff: List[Staff] = None
    comments: List[Comment] = None
    alerts: List[Alert] = None
    content: List[Content] = None
    log: List[LogMessage] = None
    reminder_date: date = None
    closing_date: date = None

    def __repr__(self):
        return f"Request(Id: {self.id_}, Title: {self.title}, Status: {self.status})"

@dataclass
class RequestOrReqError:
    req_error: ReqError = None
    request: Request = None