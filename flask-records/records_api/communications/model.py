from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from records_api.users.model import User


@dataclass
class Communication:
    id_: UUID
    create_dt: datetime
    message: str


class Comment(Communication):
    def __init__(
        self, id_: UUID, create_dt: datetime, message: str, commenter: User = None
    ) -> None:
        Communication.__init__(self, id_, create_dt, message)
        self.commenter = commenter


class Alert(Communication):
    def __init__(
        self, id_: UUID, create_dt: datetime, message: str, sender: User, recipient: User = None
    ) -> None:
        Communication.__init__(self, id_, create_dt, message)
        self.sender = sender
        self.recipient = recipient


class AlertedItem:
    def __init__(self, id_: UUID, alert: Alert = None) -> None:
        self.id_ = id_
        self.alert = alert

    def __repr__(self):
        return f"AlertedItem(Id: {self.id_}, Alert: {self.alert})"
