from typing import List
from uuid import UUID

from records_api.departments.service import DepartmentService
from records_api.requests.model import Request
from records_api.service import Service
from records_api.users.service import UserService

from .model import Alert, Comment
from .repo import CommunicationRepo

RequestService = "records_api.requests.service.RequestService" 

class CommunicationService(Service):
    def __init__(self, app):
        self.comm_repo: CommunicationRepo = CommunicationRepo()
        self.user_service: UserService = Service.get_service(UserService)
        self.department_service: DepartmentService = Service.get_service(DepartmentService)
        self.request_service: RequestService = Service.get_service(RequestService)

    def can_update(self, request_id: str) -> bool:
        request: Request = self.request_service.get_info(request_id)
        return self.request_service.can_update(request)

    def new_comment(self, request_id: str, comment: Comment) -> Comment:
        return self.comm_repo.new_comment(request_id, comment)

    def new_alert(self, request_id: str, recipient_id: UUID, alert: Alert):
        if not self.can_update(request_id):
            return False
        return self.comm_repo.new_alert(request_id, recipient_id, alert)

    def alert_department(self, request_id: str, dept_id: UUID, alert: Alert) -> bool:
        if not self.can_update(request_id):
            return False
        dept_man_ids = self.user_service.get_manager_ids_by_departments([dept_id])
        for man_id in dept_man_ids:
            self.new_alert(request_id, man_id, alert)
        return True

    def alert_staff(self, request_id: str, staff_id: UUID, alert: Alert) -> bool:
        if not self.can_update(request_id):
            return False
        self.new_alert(request_id, staff_id, alert)
        return True

    def get_comment(self, comment_id: UUID) -> Comment:
        comments = self.get_comments([comment_id])
        return comments[0]

    def get_comments(self, ids: List[UUID]) -> List[Comment]:
        return self.comm_repo.get_comments(ids)

    def get_alerts(self, request_id: str, recipient_id: UUID) -> List[Alert]:
        alerts = self.comm_repo.get_alerts(request_id, recipient_id)
        for a in alerts:
            a.sender = self.user_service.get_staff_by_id(a.sender)
            a.recipient = self.user_service.get_staff_by_id(a.recipient)
        return alerts

    def get_request_comments(self, request_id: str) -> List[Comment]:
        return self.comm_repo.get_request_comments(request_id)
