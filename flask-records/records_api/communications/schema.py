from datetime import datetime, timezone
from uuid import uuid4

from marshmallow import Schema, fields, post_load

from records_api.users.schema import StaffSchema, UserSchema

from .model import Alert, AlertedItem, Comment

class CommentSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    commenter = fields.Nested(UserSchema, allow_none=True)
    message = fields.Str()

    @post_load
    def make_comment(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        if "create_dt" not in data:
            data["create_dt"] = datetime.now(tz=timezone.utc)
        return Comment(**data)


class AlertSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    sender = fields.Nested(StaffSchema)
    recipient = fields.Nested(StaffSchema, allow_none=True)
    message = fields.Str()

    @post_load
    def make_alert(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        if "create_dt" not in data:
            data["create_dt"] = datetime.now(tz=timezone.utc)
        return Alert(**data)


class AlertedItemSchema(Schema):
    id_ = fields.UUID(data_key="id")
    alert = fields.Nested(AlertSchema, allow_none=True)

    @post_load
    def make_alerted_item(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = uuid4()
        if "alert" not in data:
            data["alert"] = None
        return AlertedItem(**data)
