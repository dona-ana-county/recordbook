from datetime import datetime, timezone
from typing import List
from uuid import UUID, uuid4

from records_api import db
from records_api.serializer.db_schema import Alert as DbAlert
from records_api.serializer.db_schema import Comment as DbComment
from records_api.serializer.db_schema import Communication as DbCommunication
from records_api.serializer.db_schema import User as DbUser
from records_api.users.model import User

from .model import Alert, Comment


class CommunicationRepo:
    def __init__(self):
        pass

    def new_comment(self, request_id: str, comment: Comment) -> UUID:
        new_id = uuid4()
        db_comm = DbCommunication(id_=new_id, create_dt=datetime.now(tz=timezone.utc))
        db.session.add(db_comm)
        db.session.commit()
        db_comment = DbComment(
            id_=new_id,
            commenter_id=comment.commenter.id_,
            request_id=request_id,
            message=comment.message,
        )
        db.session.add(db_comment)
        db.session.commit()
        comment.id_ = new_id

        return comment

    def new_alert(self, request_id: str, recipient_id: UUID, alert: Alert) -> UUID:
        new_id = uuid4()
        db_comm = DbCommunication(id_=new_id, create_dt=datetime.now(tz=timezone.utc))
        db.session.add(db_comm)
        db.session.commit()
        db_alert = DbAlert(
            id_=new_id,
            request_id=request_id,
            sender_id=alert.sender.id_,
            recipient_id=recipient_id,
            message=alert.message,
        )
        db.session.add(db_alert)
        db.session.commit()
        return new_id

    def get_comments(self, comment_ids: List[UUID]) -> List[Comment]:
        db_comments = (
            db.session.query(DbComment, DbCommunication, DbUser)
            .join(DbCommunication, DbCommunication.id_ == DbComment.id_)
            .join(DbUser, DbUser.id_ == DbComment.commenter_id)
            .filter(DbComment.id_.in_(comment_ids))
            .order_by(DbCommunication.create_dt)
            .all()
        )

        comments = []
        for c in db_comments:
            user = User(c[2].id_, c[2].first_name, c[2].last_name)
            comment = Comment(id_=c[0].id_, create_dt=c[1].create_dt, message=c[0].message)
            comment.commenter = user
            comments.append(comment)

        return comments

    def get_alerts(self, request_id: str, recipient_id: UUID) -> List[Alert]:
        db_alerts = (
            db.session.query(DbAlert, DbCommunication)
            .join(DbCommunication, DbCommunication.id_ == DbAlert.id_)
            .filter(DbAlert.request_id == request_id, DbAlert.recipient_id == recipient_id)
            .order_by(DbCommunication.create_dt)
            .all()
        )

        alerts = []
        for db_a in db_alerts:
            alert = Alert(
                id_=db_a[1].id_,
                create_dt=db_a[1].create_dt,
                message=db_a[0].message,
                sender=db_a[0].sender_id,
                recipient=db_a[0].recipient_id,
            )
            alerts.append(alert)

        return alerts

    def get_request_comments(self, request_id: str) -> List[Comment]:
        db_comment_ids = [c.id_ for c in 
            db.session.query(DbComment.id_).filter(DbComment.request_id == request_id).all()
        ]
        return self.get_comments(db_comment_ids)
