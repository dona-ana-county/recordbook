from datetime import datetime, timezone
from typing import List
from uuid import UUID, uuid4

from records_api import db
from records_api.constant import content_types, release_types
from records_api.content.model import (Content, ContentType, Instruction, Link,
                                       Note, ReleaseType, Resource)
from records_api.serializer.db_schema import Content as DbContent
from records_api.serializer.db_schema import Instruction as DbInstruction
from records_api.serializer.db_schema import Link as DbLink
from records_api.serializer.db_schema import Note as DbNote
from records_api.serializer.db_schema import Request as DbRequest
from records_api.serializer.db_schema import RequestContent as DbRequestContent
from records_api.serializer.db_schema import Resource as DbResource
from records_api.serializer.db_schema import \
    ResourceMessage as DbResourceMessage


class ContentRepo:
    def __init__(self) -> None:
        pass

    def __new_content(self, request_id: str, content: Content, content_type: ContentType) -> None:
        db_content = DbContent(
            id_=content.id_,
            title=content.title,
            create_dt=content.create_dt,
            content_type=content_type.code,
            release_type=content.release_type.code,
            creator=content.creator.id_,
            editable=content.editable,
        )
        db.session.add(db_content)
        db.session.commit()

    def __new_request_content(self, request_id: str, content_id: UUID) -> None:
        db_request_content = DbRequestContent(request_id=request_id, content_id=content_id)
        db.session.add(db_request_content)
        db.session.commit()

    def new_note(self, request_id: str, note: Note) -> Note:
        self.__new_content(request_id, note, content_types["NOT"])

        db_note = DbNote(id_=note.id_, message=note.message)
        db.session.add(db_note)
        db.session.commit()

        self.__new_request_content(request_id, note.id_)

        return note

    def new_instruction(self, request_id: str, instruction: Instruction) -> Instruction:
        # TODO VT: Refactor out the creating the entry in the content table
        db_content = DbContent(
            id_=uuid4(),
            title=instruction.title,
            create_dt=datetime.now(tz=timezone.utc),
            content_type=content_types["INS"].code,
            release_type=instruction.release_type.code,
            creator=instruction.creator.id_,
            editable=instruction.editable,
        )
        db.session.add(db_content)
        db.session.commit()

        db_instruction = DbInstruction(id_=db_content.id_, message=instruction.message)
        db.session.add(db_instruction)
        db.session.commit()

        db_request_content = DbRequestContent(request_id=request_id, content_id=db_content.id_)
        db.session.add(db_request_content)
        db.session.commit()

        instruction.id_ = db_content.id_
        instruction.create_dt = db_content.create_dt

        return instruction

    def new_link(self, request_id: str, link: Link) -> Link:
        # TODO VT: Refactor out the creating the entry in the content table
        db_content = DbContent(
            id_=uuid4(),
            title=link.title,
            create_dt=datetime.now(tz=timezone.utc),
            content_type=content_types["LNK"].code,
            release_type=link.release_type.code,
            creator=link.creator.id_,
            editable=link.editable,
        )
        db.session.add(db_content)
        db.session.commit()

        db_link = DbLink(id_=db_content.id_, description=link.description, url=link.url)
        db.session.add(db_link)
        db.session.commit()

        db_request_content = DbRequestContent(request_id=request_id, content_id=db_content.id_)
        db.session.add(db_request_content)
        db.session.commit()

        link.id_ = db_content.id_
        link.create_dt = db_content.create_dt

        return link

    def new_resource(self, request_id: str, resource: Resource) -> Resource:
        # TODO VT: Refactor out the creating the entry in the content table
        db_content = DbContent(
            id_=resource.id_,
            title=resource.title,
            create_dt=datetime.now(tz=timezone.utc),
            content_type=content_types["RES"].code,
            release_type=resource.release_type.code,
            creator=resource.creator.id_,
            editable=resource.editable,
        )
        db.session.add(db_content)
        db.session.commit()

        db_resource = DbResource(
            id_=db_content.id_,
            extension=resource.extension,
            file_path=resource.file_path,
            needs_review=resource.needs_review,
            under_review=resource.under_review,
            description=resource.description,
            revised=False,
        )
        db.session.add(db_resource)
        db.session.commit()

        db_request_content = DbRequestContent(request_id=request_id, content_id=db_content.id_)
        db.session.add(db_request_content)
        db.session.commit()

        if resource.message:
            db_resource_message = DbResourceMessage(
                resource_id=db_content.id_, message=resource.message
            )
            db.session.add(db_resource_message)
            db.session.commit()

        resource.id_ = db_content.id_
        resource.create_dt = db_content.create_dt
        resource.release_type = release_types[resource.release_type.code]

        return resource

    def get_content_by_id(self, content_id: UUID) -> Content:
        db_content = (
            db.session.query(DbContent, DbRequestContent)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .filter(DbContent.id_ == content_id)
            .first()
        )
        content = Content(
            id_=db_content[0].id_,
            request_id=db_content[1].request_id,
            title=db_content[0].title,
            create_dt=db_content[0].create_dt,
            release_type=release_types[db_content[0].release_type],
            creator=db_content[0].creator,
            editable=db_content[0].editable,
        )
        return content

    def get_note_by_id(self, content_id: UUID) -> Note:
        db_note = (
            db.session.query(DbNote, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbNote.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(DbContent.id_ == content_id)
            .first()
        )
        note = Note(
            id_=db_note[1].id_,
            request_id=db_note[2].request_id,
            title=db_note[1].title,
            create_dt=db_note[1].create_dt,
            release_type=release_types[db_note[1].release_type],
            creator=db_note[1].creator,
            message=db_note[0].message,
            editable=db_note[1].editable,
        )
        return note

    def get_link_by_id(self, content_id: UUID) -> Link:
        db_link = (
            db.session.query(DbLink, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbLink.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(DbContent.id_ == content_id)
            .first()
        )
        link = Link(
            id_=db_link[1].id_,
            request_id=db_link[2].request_id,
            title=db_link[1].title,
            create_dt=db_link[1].create_dt,
            release_type=release_types[db_link[1].release_type],
            creator=db_link[1].creator,
            description=db_link[0].description,
            url=db_link[0].url,
            editable=db_link[1].editable,
        )
        return link

    def get_instruction_by_id(self, content_id: UUID) -> Instruction:
        db_inst = (
            db.session.query(DbInstruction, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbInstruction.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(DbContent.id_ == content_id)
            .first()
        )
        instruction = Instruction(
            id_=db_inst[1].id_,
            request_id=db_inst[2].request_id,
            title=db_inst[1].title,
            create_dt=db_inst[1].create_dt,
            release_type=release_types[db_inst[1].release_type],
            creator=db_inst[1].creator,
            message=db_inst[0].message,
            editable=db_inst[1].editable,
        )
        return instruction

    def get_resource_by_id(self, resource_id: UUID) -> Resource:
        db_resource = (
            db.session.query(DbResource, DbContent, DbRequestContent, DbResourceMessage)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .outerjoin(DbResourceMessage, DbResourceMessage.resource_id == DbResource.id_)
            .filter(DbResource.id_ == resource_id)
            .first()
        )
        if db_resource is None:
            return None
        resource = Resource(
            id_=db_resource[1].id_,
            request_id=db_resource[2].request_id,
            title=db_resource[1].title,
            create_dt=db_resource[1].create_dt,
            release_type=release_types[db_resource[1].release_type],
            creator=db_resource[1].creator,
            extension=db_resource[0].extension,
            file_path=db_resource[0].file_path,
            message="",
            needs_review=db_resource[0].needs_review,
            under_review=db_resource[0].under_review,
            editable=db_resource[1].editable,
            description=db_resource[0].description,
            revised=db_resource[0].revised,
        )
        if db_resource[3]:
            resource.message = db_resource[3].message

        return resource

    def get_notes(self, request_id: str, release_type: ReleaseType) -> List[Note]:
        notes = []
        db_notes = (
            db.session.query(DbNote, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbNote.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(
                DbRequest.id_ == request_id,
                DbContent.release_type.in_(self.allowed_types(release_type)),
            )
            .order_by(DbContent.create_dt.desc() )
            .all()
        )
        for db_note in db_notes:
            note = Note(
                id_=db_note[1].id_,
                request_id=db_note[2].request_id,
                title=db_note[1].title,
                create_dt=db_note[1].create_dt,
                release_type=release_types[db_note[1].release_type],
                creator=db_note[1].creator,
                message=db_note[0].message,
                editable=db_note[1].editable,
            )
            notes.append(note)

        return notes

    def get_instructions(self, request_id: str, release_type: ReleaseType) -> List[Instruction]:
        instructions = []
        db_instructions = (
            db.session.query(DbInstruction, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbInstruction.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(
                DbRequest.id_ == request_id,
                DbContent.release_type.in_(self.allowed_types(release_type)),
            )
            .order_by(DbContent.create_dt.desc() )
            .all()
        )
        for db_inst in db_instructions:
            instruction = Instruction(
                id_=db_inst[1].id_,
                request_id=db_inst[2].request_id,
                title=db_inst[1].title,
                create_dt=db_inst[1].create_dt,
                release_type=release_types[db_inst[1].release_type],
                creator=db_inst[1].creator,
                message=db_inst[0].message,
                editable=db_inst[1].editable,
            )
            instructions.append(instruction)

        return instructions

    def get_links(self, request_id: str, release_type: ReleaseType) -> List[Link]:
        links = []
        db_links = (
            db.session.query(DbLink, DbContent, DbRequestContent)
            .join(DbContent, DbContent.id_ == DbLink.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .filter(
                DbRequest.id_ == request_id,
                DbContent.release_type.in_(self.allowed_types(release_type)),
            )
            .order_by(DbContent.create_dt.desc() )
            .all()
        )
        for db_link in db_links:
            link = Link(
                id_=db_link[1].id_,
                request_id=db_link[2].request_id,
                title=db_link[1].title,
                create_dt=db_link[1].create_dt,
                release_type=release_types[db_link[1].release_type],
                creator=db_link[1].creator,
                description=db_link[0].description,
                url=db_link[0].url,
                editable=db_link[1].editable,
            )
            links.append(link)

        return links

    def get_resources(self, request_id: str, release_type: ReleaseType) -> List[Resource]:
        resources = []
        db_resources = (
            db.session.query(DbResource, DbContent, DbRequestContent, DbResourceMessage)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .join(DbRequest, DbRequest.id_ == DbRequestContent.request_id)
            .outerjoin(DbResourceMessage, DbResourceMessage.resource_id == DbResource.id_)
            .filter(
                DbRequest.id_ == request_id,
                DbContent.release_type.in_(self.allowed_types(release_type)),
            )
            .order_by(DbContent.create_dt.desc() )
            .all()
        )
        for db_res in db_resources:
            resource = Resource(
                id_=db_res[1].id_,
                request_id=db_res[2].request_id,
                title=db_res[1].title,
                create_dt=db_res[1].create_dt,
                release_type=release_types[db_res[1].release_type],
                creator=db_res[1].creator,
                extension=db_res[0].extension,
                file_path=db_res[0].file_path,
                needs_review=db_res[0].needs_review,
                under_review=db_res[0].under_review,
                editable=db_res[1].editable,
                message="",
                description=db_res[0].description,
                revised=db_res[0].revised,
            )
            if db_res[3]:
                resource.message = db_res[3].message

            # Filter items that need_review if release_type is not PRV
            if release_type == release_types["PRV"] or not resource.needs_review:
                resources.append(resource)

        return resources

    def get_resource_by_release_type(
        self, resource_id: UUID, release_type: ReleaseType
    ) -> Resource:
        db_resource = (
            db.session.query(DbResource, DbContent, DbRequestContent, DbResourceMessage)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .outerjoin(DbResourceMessage, DbResourceMessage.resource_id == DbResource.id_)
            .filter(
                DbResource.id_ == resource_id,
                DbContent.release_type.in_(self.allowed_types(release_type)),
            )
            .first()
        )
        if db_resource is None:
            return None

        resource = Resource(
            id_=db_resource[1].id_,
            request_id=db_resource[2].request_id,
            title=db_resource[1].title,
            create_dt=db_resource[1].create_dt,
            release_type=release_types[db_resource[1].release_type],
            creator=db_resource[1].creator,
            extension=db_resource[0].extension,
            file_path=db_resource[0].file_path,
            needs_review=db_resource[0].needs_review,
            under_review=db_resource[0].under_review,
            editable=db_resource[1].editable,
            message="",
            description=db_resource[0].description,
            revised=db_resource[0].revised,
        )
        if db_resource[3]:
            resource.message = db_resource[3].message

        if release_type == release_types["PRV"] or not resource.needs_review:
            return resource
        return None

    def update_note(self, note: Note) -> Note:
        db_note = (
            db.session.query(DbNote, DbContent)
            .join(DbContent, DbContent.id_ == DbNote.id_)
            .filter(DbContent.id_ == note.id_, DbContent.editable)
            .first()
        )

        if db_note:
            db_note[0].message = note.message
            db_note[1].title = note.title
            db_note[1].release_type = note.release_type.code
            db.session.commit()
            return note
        return None

    def update_instruction(self, instruction: Instruction) -> Instruction:
        db_instruction = (
            db.session.query(DbInstruction, DbContent)
            .join(DbContent, DbContent.id_ == DbInstruction.id_)
            .filter(DbContent.id_ == instruction.id_, DbContent.editable)
            .first()
        )
        if db_instruction:
            db_instruction[0].message = instruction.message
            db_instruction[1].title = instruction.title
            db_instruction[1].release_type = instruction.release_type.code
            db.session.commit()
            return instruction
        return None

    def update_link(self, link: Link) -> Link:
        db_link = (
            db.session.query(DbLink, DbContent)
            .join(DbContent, DbContent.id_ == DbLink.id_)
            .filter(DbContent.id_ == link.id_, DbContent.editable)
            .first()
        )
        if db_link:
            db_link[0].description = link.description
            db_link[0].url = link.url
            db_link[1].title = link.title
            db_link[1].release_type = link.release_type.code
            db.session.commit()
            return link
        return None

    def update_resource(self, resource: Resource) -> Resource:
        db_resource = (
            db.session.query(DbResource, DbContent)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .filter(DbContent.id_ == resource.id_, DbContent.editable)
            .first()
        )
        if db_resource:
            db_resource[1].title = resource.title
            db_resource[0].extension = resource.extension
            db_resource[0].description = resource.description
            if resource.file_path is not None:
                db_resource[0].file_path = resource.file_path
            db_resource[1].release_type = resource.release_type.code
            db.session.commit()
            return resource
        return None

    def update_release_type(self, content_id: UUID, new_type: ReleaseType) -> UUID:
        db_content = db.session.query(DbContent).filter(DbContent.id_ == content_id).first()
        db_content.release_type = new_type.code
        db.session.commit()
        return db_content.id_

    def update_under_review(self, content_id: UUID, status: bool) -> UUID:
        db_content = db.session.query(DbResource).filter(DbResource.id_ == content_id).first()
        db_content.under_review = status
        db.session.commit()
        return db_content.id_

    def update_needs_review(self, content_id: UUID, status: bool) -> UUID:
        db_content = db.session.query(DbResource).filter(DbResource.id_ == content_id).first()
        db_content.needs_review = status
        db.session.commit()
        return db_content.id_

    def update_revised(self, content_id: UUID) -> UUID:
        db_content = db.session.query(DbResource).filter(DbResource.id_ == content_id).first()
        db_content.revised = True
        db.session.commit()
        return db_content.id_        

    def delete_resource(self, resource_id: UUID) -> bool:
        db_resource = (
            db.session.query(DbResource, DbContent, DbRequestContent, DbResourceMessage)
            .join(DbContent, DbContent.id_ == DbResource.id_)
            .join(DbRequestContent, DbRequestContent.content_id == DbContent.id_)
            .outerjoin(DbResourceMessage, DbResourceMessage.resource_id == DbResource.id_)
            .filter(DbResource.id_ == resource_id)
            .first()
        )
        if db_resource[3]:
            db.session.delete(db_resource[3])
            db.session.commit()
        db.session.delete(db_resource[0])
        db.session.commit()
        db.session.delete(db_resource[2])
        db.session.commit()
        db.session.delete(db_resource[1])
        db.session.commit()

        return True

    def allowed_types(self, release_type: ReleaseType) -> List[str]:
        if release_type == release_types["PRV"]:
            return [
                release_types["PRV"].code,
                release_types["REQ"].code,
                release_types["PUB"].code,
                release_types["DOC"].code,
            ]
        if release_type == release_types["REQ"]:
            return [release_types["REQ"].code, release_types["PUB"].code]
        if release_type == release_types["PUB"]:
            return [release_types["PUB"].code]
    def has_legal_review_resources(self, request_id: UUID) -> bool:
        value = (db.session.query(DbResource.id_)
                          .join(DbRequestContent, DbResource.id_ == DbRequestContent.content_id)
                          .filter(DbRequestContent.request_id == request_id)
                          .filter(DbResource.needs_review == True)
                          .scalar()) 
        return value is not None
