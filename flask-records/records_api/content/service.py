import os
from datetime import date, datetime, timezone
from typing import List, Tuple
from uuid import UUID, uuid4

from flask import current_app, send_file
from marshmallow import Schema
from werkzeug.utils import secure_filename

from records_api.constant import content_types, release_types
from records_api.content.schema import (InstructionSchema, LinkSchema,
                                        NoteSchema, ResourceSchema)
from records_api.requests.model import Request
from records_api.serializer.domain_serializer import DomainSerializer as ds
from records_api.service import Service
from records_api.users.service import UserService

from .model import (Content, ContentType, Instruction, Link, Note, ReleaseType,
                    Resource)
from .repo import ContentRepo

RequestService = "records_api.requests.service.RequestService"

class ContentService(Service):
    def __init__(self, app) -> None:
        self.repo: ContentRepo = ContentRepo()
        # Specify extension that are not allowed
        # Empty array would allow all files
        self.extension_not_allowed = ["dll", "exe", "bat", "app", "py", "java", "js"]
        self.user_service: UserService = Service.get_service(UserService)
        self.request_service: RequestService = Service.get_service(RequestService)

    def can_update(self, request_id: str) -> bool:
        request: Request = self.request_service.get_info(request_id)
        return self.request_service.can_update(request)

    def get_content_schema(self, content_type: ContentType, many: bool = False) -> Schema:
        if content_type == content_types["NOT"]:
            return NoteSchema(many=many)
        if content_type == content_types["INS"]:
            return InstructionSchema(many=many)
        if content_type == content_types["LNK"]:
            return LinkSchema(many=many)
        if content_type == content_types["RES"]:
            return ResourceSchema(many=many)

    def get_content(
        self, request_id: str, content_type: ContentType, release_type: ReleaseType
    ) -> List[Content]:
        if content_type == content_types["NOT"]:
            return self.get_notes(request_id, release_type)
        if content_type == content_types["INS"]:
            return self.get_instructions(request_id, release_type)
        if content_type == content_types["LNK"]:
            return self.get_links(request_id, release_type)
        if content_type == content_types["RES"]:
            return self.get_resources(request_id, release_type)

    def get_content_by_id(self, content_id: UUID) -> Content:
        content = self.repo.get_content_by_id(content_id)
        content.creator = self.user_service.get_staff_by_id(content.creator)
        return content

    def get_note_by_id(self, content_id: UUID) -> Note:
        res = self.repo.get_note_by_id(content_id)
        res.creator = self.user_service.get_staff_by_id(res.creator)
        return res

    def get_instruction_by_id(self, content_id: UUID) -> Instruction:
        res = self.repo.get_instruction_by_id(content_id)
        res.creator = self.user_service.get_staff_by_id(res.creator)
        return res

    def get_link_by_id(self, content_id: UUID) -> Link:
        res = self.repo.get_link_by_id(content_id)
        res.creator = self.user_service.get_staff_by_id(res.creator)
        return res

    def get_resource_by_id(self, content_id: UUID) -> Resource:
        res = self.repo.get_resource_by_id(content_id)
        res.creator = self.user_service.get_staff_by_id(res.creator)
        return res

    def get_notes(self, request_id: str, release_type: ReleaseType) -> List[Note]:
        notes = self.repo.get_notes(request_id, release_type)
        for n in notes:
            n.creator = self.user_service.get_staff_by_id(n.creator)
        return ds.to_json(NoteSchema(many=True), notes)

    def get_instructions(self, request_id: str, release_type: ReleaseType) -> List[Instruction]:
        instructions = self.repo.get_instructions(request_id, release_type)
        for i in instructions:
            i.creator = self.user_service.get_staff_by_id(i.creator)
        return ds.to_json(InstructionSchema(many=True), instructions)

    def get_links(self, request_id: str, release_type: ReleaseType) -> List[Link]:
        links = self.repo.get_links(request_id, release_type)
        for l in links:
            l.creator = self.user_service.get_staff_by_id(l.creator)
        return ds.to_json(LinkSchema(many=True), links)

    def get_resources(self, request_id: str, release_type: ReleaseType) -> List[Resource]:
        resources = self.repo.get_resources(request_id, release_type)
        for r in resources:
            r.creator = self.user_service.get_staff_by_id(r.creator)
        return ds.to_json(ResourceSchema(many=True), resources)

    def get_resource_by_release_type(self, content_id: UUID, release_type: ReleaseType) -> Resource:
        return self.repo.get_resource_by_release_type(content_id, release_type)

    def new_note(self, request_id: str, note: Note) -> Note:
        if not self.can_update(request_id):
            return None
        note.id_ = uuid4()
        note.create_dt = datetime.now(tz=timezone.utc)
        new_note = self.repo.new_note(request_id, note)
        return new_note

    def new_instruction(self, request_id: str, instruction: Instruction) -> Instruction:
        if not self.can_update(request_id):
            return None
        instruction.id_ = uuid4()
        instruction.create_dt = datetime.now(tz=timezone.utc)
        new_instruction = self.repo.new_instruction(request_id, instruction)
        return new_instruction

    def new_link(self, request_id: str, link: Link) -> Link:
        if not self.can_update(request_id):
            return None
        link.id_ = uuid4()
        link.create_dt = datetime.now(tz=timezone.utc)
        new_link = self.repo.new_link(request_id, link)
        return new_link

    def new_resource(self, request_id: str, resource: Resource) -> Resource:
        if not self.can_update(request_id):
            return None
        resource.create_dt = datetime.now(tz=timezone.utc)
        resource.title = secure_filename(resource.title)
        new_resource = self.repo.new_resource(request_id, resource)
        return new_resource

    def update_content(self, request_id: str, content_type: ContentType, content: Content) -> Tuple[Content, ReleaseType]:
        old_content = self.get_content_by_id( content.id_ )
        old_release_type = old_content.release_type
        if content_type == content_types["NOT"]:
            return self.update_note(request_id, content), old_release_type
        if content_type == content_types["INS"]:
            return self.update_instruction(request_id, content), old_release_type
        if content_type == content_types["LNK"]:
            return self.update_link(request_id, content), old_release_type
        if content_type == content_types["RES"]:
            return self.update_resource(request_id, content), old_release_type

    def update_note(self, request_id: str, note: Note) -> Note:
        if not self.can_update(request_id):
            return None        
        updated_note = self.repo.update_note(note)
        if updated_note:
            return self.get_content_by_id(updated_note.id_)
        else:
            return None

    def update_instruction(self, request_id: str, instruction: Instruction) -> Instruction:
        if not self.can_update(request_id):
            return None
        updated_instruction = self.repo.update_instruction(instruction)
        if updated_instruction:
            return self.get_content_by_id(updated_instruction.id_)
        else:
            return None

    def update_link(self, request_id: str, link: Link) -> Link:
        if not self.can_update(request_id):
            return None
        updated_link = self.repo.update_link(link)
        if updated_link:
            return self.get_content_by_id(updated_link.id_)
        else:
            return None

    def update_resource(self, request_id: str, resource: Resource) -> Resource:
        if not self.can_update(request_id):
            return None
        resource.file_path = None
        resource.title = secure_filename(resource.title)

        title, title_ext = os.path.splitext(resource.title)
        if title_ext:
            title_ext = title_ext[1:].lower()
        if title_ext != resource.extension:
            title = resource.title
        if resource.extension:
            title += "." + resource.extension
        resource.title = title

        updated_resource = self.repo.update_resource(resource)
        if updated_resource:
            return self.get_resource_by_id(updated_resource.id_)
        else:
            return None

    def reject_resource(self, content_id: UUID, staff_id: UUID, reason: str) -> Resource:
        resource = self.get_resource_by_id(content_id)
        if not self.can_update(resource.request_id):
            return None
        if not resource.needs_review:
            return None
        file_deleted = self.delete_file(resource.file_path)
        db_deleted = self.repo.delete_resource(content_id)

        if file_deleted and db_deleted:
            return resource
        return None

    def delete_content(self, content_id: UUID, content_type: ContentType, staff_id: UUID, reason: str) -> Resource:
        content = self.get_content_by_id(content_id)
        if not self.can_update(content.request_id):
            return None        

        if content_type == content_types['RES']:
            resource = self.get_resource_by_id(content_id)
            file_deleted = self.delete_file(resource.file_path)
            db_deleted = self.repo.delete_resource(content_id)
        else:
            file_deleted = True
            db_deleted = False

        if file_deleted and db_deleted:
            return resource
        return None

    def delete_file(self, file_name: str):
        folder = current_app.config["UPLOAD_FOLDER"]
        file_path = os.path.join(folder, file_name)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
                return True
        except Exception as e:
            current_app.logger.debug(e)
            return False

    def allowed_file(self, file_name):
        return (
            "." in file_name
            and file_name.rsplit(".", 1)[1].lower() not in self.extension_not_allowed
        )

    def download_file(self, resource_id: UUID, release_type: ReleaseType):
        resource = self.repo.get_resource_by_release_type(resource_id, release_type)
        if resource is None or resource.needs_review:
            return None
        path = os.path.join(current_app.config["UPLOAD_FOLDER"], resource.file_path)
        return send_file(
            path_or_file=path,
            as_attachment=True,
            download_name=resource.title,
        )
    

    def download_private_file(self, resource_id: UUID):
        resource = self.repo.get_resource_by_release_type(resource_id, release_types["PRV"])
        if resource is None:
            return None
        path = os.path.join(current_app.config["UPLOAD_FOLDER"], resource.file_path)
        return send_file(
            path_or_file=path,
            as_attachment=True,
            download_name=resource.title,
        )

    def upload_file(self, request, resource) -> Resource:
        if "file" not in request.files:
            current_app.logger.debug("No File")
            return None

        upload = request.files["file"]
        
        if upload and self.allowed_file(upload.filename):
            file_name = secure_filename(upload.filename)
            if file_name == "":
                current_app.logger.debug("No Filename")
                return None            
            today = date.today()
            file_path = os.path.join(str(today.year), str(today.month), str(today.day))
            base_path = current_app.config["UPLOAD_FOLDER"]
            # Create file path if it does not exist
            if not os.path.exists(os.path.join(base_path, file_path)):
                os.makedirs(os.path.join(base_path, file_path))
            
            file_name, ext = os.path.splitext(file_name)
            if ext:
                ext = ext[1:].lower()
            # use random filename to prevent collisions
            resource.id_ = uuid4()
            file_path = os.path.join(file_path, resource.id_.hex)
            upload.save(os.path.join(base_path, file_path))
            resource.file_path = file_path
            resource.under_review = False
            resource.extension = ext

            title, title_ext = os.path.splitext(resource.title)
            if title_ext:
                title_ext = title_ext[1:].lower()
            if title_ext != ext:
                title = resource.title
            if ext:
                title += "." + ext
            resource.title = title

            return self.new_resource(resource.request_id, resource)

        return None

    def upload_revised_file(self, request, resource_id: UUID) -> Resource:
        resource = self.repo.get_resource_by_id(resource_id)
        if resource is None:
            return None
        if not self.can_update(resource.request_id):
            return None

        if "file" not in request.files:
            current_app.logger.debug("No File")
            return None

        upload = request.files["file"]

        if upload and self.allowed_file(upload.filename):
            file_name = secure_filename(upload.filename)
            if file_name == "":
                current_app.logger.debug("No Filename")
                return None

            base_path = current_app.config["UPLOAD_FOLDER"]
            today = date.today()
            file_path = os.path.join(str(today.year), str(today.month), str(today.day))
            # Create file path if it does not exist
            if not os.path.exists(os.path.join(base_path, file_path)):
                os.makedirs(os.path.join(base_path, file_path))
            
            # use random filename to prevent collisions
            file_path = os.path.join(file_path, uuid4().hex)

            upload.save(os.path.join(base_path, file_path))

            old_file_path = resource.file_path
            resource.file_path = file_path
            # TODO: Enable frontend to specify title
            # resource.title = file_name
            file_name, ext = os.path.splitext(file_name)
            if ext:
                ext = ext[1:].lower()
            resource.extension = ext

            title, title_ext = os.path.splitext(resource.title)
            if title_ext:
                title_ext = title_ext[1:].lower()
            if ext and title_ext != ext:
                resource.title = title + "." + ext
            

            updated_resource = self.repo.update_resource(resource)
            self.repo.update_revised(resource.id_)
            
            file_deleted = self.delete_file(old_file_path)            

            if file_deleted and updated_resource:
                return updated_resource
            else:
                return None

        return None

    def update_under_review(self, content_id: UUID, status: bool) -> UUID:
        content = self.get_content_by_id(content_id)
        if not self.can_update(content.request_id):
            return None        
        return self.repo.update_under_review(content_id, status)

    def approve_resource(self, content_id: UUID, release_type: ReleaseType) -> Resource:
        resource = self.get_resource_by_id(content_id)
        if not self.can_update(resource.request_id):
            return None
        if not resource.needs_review:
            return None
        if not self.repo.update_under_review(resource.id_, False):
            return None
        if not self.repo.update_needs_review(resource.id_, False):
            return None
        if not  self.repo.update_release_type(content_id, release_type):
            return None
        resource = self.get_resource_by_id(content_id)            
        return resource

    def has_legal_review_resources(self, request_id: UUID) -> bool:
        return self.repo.has_legal_review_resources(request_id)
