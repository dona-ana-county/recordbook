from marshmallow import Schema, fields, post_load

from records_api.constants.schema import LookupSchema
from records_api.content.model import Instruction, Link, Note, Resource
from records_api.users.schema import StaffSchema


class NoteSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    request_id = fields.Str(data_key="requestId", allow_none=True)
    title = fields.Str()
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    creator = fields.Nested(StaffSchema, allow_none=True)
    release_type = fields.Nested(LookupSchema, data_key="releaseType")
    editable = fields.Boolean()
    message = fields.Str()

    @post_load
    def make_note(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        if "request_id" not in data:
            data["request_id"] = None
        if "create_dt" not in data:
            data["create_dt"] = None
        return Note(**data)


class InstructionSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    request_id = fields.Str(data_key="requestId", allow_none=True)
    title = fields.Str()
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    creator = fields.Nested(StaffSchema, allow_none=True)
    release_type = fields.Nested(LookupSchema, data_key="releaseType")
    editable = fields.Boolean()
    message = fields.Str()

    @post_load
    def make_instruction(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        if "request_id" not in data:
            data["request_id"] = None
        if "create_dt" not in data:
            data["create_dt"] = None
        return Instruction(**data)


class LinkSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    request_id = fields.Str(data_key="requestId", allow_none=True)
    title = fields.Str()
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    creator = fields.Nested(StaffSchema, allow_none=True)
    release_type = fields.Nested(LookupSchema, data_key="releaseType")
    editable = fields.Boolean()
    description = fields.Str()
    url = fields.Str()

    @post_load
    def make_link(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        if "request_id" not in data:
            data["request_id"] = None
        if "create_dt" not in data:
            data["create_dt"] = None
        return Link(**data)


class ResourceSchema(Schema):
    id_ = fields.UUID(data_key="id", allow_none=True)
    request_id = fields.Str(data_key="requestId", allow_none=True)
    title = fields.Str()
    create_dt = fields.DateTime(data_key="createDate", allow_none=True)
    creator = fields.Nested(StaffSchema, allow_none=True)
    release_type = fields.Nested(LookupSchema, data_key="releaseType")
    editable = fields.Boolean()
    extension = fields.Str(data_key="extension", allow_none=True)
    needs_review = fields.Boolean(data_key="needsReview")
    under_review = fields.Boolean(data_key="underReview")
    message = fields.Str(allow_none=True)
    description = fields.Str(allow_none=True)
    revised = fields.Boolean(allow_none=True)

    @post_load
    def make_resource(self, data, **kwargs):
        if "id_" not in data:
            data["id_"] = None
        if "request_id" not in data:
            data["request_id"] = None
        if "create_dt" not in data:
            data["create_dt"] = None
        if "extension" not in data:
            data["extension"] = None
        if "under_review" not in data:
            data["under_review"] = False
        if "message" not in data:
            data["message"] = None
        if "description" not in data:
            data["description"] = None
        if "revised" not in data:
            data["revised"] = False
        return Resource(**data)
