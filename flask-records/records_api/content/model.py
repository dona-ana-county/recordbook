from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from records_api.constants.model import Lookup
from records_api.users.model import Staff


@dataclass
class ReleaseType(Lookup):
    def __repr__(self):
        return f"ReleaseType(Code: {self.code}, Value: {self.value})"


@dataclass
class ContentType(Lookup):
    def __repr__(self):
        return f"ContentType(Code: {self.code}, Value: {self.value})"


@dataclass
class Content:
    id_: UUID
    request_id: str
    title: str
    create_dt: datetime
    release_type: ReleaseType
    creator: Staff
    editable: bool


class Resource(Content):
    def __init__(
        self,
        id_: UUID,
        request_id: str,
        title: str,
        create_dt: datetime,
        extension: str,
        needs_review: bool,
        under_review: bool,
        message: str,
        release_type: ReleaseType,
        revised: bool,
        creator: Staff = None,
        editable: bool = True,
        file_path: str = None,
        description: str = None,
    ):
        Content.__init__(self, id_, request_id, title, create_dt, release_type, creator, editable)
        self.extension = extension
        self.file_path = file_path
        self.needs_review = needs_review
        self.under_review = under_review
        self.message = message
        self.description = description
        self.revised = revised


class Link(Content):
    def __init__(
        self,
        id_: UUID,
        request_id: str,
        title: str,
        create_dt: datetime,
        description: str,
        url: str,
        release_type: ReleaseType,
        creator: Staff = None,
        editable: bool = True,
    ):
        Content.__init__(self, id_, request_id, title, create_dt, release_type, creator, editable)
        self.description = description
        self.url = url


class Note(Content):
    def __init__(
        self,
        id_: UUID,
        request_id: str,
        title: str,
        create_dt: datetime,
        message: str,
        release_type: ReleaseType,
        creator: Staff = None,
        editable: bool = True,
    ):
        Content.__init__(self, id_, request_id, title, create_dt, release_type, creator, editable)
        self.message = message


class Instruction(Content):
    def __init__(
        self,
        id_: UUID,
        request_id: str,
        title: str,
        create_dt: datetime,
        message: str,
        release_type: ReleaseType,
        creator: Staff = None,
        editable: bool = True,
    ):
        Content.__init__(self, id_, request_id, title, create_dt, release_type, creator, editable)
        self.message = message
