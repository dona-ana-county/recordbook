from records_api.content.model import ContentType, ReleaseType
from records_api.notifications.model import EventCode
from records_api.queues.model import QueueItemType, QueueType
from records_api.requests.model import ExtensionStatus, RequestStatus
from records_api.users.model import ContactType, Group, Role

# TODO VT: Maybe just move this to the contants subsystem
groups = {
    "IPA": Group("IPA", "IPRA Admin"),
    "IPC": Group("IPC", "IPRA Clerk"),
    "LEG": Group("LEG", "Legal"),
}

roles = {
    "SUP": Role("SUP", "Super User"),
    "WRK": Role("WRK", "Worker"),
    "MAN": Role("MAN", "Manager"),
}

request_statuses = {
    "OPN": RequestStatus("OPN", "Open"),
    "CLS": RequestStatus("CLS", "Closed"),
    "DNY": RequestStatus("DNY", "Denied"),
}

resource_statuses = {
    "UND": RequestStatus("UND", "under_review"),
    "NUR": RequestStatus("NUR", "not_under_review"),
}

content_types = {
    "NOT": ContentType("NOT", "Note"),
    "RES": ContentType("RES", "Document"),
    "LNK": ContentType("LNK", "Link"),
    "INS": ContentType("INS", "Instruction"),
}

release_types = {
    "PUB": ReleaseType("PUB", "Public"),
    "REQ": ReleaseType("REQ", "Requestor Only"),
    "PRV": ReleaseType("PRV", "Staff Only"),
    "DOC": ReleaseType("DOC", "Request Document"),
}

contact_types = {
    "EML": ContactType("EML", "Email"),
    "PHN": ContactType("PHN", "Phone"),
    "FAX": ContactType("FAX", "Fax"),
    "ADD": ContactType("ADD", "Mail"),
}


event_codes = {
    "CRE": EventCode("CRE", "Created Request Event"),
    "AQE": EventCode("AQE", "Assignment Queue Event"),
    "RCE": EventCode("RCE", "Request Closed Event"),
    "RDE": EventCode("RDE", "Request Denied Event"),
    "RFE": EventCode("RFE", "Request Forwarded Event"),
    "SME": EventCode("SME", "Staff Message Event"),
    "RME": EventCode("RME", "Requestor Message Event"),
    "CAE": EventCode("CAE", "Content Added Event"),
    "CDE": EventCode("CDE", "Content Deleted Event"),
    "CUE": EventCode("CUE", "Content Updated Event"),
    "DAE": EventCode("DAE", "Department Assigned Event"),
    "DUE": EventCode("DUE", "Department Unassigned Event"),
    "WAE": EventCode("WAE", "Worker Assigned Event"),
    "WUE": EventCode("WUE", "Worker Unassigned Event"),
    "LQE": EventCode("LQE", "Legal Queue Event"),
    "LDE": EventCode("LDE", "Legal Denied Event"),
    "LAE": EventCode("LAE", "Legal Approved Event"),
    "LRE": EventCode("LRE", "Legal Revised Event"),
    "RUE": EventCode("RUE", "Request Updated Event"),
    "RRE": EventCode("RRE", "Request Re-opened Event"),
    "RDU": EventCode("RDU", "Request Due-Date Updated Event"),
    "REE": EventCode("REE", "Resource Updated Event"),
    "SDR": EventCode("SDR", "Staff Sent Document To Requestor"),
    "LSE": EventCode("LSE", "Legal Status Event"),
    "DCT": EventCode("DCT", "Department Task Complete"),
    "DIT": EventCode("DIT", "Department Task Incomplete")
}

queue_items = {"REQ": QueueItemType("REQ", "Request"), "RES": QueueItemType("RES", "Resource")}

queue_types = {
    "ASQ": QueueType("ASQ", "Assignment Queue"),
    "DPQ": QueueType("DPQ", "Department Queue"),
    "LRQ": QueueType("LRQ", "Legal Review Queue"),
    "SAR": QueueType("SAR", "Staff Assigned Requests"),
}

extension_statuses = {
    "NON": ExtensionStatus("NON", "None"),
    "INI": ExtensionStatus("INI", "3-Day Letter Sent"),
    "BUR": ExtensionStatus("BUR", "Burdensome"),
}
