import logging
from uuid import UUID

import pytest
from flask import Flask
from flask.testing import FlaskClient
from flask_sqlalchemy import SignallingSession

from records_api import constant, mail
from records_api.manage.alembic import verify_schema
from records_api.manage.db_init import insert_test_data
from records_api.serializer import db_schema as dbs

from .util import (UserType, create_app, create_user, destroy_database,
                   get_temporary_uri, get_token)
from .util import new_db as _new_db

log = logging.getLogger(__name__)

@pytest.fixture(scope="session") 
# pylint: disable=redefined-outer-name
def db_uri(config_name: str) -> str: 
    from records_api.config import config
    return get_temporary_uri(config[config_name].SQLALCHEMY_DATABASE_URI)

@pytest.yield_fixture(scope="session")
# pylint: disable=redefined-outer-name
def new_db(db_uri: str) -> str:
    _new_db(db_uri)
    yield db_uri
    destroy_database(db_uri)
    
@pytest.fixture(scope="session")
def config_name() -> str:
    return "test"

@pytest.fixture(scope="session")
# pylint: disable=redefined-outer-name
def new_app(new_db: Flask, config_name: str) -> Flask:
    app = create_app(new_db, config_name)
    verify_schema(app, do_upgrade=True)
    with app.app_context():
        insert_test_data(app.extensions['sqlalchemy'].db)

    return app

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def app_context(new_app: Flask) -> Flask:
    with new_app.app_context():
        yield new_app

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def client(new_app: Flask) -> FlaskClient:
    yield new_app.test_client()

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def db_session(app_context: Flask) -> SignallingSession:
    db = app_context.extensions['sqlalchemy'].db
    return db.session


@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def dept1(db_session: SignallingSession) -> UUID:
    return db_session.query(dbs.Department).first().id_

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def user(db_session: SignallingSession, dept1: UUID) -> UserType:
    return create_user(db_session, "login",constant.roles["WRK"], dept1, [])

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def user_token(client: FlaskClient, user: UserType ) -> str:
    return get_token(client, user)

@pytest.fixture
# pylint: disable=redefined-outer-name
def ipra_request(client: FlaskClient, user_token: str) -> str:
    rv = client.post(
        "/request/newStaffRequest",
        json={
            "request": {
                "title": "test",
                "description": "test",
                "requestor": {
                    "firstName": "test",
                    "lastName": "test",
                    "primaryContact": constant.contact_types['EML'],
                    "contactInfo": {
                        "email": "test@test.com",
                        "phone": "5555555555",
                        "address1": "123 Test Rd",
                        "city": "Test City",
                        "state": "New Mexico",
                        "postalCode": "88001"
                    }
                }
            }

        },
        headers=[('Authorization', user_token)])
    
    assert rv.is_json
    return rv.get_json()['request']['id']




@pytest.fixture(autouse=True)
def no_email(monkeypatch):
    def mock_email(*args):
        return
    monkeypatch.setattr(mail, 'send', mock_email)
