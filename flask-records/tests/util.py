import logging
from typing import Iterable, Tuple, Dict, Any
from uuid import UUID, uuid4

from flask import Flask
from flask.testing import FlaskClient
from flask_sqlalchemy import SignallingSession
from sqlalchemy import create_engine, inspect
from sqlalchemy.pool import NullPool

from records_api.constant import groups, roles
from records_api.serializer import db_schema as dbs
from records_api.users.model import Group, Role
from records_api.users.service import hash_password

import warnings


with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    import sqlalchemydiff
    import sqlalchemydiff.util


from sqlalchemydiff.util import destroy_database, get_temporary_uri, new_db
from sqlalchemydiff import compare



UserType = Tuple[UUID, str, str]

def create_app(db_uri, config_name) -> Flask:
    from records_api import create_app as _create_app
    from records_api.config import config
    config[config_name].SQLALCHEMY_DATABASE_URI = db_uri
    logging.info("Creating app for %s...", db_uri)
    return _create_app(config_name)

def create_user(session, prefix: str, role: Role, dept: UUID, user_groups: Iterable[Group]) -> UserType:
    username = prefix + "-" + str(uuid4())
    password = str(uuid4())
    user = dbs.User(
        id_=uuid4(),
        first_name=prefix,
        last_name ="Test"
    )
    session.add(user)
    session.commit()

    staff = dbs.Staff(
        id_=user.id_,
        username=username,
        password=hash_password(password),
        email="test@test.com",
        role=role.code,
        department=dept,
        disabled=False,
        delete_date=None,
        external_auth=False
    )
    session.add(staff)
    session.commit()

    session.add_all((
        dbs.StaffGroup(
            staff_id=user.id_,
            group_code=group.code
        ) for group in user_groups
    ))
    session.commit()

    return user.id_, username, password
    # session.query(dbs.AuthToken).filter(dbs.AuthToken.staff_id == user.id_).delete()
    # session.commit()
    # session.query(dbs.StaffGroup).filter(dbs.StaffGroup.staff_id == user.id_).delete()
    # session.commit()
    # session.delete(staff)
    # session.commit()
    # session.delete(user)
    # session.commit()


def get_token(client: FlaskClient, user: UserType) -> str:
    rv = client.post ("/auth/login", json={
        'username':user[1],
        'password':user[2]
    })
    data = rv.get_json()
    assert data and 'token' in data and data['token'] and 'token' in data['token'], rv
    return data['token']['token']
class Users:
    def __init__(self, user_info: Dict[str, Dict[str, Any]], db_session: SignallingSession, client: FlaskClient):
        self.user_info = user_info
        self.db_session = db_session
        self.client = client
        self.users = {}

    def login(self, user: str) -> str:
        user_info = self.get(user)
        if 'token' not in user_info:
            user_info['token'] =  get_token(self.client, (user_info['id'], user_info['username'], user_info['password']))
        return user_info['token']
    def get(self, user: str) -> Dict[str, Any]:
        user_info = self.user_info[user]
        if 'id' not in user_info:
            id_, username, password = create_user(
                self.db_session,
                user, 
                roles[user_info['role']],
                user_info['dept'],
                [groups[g] for g in user_info['groups']]
            )
            user_info['id'] = id_
            user_info['username'] = username
            user_info['password'] = password
        return user_info
def Success(data) -> bool:
    assert data.get('success') == 1
    return True

def InvalidPermission(data) -> bool:
    assert data == {"type": "Unauthorized", "error": "Invalid Permissions"}
    return True

def UpdateError(data) -> bool:
    assert data['type'] == "Update Error"
    return True
    
class InspectorFactory(sqlalchemydiff.util.InspectorFactory):
    '''Monkeypatch of InspectorFactor to use a NullPool

    This ensures connections are closed after compare and eliminates 
    a "FATAL: terminating connection due to administrator command" in the
    database logs
    '''
    @classmethod
    def from_uri(cls, uri):
        engine = create_engine(uri, poolclass=NullPool)
        inspector = inspect(engine)
        return inspector
def monkey_patch():
    sqlalchemydiff.util.InspectorFactory = InspectorFactory
    sqlalchemydiff.comparer.InspectorFactory = InspectorFactory
monkey_patch()