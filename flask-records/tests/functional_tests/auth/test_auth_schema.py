from datetime import datetime, timezone
from uuid import uuid4

from marshmallow import pprint

from records_api.auth.schema import AuthTokenSchema
from records_api.auth.model import AuthToken


def test_auth_load():
    test_id = uuid4()
    now = datetime.now(tz=timezone.utc)

    test_token = AuthToken(
        token = "testToken",
        staff_id = test_id,
        issued_dt = now,
        expiration_dt = now,
        revoked_dt = now
    )

    schema = AuthTokenSchema()
    res = schema.dump(test_token)
    pprint(res)
