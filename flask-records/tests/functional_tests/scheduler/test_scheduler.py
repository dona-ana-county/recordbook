from datetime import date, timedelta
from unittest.mock import Mock, patch
from uuid import UUID

import pytest

from records_api.requests.model import Request
from records_api.requests.service import RequestService
from records_api.users.service import UserService
from records_api.scheduler.reminders import ReminderScheduler
from records_api.service import Service

from ...util import Users


@pytest.fixture(scope="module")
def scheduler(app_context) -> ReminderScheduler:
    s = ReminderScheduler()
    s.init_app(app_context)
    return s

@pytest.yield_fixture
def date_mock():
    from records_api.requests import service
    from records_api.scheduler import reminders

    with patch.object(reminders, "date") as mock, patch.object(service, "date", new=mock):
        mock.today.return_value = date.today()
        yield mock

@pytest.yield_fixture
# pylint: disable=redefined-outer-name
def notification_mock(scheduler):
    with patch.object(scheduler, "notification_service") as mock:
        yield mock

@pytest.fixture(scope="module")
def request_service() -> RequestService:
    return Service.get_service(RequestService)

@pytest.fixture(scope="module")
def user_service() -> UserService:
    return Service.get_service(UserService)

@pytest.fixture
# pylint: disable=redefined-outer-name
def full_request(ipra_request, request_service) -> Request:
    return request_service.get_info(ipra_request)

@pytest.fixture(scope="module")
def users(db_session, client, dept1):
    return Users(
        {
            'ipra-admin': {'role': 'MAN', 'dept': dept1, 'groups': ['IPA']},
            'user': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'user2': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'manager': {'role': 'MAN', 'dept': dept1, 'groups': []},
            'legal': {'role': 'WRK', 'dept': dept1, 'groups': ['LEG']}
        },
        db_session,client
    )

def filtered_calls(mock, request_arg, request_id):
    return [call_args for call_args in mock.call_args_list
            if call_args[0][request_arg].id_ == request_id]

def test_due_date_assigned(
    date_mock,
    notification_mock, 
    dept1: UUID, 
    scheduler: ReminderScheduler, 
    full_request: Request,
    request_service: RequestService,
    mocker,
):  # pylint: disable=redefined-outer-name
    ''' Test sending due date and only due date '''
    request_service.assign_department(full_request, dept1, "Primary")
    
    assert full_request.create_dt == date.today()
    next_date = full_request.due_dt
    date_mock.today.return_value = next_date

    notification_mock.reminder.reset_mock()

    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)

    assert len(call_args) == 2

    call_pos, call_kwargs = call_args[0]
    assert call_pos[0] == 'duedate'
    assert 'ipra' not in call_kwargs
    assert 'emails' in call_kwargs
    
    call_pos, call_kwargs = call_args[1]
    assert call_pos[0] == 'duedate'
    assert call_kwargs.get('ipra', None) == True
    assert 'emails' not in call_kwargs

def test_due_date_unassigned( 
    date_mock,
    notification_mock,
    scheduler: ReminderScheduler, 
    full_request: Request,
    request_service: RequestService,
    mocker,
): # pylint: disable=redefined-outer-name
    ''' Test sending due date and only due date '''
    next_date = full_request.due_dt
    date_mock.today.return_value = next_date

    notification_mock.reminder.reset_mock()

    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert len(call_args) == 1

    call_pos, call_kwargs = call_args[0]
    assert call_pos[0] == 'duedate'
    assert call_kwargs.get('ipra', None) == True
    assert 'emails' not in call_kwargs

def test_reassign_reminder( 
    date_mock,
    notification_mock,
    dept1: UUID, 
    scheduler: ReminderScheduler,  
    full_request: Request,
    request_service: RequestService,
    users: Users,
    mocker,
):  # pylint: disable=redefined-outer-name
    user_list =  [users.get('user')['id']]

    request_service.assign_department(full_request, dept1, "Primary")
    request_service.assign_request_staff(full_request, user_list)

    next_date = full_request.due_dt - timedelta(days=1)
    date_mock.today.return_value = next_date
    
    notification_mock.reminder.reset_mock()

    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    notification_mock.reminder.reset_mock()

    request_service.unassign_request_staff(full_request, user_list )
    request_service.assign_request_staff(full_request, user_list )
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    
    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert not call_args


def test_assign_reminder( 
    date_mock,
    notification_mock,
    dept1: UUID, 
    scheduler: ReminderScheduler,  
    full_request: Request,
    request_service: RequestService,
    user_service: UserService,
    users: Users,
    mocker,
):  # pylint: disable=redefined-outer-name
    user_list =  [users.get('user')['id']]

#    request_service.assign_department(full_request, dept1, "Primary")
#    request_service.assign_request_staff(full_request, user_list)
    staff = user_service.get_staff_by_id(user_list[0])
    request_service.extend_due_date(full_request.id_, "Stuff", None, staff)
    full_request = request_service.get_info(full_request.id_)

    next_date = full_request.due_dt - timedelta(days=3)
    date_mock.today.return_value = next_date
    
    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    request_service.assign_request_staff(full_request, user_list )

    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    
    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert not call_args

    date_mock.today.return_value =  date_mock.today.return_value + timedelta(days=1)

    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    
    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert not call_args


def test_no_5day(
    date_mock,
    notification_mock,
    dept1: UUID, 
    scheduler: ReminderScheduler,  
    full_request: Request,
    request_service: RequestService,
    users: Users,
    mocker,
): # pylint: disable=redefined-outer-name
    request_service.assign_department(full_request, dept1, "Primary")

    date_mock.today.return_value=date.today() + timedelta(days=1)

    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert not call_args


def test_breach(
    date_mock,
    notification_mock,
    dept1: UUID, 
    scheduler: ReminderScheduler,  
    full_request: Request,
    request_service: RequestService,
    users: Users,
    mocker,
): # pylint: disable=redefined-outer-name
    date_mock.today.return_value=full_request.due_dt + timedelta(days=1)

    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert len(call_args) == 1
    call_pos, call_kwargs = call_args[0]
    assert call_pos[0] == 'breach'
    assert call_kwargs.get('ipra', None) == True
    assert 'emails' not in call_kwargs

    date_mock.today.return_value=full_request.due_dt + timedelta(days=2)
    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert not call_args

    date_mock.today.return_value=full_request.due_dt + timedelta(days=7)
    notification_mock.reminder.reset_mock()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()
    scheduler.send_all_reminders()

    call_args = filtered_calls(notification_mock.reminder, 1, full_request.id_)
    assert len(call_args) == 1
    call_pos, call_kwargs = call_args[0]
    assert call_pos[0] == 'breach'
    assert call_kwargs.get('ipra', None) == True
    assert 'emails' not in call_kwargs