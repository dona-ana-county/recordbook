from io import BytesIO
import json

def upload(client, worker_token, worker, ipra_request, data):
    rv = client.post(
        "/content/uploadFile/" + ipra_request,
        data={
            'file': (BytesIO(b""), 'test.txt'),
            'resource': json.dumps({
                "title": "test.txt",
                "creator": {
                    "id": str(worker['id']),  # Only id is used
                    "firstName": "",
                    "lastName": "",
                    "email": "",
                    "role": {"code": "", "value": ""},
                    "disabled": False,
                    "externalAuth": False,
                    "username": "",
                    "groups": [],
                },
                "needsReview": data.get('needsReview', False),
                "releaseType": {"code": "PUB", "value": "Public"},
                "underReview": False,
                "description": None
            })

        },
        headers=[('Authorization', worker_token )]
    )
    assert rv.is_json
    assert "error" not in rv.get_json()
    return rv.get_json()['id']
