
import pytest

from ...util import InvalidPermission, Success, UpdateError, Users
from ..request import assign, assign_worker, close_request
from . import upload


@pytest.fixture(scope="module")
def users(db_session, client, dept1):
    return Users(
        {
            'ipra-admin': {'role': 'MAN', 'dept': dept1, 'groups': ['IPA']},
            'user': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'user2': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'manager': {'role': 'MAN', 'dept': dept1, 'groups': []},
            'legal': {'role': 'WRK', 'dept': dept1, 'groups': ['LEG']}
        },
        db_session,client
    )



@pytest.mark.parametrize("worker,deleter,should_close,check",
                          [
                              pytest.param('user', 'user', False, Success, id="own"),
                              pytest.param('user', 'manager', False, Success, id="manager"),
                              pytest.param('user', 'ipra-admin', False, Success, id="ipra-admin"),
                              pytest.param('user2','user', False, InvalidPermission, id="other-user"),
                              pytest.param('user', 'ipra-admin', True, UpdateError, id="closed")
                          ]
)
# pylint: disable=redefined-outer-name
def test_delete(client, ipra_request, users, dept1, worker, deleter, should_close, check):
    
    assign(client, users.login('ipra-admin'), ipra_request, dept1)
    assign_worker(client, users.login('ipra-admin'), ipra_request, users.get(worker))
    resource_id = upload(client, users.login(worker), users.get(worker), ipra_request, {})
    if should_close:
        rv = close_request(client, users.login('ipra-admin'), ipra_request)
        assert Success(rv)

    rv = client.post(
        "/content/deleteContent/" + resource_id,
        json={
            "reason": "stuff",
            "type": "RES",
        },
        headers=[('Authorization', users.login(deleter) )]
    )
    assert rv.is_json
    assert check(rv.get_json())    

# pylint: disable=redefined-outer-name
def test_close_legal_review(client, ipra_request, users, dept1):
    assign(client, users.login('ipra-admin'), ipra_request, dept1)
    resource_id = upload(client, users.login('ipra-admin'), users.get('ipra-admin'), ipra_request, {'needsReview': True})
    assert resource_id

    rv = close_request(client, users.login('ipra-admin'), ipra_request)
    assert UpdateError(rv)

@pytest.mark.parametrize("rejector,review,check",
                          [
                              pytest.param('legal', True, Success, id="legal"),
                              pytest.param('manager', True, InvalidPermission, id="non-legal"),
                              pytest.param('legal', False, UpdateError, id="no-needs-review"),
                          ]
)
# pylint: disable=redefined-outer-name
def test_reject(client, ipra_request, users, dept1, rejector, review, check):
    assign(client, users.login('ipra-admin'), ipra_request, dept1)
    resource_id = upload(client, users.login('ipra-admin'), users.get('ipra-admin'), ipra_request, {'needsReview': review})
    assert resource_id

    rv = client.post(
        "/content/rejectResource/" + resource_id,
        json={
            "staff": str(users.get(rejector)['id']),
            "reason": "stuff"
        },
        headers = [('Authorization', users.login(rejector))]
    )
    assert rv.is_json
    assert check(rv.get_json())