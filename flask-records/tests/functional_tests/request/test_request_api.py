from datetime import date, timedelta

import pytest

from ...util import Success, Users
from . import get_request_info


@pytest.fixture(scope="module")
def users(db_session, client, dept1):
    return Users(
        {
            'ipra-admin': {'role': 'MAN', 'dept': dept1, 'groups': ['IPA']},
            'user': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'user2': {'role': 'WRK', 'dept': dept1, 'groups': []},
            'manager': {'role': 'MAN', 'dept': dept1, 'groups': []},
            'legal': {'role': 'WRK', 'dept': dept1, 'groups': ['LEG']}
        },
        db_session,client
    )


def test_extend(client, ipra_request, users): # pylint: disable=redefined-outer-name
    info = get_request_info(client, users.login('ipra-admin'), ipra_request)
    assert info['extensionStatus']['code'] == 'NON'
    ev = client.post(
        "/request/extendDueDate/" + ipra_request,
        json={
            'description': 'just because',
        },
        headers=[('Authorization', users.login('ipra-admin'))]
    )
    assert ev.is_json
    assert Success(ev.get_json())
    new_info = get_request_info(client, users.login('ipra-admin'), ipra_request)
    assert date.fromisoformat(new_info['dueDate']) > date.fromisoformat(info['dueDate'])
    assert new_info['extensionStatus']['code'] == 'INI'

    info = new_info

    ev = client.post(
        "/request/extendDueDate/" + ipra_request,
        json={
            'description': 'just because',
            'dueDate': (date.fromisoformat(info['dueDate']) + timedelta(days=30)).isoformat()
        },
        headers=[('Authorization', users.login('ipra-admin'))]
    )
    assert ev.is_json
    assert Success(ev.get_json())
    new_info = get_request_info(client, users.login('ipra-admin'), ipra_request)
    assert date.fromisoformat(new_info['dueDate']) > date.fromisoformat(info['dueDate'])
    assert new_info['extensionStatus']['code'] == 'BUR'