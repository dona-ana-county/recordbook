import json

def assign(client, ipra_admin_token, ipra_request, dept):
    rv = client.post(
        '/request/updateDepartments/' + ipra_request,
        json={
            "primaryDepartments": {"id": str(dept)},
            "secondaryDepartments": []
        },
        headers=[('Authorization', ipra_admin_token)]
    )
    assert rv.is_json
    assert rv.get_json().get('success') == 1

def assign_worker(client, manager_token, ipra_request, worker) :
    rv = client.post(
        "/request/updateStaff/" + ipra_request,
        json=[
            {"id": str(worker['id'])}
        ],
        headers=[('Authorization', manager_token)]
    )
    assert rv.is_json
    assert rv.get_json().get('success') == 1

def close_request(client, manager_token, ipra_request):
    rv = client.post(
        "/request/closeRequest/" + ipra_request,
        json={
            "code": "CLS",
            "value": "Stuff"
        },
        headers=[('Authorization', manager_token)]
    )
    assert rv.is_json
    return rv.get_json()   

def get_request_info(client, token, ipra_request):
    rv = client.get(
        "/request/getRequestInfo/" + ipra_request,
        headers=[('Authorization', token)]
    )
    assert rv.is_json
    return rv.get_json()