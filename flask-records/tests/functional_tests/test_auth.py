import pytest

from records_api.users.schema import StaffSchema

def test_login_logout(client, user):
    rv = client.post ("/auth/login", json={
        'username':user[1],
        'password':user[2]
    })
    assert rv.is_json
    data = rv.get_json()
    token = data['token']['token']
    assert StaffSchema().validate(data) == {}


    rv = client.post("/auth/validateToken", 
                     headers=[('Authorization', token)])
    assert rv.is_json
    data = rv.get_json()
    assert data == {'success':1}


    rv = client.post("/auth/logout", 
        json={
            'username': user[1],
            'token':token
        },
        headers=[('Authorization',token)])
    assert rv.is_json
    data = rv.get_json()

    assert 'success' in data
    assert data['success'] == 1
    assert 'msg' in data


@pytest.mark.parametrize("url", [
    '/auth/validateToken',
    '/auth/logout'
])
def test_bad_token(client, url):
    rv = client.post(url)
    assert rv.is_json
    assert rv.get_json() ==  {"type": "Unauthorized", "error": "Authorization Error"}

    rv = client.post(url, headers=[("Authorization", "BadValue")])
    assert rv.is_json
    assert rv.get_json() ==  {"type": "Unauthorized", "error": "Authorization Error"}
