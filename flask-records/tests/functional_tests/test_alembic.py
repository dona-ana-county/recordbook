import logging
import warnings

import pytest
from sqlalchemy.exc import SADeprecationWarning

from ..util import create_app, destroy_database, get_temporary_uri, new_db, compare

@pytest.fixture(scope="module")
def model_uri(db_uri):
    return get_temporary_uri(db_uri) + "_model"

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def model_new_db(model_uri):
    logging.warning("New model DB...")
    new_db(model_uri)
    yield model_uri
    destroy_database(model_uri)


@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def model_app(model_new_db, config_name):
    return create_app(model_new_db, config_name)


@pytest.yield_fixture(scope="module")
# pylint: disable=redefined-outer-name
def model_app_context(model_app):
    with model_app.app_context():
        yield model_app

@pytest.fixture
# pylint: disable=redefined-outer-name
def model_app_db(model_app):
    return model_app.extensions['sqlalchemy'].db

@pytest.fixture(scope="module")
def migration_uri(db_uri):
    return get_temporary_uri(db_uri) + "_migration"

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def migration_new_db(migration_uri):
    logging.warning("New migration DB...")
    new_db(migration_uri)
    yield migration_uri
    destroy_database(migration_uri)

@pytest.fixture(scope="module")
# pylint: disable=redefined-outer-name
def migration_app(migration_new_db, config_name):
    return create_app(migration_new_db, config_name)

@pytest.fixture
# pylint: disable=redefined-outer-name
def migration_app_alembic(migration_app):
    return migration_app.extensions['alembic']


def test_upgrade_and_downgrade( migration_app, migration_app_alembic): # pylint: disable=redefined-outer-name
    """Test all migrations up and down.

    Tests that we can apply all migrations from a brand new empty
    database, and also that we can remove them all.
    """
    with migration_app.app_context():
        alembic = migration_app_alembic
        alembic.upgrade()
    
        head = alembic.heads()
        current = alembic.current()

        assert head == current

        while current != ():
            alembic.downgrade()
            current = alembic.current()

def test_model_and_migration_schemas_are_the_same(
        migration_app, migration_app_alembic, 
        migration_uri, model_app, model_app_db, model_uri
): # pylint: disable=redefined-outer-name
    """Compare two databases.

    Compares the database obtained with all migrations against the
    one we get out of the models.
    
    """
    with migration_app.app_context():
        migration_app_alembic.upgrade()

    with model_app.app_context():
        model_app_db.create_all()

    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=SADeprecationWarning)

        result = compare(migration_uri, model_uri, set(['alembic_version']))

    assert result.is_match        
