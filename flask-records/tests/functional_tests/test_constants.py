import pytest
from records_api.constants.schema import LookupSchema, GovernmentEntitySchema

@pytest.mark.parametrize("url", [
        "/constants/getAllGroups", 
        "/constants/getAllRoles", 
        "/constants/getAllRequestStatuses", 
        "/constants/getAllContentTypes",
        "/constants/getAllReleaseTypes", 
        "/constants/getAllContactTypes", 
        "/constants/getAllEventCodes", 
        "/constants/getAllQueueItems", 
        "/constants/getAllQueueTypes", 
        "/constants/getAllDenialReasons", 
        "/constants/getAllCloseReasons", 
])
def test_lookups(client, url):
    rv = client.get(url)
    assert rv.is_json
    data = rv.get_json()
    assert LookupSchema().validate(data, many=True) == {}


def test_gov_entitites(client, user_token):
    url = "/constants/getAllGovernmentEntities"
    rv = client.get(url)
    assert rv.is_json
    data = rv.get_json()
    assert data == {"type": "Unauthorized", "error": "Authorization Error"}

    rv = client.get(url, headers=[("Authorization", user_token)])
    assert rv.is_json
    data = rv.get_json()
    assert GovernmentEntitySchema().validate(data, many=True) == {}
