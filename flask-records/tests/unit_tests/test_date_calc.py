from datetime import date, datetime

import pytest
from dateutil.parser import parse

from records_api.requests.service import DateCalculator
# Monday - 0
# Tuesday - 1
# Wednesday - 2
# Thursday - 3
# Friday - 4
# Saturday - 5
# sunday - 6


example_dates = [
    # create_timestamp,       create_dt,wd     received,wd    three_dt,wd     fifteen_dt,wd
    ('2019-09-10 12:01-0600', '2019-09-10',1, '2019-09-11',2, '2019-09-13',4, '2019-09-25',2),
    ('2019-10-10 01:00-0600', '2019-10-10',3, '2019-10-11',4, '2019-10-15',1, '2019-10-25',4),
    ('2019-05-10 01:00-0600', '2019-05-10',4, '2019-05-13',0, '2019-05-15',2, '2019-05-27',0),
    ('2019-05-09 01:00-0600', '2019-05-09',3, '2019-05-10',4, '2019-05-14',1, '2019-05-24',4),
    ('2019-05-10 14:00-0600', '2019-05-10',4, '2019-05-13',0, '2019-05-15',2, '2019-05-27',0),
    ('2019-05-10 10:00-0600', '2019-05-10',4, '2019-05-13',0, '2019-05-15',2, '2019-05-27',0),
    ('2019-08-10 09:00-0600', '2019-08-10',5, '2019-08-12',0, '2019-08-14',2, '2019-08-26',0),

]

@pytest.mark.parametrize("create_timestamp, create_dt, create_wd, rec_dt, rec_wd, three_dt, three_wd, fifteen_dt, fifteen_wd",
    example_dates
)
def test_date_calculator(
   
    create_timestamp, create_dt, create_wd, rec_dt, rec_wd, three_dt, three_wd, fifteen_dt, fifteen_wd
):
    calculator = DateCalculator()
    create_timestamp: datetime = datetime.strptime(create_timestamp, "%Y-%m-%d %H:%M%z")
    create_dt: date = parse(create_dt).date()
    three_dt: date = parse(three_dt).date()
    fifteen_dt: date = parse(fifteen_dt).date()

    assert create_timestamp.date() == create_dt
    assert create_dt.weekday() == create_wd

    three_dt_ = calculator.initial_due_date(create_dt)
    assert three_dt_ == three_dt
    assert three_dt_.weekday() == three_wd

    fifteen_dt_ = calculator.extended_due_date(create_dt)
    assert fifteen_dt_ == fifteen_dt
    assert fifteen_dt_.weekday() == fifteen_wd
