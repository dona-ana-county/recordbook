# RecordBook

RecordBook is an web based application developed at Doña Ana County to aid in the submission
and processing of Inspection of Public Record Act (IPRA) requests.

IPRA requests are to New Mexico government agencies what the Freedom of Information Act (FOIA) 
is to the US Federal Government.  This application has been designed to comply with
the requirements of the IPRA law as well as to follow a hypothetical workflow we 
follow here at Doña Ana County.  As such it should be usable at other institutions
in New Mexico if you choose to adopt our processing workflow.  Outside of New Mexico
the application may require some small modification to be in keeping with your laws.

This application has a sister application 'RecordBook-runner' which provides a docker-based
runtime for the application for quick and easy deployments:

[RecordBook-runner](https://gitlab.com/dona-ana-county/recordbook-runner)

The RecordBook-runner repository has deployment instructions for the application.

## Documentation

We have provided user documentation on the wiki here:

[RecordBook User Guide](https://gitlab.com/dona-ana-county/recordbook/wikis/RecordBook-User-Guide)

Currently the documentation is promarily overview level, but once you understand the workflow
most of the application is fairly self explanitory.

## Credits

Much of the funding for the development of this application was provided by New
Mexico Office of the Attorney General as a part of a grant.  Doña County provided 
additional funds and significant staff time.  Most of the development work was 
performed under contract by [Spectrum Technologies](https://www.spectrumistechnology.com).