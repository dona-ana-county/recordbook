import os
from smtpd import PureProxy, DebuggingServer

class RedirectProxy(DebuggingServer, PureProxy ):
    def __init__(self, *args, **kwargs):
        self.rcpttos = [os.environ["REDIR_TO"]]
        super(RedirectProxy, self).__init__(*args, decode_data=True, **kwargs)
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        DebuggingServer.process_message(self, peer, mailfrom, rcpttos, data)
        PureProxy.process_message(self, peer, mailfrom, self.rcpttos, data)

class LoggingProxy(DebuggingServer, PureProxy ):
    def __init__(self, *args, **kwargs):
        super(LoggingProxy, self).__init__(*args, decode_data=True, **kwargs)
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        DebuggingServer.process_message(self, peer, mailfrom, rcpttos, data)
        PureProxy.process_message(self, peer, mailfrom, rcpttos, data)        